//
//  CustomCells.swift
//  Tambola Bingo
//
//  Created by Nivedita on 02/02/18.
//  Copyright © 2018 Anisha. All rights reserved.
//

import Foundation
import UIKit


// MARK: -  Settings ViewController 
// MARK: -
// MARK: - ***** SettingList  Cell *****
class SettingListCell: UITableViewCell {
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var showDetailArrow: UIImageView!
    @IBOutlet weak var switchbutton: UIButton!
    
}
// MARK: - ***** Private Games  Cell *****
class myGamesCell: UITableViewCell {
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var gameTitleText: UILabel!
    @IBOutlet weak var gameDateText: UILabel!
    @IBOutlet weak var lblViewHistory: UILabel!
    @IBOutlet weak var invitebutton: UIButton!
    @IBOutlet weak var playbutton: UIButton!
    @IBOutlet weak var acceptbutton: UIButton!
    @IBOutlet weak var rejectbutton: UIButton!
    
}
// MARK: - ***** SettingList  Cell *****
class gameResultCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
}
// MARK: - ***** Private Games  Cell *****
class pubileGamesCell: UITableViewCell {
    @IBOutlet weak var gameTitleText: UILabel!
    @IBOutlet weak var gameDateText: UILabel!
    @IBOutlet weak var lblgameClosed: UILabel!
    @IBOutlet weak var lblPlayesCount: UILabel!
    @IBOutlet weak var gamebutton: UIButton!
    
    
}
// MARK: - ***** LeaderBoard Cell *****
class leaderBoardCell: UITableViewCell {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblViewDetail: UILabel!
    @IBOutlet weak var lblLuckRating: UILabel!
    @IBOutlet weak var lblGamesPlayed: UILabel!
    @IBOutlet weak var ShareBtn: UIButton!
}
// MARK: - ***** My Friends Cell *****
class myFriendsCell: UITableViewCell {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserCoin: UILabel!
    @IBOutlet weak var btnViewDetail: UIButton!
    @IBOutlet weak var btnAddToGroup: UIButton!
}
// MARK: - ***** My Groups Cell *****
class myGroupsCell: UITableViewCell {
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var btnEditGroup: UIButton!
    @IBOutlet weak var btnDeleteGroup: UIButton!
    @IBOutlet weak var btnCreateGame: UIButton!
}
// MARK: - ***** My Groups Cell *****
class groupMemberCell: UITableViewCell {
    @IBOutlet weak var memberImage: UIImageView!
    @IBOutlet weak var lblMemberName: UILabel!
    @IBOutlet weak var btnAddDelete: UIButton!
    var isSelcted = false
}
// MARK: - *****Chatting Cell *****
//OtherMember cell
class messageSendRecivedCell: UITableViewCell {
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var message: UILabel!
    //isRecived : true(Recived) false(Send)
    func MessageCellSetUp (_ info: MessageModel, isRecived: Bool) {
        container.roundCorners(radius: 10.0, isBottomLeft: isRecived)
        senderName.text     = info.authorName
        let msg = info.message.decodeEmoji
        message.text        = msg
    }
}


// MARK: -  TicketCollectionViewCell 
class TicketCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgCross: UIImageView!
}
// MARK: -  Players CollectionViewCell 
class playersCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgPlayer: UIImageView!
}
// MARK: -  Score Board CollectionViewCell 
class scoreCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblNumber: UILabel!
}
// MARK: -  Participant CollectionViewCell 
class participantCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgParticipant: UIImageView!
}
// MARK: -  Score Board CollectionViewCell 
class shopeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var imgShopping: UIImageView!
    @IBOutlet weak var btnBuy: UIButton!
}
// MARK: -  Score Board CollectionViewCell 
class buyCoinCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblCoins: UILabel!
    @IBOutlet weak var btnOffer: UIButton!
    @IBOutlet weak var btnBuy: UIButton!
}
