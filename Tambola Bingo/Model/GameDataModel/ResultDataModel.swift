//
//  ResultDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 23/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class ResultDataModel: NSObject {
    var userId:String = ""
    var image_url:String = ""
    var name:String = ""
    var resultArray = [String]()
    init(_ id: String,_ image_url: String, _ name: String, _ result: String) {
        super.init()
        self.userId = id
        self.image_url = image_url
        self.name = name
        self.resultArray.append(result)
    }
}
