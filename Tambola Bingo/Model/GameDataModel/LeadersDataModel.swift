//
//  LeadersDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 07/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class LeadersDataModel: NSObject {
    
    var luckRate:Double = 0.00
    var totalwins:String = ""
    var claims:String = ""
    var coins:String = ""
    var gamePlayed:String = ""
    var img_url:String = ""
    var user_id:String = ""
    var username:String = ""
    var isDetailAdded = false
    
    //BlockUnBlock and Report as Spam
    var user_blocked:String = ""    //block/unblocked and empty while Play private Game
    var gameType:String = ""          //Public/Private and empty for others
    
    var score = ScoreModel()
}
class ScoreModel: NSObject {
    var center:Int = 0
    var circle:Int = 0
    var corner:Int = 0
    var earlyFive:Int = 0
    var firstLine:Int = 0
    var houseFull:Int = 0
    var luckSeven:Int = 0
    var pyramid:Int = 0
    var secondLine:Int = 0
    var thirdLine:Int = 0
}
