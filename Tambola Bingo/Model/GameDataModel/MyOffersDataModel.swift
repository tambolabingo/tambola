//
//  MyOffersDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 30/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class MyOffersDataModel: NSObject {
    var coins:String = ""
    var title:String = ""
    var image_url:String = ""
    var offer_desc:String = ""
    var offer_link:String = ""
    
    init(_ link: String, desc: String, coins:String, title:String, img:String) {
        super.init()
        self.coins = coins
        self.title = title
        self.image_url = img
        self.offer_desc = desc
        self.offer_link = link
    }
}
