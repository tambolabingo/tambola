//
//  OptionsModel.swift
//  Talupp
//
//  Created by Talupp on 01/11/17.
//  Copyright © 2017 Talupp. All rights reserved.
//

import UIKit

class PlayersDataModel: NSObject {
    var image_url:String = ""
    var name:String = ""
    var playerId:String = ""
    var userTicketsArray: TicketsModel!
    var topLineArray = [String]()
    var middleLineArray = [String]()
    var thirdLineArray = [String]()
    var pyramidArray = [String]()
    var cornerArray = [String]()
    var circleArray = [String]()
    var crossedSequenceArray = [String]()
    init(_ image_url: String, _ name: String, _ id: String, _ isdummyUser: Bool) {
            super.init()
            self.image_url = image_url
            self.name = name
            self.playerId = id
            if isdummyUser {
                let ticketsArray = EventManager.createTicket(1)
                self.userTicketsArray = TicketsModel(ticketsArray)
                self.getClaimData()
            }
        }
     func getClaimData(){
        for i in 0..<9 {
            let num =  self.userTicketsArray.ticketsArray[0][i].number
            if num != "0"{
                topLineArray.append(num)
            }
        }
        for i in 9..<18 {
            let num =  self.userTicketsArray.ticketsArray[0][i].number
            if num != "0"{
                middleLineArray.append(num)
            }
        }
        for i in 18..<27 {
            let num =  self.userTicketsArray.ticketsArray[0][i].number
            if num != "0"{
                thirdLineArray.append(num)
            }
        }
        let countOne = topLineArray.count
        let countLast = thirdLineArray.count
        cornerArray = [topLineArray[0], topLineArray[countOne - 1], thirdLineArray[0], thirdLineArray[countLast - 1]]
        pyramidArray = [topLineArray[2], middleLineArray[1], middleLineArray[3], thirdLineArray[0], thirdLineArray[2], thirdLineArray[4]]
        circleArray = [topLineArray[2], middleLineArray[1], middleLineArray[3], thirdLineArray[2]]
        
    }
}
