//
//  PlayGameDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 27/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class PlayGameDataModel: NSObject {
    var totalTickets = 3
    var gameId = ""
    var gameName = ""
    var gameStartTime = ""
    var serverTime = ""
    var gameType = "1"
    var claimPrices = [String:Int]()
    var sequenceArray = [String]()
    var players = [PlayersDataModel]()
    var isPracticeGame = false
    var isPublic = false
    var ticketStatusArray = [Bool]()
    init(_ tickets: Int, gameType: String, gameName: String, gameId: String, startTime: String, serverDTime: String, players: [PlayersDataModel], sequence: [String], practice: Bool, claims: [String: Int] ,isPublic: Bool) {
        super.init()
        self.totalTickets          = tickets
        self.players               = players
        self.sequenceArray         = sequence
        self.gameId                = gameId
        self.gameName              = gameName
        self.gameStartTime         = startTime
        self.gameType              = gameType
        self.serverTime            = serverDTime
        self.isPracticeGame        = practice
        self.isPublic              = isPublic
        self.claimPrices           = claims
        self.ticketStatusArray     = practice ? [true, true, true] : [true, false, false]
    }
}
