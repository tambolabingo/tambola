//
//  GameListDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 12/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class GameDetailDataModel: NSObject {
    //For Both
    var game_id:String = ""
    var game_key:String = ""
    var game_key_url:String = ""
    var game_name:String = ""
    var game_status:String = ""
    var game_type:String = ""
    var game_time:String = ""
    var server_time:String = ""
    var participant = [ParticipantDataModel]()
    init(_ gameData: [String: Any],_ playersArray: [[String: Any]]) {
        self.game_id = gameData["game_id"] as? String ?? ""
        self.game_key = gameData["game_id"] as? String ?? ""
        self.game_key_url = gameData["game_key_url"] as? String ?? ""
        self.game_name = gameData["game_name"] as? String ?? ""
        self.game_status = gameData["game_status"] as? String ?? ""
        self.game_type = gameData["game_type"] as? String ?? ""
        self.game_time = gameData["game_time"] as? String ?? ""
        self.server_time = gameData["response_time"] as? String ?? ""
        for player in playersArray {
            let partiModel = ParticipantDataModel(player)
            self.participant.append(partiModel)
        }
    }
}
class ParticipantDataModel: NSObject {
    var image_url:String = ""
    var name:String = ""
    var id:String = ""
    var type:String = ""
    var account_type:String = ""
    var blockStatus:String = ""
    var userStatus:String = ""
    var coins:String = ""
    
    init(_ player: [String: Any]) {
        let user_id = (player["user_id"] as? String)!
        let account_type = player["user_acc_type"] as? String ?? ""
        let url = String(format:ConstantModel.fbImageURL, user_id)
        self.image_url = account_type == "facebook" ? url : ""
        self.id = user_id
        self.name = (player["user_name"] as? String)!
        self.type = (player["user_type"] as? String)!
        self.account_type = account_type
        self.userStatus = (player["user_status"] as? String)!
        self.blockStatus = (player["user_blocked"] as? String)!
        self.coins = player["UserCoins"] as? String ?? ""
        
    }
}

