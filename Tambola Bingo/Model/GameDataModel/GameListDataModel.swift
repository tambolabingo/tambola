//
//  GameListDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 12/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class GameListDataModel: NSObject {
    //For Both
    var game_id:String = ""
    var game_key:String = ""
    var game_key_url:String = ""
    var game_name:String = ""
    var game_status:String = ""
    var participant_status:String = "";
    var play_status:Int = 0;
    var start_time:String = ""
    
//For Public
    var invitees_count:String = "0"
    var joining:String = ""
    var server_time:String = ""
//For Private
    var game_type:String = ""
    var isMyGames:Int = 0;
    var user_type:String = ""
    init(_ gameData: [String: Any],_ ispublic: Bool) {
        self.game_id = gameData["game_id"] as? String ?? ""
        self.game_key = gameData["game_id"] as? String ?? ""
        self.game_key_url = gameData["game_key_url"] as? String ?? ""
        self.game_name = gameData["game_name"] as? String ?? ""
        self.game_status = gameData["game_status"] as? String ?? ""
        self.participant_status = gameData["participant_status"] as? String ?? ""
        self.play_status = gameData["play_status"] as? Int ?? 0
        self.start_time = gameData["start_time"] as? String ?? ""
        
        if ispublic {
            self.invitees_count = String(gameData["invitees_count"] as! Int)
//            self.invitees_count = gameData["invitees_count"] as? String ?? ""
//            print(self.invitees_count)
            print(String(gameData["invitees_count"] as! Int))
            self.joining = gameData["joining"] as? String ?? ""
            self.server_time = gameData["server_time"] as? String ?? ""
        } else {
            self.game_type = gameData["game_type"] as? String ?? ""
            self.isMyGames = gameData["mygames"] as? Int ?? 0
            self.user_type = gameData["user_type"] as? String ?? ""
        }
    }
}
