//
//  ChatModel.swift
//  Chatmate
//
//  Created by Pooja Rana on 18/04/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class ChatModel: NSObject {
    var chatId: String               = ""
    
    var messages: [MessageModel]?
    
    init(chat: [String : Any]) {
        chatId            = String(format: "%d", chat["id"] as? Int ?? 0)
        
    }
}

class MessageModel: NSObject {
    
    var messageId: String          = ""
    var messageType: String        = ""
    var authorName: String         = ""
    var message: String            = ""
    var timestamp: Date?
}
