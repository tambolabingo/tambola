//
//  FBFriendsDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 04/09/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class FBFriendsDataModel: NSObject {
    var img_url:String = ""
    var user_id:String = ""
    var username:String = ""
    init(_ friend: [String: Any]) {
        super.init()
        let user_id = (friend["id"] as? String)!
        let url = String(format:ConstantModel.fbImageURL, user_id)
        self.img_url = url
        self.user_id = user_id
        self.username = (friend["name"] as? String)!
    }
}
