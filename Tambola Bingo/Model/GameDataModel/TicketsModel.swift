//
//  OptionsModel.swift
//  Talupp
//
//  Created by Talupp on 01/11/17.
//  Copyright © 2017 Talupp. All rights reserved.
//

import UIKit

class TicketsModel: NSObject {
    var ticketsArray = [[TicketModel]]()
    init(_ tickettag_Array: [[String : [String]]]) {
        let ticketCount = tickettag_Array.count
        for index in 0..<ticketCount{
            let numberArray = tickettag_Array[index]["number"]
            let tagArray    = tickettag_Array[index]["tag"]
            let ticketPositionArray = [1,4,7,10,13,16,19,22,25,2,5,8,11,14,17,20,23,26,3,6,9,12,15,18,21,24,27]
            var mainArray = [TicketModel]()
                for i in ticketPositionArray {
                    let tag = String(i)
                    if (tagArray?.contains(tag))! {
                        let tagIndex = tagArray?.firstIndex(of: tag)
                        let num = numberArray![tagIndex!]
                        let ticketVal = TicketModel(false, num, tag)
                        mainArray.append(ticketVal)
                    }else {
                        let ticketVal = TicketModel(true, "0", tag)
                        mainArray.append(ticketVal)
                    }
            }
            self.ticketsArray.append(mainArray)
        }
        }
}
class TicketModel: NSObject {
    var isCross:Bool = false
    var number:String = "0"
    var tag:String = "0"
    init(_ is_Cross: Bool, _ number: String, _ tag: String) {
        self.isCross = is_Cross
        self.number = number
        self.tag = tag
    }
}
