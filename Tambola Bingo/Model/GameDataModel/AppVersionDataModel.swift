//
//  AppVersionDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 06/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class AppVersionDataModel: NSObject, NSCoding {
    var apiVersion:String = ""
    var purchaseTicketMessage:String = ""
    var purchaseCoinsMessage:String = ""
    
    init(_ api: String, ticketMsg: String, coinMsg:String) {
        super.init()
        self.apiVersion = api
        self.purchaseTicketMessage = ticketMsg
        self.purchaseCoinsMessage = coinMsg
    }
    required convenience init(coder aDecoder: NSCoder) {
        let apiVersion              = aDecoder.decodeObject(forKey: "apiVersion") as! String
        let ticketTxt               = aDecoder.decodeObject(forKey: "purchaseTicketMessage") as! String
        let coinTxt                 = aDecoder.decodeObject(forKey: "purchaseCoinsMessage") as! String
        self.init(apiVersion, ticketMsg: ticketTxt, coinMsg: coinTxt)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(apiVersion, forKey: "apiVersion")
        aCoder.encode(purchaseTicketMessage, forKey: "purchaseTicketMessage")
        aCoder.encode(purchaseCoinsMessage, forKey: "purchaseCoinsMessage")
    }
}

class purchasesModel: NSObject, NSCoding {
    var amount:String = ""
    var coins:String = ""
    var id:String = ""
    var info:String = ""
    var label:String = ""
    var product:String = ""
    var product_id:String = ""
    var type:String = ""
    init(_ id: String, amount: String, coins:String, info:String, label:String, product:String, type:String,  productId:String) {
        super.init()
        self.id = id
        self.amount = amount
        self.coins = coins
        self.info = info
        self.label = label
        self.product = product
        self.type = type
        self.product_id = productId
    }
    required convenience init(coder aDecoder: NSCoder) {
        
        let pId                   = aDecoder.decodeObject(forKey: "id") as! String
        let pAmount               = aDecoder.decodeObject(forKey: "amount") as! String
        let pCoin                 = aDecoder.decodeObject(forKey: "coins") as! String
        let pInfo                 = aDecoder.decodeObject(forKey: "info") as! String
        let pLable                = aDecoder.decodeObject(forKey: "label") as! String
        let pProduct              = aDecoder.decodeObject(forKey: "product") as! String
        let pType                 = aDecoder.decodeObject(forKey: "type") as! String
        let productId             = aDecoder.decodeObject(forKey: "product_id") as? String ?? ""
        self.init(pId, amount: pAmount, coins: pCoin, info: pInfo, label: pLable, product: pProduct, type: pType,productId:productId)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(amount, forKey: "amount")
        aCoder.encode(coins, forKey: "coins")
        aCoder.encode(info, forKey: "info")
        aCoder.encode(label, forKey: "label")
        aCoder.encode(product, forKey: "product")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(product_id, forKey: "product_id")
    }
}

class gamePriceModel: NSObject, NSCoding {
    var autoCut:Int = 0
    var gameAccept:Int = 0
    var gameCreateExtraLimt:Int = 0
    var gameCreateLimt:Int = 0
    var gameCreateLowLimt:Int = 0
    var pvtGameCreateCost:Int = 0
    var ticketprice:String = ""
    var dailyRewards:String = ""
    init(_ autoCut: Int, gameAccept: Int, gameCExtraLimt:Int, gameCLimt:Int, gameCLowLimt:Int,pvtGameCost:Int, ticketprice:String, dailyRewards:String) {
        super.init()
        self.autoCut = autoCut
        self.gameAccept = gameAccept
        self.gameCreateExtraLimt = gameCExtraLimt
        self.gameCreateLimt = gameCLimt
        self.gameCreateLowLimt = gameCLowLimt
        self.ticketprice = ticketprice
        self.dailyRewards = dailyRewards
        self.pvtGameCreateCost = pvtGameCost
        
    }
    required convenience init(coder aDecoder: NSCoder) {
        
        let autoCut                      = aDecoder.decodeCInt(forKey: "autoCut")
        let gameAccept                  = aDecoder.decodeCInt(forKey: "gameAccept")
        let gameCExtraLimt         = aDecoder.decodeCInt(forKey: "gameCreateExtraLimt")
        let gameCLimt              = aDecoder.decodeCInt(forKey: "gameCreateLimt")
        let pvtGameCost             = aDecoder.decodeCInt(forKey: "pvtGameCreateCost")
        let gameCLowLimt           = aDecoder.decodeCInt(forKey: "gameCreateLowLimt")
        let ticketprice                 = aDecoder.decodeObject(forKey: "ticketprice") as! String
        let dailyRewards                = aDecoder.decodeObject(forKey: "dailyRewards") as! String
        self.init(Int(autoCut), gameAccept: Int(gameAccept), gameCExtraLimt: Int(gameCExtraLimt), gameCLimt: Int(gameCLimt), gameCLowLimt: Int(gameCLowLimt),pvtGameCost: Int(pvtGameCost) ,ticketprice: ticketprice, dailyRewards: dailyRewards)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(autoCut, forKey: "autoCut")
        aCoder.encode(gameAccept, forKey: "gameAccept")
        aCoder.encode(gameCreateExtraLimt, forKey: "gameCreateExtraLimt")
        aCoder.encode(gameCreateLimt, forKey: "gameCreateLimt")
        aCoder.encode(pvtGameCreateCost, forKey: "pvtGameCreateCost")
        aCoder.encode(gameCreateLowLimt, forKey: "gameCreateLowLimt")
        aCoder.encode(ticketprice, forKey: "ticketprice")
        aCoder.encode(dailyRewards, forKey: "dailyRewards")
    }
}

class pvtgamePriceModel: NSObject, NSCoding {
    var gameMinPlayer:String = "0"
    var gameMaxPlayer:String = "0"
    var gameCreateCost:String = "0"
    var gamePlayerCost:String = "0"
    init(_ minPlayer: String,_ maxPlayer: String,_ createPrice:String,_ playerPrice:String) {
        super.init()
        self.gameMinPlayer = minPlayer
        self.gameMaxPlayer = maxPlayer
        self.gameCreateCost = createPrice
        self.gamePlayerCost = playerPrice
        
    }
    required convenience init(coder aDecoder: NSCoder) {
        let minPlayer           = aDecoder.decodeObject(forKey: "gameMinPlayer") as! String
        let maxPlayer           = aDecoder.decodeObject(forKey: "gameMaxPlayer") as! String
        let createPrice         = aDecoder.decodeObject(forKey: "gameCreateCost") as! String
        let playerPrice         = aDecoder.decodeObject(forKey: "gamePlayerCost") as! String
        self.init(minPlayer, maxPlayer, createPrice, playerPrice)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(gameMinPlayer, forKey: "gameMinPlayer")
        aCoder.encode(gameMaxPlayer, forKey: "gameMaxPlayer")
        aCoder.encode(gameCreateCost, forKey: "gameCreateCost")
        aCoder.encode(gamePlayerCost, forKey: "gamePlayerCost")
    }
}
