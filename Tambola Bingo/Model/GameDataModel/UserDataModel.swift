//
//  userDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 24/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class UserDataModel: NSObject, NSCoding {

    
    var fbID:String?
    var fName:String?
    var lName:String?
    var email:String?
    var profilePhotoURL:String?
    var isGuest: Bool
    var privateChat:String?
    var publicChat:String?
    var userCoins:String?
    var userId:String?
    var accType:String?
    
    init(userDict: [String : Any]) {
        
        self.fbID              =  userDict["fb_id"] as? String ?? ""
        self.fName             =  userDict["first_name"] as? String ?? ""
        self.lName             =  userDict["last_name"] as? String ?? ""
        self.email             =  userDict["email"] as? String ?? ""
        self.profilePhotoURL   =  userDict["PhotoURL"] as? String ?? ""
        self.isGuest           =  userDict["isGuest"] as? Bool ?? false
        self.privateChat       =  userDict["private_chat"] as? String ?? ""
        self.publicChat        =  userDict["public_chat"] as? String ?? ""
        self.userCoins         =  userDict["userCoins"] as? String ?? "0000"
        self.userId            =  userDict["user_id"] as? String ?? ""
        self.accType            =  userDict["acc_type"] as? String ?? ""
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        let fbID            = aDecoder.decodeObject(forKey: "fbID") as! String
        let fName           = aDecoder.decodeObject(forKey: "fName") as! String
        let lName           = aDecoder.decodeObject(forKey: "lName") as! String
        let email           = aDecoder.decodeObject(forKey: "email") as! String
        let pic             = aDecoder.decodeObject(forKey: "profilePhotoURL") as! String
        let role            = aDecoder.decodeBool(forKey: "isGuest")
        let privateChat     = aDecoder.decodeObject(forKey: "privateChat") as! String
        let publicChat      = aDecoder.decodeObject(forKey: "publicChat") as! String
        let userCoins       = aDecoder.decodeObject(forKey: "userCoins") as? String ?? "0000"
        let userId          = aDecoder.decodeObject(forKey: "userId") as! String
        let accType          = aDecoder.decodeObject(forKey: "accType") as? String ?? ""
        self.init(userDict: ["fb_id": fbID, "first_name": fName, "last_name": lName, "email": email, "PhotoURL": pic, "isGuest": role,"private_chat": privateChat,"public_chat": publicChat,"userCoins": userCoins,"user_id": userId,"acc_type":accType])
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(fbID, forKey: "fbID")
        aCoder.encode(fName, forKey: "fName")
        aCoder.encode(lName, forKey: "lName")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(profilePhotoURL, forKey: "profilePhotoURL")
        aCoder.encode(isGuest, forKey: "isGuest")
        aCoder.encode(privateChat, forKey: "privateChat")
        aCoder.encode(publicChat, forKey: "publicChat")
        aCoder.encode(userCoins, forKey: "userCoins")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(accType, forKey: "accType")
    }
}
