//
//  GetParamModel.swift
//   Tambola Bingo
//
//  Created by Signity on 26/05/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit
import CoreLocation

class GetParamModel: NSObject {

    static let shared = GetParamModel()
//    static let user = UserDataManager.shared.getUserFromDB()
    // MARK: -  Login ViewController 
    // MARK: -  Login/Logout Parameters 
    func getLoginParam(fNameText: String,lNameText: String,emailText: String,fbId: String) -> [String: Any]? {
        let os_version = UIDevice.current.systemName + " " + UIDevice.current.systemVersion
        let param = ["first_name":fNameText,
                     "last_name":lNameText,
                     "email":emailText,
                     "fb_id": fbId,
                     "device_id": UserDefaultsHandler.deviceToken,
                     "device_token": UserDefaultsHandler.deviceToken,
                     "platform":"iphone",
                     "app_version":Bundle.main.infoDictionary!["CFBundleVersion"] as! String,
                     "mobile_device":UIDevice.current.model,
                     "ip_address":"",
                     "device_model_name":UIDevice.current.name,
                     "os_version":os_version,
                     "mobile_brand":"Apple",
                     "mobile_manufacturer":"Apple"] as [String: Any]
        return param
    }
    func getAppleSigninParam(fNameText: String,lNameText: String,emailText: String,appleId: String) -> [String: Any]? {
        let os_version = UIDevice.current.systemName + " " + UIDevice.current.systemVersion
        let param = ["first_name":fNameText,
                     "last_name":lNameText,
                     "email":emailText,
                     "apple_id": appleId,
                     "device_id": UserDefaultsHandler.deviceToken,
                     "device_token": UserDefaultsHandler.deviceToken,
                     "user_acc_type":"apple",
                     "platform":"iphone",
                     "app_version":Bundle.main.infoDictionary!["CFBundleVersion"] as! String,
                     "mobile_device":UIDevice.current.model,
                     "ip_address":"",
                     "device_model_name":UIDevice.current.name,
                     "os_version":os_version,
                     "mobile_brand":"Apple",
                     "mobile_manufacturer":"Apple"] as [String: Any]
        return param
    }
    func getSignInParm(emailText: String,password: String,acc_type: String) -> [String: Any]? {
        let username = acc_type == "email" ? "email" : "mobile_no"
        let os_version = UIDevice.current.systemName + " " + UIDevice.current.systemVersion
        let param = [username:emailText,
                     "password":password,
                     "user_acc_type": acc_type,
                     "platform":"iphone",
                     "app_version":Bundle.main.infoDictionary!["CFBundleVersion"] as! String,
                     "mobile_device":UIDevice.current.model,
                     "ip_address":"",
                     "device_model_name":UIDevice.current.name,
                     "os_version":os_version,
                     "mobile_brand":"Apple",
                     "mobile_manufacturer":"Apple"] as [String: Any]
        return param
    }
    func getSignUpParam(fNameText: String,lNameText: String,emailText: String,password: String,acc_type: String, code: String) -> [String: Any]? {
        let username = acc_type == "email" ? "email" : "mobile_no"
        let param = ["first_name":fNameText,
                     "last_name":lNameText,
                     username:emailText,
                     "password": password,
                     "user_acc_type":acc_type,
                     "platform":"iphone",
                     "country_code":code] as [String: Any]
        return param
    }
    func getVerifyMobileParam(mobileNum: String,otp: String,acc_type: String)-> [String: Any]?{
        let param = ["mobile_no":mobileNum,
                     "otp":otp,
                     "user_acc_type":acc_type,
                     "platform":"iphone"] as [String: Any]
        return param
    }
    func getLogoutParam()-> [String: Any]?{
        let param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                     "platform":"iphone"] as [String: Any]
        return param
    }
    func getVersionParam()-> [String: Any]?{
        let currentAppVersion = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
        let device = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                     "platform":"iphone",
                     "app_version":currentAppVersion,
                     "device_id": device] as [String: Any]
        return param
    }
    /*{ip=25.3.174.77, platform=android, app_version=4.10, device_id=cb60e3fd439b0d7e, API_VERSION=3.0, partner=3.0, user_id=168853}
 */
    func getUserCoinsParam() -> [String: Any]?{
        let param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getUserDetailParam(_ username: String, _ isnum:Bool) -> [String: Any]?{
        let titlename = isnum ? "mobile_no" : "email"
        let param = [titlename:username,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func updatePurchaseInfoParam(_ coinId: String) -> [String: Any]?{
           
           let os_version = UIDevice.current.systemName + " " + UIDevice.current.systemVersion
           let param = ["coins_id":coinId,
                        "user_id":(UserDefaultsHandler.user?.userId)!,
                        "current_app_version":Bundle.main.infoDictionary!["CFBundleVersion"] as! String,
                        "billing_response_code":UserDefaultsHandler.transactionCode ,
                        "debug_message":UserDefaultsHandler.transactionError ,
                        "ip_address":"",
                        "device_model_name":UIDevice.current.name,
                        "os_version":os_version,
                        "mobile_brand":"Apple",
                        "purpose":"inAppPurchase",
                        "platform":"iphone",
                        "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
           return param
       }
    func setUserCoinsParam(_ coins:String,_ game_id:String,_ transaction_type:String,_ purpose:String,_ forFriend:Bool) -> [String: Any]?{
          var param = [String: Any]()
          let purchaseDict = ["TransactionID": UserDefaultsHandler.transactionCode, "PurchaseMessage": UserDefaultsHandler.transactionError, "PurchaseDate":UserDefaultsHandler.transactiondate,"recieptData":UserDefaultsHandler.transactionreceiptData] as [String: String]
         //  let encoder = JSONEncoder()
        let encoder = JSONEncoder()
        if #available(iOS 13.0, *) {
          encoder.outputFormatting = .withoutEscapingSlashes
        } else {
          // Fallback on earlier versions
        }
            if let jsonData = try? encoder.encode(purchaseDict) {
              if let jsonString = String(data: jsonData, encoding: .utf8) {
                param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                          "purchase_json":jsonString,
                          "coins":coins,
                          "game_id":game_id,
                          "transaction_type":transaction_type,
                          "purpose":purpose,
                          "platform":"iphone",
                          "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
              }
            }
        
        if forFriend {
            param.updateValue((UserDefaultsHandler.user?.userId)!, forKey: "buyer_id")
        }
          return param
    }

    func getNotificationParam(state: String) -> [String: Any]?{
        let param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                     "state":state] as [String: Any]
        return param
    }
    func getPrivateGameListParam() -> [String: Any]?{
        let param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                     "time_zone":ConstantModel.time_Zone,
                     "time_zone_str": ConstantModel.localTimeZoneName,
                     "platform":"iphone"] as [String: Any]
        return param
    }
    func getPublicGameListParam() -> [String: Any]?{
        let param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                     "time_zone":ConstantModel.time_Zone,
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getJoinPublicGameParam(gameId: String) -> [String: Any]?{
        let param = ["game_id":gameId,
                     "user_id":(UserDefaultsHandler.user?.userId)!,
                     "time_zone":ConstantModel.time_Zone,
                     "time_zone_str": ConstantModel.localTimeZoneName,
                     "platform":"iphone"] as [String: Any]
        return param
    }
    func getPlayPublicGameParam(gameId: String) -> [String: Any]?{
        let param = ["game_id":gameId,
                     "API_VERSION":UserDefaultsHandler.apiVersion,
                     "invitee_id":(UserDefaultsHandler.user?.userId)!,
                     "time_zone":ConstantModel.time_Zone] as [String: Any]
        return param
    }
    func getPlayGameParam(gameId: String, startTime: String) -> [String: Any]?{
        let param = ["game_id":gameId,
                     "start_time":startTime,
                     "time_zone":ConstantModel.time_Zone,
                     "time_zone_str": ConstantModel.localTimeZoneName,
                     "platform":"iphone"] as [String: Any]
        return param
    }
    func getGameStatusParam(gameId: String, userType: String) -> [String: Any]?{
        
        let param = ["game_id":gameId,
                     "user_id":(UserDefaultsHandler.user?.userId)!,
                     "time_zone":ConstantModel.time_Zone,
                     "API_VERSION":UserDefaultsHandler.apiVersion,
                     "user_type": userType] as [String: Any]
        return param
    }
    func getGameClaimsParam(gameId: String) -> [String: Any]?{
        let param = ["game_id":gameId,
                     "API_VERSION":UserDefaultsHandler.apiVersion,
                     "platform":"iphone"] as [String: Any]
        return param
    }
    func getClaimPriceParam(gameId: String, claimStatus: String, numberSequence: String, claimName: String) -> [String: Any]?{
        let currentTime = DateConvertor.convertToString(slotDate: Date(), dateFormat: DateFormats.ServerDateFormat)
        let param = ["game_id":gameId,
                     "user_id":(UserDefaultsHandler.user?.userId)!,
                     "time_zone":ConstantModel.time_Zone,
                     "time_zone_str": ConstantModel.localTimeZoneName,
                     "API_VERSION":UserDefaultsHandler.apiVersion,
                     "status": claimStatus,
                     "numbers": numberSequence,
                     "claim_name": claimName,
                     "device_time":currentTime,
                     "claimVersion":3,
                     "platform":"iphone"] as [String: Any]
        return param
    }
    func getQuitGameParam(gameId: String) -> [String: Any]?{
        let param = ["game_id":gameId,
                     "user_id":(UserDefaultsHandler.user?.userId)!,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getEndGameParam(gameId: String) -> [String: Any]?{
        let param = ["game_id":gameId,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getShareResultParam(resultImage: UIImage) -> [String: Any]?{
        let param = ["txt_imageName":resultImage,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getGameHistoryParam(gameId: String) -> [String: Any]?{
        let param = ["game_id":gameId,
                     "user_id":(UserDefaultsHandler.user?.userId)!,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getAcceptRejectGameParam(gameId: String, status: String) -> [String: Any]?{
        let param = ["game_id":gameId,
                     "invitee_id":(UserDefaultsHandler.user?.userId)!,
                     "status": status,
                     "time_zone":ConstantModel.time_Zone,
                     "time_zone_str": ConstantModel.localTimeZoneName,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getCreateGameParam(gameName: String, staretTime: String, gametype:String, playersCount:String) -> [String: Any]?{
        let param = ["game_name":gameName,
                     "pvt_game_create_type":gametype,
                     "pvt_game_players":playersCount,
                     "user_id":(UserDefaultsHandler.user?.userId)!,
                     "start_time": staretTime,
                     "time_zone":ConstantModel.time_Zone,
                     "time_zone_str": ConstantModel.localTimeZoneName,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getInviteKeyGameParam(_ gamekey: String) -> [String: Any]?{
        let userName = (UserDefaultsHandler.user?.fName)! + " " + (UserDefaultsHandler.user?.lName)!
        let param = ["game_key":gamekey,
                     "invitee_name":userName,
                     "invitee_id":(UserDefaultsHandler.user?.userId)!,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getLeaderBoardParam(_ timeVal: String, myFriends: String) -> [String: Any]?{
        let param = ["time":timeVal,
                     "friends":myFriends,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getMyFriendslistParam() -> [String: Any]?{
        let param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getUserRecordsParam(userId: String) -> [String: Any]?{
        let param = ["user_id":userId,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getInviteFriendsParam(_ game_id: String, invitee_id: String, invitee_name: String) -> [String: Any]?{
        let param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                     "game_id":game_id,
                     "invitee_name":invitee_name,
                     "invitee_id":invitee_id,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    func getBlockUnblockParam(_ invite_id: String,_ gameId:String,_ status: Int) -> [String: Any]?{
        let param = ["user_id":(UserDefaultsHandler.user?.userId)!,
                     "invitee_id": invite_id,
                     "game_id":gameId,
                     "status":status,
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
    /*
     "“spam_fbid”:<fbid of person whose is being spammed>,
     ”request_fbid”:<fbid of requesting person>,
     ”platform”:<platform of app e.g android>"
 */
    func getSpamUserParam(_ spam_id:String) -> [String: Any]?{
        let param = ["request_fbid":(UserDefaultsHandler.user?.userId)!,
                     "spam_fbid":"",
                     "platform":"iphone",
                     "API_VERSION":UserDefaultsHandler.apiVersion] as [String: Any]
        return param
    }
}
