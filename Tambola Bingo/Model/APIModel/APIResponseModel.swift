//
//  APIResponseModel.swift
//  Tambola Bingo
//
//  Created by Signity on 14/06/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit

typealias CallbackHandler = ((Any?) -> Void)?

class APIResponseModel: NSObject {
    
    static let shared = APIResponseModel()
    // MARK: -  Login/Logout Response 
    func parseLoginResponse(response: [String: Any]?, accType:String, isSocial: Bool, sender: UIViewController, callback: CallbackHandler) {
        let userData = response?["data"] as? [String: Any]
        let userCoins = response?["userCoins"] as? String ?? "00"
        self.updateUser(userData: userData, isSocial: isSocial, coins: userCoins, acc_type: accType)
        self.performUIChanges(sender) { _ in
            callback!(userData)
        }
    }
    func updateUser(userData: [String: Any]?, isSocial: Bool , coins: String, acc_type:String) {
        let fName = userData?["first_name"] as? String
        let lName = userData?["last_name"] as? String
        if !isSocial {
            let userDict = ["first_name":fName as Any,
                            "last_name":lName as Any,
                            "isGuest": false,
                            "acc_type": acc_type ] as [String : Any]
            UserDefaultsHandler.user = UserDataModel.init(userDict: userDict)
        }
        
        let user = UserDefaultsHandler.user
        if fName != nil && lName != nil {
            user?.fName = fName
            user?.lName = lName
        }
        user?.privateChat = userData!["private_chat"] as? String
        user?.publicChat  = userData!["public_chat"] as? String
        user?.userId = userData!["user_id"] as? String
        user?.userCoins = coins
       UserDefaultsHandler.user = user
    }
// MARK: -  Dummy User Response 
    func parseDummyUserResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let users = response?["Users"] as? [[String: Any]]
        let players = self.saveDummyUsers(userData: users)
        self.performUIChanges(sender) { _ in
            callback!(players)
        }
    }
    func saveDummyUsers(userData: [[String: Any]]?) -> [PlayersDataModel]{
        var playersArray = [PlayersDataModel]()
        for user in userData!{
            let player = PlayersDataModel(user["img_url"] as! String, user["name"] as! String, "0000", true)
            playersArray.append(player)
        }
        return playersArray
    }
    // MARK: -  firebase token User Response 
    func parseFirebaseTokenResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let firbasetoken = response?["token"] as? String
        UserDefaultsHandler.gamefirebasetoken = firbasetoken ?? ""
        self.performUIChanges(sender) { _ in
            callback!(firbasetoken)
        }
    }
// MARK: -  My Games List  Response 
    func parseMyGameListResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let data = response?["data"] as? [[String: Any]]
        let games = self.saveGameList(gameData: data)
        self.performUIChanges(sender) { _ in
            callback!(games)
        }
    }
     func saveGameList(gameData: [[String: Any]]?) -> [GameListDataModel]{
        var gamesArray = [GameListDataModel]()
        for game in gameData! {
            let gameModel = GameListDataModel(game, false)
            gamesArray.append(gameModel)
        }
        return gamesArray
    }
    // MARK: -  Public Games List  Response 
    func parsePublicGameListResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let data = response?["games"] as? [[String: Any]]
        let games = self.savePublicGameList(gameData: data)
        self.performUIChanges(sender) { _ in
            callback!(games)
        }
    }
    func savePublicGameList(gameData: [[String: Any]]?) -> [GameListDataModel]{
        var gamesArray = [GameListDataModel]()
        for game in gameData! {
            let gameModel = GameListDataModel(game, true)
            gamesArray.append(gameModel)
        }
        return gamesArray
    }
// MARK: -  Join Public Games List  Response 
    func parseJoinGameResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let data = response!["message"] as? String
        CalendarManager.addEventToCalender(gameScheduleData: response!) { isSuccess in
            if !isSuccess {
                //If error while adding to callender
            }
        }
        self.performUIChanges(sender) { _ in
            callback!(data)
        }
    }
    
// MARK: -  Start Play Public Games List  Response 
    func parsePlayPublicGameResponse(response: [String: Any]?, isPublic:Bool, sender: UIViewController, callback: CallbackHandler) {
        
        let result = self.saveGameData(data: response, isPublic: isPublic)
        self.performUIChanges(sender) { _ in
            callback!(result)
        }
    }
    func saveGameData(data: [String: Any]?, isPublic:Bool) -> PlayGameDataModel{
        let sequence = data!["sequence"] as? String
        let sequenceArr = sequence?.components(separatedBy: ",")
        let ServerCTime  = data!["response_time"] as? String ?? ""
        let gameSTime   = data!["start_time"] as? String ?? ""
        let users = data!["usersDetail"] as? [[String: Any]]
        let claims = data!["GameWinPrizes"] as? [String: Int] ?? ["":0]
        let firebasetoken = data!["firebase_token"] as? String ?? ""
        let gametype = data!["pvt_game_create_type"] as? String ?? "1"
        UserDefaultsHandler.gamefirebasetoken = firebasetoken
        let players = self.saveInviteeUsers(userData: users)
        let gameDetails = PlayGameDataModel.init(3,gameType:gametype, gameName: "", gameId: "", startTime: gameSTime, serverDTime: ServerCTime, players: players, sequence: sequenceArr!, practice: false, claims: claims,isPublic: isPublic)
        return gameDetails
    }
    func saveInviteeUsers(userData: [[String: Any]]?) -> [PlayersDataModel]{
        var playersArray = [PlayersDataModel]()
        for user in userData!{
            var imgUrl = user["img_url"] as? String ?? ""
            let acc_type = user["user_acc_type"] as? String
            if acc_type == "facebook" && imgUrl == ""{
                imgUrl = String(format:ConstantModel.fbImageURL, (user["invitee_id"] as? String)!)
            }
            let player = PlayersDataModel(imgUrl, user["invitee_name"] as! String, (user["invitee_id"] as? String)!, false)
            playersArray.append(player)
        }
        return playersArray
    }
    // MARK: -  Public Game Detail Status Response 
    func parseMyFriendsListResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        
        let fbFriends = response?["Participants"] as? [[String: Any]]
        let finalResult = self.saveMyFriendsList(data: fbFriends!)
        self.performUIChanges(sender) { _ in
            callback!(finalResult)
        }
    }
    func saveMyFriendsList(data: [[String:Any]]) -> [LeadersDataModel]{
        var leaderArray = [LeadersDataModel]()
        for user in data {
                let leader = LeadersDataModel()
                leader.coins  = user["UserCoins"] as? String ?? ""
                leader.user_id  = user["user_id"] as? String ?? ""
                let acc_type = user["user_acc_type"] as? String ?? ""
                let imgUrl = acc_type == "facebook" ?  String(format:ConstantModel.fbImageURL, user["user_id"] as? String ?? "") : ""
                leader.img_url  = imgUrl
                leader.username  = user["user_name"] as? String ?? ""
                let scoreList = user["Scores"] as? [String:Any]
                leader.score.center = scoreList!["CENTER"] as? Int ?? 0
                leader.score.circle = scoreList!["CIRCLE"] as? Int ?? 0
                leader.score.corner = scoreList!["CORNER"] as? Int ?? 0
                leader.score.earlyFive = scoreList!["EARLY_FIVE"] as? Int ?? 0
                leader.score.firstLine = scoreList!["FIRST_LINE"] as? Int ?? 0
                leader.score.houseFull = scoreList!["HOUSEFULL"] as? Int ?? 0
                leader.score.luckSeven = scoreList!["LUCKY_SEVEN"] as? Int ?? 0
                leader.score.pyramid = scoreList!["PYRAMID"] as? Int ?? 0
                leader.score.secondLine = scoreList!["SECOND_LINE"] as? Int ?? 0
                leader.score.thirdLine = scoreList!["THIRD_LINE"] as? Int ?? 0
            //Add Values for BlockUnblock
                leaderArray.append(leader)
            }
        
        return leaderArray
    }
// MARK: -  Public Game Detail Status Response 
    func parseLeaderBoardListResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
       
        let fbFriends = response?["friends"] as? [String: Any]
        let globalFriends = response?["TopUser"] as? [String: Any]
        let finalResult = ["fbFriends": self.saveFriendsList(data: fbFriends!), "GlobalFriends": self.saveFriendsList(data: globalFriends!)]
        self.performUIChanges(sender) { _ in
            callback!(finalResult)
        }
    }
    func saveFriendsList(data: [String:Any]) -> [LeadersDataModel]{
        var leaderArray = [LeadersDataModel]()
        if (data["success"] != nil && data["success"] as! Int == 1) {
            let users = data["users"] as? [[String: Any]]
            for user in users! {
                let leader = LeadersDataModel()
                leader.luckRate = user["LuckRate"] as? Double ?? 0.00
                leader.totalwins = user["Totalwins"] as? String ?? ""
                leader.claims  = user["claims"] as? String ?? ""
                leader.coins  = user["coins"] as? String ?? ""
                leader.gamePlayed  = user["gamePlayed"] as? String ?? ""
                let id = user["user_id"] as? String ?? user["friend_id"] as? String ?? ""
                leader.user_id  = id
                let imgUrl = String(format:ConstantModel.fbImageURL, id)
                leader.img_url  = imgUrl
                leader.username  = user["username"] as? String ?? ""
                let scoreList = user["scores"] as? [String:Any]
                leader.score.center = scoreList!["CENTER"] as? Int ?? 0
                leader.score.circle = scoreList!["CIRCLE"] as? Int ?? 0
                leader.score.corner = scoreList!["CORNER"] as? Int ?? 0
                leader.score.earlyFive = scoreList!["EARLY_FIVE"] as? Int ?? 0
                leader.score.firstLine = scoreList!["FIRST_LINE"] as? Int ?? 0
                leader.score.houseFull = scoreList!["HOUSEFULL"] as? Int ?? 0
                leader.score.luckSeven = scoreList!["LUCKY_SEVEN"] as? Int ?? 0
                leader.score.pyramid = scoreList!["PYRAMID"] as? Int ?? 0
                leader.score.secondLine = scoreList!["SECOND_LINE"] as? Int ?? 0
                leader.score.thirdLine = scoreList!["THIRD_LINE"] as? Int ?? 0
                //Add Values for BlockUnblock
               leaderArray.append(leader)
            }
        }
        return leaderArray
    }
// MARK: -  Public Game Detail Status Response 
    func parsePublicGameDetailResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        if response?["data"] != nil{
            self.performUIChanges(sender) { _ in
                callback!(response)
            }
        } else {
            let userArray = response?["users_detail"] as? [[String: Any]]
            let gameDetail = response?["game_detail"] as? [String: Any]
            let gameDetailData = GameDetailDataModel(gameDetail!, userArray!)
            self.performUIChanges(sender) { _ in
                callback!(gameDetailData)
            }
        }
    }
// MARK: -  Public/Private Claim Price Response 
    func parseClaimPriceResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        self.performUIChanges(sender) { _ in
            callback!(response)
        }
    }
// MARK: -  Public/Private Game Claim Status Response 
    func parseGameClaimsResponse(response: [String: Any]?, claimPrices: [String:Int]?,sender: UIViewController, callback: CallbackHandler) {
        let claimData = response?["claims"] as? [String: Any]
        let resultData = self.saveClaimData(claimData: claimData!, prices: claimPrices)
        self.performUIChanges(sender) { _ in
            callback!(resultData)
        }
    }
    func saveClaimData(claimData: [String: Any], prices: [String:Int]?) -> [ResultDataModel]{
        var resultArray = [ResultDataModel]()
        let claimEarlyFive = claimData["EARLY_FIVE"] as? [String: Any]
        let claimLuckySeven = claimData["LUCKY_SEVEN"] as? [String: Any]
        let claimTopLine = claimData["FIRST_LINE"] as? [String: Any]
        let claimMiddleLine = claimData["SECOND_LINE"] as? [String: Any]
        let claimBottomLine = claimData["THIRD_LINE"] as? [String: Any]
        let claimPyramid = claimData["PYRAMID"] as? [String: Any]
        let claimCorner = claimData["CORNER"] as? [String: Any]
        let claimCenter = claimData["CENTER"] as? [String: Any]
        let claimCircle = claimData["CIRCLE"] as? [String: Any]
        let claimFullHouse = claimData["HOUSEFULL"] as? [String: Any]
        let myUserId = UserDefaultsHandler.user?.userId
        var winningAmount = 0
        if claimEarlyFive!["claim"] as? String == "1" {
            let userName = claimEarlyFive!["username"] as? String
            let id = claimEarlyFive!["user_id"] as? String
            let acc_type = claimEarlyFive!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimEarlyFive)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(ConstantModel.claim.claimEarlyFive)
                }
            }
            winningAmount = myUserId == id ? (winningAmount + (prices?["EARLY_FIVE"] ?? 0)) : winningAmount
        }
        if claimLuckySeven!["claim"] as? String == "1" {
            let userName = claimLuckySeven!["username"] as? String
            let id = claimLuckySeven!["user_id"] as? String
            let acc_type = claimLuckySeven!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            winningAmount = myUserId == id ? (winningAmount + (prices?["LUCKY_SEVEN"] ?? 0)) : winningAmount
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimLuckySeven)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(ConstantModel.claim.claimLuckySeven)
                }
            }
        }
        if claimTopLine!["claim"] as? String == "1" {
            let userName = claimTopLine!["username"] as? String
            let id = claimTopLine!["user_id"] as? String
            let acc_type = claimTopLine!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            winningAmount = myUserId == id ? (winningAmount + (prices?["FIRST_LINE"] ?? 0)) : winningAmount
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimTopLine)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(ConstantModel.claim.claimTopLine)
                }
            }
        }
        if claimMiddleLine!["claim"] as? String == "1" {
            let userName = claimMiddleLine!["username"] as? String
            let id = claimMiddleLine!["user_id"] as? String
            let acc_type = claimMiddleLine!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            winningAmount = myUserId == id ? (winningAmount + (prices?["SECOND_LINE"] ?? 0)) : winningAmount
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimMiddleLine)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(ConstantModel.claim.claimMiddleLine)
                }
            }
        }
        if claimBottomLine!["claim"] as? String == "1" {
            let userName = claimBottomLine!["username"] as? String
            let id = claimBottomLine!["user_id"] as? String
            let acc_type = claimBottomLine!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            winningAmount = myUserId == id ? (winningAmount + (prices?["THIRD_LINE"] ?? 0)) : winningAmount
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimBottomLine)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(ConstantModel.claim.claimBottomLine)
                }
            }
        }
        if claimPyramid!["claim"] as? String == "1" {
            let userName = claimPyramid!["username"] as? String
            let id = claimPyramid!["user_id"] as? String
            let acc_type = claimPyramid!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            winningAmount = myUserId == id ? (winningAmount + (prices?["PYRAMID"] ?? 0)) : winningAmount
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimPyramid)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(ConstantModel.claim.claimPyramid)
                }
            }
        }
        if claimCircle!["claim"] as? String == "1" {
            let userName = claimCircle!["username"] as? String
            let id = claimCircle!["user_id"] as? String
            let acc_type = claimCircle!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            winningAmount = myUserId == id ? (winningAmount + (prices?["CIRCLE"] ?? 0)) : winningAmount
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimCircle)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(ConstantModel.claim.claimCircle)
                }
            }
        }
        if claimCorner!["claim"] as? String == "1" {
            let userName = claimCorner!["username"] as? String
            let id = claimCorner!["user_id"] as? String
            let acc_type = claimCorner!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            winningAmount = myUserId == id ? (winningAmount + (prices?["CORNER"] ?? 0)) : winningAmount
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimCorner)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(ConstantModel.claim.claimCorner)
                }
            }
        }
        if claimCenter!["claim"] as? String == "1" {
            let userName = claimCenter!["username"] as? String
            let id = claimCenter!["user_id"] as? String
            let acc_type = claimCenter!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            winningAmount = myUserId == id ? (winningAmount + (prices?["CENTER"] ?? 0)) : winningAmount
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimCenter)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(ConstantModel.claim.claimCenter)
                }
            }
        }
        if claimFullHouse!["claim"] as? String == "1" {
            let userName = claimFullHouse!["username"] as? String
            let id = claimFullHouse!["user_id"] as? String
            let acc_type = claimFullHouse!["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            winningAmount = myUserId == id ? (winningAmount + (prices?["HOUSEFULL"] ?? 0)) : winningAmount
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,ConstantModel.claim.claimFullHouse)
            let dataArray = resultArray.filter{$0.userId == id}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                  winner.resultArray.append(ConstantModel.claim.claimFullHouse)
                }
            }
        }
        UserDefaultsHandler.winningAmount = winningAmount
        return resultArray
    }
    // MARK: -  Public/Private Game History Response 
    func parseGameHistoryResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let gameData = response?["data"] as? [[String: Any]]
        let resultData = self.saveHistoryData(gameData!)
        self.performUIChanges(sender) { _ in
            callback!(resultData)
        }
    }
    func saveHistoryData(_ data: [[String: Any]]) -> [ResultDataModel]{
         var resultArray = [ResultDataModel]()
        for result in data {
            let userName = result["user"] as? String
            let id = result["user_id"] as? String
            let status = result["status"] as? String
            let acc_type = result["user_acc_type"] as? String ?? ""
            let imageUrl = acc_type == "facebook" ? String(format:ConstantModel.fbImageURL, id!) : ""
            let claimStr = self.getCliamString(claim: status!)
            let WinnerModel = ResultDataModel(id!, imageUrl, userName!,claimStr)
            let dataArray = resultArray.filter{$0.userId == id!}
            if dataArray.count == 0 {
                resultArray.append(WinnerModel)
            } else {
                for winner in dataArray {
                    winner.resultArray.append(claimStr)
                }
            }
        }
        return resultArray
    }
    func getCliamString(claim: String) -> String{
        var result = ""
        switch claim {
        case "Completed Top Line.":
            result = ConstantModel.claim.claimTopLine
            break
        case "Completed Middle Line.":
            result = ConstantModel.claim.claimMiddleLine
            break
        case "Completed Bottom Line.":
            result = ConstantModel.claim.claimBottomLine
            break
        case "Completed Corners.":
            result = ConstantModel.claim.claimCorner
            break
        case "Completed Early Five.":
            result = ConstantModel.claim.claimEarlyFive
            break
        case "Completed Lucky Seven.":
            result = ConstantModel.claim.claimLuckySeven
            break
        case "Completed Center.":
            result = ConstantModel.claim.claimCenter
            break
        case "Completed Pyramid.":
            result = ConstantModel.claim.claimPyramid
            break
        case "Completed Circle.":
            result = ConstantModel.claim.claimCenter
            break
        case "Completed House.":
            result = ConstantModel.claim.claimFullHouse
            break
        default:
            break
        }
        return result
    }
// MARK: -  Purchase API Response 
    func parsePurchaseAPIResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        
        let purchaseData = response!["purchases"] as? [[String: Any]]
        let finalResult = self.savePurchaseInfo(purchaseData!)
        self.performUIChanges(sender) { _ in
            callback!(finalResult)
        }
    }
    func savePurchaseInfo(_ purchaseData: [[String: Any]]) -> Bool{
        var purchaseArray = [purchasesModel]()
        for pur in purchaseData {
            let id = (pur["id"] as? String)!
            if id != "6" {
                let pAmount = (pur["amount"] as? String)!
                let pCoin = (pur["coins"] as? String)!
                let pInfo = (pur["info"] as? String)!
                let pLable = (pur["label"] as? String)!
                let pProduct = (pur["product"] as? String)!
                let pType = (pur["type"] as? String)!
                let proId = pur["productId"] as? String ?? ""
                let purchase = purchasesModel(id, amount: pAmount, coins: pCoin, info: pInfo, label: pLable, product: pProduct, type: pType, productId: proId)
                purchaseArray.append(purchase)
            }
        }
        UserDefaultsHandler.coinPurchaseInfo = purchaseArray
        return true
    }
    // MARK: -  Purchase API Response 
    func parseOffersAPIResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        
        let offersData = response!["data"] as? [[String: Any]]
        let finalResult = self.saveOffersInfo(offersData!)
        self.performUIChanges(sender) { _ in
            callback!(finalResult)
        }
    }
    func saveOffersInfo(_ offersData: [[String: Any]]) -> [MyOffersDataModel]{
        var offersArray = [MyOffersDataModel]()
        for offer in offersData {
            let offerCoins = (offer["coins"] as? String)!
            let offerLink = (offer["offer_link"] as? String)!
            let offerDecs = (offer["description"] as? String)!
            let offerImage = (offer["image"] as? String)!
            let offerTitle = (offer["title"] as? String)!
            let offer = MyOffersDataModel(offerLink, desc: offerDecs, coins: offerCoins, title: offerTitle, img: offerImage)
            offersArray.append(offer)
        }
        
        return offersArray
    }
// MARK: -  Common Response 
    func parseCommonResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        self.performUIChanges(sender) { _ in
            callback!(response)
        }
    }
    
// MARK: -   Perform UI Changes 
    func performUIChanges(_ sender: UIViewController, callback: CallbackHandler) {
        let when = DispatchTime.now() + 0.15
        DispatchQueue.main.asyncAfter(deadline: when) {
            EventManager.hideloader()
            callback!(nil)
        }
    }
}
