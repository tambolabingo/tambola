//
//  ApiCallModel.swift
//  Tambola Bingo
//
//  Created by Signity on 05/06/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit

typealias SuccessHandler = ([String: Any]?) -> Void
typealias FailureHandler = ((String?) -> Void)?

struct APIConstants {
    //Not using static base url we are using dynamic from "getMaintenanceAPI" and setting up the endpoit value to userdefault "projectBaseUrl"
    //Staging
//    static let BaseURL      = "https://tambolabingo.com/api2/api/"
//    static let BaseURL      = "https://tambolabingo.com/api2/api/"
    //Live
//    static let BaseURL      = "https://tambolabingo.com/api/api/"
//    static let BaseURL      = "https://tambolabingo.com/api/public/index.php?_url=/api/"
    
    static let amazoneEPapiURL      = "https://s3.amazonaws.com/tambola/api/endpoint.json"
    static let GetAPI               = "GET"
    static let PostAPI              = "POST"
    static let PutAPI               = "PUT"
    
    static let getServerTime        = "currentTime"
    static let getToken             = "createToken"
    static let Version              = "version"
    static let userCoins            = "userCoins"
    static let userInfo             = "findUser"
    static let setUserCoins         = "setUserCoins"
    static let getFirbaseToken      = "JwtFbToken"
    static let updatePurchaseinfo   = "billingError"
    static let setFriendsCoins      = "setUserCoinsOther"
    static let Register             = "registration"
    static let SignUp               = "userSignUp"
    static let MobileSignUp         = "userSignUpMobile"
    static let resendOTP            = "reSendOtp"
    static let Signin               = "signin"
    static let Logout               = "userLogout"
    static let verifyOtp            = "verifyMobile"
    static let forgotPass           = "forgotPassword"
    static let notification         = "onOffNotification"
    static let dummyUsers           = "dummyUsers"
    static let MyGamesList          = "myGames"
    static let publicGamesList      = "publicGames"
    static let joinPublicGame       = "joinPublicGame"
    static let GamesHistory         = "gamesHistory"
    static let AcceptRejectGame    = "acceptGame"
    static let StartOrganizerGame   = "organizerStartGame"
    static let StartPublicGame      = "inviteeStartGame"
    static let PublicGameStatus     = "playingGameStatus"
    static let GetGameClaims        = "getGameClaims"
    static let claimPrice           = "claim"
    static let quitGame             = "userQuitGame"
    static let endGame              = "endGame"
    static let shareResult          = "uploadImage"
    static let createNewGame        = "creategame"
    static let inviteViaKey         = "inviteviakey"
    static let leaderboard          = "leaderBoard"
    static let myLatestInvite       = "myLatestInvitee"
    static let myDetails            = "myPlayRecords"
    static let inviteFriends        = "invite"
    static let purchaseCoins        = "inAppPurchaseInfo"
    static let getFreeCoinOffers    = "getOffers"
    static let reportUserSpam       = "reportSpam"
    static let blockUnblockUser     = "blockParticipant"
}

class ApiCallModel: NSObject {

    static let shared = ApiCallModel()

    func apiCall(isSilent: Bool, url: String, parameters: [String: Any]?, type: String, isMultipart: Bool, isKey: Bool, delegate: UIViewController, success: @escaping SuccessHandler, failure: FailureHandler) {
        if EventManager.checkForInternetConnection() {
            
            if isSilent == false { EventManager.showloader() }
            
            WebService().uploadOrFetchData(paramRequest: parameters, baseURL: url, method: type, isMultipart: isMultipart, isKey: isKey, withCompletion: { response in
                
                if (response["Add_Maintenance"] != nil) {
                     success(response)
                }else if (response["success"] as? Bool ?? false)!{
                    success(response)
                } else {
                    DispatchQueue.main.async {
                        EventManager.hideloader()
                        var payload = response["payload"] as? [String: Any]
                        if payload == nil && response["errorcode"] != nil{
                            payload = response
                        }
                        if payload != nil && payload!["errorcode"] != nil && (payload!["errorcode"] as? Int == 901 || payload!["errorcode"] as? String == "901" ){
                            
                           failure?(ConstantModel.message.tokenError)
                            
                        } else if payload != nil && payload!["tokenError"] != nil && (payload!["tokenError"] as? Bool)! == true {
                            DispatchQueue.main.async {
                                failure?(response["message"] as? String)
//                                DbHandler.shared.removeUser()
                                UserDefaultsHandler.removeAllUserDefaults()
                                NavigationManager.checkWhereToNav()
                                //Logout and Back To home screen
                            }
                        } else if payload == nil && (response["claims"] != nil || response["url"] != nil || response["friends"] != nil || response["purchases"] != nil || response["data"] != nil){
                            success(response)
                        } else {
                            failure?(response["message"] as? String)
                        }
                    }
                }
            }, failure: { error in
                DispatchQueue.main.async {
                    EventManager.hideloader()
                    failure?(error.localizedDescription)
                    
                }
            })
        } else {
           
            failure?(ConstantModel.api.NoInternet)
        }
    }
}
