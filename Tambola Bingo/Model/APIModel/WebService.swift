//
//  WebService.swift
//  Tambola Bingo
//
//  Created by Signity on 4/27/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit
import Foundation

class WebService: NSObject {
    var completion: (([String: Any]) -> Void)?
    var failure: ((Error) -> Void)?
    
    internal  func uploadOrFetchData(paramRequest: [String: Any]?, baseURL urlString: String, method methodName: String, isMultipart: Bool, isKey: Bool, withCompletion completionBlock: (([String: Any]) -> Void)?, failure failureBlock: ((Error) -> Void)?) {
        self.completion = completionBlock
        self.failure    = failureBlock
        do { 
            var urlRequest = URLRequest(url: URL(string: urlString)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 40)
            if isKey {
                urlRequest.setValue(UserDefaultsHandler.secretToken, forHTTPHeaderField: "X-SECRET-KEY")
            } else {
                urlRequest.setValue(UserDefaultsHandler.secretToken, forHTTPHeaderField: "X-SECRET-TOKEN")
            }
//            urlRequest.setValue(UserDefaultsHandler.secretToken, forHTTPHeaderField: "X-SECRET-TOKEN")
            urlRequest.httpMethod = methodName
            if isMultipart{
                let boundary = "Boundary-\(UUID().uuidString)"
                urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                if paramRequest != nil {
                    urlRequest.httpBody = createBody(parameters: paramRequest!,
                                                 boundary: boundary)
                }
            } else {
                var paramData: Data!
                if paramRequest != nil {
                    paramData = try JSONSerialization.data(withJSONObject: paramRequest as Any, options: .prettyPrinted)
                }
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = paramData
            }
            self.getDataByRequest(urlRequest)
        } catch {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.completion!(["success": false, "description": ConstantModel.api.WeakInternet] as [String: Any])
        }
    }
    func createBody(parameters: [String: Any],
                    boundary: String) -> Data {
        
        let body = NSMutableData()
        let boundaryPrefix = "--\(boundary)\r\n"
        var fname    = "txt_imageName.png"
        var mimetype = "image/png"
        for (key, value) in parameters {
            if value is UIImage {
                //Define the multipart request type
                let image_data = (value as? UIImage)!.pngData()
                body.append("--\(boundary)\r\n".data(using: .utf8)!)
                body.append(("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(fname)\"\r\n").data(using: .utf8)!)
                body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!)
                body.append(image_data!)
                body.append("\r\n".data(using: .utf8)!)
                body.append("--\(boundary)--\r\n".data(using: .utf8)!)
                
            }else if value is NSURL {
                var fileData: NSData?
                let fileValue = (value as? NSURL)!
                fileData = NSData(contentsOf: fileValue as URL)
                if fileData != nil{
                    body.append("--\(boundary)\r\n".data(using: .utf8)!)
                    fname    = UserDefaultsHandler.attachementName
                    mimetype = UserDefaultsHandler.mimeType
                    body.append(("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(fname)\"\r\n").data(using: .utf8)!)
                    body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!)
                    body.append(fileData! as Data)
                    body.append("\r\n".data(using: .utf8)!)
                    body.append("--\(boundary)--\r\n".data(using: .utf8)!)
                }
            } else {
                body.appendString(boundaryPrefix)
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        body.appendString("--".appending(boundary.appending("--")))
        return body as Data
    }
    func getDataByRequest(_ request: URLRequest) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let _dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, _, error) in
            if error?._code == -1001 || error?.localizedDescription == "The request timed out." {
                self.failure!((error! as Error?)!)
            } else if error?.localizedDescription != "cancelled" {
                if error == nil {
                    self.getData(data!)
                } else {
                    self.failure!((error! as Error?)!)
                }
            } else {
                self.failure!((error! as Error?)!)
            }
        }) 
        _dataTask.resume()
    }
    
    fileprivate func getData(_ data: Data) {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            print(json!)
            self.completion!(json!)
        } catch let error as NSError {
            self.failure!(error)
        }
    }
}
extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
