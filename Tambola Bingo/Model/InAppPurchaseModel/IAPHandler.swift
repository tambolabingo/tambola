//
//  IAPHandler.swift
//  Tambola Bingo
//
//  Created by signity on 23/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import StoreKit
enum IAPHandlerAlertType{
    case disabled
    case restored
    case purchased
    case fetched
    case canceled
    func message() -> String{
        switch self {
        case .disabled: return "Purchases are disabled in your device!"
        case .restored: return "You've successfully restored your purchase!"
        case .purchased: return "You've successfully bought this purchase!"
        case .fetched: return "You've successfully fetch all products!"
        case .canceled: return "You've canceled or failed to made a purchase!"
        }
    }
}
class IAPHandler: NSObject {
    static let shared = IAPHandler()
    
//    let CONSUMABLE_PURCHASE_PRODUCT_ID = "testpurchase"
//    let NON_CONSUMABLE_PURCHASE_PRODUCT_ID = "non.consumable"
//    let CONSUMABLE_PURCHASE_PRODUCT_ID = ["com.tambolabingo.5k", "com.tambolabingos.20k", "com.tambolabingos.50k", "com.tambolabingo.100k","com.tambolabingo.updateproversion"]
    let CONSUMABLE_PURCHASE_PRODUCT_ID = ["com.tambolabingo.5k", "com.tambolabingo.10k", "com.tambolabingos.30k", "com.tambolabingos.50k"]

    fileprivate var productID = ""
    fileprivate var productsRequest = SKProductsRequest()
    fileprivate var iapProducts = [SKProduct]()
    
    var purchaseStatusBlock: ((IAPHandlerAlertType) -> Void)?
    
    // MARK: - MAKE PURCHASE OF A PRODUCT
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    
    func purchaseMyProduct(index: Int){
        if iapProducts.count == 0 {
            purchaseStatusBlock?(.canceled)
            return
        }
    
        if self.canMakePurchases() {
            var productAvilable = false
            for product in iapProducts{
                if product.productIdentifier == CONSUMABLE_PURCHASE_PRODUCT_ID[index]{
                    productAvilable = true
                    let payment = SKPayment(product: product)
                    SKPaymentQueue.default().add(self)
                    SKPaymentQueue.default().add(payment)
                    
                    print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
                    productID = product.productIdentifier
                }
            }
            if !productAvilable {
                purchaseStatusBlock?(.canceled)
            }
//            purchaseStatusBlock?(.fetched)
//            let product = iapProducts[index]
            
        } else {
            purchaseStatusBlock?(.disabled)
        }
    }
    func purchaseMyProduct(_ productId:String){
            if iapProducts.count == 0 {
                purchaseStatusBlock?(.canceled)
                return
            }
        
            if self.canMakePurchases() {
                var productAvilable = false
                for product in iapProducts{
                    if product.productIdentifier == productId{
                        productAvilable = true
                        let payment = SKPayment(product: product)
                        SKPaymentQueue.default().add(self)
                        SKPaymentQueue.default().add(payment)
                        
                        print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
                        productID = product.productIdentifier
                    }
                }
                if !productAvilable {
                    purchaseStatusBlock?(.canceled)
                }
    //            purchaseStatusBlock?(.fetched)
    //            let product = iapProducts[index]
                
            } else {
                purchaseStatusBlock?(.disabled)
            }
        }
    // MARK: - RESTORE PURCHASE
    func restorePurchase(){
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    
    // MARK: - FETCH AVAILABLE IAP PRODUCTS
    func fetchAvailableProducts(_ productIds: [String]){
        // Put here your IAP Products ID's
        if SKPaymentQueue.canMakePayments() {
//            let productIdentifiers = NSSet(array: CONSUMABLE_PURCHASE_PRODUCT_ID)
            let productIdentifiers = NSSet(array: productIds)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as Set<NSObject> as! Set<String>)
            
            productRequest.delegate = self
            productRequest.start()
            
        } else {
            purchaseStatusBlock?(.disabled)
            print("Cannot perform In App Purchases.")
        }
    }
}
extension IAPHandler: SKProductsRequestDelegate, SKPaymentTransactionObserver{
    // MARK: - REQUEST IAP PRODUCTS
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        
        if (response.products.count > 0) {
            iapProducts = response.products
            let purchaseArray = (UserDefaultsHandler.coinPurchaseInfo)!
            var newpurchaseArray = [purchasesModel]()
            for product in iapProducts{
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .currency
                numberFormatter.locale = product.priceLocale
                let price1Str = numberFormatter.string(from: product.price)
                print(product.localizedTitle + "\nfor just \(price1Str!)")
                let itemInlist = purchaseArray.filter{$0.product_id == product.productIdentifier}
                if itemInlist.count > 0 {
                    let funalPrice = price1Str!.replacingOccurrences(of: ".00", with: "")
                    /*let pricetext = product.localizedTitle + " for  \(funalPrice)"
                
                    itemInlist[0].label = pricetext.replacingOccurrences(of: "-", with: "")*/
                    itemInlist[0].label = funalPrice
                    newpurchaseArray.append(itemInlist[0])
                }
            }
            UserDefaultsHandler.coinPurchaseInfo = newpurchaseArray
            //First Fetching of all products
            purchaseStatusBlock?(.fetched)
        } else {
            purchaseStatusBlock?(.disabled)
            print("Cannot perform In App Purchases.")
        }
    }
    func updatePurchaseInfo(_ purchaseData: [[String: Any]]) -> Bool{
        var purchaseArray = [purchasesModel]()
        for pur in purchaseData {
            let id = (pur["id"] as? String)!
            if id != "6" {
                let pAmount = (pur["amount"] as? String)!
                let pCoin = (pur["coins"] as? String)!
                let pInfo = (pur["info"] as? String)!
                let pLable = (pur["label"] as? String)!
                let pProduct = (pur["product"] as? String)!
                let pType = (pur["type"] as? String)!
                let proId = pur["productId"] as? String ?? ""
                let purchase = purchasesModel(id, amount: pAmount, coins: pCoin, info: pInfo, label: pLable, product: pProduct, type: pType, productId: proId)
                purchaseArray.append(purchase)
            }
        }
        UserDefaultsHandler.coinPurchaseInfo = purchaseArray
        return true
    }
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        purchaseStatusBlock?(.restored)
    }
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        UserDefaultsHandler.transactionCode = queue.transactions[0].transactionIdentifier ?? ""
        UserDefaultsHandler.transactionError = error.localizedDescription
        purchaseStatusBlock?(.canceled)
    }
    // MARK:- IAP PAYMENT QUEUE
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                let trns = transaction as! SKPaymentTransaction
                UserDefaultsHandler.transactionCode = trns.transactionIdentifier ?? ""
                UserDefaultsHandler.transactionError = trns.error?.localizedDescription ?? ""
                switch trans.transactionState {
                case .purchased:
                    print("purchased")
                    UserDefaultsHandler.transactionComplete = true
                    UserDefaultsHandler.transactiondate = DateConvertor.convertIntoString(slotDate: trns.transactionDate ?? Date(), dateFormat: DateFormats.ServerDateFormat)
                    
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    purchaseStatusBlock?(.purchased)
                    self.receiptValidation()
                    break
                    
                case .failed:
                    print("failed")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    purchaseStatusBlock?(.canceled)
                    break
                case .restored:
                    print("restored")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
//                    self.receiptValidation()
                    break
                    
                default:
                   // purchaseStatusBlock?(.canceled)
                    break
                }}}
    }
    
    func receiptValidation() {
        
        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        print(recieptString ?? "")
        UserDefaultsHandler.transactionreceiptData = recieptString ?? ""
    }
    
}
