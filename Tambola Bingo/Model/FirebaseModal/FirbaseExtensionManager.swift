//
//  FirbaseExtensionManager.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 27/05/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit
import FirebaseAuth

class FirbaseExtensionManager: NSObject {
    static let shared = FirbaseExtensionManager()
    let firebaseAuth = Auth.auth()
    // MARK: -  Get User Coins API Call 
    func firbaseloginAuth(_ delegate: UIViewController){
            
            if (firebaseAuth.currentUser == nil) {
                //New user get custom token from server and signin to firbase call toke api
                BaseAPIHandler.shared.getFirbaseCustomTokeAPI(delegate: delegate) { isCallSucess in
                    let gametoken = UserDefaultsHandler.gamefirebasetoken
                    if gametoken != ""{
                        self.firebaseAuth.signIn(withCustomToken: gametoken ) { (user, error) in
                        // ...
                        print("Signin to firebase sucessfully")
                        }
                    }
                }
            } else {
                firebaseAuth.currentUser?.getIDToken(completion: { (token, error) in
                    if error != nil {
                        error?.localizedDescription
                    }
                })
        }
    }
    func firbaseSignout(){
        do {
          try firebaseAuth.signOut()
        } catch let signOutError as NSError {
          print ("Error signing out: %@", signOutError)
        }
    }
}
