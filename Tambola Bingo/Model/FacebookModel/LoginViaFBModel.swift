//
//  LoginViaFBModel.swift
//  Tambola Bingo
//
//  Created by Signity on 20/04/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViaFBModel: NSObject {
    
    static let shared = LoginViaFBModel()
    private override init() { }
    
    var success: (([String: Any]) -> Void)?
    var failure: ((String) -> Void)?
    
    
    func addFBLoginButton(sender: UIView) {
        let loginFbButton = FBLoginButton()
        loginFbButton.frame = CGRect(x:sender.frame.width/2-loginFbButton.frame.width/2, y: 260, width: loginFbButton.frame.width, height: loginFbButton.frame.height)
        sender.addSubview(loginFbButton)
    }
    func logOutFromFb(){
        if AccessToken.current != nil {
            LoginManager().logOut()
        }
    }
    func viaGraphAPI(sender: UIViewController, success: (([String: Any]) -> Void)?, failure: ((String) -> Void)?) {
        
        self.success = success
        self.failure = failure
        if AccessToken.current != nil {
            LoginManager().logOut()
            //return
        }

//        LoginManager().loginBehavior = FBSDKLoginBehavior.browser
        LoginManager().logIn(permissions: ["public_profile", "email", "user_friends"], from: sender) { (result, error) in
            if error != nil {
                self.failure?(error?.localizedDescription ?? "")
                
            } else if (result?.isCancelled)! {
                self.failure?("Cancelled")
                
            } else {
                self.getFBSDKGraphRequest(gPath: "me")
            }
        }
        
//        if (FBSDKAccessToken.current() != nil) {
//            // User is already logged in, do work such as go to next view controller.
//            print("Already logged in")
//            self.getFBSDKGraphRequest(gPath: "me")
//        } else {
//            //Perform Login
//            FBSDKLoginManager().logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: sender) { (result, error) in
//                if error != nil {
//                    self.failure?(error?.localizedDescription ?? "")
//
//                } else if (result?.isCancelled)! {
//                    self.failure?("Cancelled")
//
//                } else {
//                    self.getFBSDKGraphRequest(gPath: "me")
//                }
//            }
//        }
    }
    func getFBSDKGraphRequest(gPath:String){
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
            error != nil ? self.failure?(error?.localizedDescription ?? "") : self.success?((result as? [String: Any])!)
        })
    }
    func facebookFriendList(sender: UIViewController, success: (([String: Any]) -> Void)?, failure: ((String) -> Void)?){
        self.success = success
        self.failure = failure
        if AccessToken.current != nil && AccessToken.current!.hasGranted(permission: "user_friends") {
            self.getFriendListData()
        } else {
            self.viaGraphAPI(sender: sender, success: { response in
               self.getFriendListData()
            }) { error in
                print(error)
                self.failure?("Cancelled")
            }
            
        }
    }
    func getFriendListData(){
        GraphRequest(graphPath: "me/friends", parameters: ["fields": "id, name"], httpMethod: HTTPMethod(rawValue: "GET")).start(completionHandler: { connection, result, error in
            if let aResult = result {
                print("result:\(aResult)")
            }
            print("error:\(error?.localizedDescription ?? "")")
            if error == nil {
                print("result:\(result ?? "")")
                let response = result as? [String: Any]
                let friendData = response!["data"] as? [[String: Any]]
                if friendData?.count != 0 {
                    self.success?(response!)
                } else {
                    self.failure?("No Facebook Friend found")
                }
            } else {
                self.failure?(error?.localizedDescription ?? "")
            }
            
        })
    }
}
