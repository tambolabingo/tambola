//
//  FacebookShareModel.swift
//  Tambola Bingo
//
//  Created by signity on 30/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class FacebookShareModel: NSObject, SharingDelegate {
   
    
    static let shared = FacebookShareModel()
     var delegateObj:UIViewController!
    func shareImageOnFb(imageURL: String, delegate: UIViewController){
        self.delegateObj = delegate
        let fileUrl = NSURL(string: imageURL)
        if AccessToken.current?.hasGranted(permission: "publish_actions") ?? false {
            // TODO: publish content.
            self.postPhoto(fileUrl! as URL, withRootView: delegate)
        } else {
            let loginManager = LoginManager()
//            loginManager.loginBehavior = FBSDKLoginBehavior.web
            loginManager.logIn(permissions: ["publish_actions"], from: delegate, handler: { (result, error) in
                //TODO: process error or result.
                if (error != nil) {
                    print(error!)
                } else if (result?.isCancelled)! {
                    print("Canceled")
                    self.postContentLink(fileUrl! as URL, withRootView: delegate)
                } else if (result?.grantedPermissions.contains("publish_actions"))! {
                    print("permissions granted")// Pending for facebook approval
                    self.postPhoto(fileUrl! as URL, withRootView: delegate)
                } else {
                   self.postContentLink(fileUrl! as URL, withRootView: delegate)
                }
            })
           
        }
    }
    func postPhoto(_ imgUrl: URL?, withRootView delegate: UIViewController?) {
        let photo = SharePhoto()
        let imageData = NSData(contentsOf: imgUrl!)
        photo.image = UIImage(data: imageData! as Data)
        photo.caption = "Playing Tambola Bingo And Have some fun with Tambola Bingo."
        photo.isUserGenerated = true
        let photoContent = SharePhotoContent()
        photoContent.contentURL = URL(string: "https://stackoverflow.com/questions/25741350/invite-facebook-friends-to-use-app-from-objective-c-or-php")!
        photoContent.photos = [photo]
       let shareDialog: ShareDialog = ShareDialog()
        shareDialog.shareContent = photoContent
        shareDialog.delegate = self
        shareDialog.fromViewController = delegate
        shareDialog.show()
//        FBSDKShareAPI.share(with: photoContent, delegate: self)
    }
    
    func postContentLink(_ imgUrl: URL?, withRootView delegate: UIViewController?) {
        let content = ShareLinkContent()
        content.contentURL = imgUrl!
        content.quote = ConstantModel.message.gameQuotMessage
        let shareDialog: ShareDialog = ShareDialog()
        shareDialog.shareContent = content
        shareDialog.delegate = self
        shareDialog.fromViewController = delegate
        shareDialog.show()
        
    }
    // MARK: -  FBSDK Sharing Delegate 
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        //On Completion
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.postOnWall, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self.delegateObj, action: nil)
       }
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        //On Failer
        print("Error publishing story.")
    }
    
    func sharerDidCancel(_ sharer: Sharing) {
        //On cancel by user
        EventManager.showAlertWithAction(alertMessage: ConstantModel.message.requestCancel, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self.delegateObj, action: nil)
    }
}
