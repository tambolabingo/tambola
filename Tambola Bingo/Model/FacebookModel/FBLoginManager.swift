//
//  FBLoginManager.swift
//  Tambola Bingo
//
//  Created by signity on 24/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

typealias LogInCompleted = ((Bool) -> Void)?

class FBLoginManager: NSObject {

    static let shared = FBLoginManager()
    
    func loginWithFaceBook(delegate: UIViewController, logInCompleted: LogInCompleted){
        LoginViaFBModel.shared.viaGraphAPI(sender: delegate, success: { response in
            let userID = response["id"] as! String
            let userFName = response["first_name"] as! String
            let userLName = response["last_name"] as! String
            let userEmail = response["email"] as! String
            let facebookProfileUrl = String(format:ConstantModel.fbImageURL, userID)
            print(facebookProfileUrl)
            let userDict = ["first_name":userFName,
                            "last_name":userLName,
                            "email":userEmail,
                            "fb_id": userID,
                            "PhotoURL":facebookProfileUrl,
                            "isGuest": false,
                            "acc_type": "facebook"] as [String : Any]
//            DbHandler.shared.saveUser(user: userDict)
            UserDefaultsHandler.user = UserDataModel.init(userDict: userDict)
            //LogInAPI Call
            self.loginAPI(parameters: GetParamModel.shared.getLoginParam(fNameText: userFName, lNameText: userLName, emailText: userEmail, fbId: userID), delegate: delegate, logInCompleted: logInCompleted)
            
        }) { error in
            print(error)
            logInCompleted!(false)
        }
    }
    // MARK: -  Login API Call 
    func loginAPI(parameters: [String: Any]?, delegate: UIViewController, logInCompleted: LogInCompleted) {
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.Register, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: delegate, success: { response in
            APIResponseModel.shared.parseLoginResponse(response: response, accType: "facebook", isSocial: true, sender: delegate, callback: { result in
                // Logged in User Sucessfully
                logInCompleted!(true)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: delegate, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.loginAPI(parameters: parameters, delegate: delegate, logInCompleted: logInCompleted)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                    logInCompleted!(false)
                })
            }
        } )
    }
    
}
