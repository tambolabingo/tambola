//
//  RemoteNotificationManager.swift
//  Magic Trade
//
//  Created by Signity on 19/06/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit
// swiftlint:disable line_length
// swiftlint:disable trailing_whitespace
class RemoteNotificationManager: NSObject {

    static let root = ConstantModel.appDelegate.window?.rootViewController!
    // MARK: -  Push Notification Handler 
    class func handleRemoteNotification(response: [AnyHashable: Any]) {
//       NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        let data = response["aps"] as? [String: Any]
        let alert = data!["alert"] as? [String: Any]
        let message = "\(alert!["title"] ?? "") \(alert!["body"] ?? "")"
        EventManager.showAlertWithAction(alertMessage: message, btn1Tit: ConstantModel.btnText.btnContinue, btn2Tit: ConstantModel.btnText.btnCancel, sender: root!) { selectionTxt in
            if selectionTxt == ConstantModel.btnText.btnContinue && UserDefaultsHandler.pageToShow != nil{
                NavigationManager.checkWhereToNav()
            }
        }
    }
}
