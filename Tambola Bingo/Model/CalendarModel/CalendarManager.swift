//
//  CalendarManager.swift
//  Tambola Bingo
//
//  Created by signity on 10/09/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import EventKit
typealias calenderCallBack = (Bool) -> Void
class CalendarManager: NSObject {
    static let eventStore : EKEventStore = EKEventStore()
    class func addEventToCalender(gameScheduleData: [String: Any], eventAdded: @escaping calenderCallBack)
    {
        eventStore.requestAccess(to: .event) { (granted, error) in
            if (granted) && (error == nil) {
                let event:EKEvent   = EKEvent(eventStore: self.eventStore)
                event.title         = gameScheduleData["CALENDER_TITLE"] as? String ?? ConstantModel.ProjectName
                event.notes         = gameScheduleData["CALENDER_DESC"] as? String ?? gameScheduleData["ACCEPT_GAME_INTIMATION"] as? String ?? ConstantModel.message.gameQuotMessage
                let startDataStr    = gameScheduleData["start_time"] as? String ?? ""
                let startDate       = EventManager.getDateTimeFromSeconds(startDataStr)
                let endDateStr      = gameScheduleData["end_time"] as? String ?? ""
                let endDate         = EventManager.getDateTimeFromSeconds(endDateStr)
                event.startDate  = startDate //beginTime
                event.endDate    = endDate //endDate
                let calendar = Calendar.current
                let alarmTime = calendar.date(byAdding: .minute, value: -5, to: startDate)
                let alarm        = EKAlarm(absoluteDate: alarmTime!)//beginTime starTime5minLess
                event.addAlarm(alarm)
                event.calendar = self.eventStore.defaultCalendarForNewEvents
                do {
                    try self.eventStore.save(event, span: .futureEvents)
                    eventAdded(true)
                    print("Saved Event to calendar Sucessfully")
                } catch let error as NSError {
                    eventAdded(false)
                    print("failed to save event with error : \(error)")
                }
            }
        }
    }
}
