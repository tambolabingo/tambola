//
//  PlayGameManager.swift
//  Tambola Bingo
//
//  Created by signity on 06/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class PlayGameManager: NSObject {
    static let shared = PlayGameManager()
    static let autoCutEnable           = "Autocut Enabled"
    static let autoCutDisable          = "Autocut Disabled"
    static let gameOver                = "Game over well played"
    static let boogie                  = "Sorry you got boogie"
    
    let numberTextToAnounceArray = ["Top of the house only number one",
        "Me and you only number two",
        "A cup of tea only number three",
        "Knock the door only number four",
        "Game still alive only number five",
        "Chopping sticks only number six",
        "Made in heaven bingo it's number seven",
        "Cut the cake it's only number eight",
        "You are mine it's only number nine",
        "Hold your pen it's a number ten",
        "To beautiful legs one and one eleven",
        "One dozen one and two twelve",
        "Unlucky for some lucky for me 13",
        "Lovers day it's one and four 14",
        "Young and King one and five 15",
        "Suite one and six 16",
        "Dancing Queen one and seven 17",
        "Voting age one and eight 18",
        "End of the teens one and nine 19",
        "Blind one score 20 20",
        "Watch your son at two and one 21",
        "Two little ducks two into two 22",
        "Mikey on top of tree it's number 23",
        "Do you want some more two and four 24",
        "Silver jubilee two and five 25",
        "Take an extra two and six 26",
        "Gay way to heaven two and seven 27",
        "Don't wait up to an eight 28",
        "Rise and shine to a nine 29",
        "Women get flirty at 30 30",
        "It's time for fun three and one 31",
        "Buckle my shoe three and two 32",
        "Lovely lips three and three 33",
        "Lions roar 34",
        "Naughty wife three and five 35",
        "Three dozen three and six 36",
        "Lime and lemon three and seven 37",
        "Search the date at three and eight 38",
       " Watch your waist line three and nine 39",
        "Men get Nadia blind 40 40",
        "Get up and run four and one 41",
        "Winnie the Pooh four and two 42",
        "Kisses come free at age four and three 43",
        "All the floors it's number 44",
        "Halfway there four and five 45",
        "I just asked Bunch of sticks four and six 46",
        "Lauren seven 47",
        "For dozens four an eight 48",
        "Crossing the line at four and nine 49",
        "Golden Jubilee 50 50",
        "On the run five and one 51",
        "Deck of cards five and two 52",
        "Talk with the joker five and three 53",
        "Talk with the two jokers five and four 54",
        "Hopes alive all the five 55",
        "Hockey sticks five and six 56",
        "Wish to go to head and five and seven 57",
        "Make them wait five and eight 58",
        "Just retired five and nine 59",
        "Three score blind 60 60",
        "Bakers bun six and one 61",
        "Turn on the screw six and two 62",
        "This is comfrey six and three 63",
        "Bonjour six and four 64",
        "Old age pension six and five 65",
        "Click the click number 66",
        "Made in heaven six and seven 67",
        "Not so late at six and eight 68",
        "Either way up number 69",
        "Lucky blind 70 70",
        "Bang on the drum seven and one 71",
        "Lucky couple seven and two 72",
        "A crutch and a fleece seven and three 73",
        "Candy store seven and four 74",
        "Lucky five and seven 57",
        "Run out of tricks 76",
        "Two hockey sticks seven and seven 77",
        "Heaven's Gate lucky eight 78",
        "One more time seven and nine 79",
        "Four score blind 80 80",
        "Corner shut eight and one 81",
        "Down with a blue eight and two 82",
        "Time for tea eight and three 83",
        "Get some more eight and four 84",
        "Still alive eight and five 85",
        "Between a sticks eight and six 86",
        "Got seven eight and seven 87",
        "Two fat ladies eight and eight 88",
        "Finishing the line eight and nine 89",
        "End of the house 90 90"]
  
    func getAnounceFileName(AnounceNum:String) -> String {
        let index = Int(AnounceNum)
        let fileName = self.numberTextToAnounceArray[index!-1]
        return fileName
    }
//    func printValue(index: Int){
//        let FileName = self.numberTextToAnounceArray[index]
//        let audioURL = Bundle.main.url(forResource: FileName, withExtension: "aac")
//
//        let recognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
//        let request = SFSpeechURLRecognitionRequest(url: audioURL!)
//
//        request.shouldReportPartialResults = true
//
//        if (recognizer?.isAvailable)! {
//
//            recognizer?.recognitionTask(with: request) { result, error in
//                guard error == nil else { print("Error: \(error!)"); return }
//                guard let result = result else { print("No result!"); return }
//                print(result.bestTranscription.formattedString)
//            }
//        } else {
//            print("Device doesn't support speech recognition")
//        }
//    }
}
