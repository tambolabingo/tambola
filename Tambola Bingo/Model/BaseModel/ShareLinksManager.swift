//
//  ShareLinksManager.swift
//  Tambola Bingo
//
//  Created by signity on 17/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import MessageUI
class ShareLinksManager: NSObject ,MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate{
    static let shared = ShareLinksManager()
    func showShareOptions(_ shareKey: String, controller: UIViewController){
        let vc = UIActivityViewController(activityItems: [shareKey], applicationActivities: [])
        controller.present(vc, animated: true)
        
//        let actionSheetController = UIAlertController(title: ConstantModel.btnText.btnSelect, message: "", preferredStyle: .actionSheet)
//        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnCancel, style: .cancel, handler: nil))
//        actionSheetController.addAction(UIAlertAction.init(title: "WhatsApp", style: .default, handler: { _ in
//            self.ShareLinkBywhatsapp(shareKey, delegate: controller)
//        }))
//        actionSheetController.addAction(UIAlertAction.init(title: "Email", style: .default, handler: { _ in
//            self.ShareLinkByEmail(shareKey, delegate: controller)
//        }))
//        actionSheetController.addAction(UIAlertAction.init(title: "SMS", style: .default, handler: { _ in
//            self.ShareLinkByMessages(shareKey, delegate: controller)
//        }))
//        controller.present(actionSheetController, animated: true, completion: nil)
    }
// MARK: Share Game Or Message By WhatsApp
    func ShareLinkBywhatsapp(_ shareString: String, delegate: UIViewController)
    {
        let message = shareString.replacingOccurrences(of: "?gameKey=", with: "")
        let escapedString = message.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
        let url  = URL(string: "whatsapp://send?text=\(escapedString!)")
        if UIApplication.shared.canOpenURL(url! as URL)
        {
            UIApplication.shared.open(url! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.whatsAppError, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: nil)
        }
    }
// MARK: Share Game Or Message By Email
    func ShareLinkByEmail(_ message: String, delegate: UIViewController) {
        let mailComposeViewController = configuredMailComposeViewController(message)
        if MFMailComposeViewController.canSendMail() {
            delegate.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.EmailError, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: nil)
        }
    }
    func configuredMailComposeViewController(_ messageBody: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
//        mailComposerVC.setToRecipients(["someone@somewhere.com"])
        mailComposerVC.setSubject(ConstantModel.message.emailSubject)
        mailComposerVC.setMessageBody(messageBody, isHTML: false)
        
        return mailComposerVC
    }
// MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
// MARK: Share Game Or Message By SMS
    func ShareLinkByMessages(_ message: String, delegate: UIViewController) {
        if MFMessageComposeViewController.canSendText() == true {
//            let recipients:[String] = ["1500"]
            let messageController = MFMessageComposeViewController()
            messageController.messageComposeDelegate  = self
//            messageController.recipients = recipients
            messageController.body = message
            delegate.present(messageController, animated: true, completion: nil)
        } else {
            //handle text messaging not available
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.SMSError, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: nil)
        }
    }
// MARK: MFMessageComposeViewControllerDelegate Method
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
