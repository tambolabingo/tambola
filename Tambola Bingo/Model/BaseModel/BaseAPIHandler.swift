//
//  BaseAPIHandler.swift
//  Tambola Bingo
//
//  Created by signity on 25/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
typealias CallCompleted = ((Bool) -> Void)?

class BaseAPIHandler: NSObject {
    static let shared = BaseAPIHandler()
    // MARK:   API Call For Get User Coins 
    func getUserCoinsAPI(delegate: UIViewController, callCompleted: CallCompleted){
        //API Call
        let params = GetParamModel.shared.getUserCoinsParam()
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.userCoins, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: delegate, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: delegate, callback: { result in
                // Get User's Coins for using as a players
                let data = result as? [String: Any]
                let user = UserDefaultsHandler.user
                user?.userCoins = data!["coins"] as? String
                UserDefaultsHandler.user = user
                callCompleted!(true)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                self.getSessionTokenAPI(delegate: delegate, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getUserCoinsAPI(delegate: delegate, callCompleted: callCompleted)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                    callCompleted!(false)
                })
                
            }
        })
    }
    // MARK:   API Call For Get User Information 
    func getUserInfoAPI(isMyinfo:Bool, params:[String: Any]?, delegate: UIViewController, callCompleted: CallCompleted){
           //API Call
//           var params = GetParamModel.shared.getUserCoinsParam()
            
           ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.userInfo, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: delegate, success: { response in
               APIResponseModel.shared.parseCommonResponse(response: response, sender: delegate, callback: { result in
                   // Get User's Coins for using as a players
                let data = result as? [String: Any]
                if isMyinfo {
                    let user = UserDefaultsHandler.user
                    let myInfo = data!["data"] as? [[String: Any]]
                    user?.fName = myInfo![0]["first_name"] as? String
                    user?.lName = myInfo![0]["last_name"] as? String
                    user?.accType = myInfo![0]["user_acc_type"] as? String
                    UserDefaultsHandler.user = user
                } else {
                    //User for add coins
                    let myInfo = data!["data"] as? [[String: Any]]
                    let fName = myInfo![0]["first_name"]
                    let lName = myInfo![0]["last_name"]
                    let accType = myInfo![0]["user_acc_type"] as? String
                    var userEmail = myInfo![0]["email"] as? String
                    let userPhone = myInfo![0]["mobile_no"] as? String
                    userEmail = userEmail == "" ? userPhone : userEmail
                    let id = myInfo![0]["user_id"] as? String ?? ""
                    let facebookProfileUrl = accType == "facebook" ? String(format:ConstantModel.fbImageURL, id) : ""
                    let userDict = ["first_name":fName as Any,
                                    "last_name":lName as Any,
                                    "email":userEmail as Any,
                                    "isGuest": false,
                                    "PhotoURL": facebookProfileUrl,
                                    "user_id": id as Any,
                                    "acc_type": accType as Any ] as [String : Any]
                    UserDefaultsHandler.friendData = UserDataModel.init(userDict: userDict)
                }
                   callCompleted!(true)
               })
           }, failure: { failureResponse in
               if failureResponse == ConstantModel.message.tokenError {
                   //Call token API and recall API called
                   self.getSessionTokenAPI(delegate: delegate, callCompleted: { isCallSucess in
                       if isCallSucess{
                        self.getUserInfoAPI(isMyinfo: isMyinfo, params: params, delegate: delegate, callCompleted: callCompleted)
                       }
                   })
               } else {
                   EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                       callCompleted!(false)
                   })
                   
               }
           })
       }
    // MARK:   API Call For update user prchase info on server 
       func updateUserPurchaseAPI(params:[String:Any], delegate: UIViewController, callCompleted: CallCompleted){
           //API Call
           ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.updatePurchaseinfo, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: delegate, success: { response in
               APIResponseModel.shared.parseCommonResponse(response: response, sender: delegate, callback: { result in
                   // Get User's Coins for using as a players
                   callCompleted!(true)
               })
           }, failure: { failureResponse in
               if failureResponse == ConstantModel.message.tokenError {
                   //Call token API and recall API called
                   self.getSessionTokenAPI(delegate: delegate, callCompleted: { isCallSucess in
                       if isCallSucess{
                           self.updateUserPurchaseAPI(params: params, delegate: delegate, callCompleted: callCompleted)
                       }
                   })
               } else {
                   EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                       callCompleted!(false)
                   })
               }
           })
       }
    // MARK:   API Call For get custom token for firbase signin 
    func getFirbaseCustomTokeAPI(delegate: UIViewController, callCompleted: CallCompleted){
        //API Call
        let params = ["user_id":(UserDefaultsHandler.user?.userId)!]
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.getFirbaseToken, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: delegate, success: { response in
            APIResponseModel.shared.parseFirebaseTokenResponse(response: response, sender: delegate, callback: { result in
                // Get User's Coins for using as a players
                callCompleted!(true)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                self.getSessionTokenAPI(delegate: delegate, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getFirbaseCustomTokeAPI(delegate: delegate, callCompleted: callCompleted)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                    callCompleted!(false)
                })
            }
        })
    }
    // MARK:   API Call For Set USer Coins 
    func setUserCoinsAPI(params:[String:Any], delegate: UIViewController, callCompleted: CallCompleted){
        //API Call
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.setUserCoins, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: delegate, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: delegate, callback: { result in
                // Get User's Coins for using as a players
                self.getUserCoinsAPI(delegate: delegate) { isCallSucess in
                    callCompleted!(true)
                }
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                self.getSessionTokenAPI(delegate: delegate, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.setUserCoinsAPI(params: params, delegate: delegate, callCompleted: callCompleted)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                    callCompleted!(false)
                })
            }
        })
    }
    // MARK:   API Call For Set USer Coins 
    func setFriendsCoinsAPI(params:[String:Any], delegate: UIViewController, callCompleted: CallCompleted){
        //API Call
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.setFriendsCoins, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: delegate, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: delegate, callback: { result in
                // set User's friends Coins for using as a players
                callCompleted!(true)
                
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                self.getSessionTokenAPI(delegate: delegate, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.setFriendsCoinsAPI(params: params, delegate: delegate, callCompleted: callCompleted)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                    callCompleted!(false)
                })
            }
        })
    }
    // MARK:   API Call For Get Maintenance Info 
    func getMaintenanceAPI(delegate: UIViewController, callCompleted: CallCompleted) {
        ApiCallModel.shared.apiCall(isSilent: false, url: APIConstants.amazoneEPapiURL, parameters: nil, type: APIConstants.GetAPI, isMultipart: false,isKey: true, delegate: delegate, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: delegate, callback: { result in
                let payload = result as? [String: Any]
                let isSuccess = payload?["Add_Maintenance"] as? String
                let endpoint = payload?["endpoint"] as? String
                UserDefaultsHandler.projectBaseUrl =  endpoint != nil ? (endpoint! + "/") : UserDefaultsHandler.projectBaseUrl
                if isSuccess! == "true" {
                    let message = payload?["Add_Maintenance_Msg"] as? String
                    EventManager.showAlertWithAction(alertMessage: message ?? ConstantModel.api.defaultMtnsMessage, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action:{_ in
                        callCompleted!(false)
                    })
                }else{
                    
                    BaseAPIHandler.shared.getSessionTokenAPI(delegate: delegate, callCompleted: { isCallSucess in
                        if isCallSucess{
                            BaseAPIHandler.shared.getVersionAPI(delegate: delegate, callCompleted: { isCompleted in
                                //Version API Done
                                if isCompleted {
                                    callCompleted!(true)
                                } else {
                                    callCompleted!(false)
                                }
                            })
                        } else {
                            callCompleted!(false)
                        }
                    })
                }
            })
        }, failure: { failureResponse in
            EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.defaultMtnsMessage, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: nil)
        })
    }
    // MARK:   API Call For Get session API Token 
     func getSessionTokenAPI(delegate: UIViewController, callCompleted: CallCompleted) {
        //Set Default secret token key for generate token
        UserDefaultsHandler.secretToken = "a$x@d$swsZ4&!k*"
        //API Call
//        let url = "https://tambolabingo.com/api/api/"
        ApiCallModel.shared.apiCall(isSilent: false, url:  UserDefaultsHandler.projectBaseUrl + APIConstants.getToken, parameters: nil, type: APIConstants.PostAPI, isMultipart: false,isKey: true, delegate: delegate, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: delegate, callback: { result in
                let payload = result as? [String: Any]
                let apiToken = payload?["token"] as? String
                UserDefaultsHandler.secretToken = apiToken!
                callCompleted!(true)
               
            })
        }, failure: { failureResponse in
            EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                callCompleted!(false)
            })
            
        })
    }
    // MARK:   API Call For Get App Version Info 
    func getVersionAPI(delegate: UIViewController, callCompleted: CallCompleted) {
//        let url = "inAppPurchaseInfoNew"
        var param = [String:Any]()
        let user = UserDefaultsHandler.user
        if user != nil {
            param = GetParamModel.shared.getVersionParam()!
        }
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.Version , parameters: param, type: APIConstants.PostAPI, isMultipart: true,isKey: true, delegate: delegate, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: delegate, callback: { result in
                let payload = result as? [String: Any]
                //Set google analytics tracker Id
                let googleKey = payload!["GA_KEY"] as? String ?? "UA-56436460-1"
                UserDefaultsHandler.googleAnalyticsKey = googleKey
                ConstantModel.appDelegate.setGoogleAnalyticsTrcker()
                
                let isForceUpdate = payload!["force_update"] as? Bool
                let updateAppVersion = payload!["IPHONE_VERSION"] as! String
                UserDefaultsHandler.apiVersion = payload!["API_VERSION"] as! String
                let currentAppVersion = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
                if isForceUpdate! && updateAppVersion > currentAppVersion {
                    let messageDict = payload!["MESSAGES"] as? [[String: Any]]
                    let updateMessage = messageDict![0]["FORCE_UPDATE_MESSAGES"] as? String
                    hideClose = true
                    EventManager.showAlertWithAction(alertMessage: updateMessage ?? ConstantModel.api.forceUpdateMessage, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                        //Force Logout
//                        let bundleID = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
//                        let storeInfoURL: String = "http://itunes.apple.com/lookup?bundleId=" + bundleID
                        hideClose = false
                        let storeInfoURL = payload!["IOS_STORE"] as! String
                        guard let url = URL(string: storeInfoURL) else {
                            return
                        }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                        callCompleted!(false)
                    })
                } else {
                    self.saveVersionInfo(data: payload!, sender: delegate)
                    callCompleted!(true)
                }
                
            })
        }, failure: { failureResponse in
            EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: delegate, action: {_ in
                callCompleted!(false)
            })
        })
    }
    func saveVersionInfo(data: [String: Any], sender: UIViewController){
//        DbHandler.shared.removeAllGameInfo()
        let apiVersion = data["API_VERSION"] as! String
        let purchaseTicketMessage = data["ticketpricedesc"] as! String
        
        let isEcodingEnable = data["base64enabled"] as? String ?? "false"
        UserDefaultsHandler.isEncodingEnable = isEcodingEnable
        
        let messageArr = data["MESSAGES"] as? [[String: Any]]
        var purchaseCoinsMessage = ""
        var rewardCoinMessage = ""
        for msgDict in messageArr!{
            if msgDict["BUY_COINS_INTIMATION"] != nil{
                purchaseCoinsMessage = (msgDict["BUY_COINS_INTIMATION"] as? String)!
            } else if msgDict["COINS_SUCCESS"] != nil {
                rewardCoinMessage = (msgDict["COINS_SUCCESS"] as? String)!
            }
        }
        let purchaseData = data["purchases"] as? [[String: Any]]
        UserDefaultsHandler.appVersionInfo = AppVersionDataModel(apiVersion, ticketMsg: purchaseTicketMessage, coinMsg: purchaseCoinsMessage)
        
        let ticketprice  = (data["ticketprice"] as? String)!
        let dailyRewards = (data["DAILY_REWARDS"] as? String)!
        let gamePrice = data["GamePrices"] as? [String: Any]
        let autoCut = gamePrice?["AUTOCUT"] as? Int ?? 0
        let gameAccept = gamePrice?["GAME_ACCEPT_DEDCUTION"] as? Int ?? 0
        let gameCExtraLimt = gamePrice?["GAME_CREATE_EXTRA_LIMIT"] as? Int ?? 0
        let gameCLimt = gamePrice?["GAME_CREATE_LIMIT"] as? Int ?? 0
        let gameCLowLimt = gamePrice?["GAME_CREATE_LOW_LIMIT"] as? Int ?? 0
        let pvtGameCreateCost = gamePrice?["PVT_GAME_ACCEPT_DEDCUTION"] as? Int ?? 0
        
        UserDefaultsHandler.gamePriceInfo = gamePriceModel(autoCut, gameAccept: gameAccept, gameCExtraLimt: gameCExtraLimt, gameCLimt: gameCLimt, gameCLowLimt: gameCLowLimt,pvtGameCost: pvtGameCreateCost, ticketprice: ticketprice, dailyRewards: dailyRewards)
        
        let gameMinPlayer = data["PVT_GAME_MIN_PLAYER"] as? String ?? ""
        let gameMaxPlayer = data["PVT_GAME_MAX_PLAYER"] as? String ?? ""
        let gameCreateCost = data["PVT_GAME_CREATE_COST"] as? String ?? ""
        let gamePlayerCost = data["PVT_GAME_PLAYER_COST"] as? String ?? ""
        
        UserDefaultsHandler.gameCreatePriceInfo = pvtgamePriceModel(gameMinPlayer, gameMaxPlayer, gameCreateCost, gamePlayerCost)
        
//        pvtgamePriceModel
        var purchaseArray = [purchasesModel]()
        for pur in purchaseData! {
            let id = (pur["id"] as? String)!
            if id != "6" {
                let pAmount = (pur["amount"] as? String)!
                let pCoin = (pur["coins"] as? String)!
                
                let pInfo = (pur["info"] as? String)!
                let pLable = (pur["label"] as? String)!
                let pProduct = (pur["product"] as? String)!
                let pType = (pur["type"] as? String)!
                let proId = pur["productId"] as? String ?? ""
                let purchase = purchasesModel(id, amount: pAmount, coins: pCoin, info: pInfo, label: pLable, product: pProduct, type: pType, productId: proId)
                purchaseArray.append(purchase)
            }
        }
        UserDefaultsHandler.coinPurchaseInfo = purchaseArray
        //save create private game user and coins list
//        let creategameInfo = data["pvt_game_pts_list"] as? [[String: Any]]
//        (creategameInfo != nil && creategameInfo!.count != 0) ? DbHandler.shared.saveGameInfo(creategameInfo!) : nil
        
        
        if data["randomReward"] != nil{
            let coins = data["randomReward"] as? String ?? "some"
            if coins != "" {
//                let message = String(format: ConstantModel.chatFormat.rewardPointFormat, coins)
                EventManager.showAlertWithAction(alertMessage: rewardCoinMessage, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: sender , action: nil)
            }
        }
    }
  // MARK:   API Call For Invite Facebook Friends For A game 
    
    func inviteFBFriendsAPI(_ parameters: [String: Any], _ controller: UIViewController , inviteSucessfully: CallCompleted){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.inviteFriends, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: controller, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: controller, callback: { result in
                //  invite Group Members Sucessfully
                self.getUserCoinsAPI(delegate: controller) { isCallSucess in
                    //SucessCallBack
                    inviteSucessfully!(true)
                }
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: controller, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.inviteFBFriendsAPI(parameters, controller, inviteSucessfully: inviteSucessfully)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: controller, action: {_ in
                    inviteSucessfully!(false)
                })
            }
        } )
    }
    
    
    
}
