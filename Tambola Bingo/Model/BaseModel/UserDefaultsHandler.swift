//
//  UserDefaultsHandler.swift
//    Tambola Bingo
//
//  Created by Signity on 12/02/18.
//  Copyright © 2018 Signity. All rights reserved.
//
import UIKit
import MapKit
class UserDefaultsHandler: NSObject {

    static let memory = UserDefaults.standard

    static var user: UserDataModel? {
        get {
            let decoded     = memory.object(forKey: "user") as? Data
            return decoded == nil ? nil : NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? UserDataModel
        }
        set (userModel) {
            userModel == nil ? memory.removeObject(forKey: "user") : memory.set(NSKeyedArchiver.archivedData(withRootObject: userModel!), forKey: "user")
            memory.synchronize()
        }
    }
    static var friendData: UserDataModel? {
           get {
               let decoded     = memory.object(forKey: "friend") as? Data
               return decoded == nil ? nil : NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? UserDataModel
           }
           set (userModel) {
               userModel == nil ? memory.removeObject(forKey: "friend") : memory.set(NSKeyedArchiver.archivedData(withRootObject: userModel!), forKey: "friend")
               memory.synchronize()
           }
       }
    static var mimeType: String {
        get {
            return memory.value(forKey: "pathExtension") as? String ?? ""
        }
        set (idStr){
            memory.setValue(idStr, forKey: "pathExtension")
            memory.synchronize()
        }
    }
    static var gamefirebasetoken: String {
        get {
            return memory.value(forKey: "gamefirebasetoken") as? String ?? ""
        }
        set (token){
            memory.setValue(token, forKey: "gamefirebasetoken")
            memory.synchronize()
        }
    }
    static var attachementName: String {
        get {
            return memory.value(forKey: "uploadFileName") as? String ?? ""
        }
        set (idStr){
            memory.setValue(idStr, forKey: "uploadFileName")
            memory.synchronize()
        }
    }
    static var deviceToken: String {
        get {
            return memory.value(forKey: "device_token") as? String ?? "Simulator1234567890"
        }
        set (token){
            memory.setValue(token, forKey: "device_token")  
            memory.synchronize()
        }
    }
    static var transactionComplete: Bool {
        get {
            return memory.bool(forKey: "isTransactionDone")
        }
        set (trans){
            memory.setValue(trans, forKey: "isTransactionDone")
            memory.synchronize()
        }
    }
    static var friendTransaction: Bool {
        get {
            return memory.bool(forKey: "friendTransaction")
        }
        set (trans){
            memory.setValue(trans, forKey: "friendTransaction")
            memory.synchronize()
        }
    }
    static var coinPurchased: String {
        get {
            return memory.value(forKey: "coinPurchased") as? String ?? "0000"
        }
        set (coins){
            memory.setValue(coins, forKey: "coinPurchased")
            memory.synchronize()
        }
    }
    static var transactionCode: String {
          get {
              return memory.value(forKey: "transactionCode") as? String ?? ""
          }
          set (code){
              memory.setValue(code, forKey: "transactionCode")
              memory.synchronize()
          }
    }
    static var transactiondate: String {
          get {
              return memory.value(forKey: "transactiondate") as? String ?? ""
          }
          set (trasdate){
              memory.setValue(trasdate, forKey: "transactiondate")
              memory.synchronize()
          }
    }
    static var transactionError: String {
           get {
               return memory.value(forKey: "transactionError") as? String ?? ""
           }
           set (error){
               memory.setValue(error, forKey: "transactionError")
               memory.synchronize()
           }
       }
    static var transactionreceiptData: String {
        get {
            return memory.value(forKey: "transactionreceiptData") as? String ?? ""
        }
        set (rdata){
            memory.setValue(rdata, forKey: "transactionreceiptData")
            memory.synchronize()
        }
    }
    static var isEncodingEnable: String {
        get {
            return memory.value(forKey: "encodingEnable") as? String ?? "false"
        }
        set (encoding){
            memory.setValue(encoding, forKey: "encodingEnable")
            memory.synchronize()
        }
    }
    static var winningAmount: Int {
        get {
            return memory.value(forKey: "winningAmount") as? Int ?? 0
        }
        set (amount){
            memory.setValue(amount, forKey: "winningAmount")
            memory.synchronize()
        }
    }
    static var appVersionInfo: AppVersionDataModel? {
        get {
        let decoded     = memory.object(forKey: "app_Version_info") as? Data
        return decoded == nil ? nil : NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? AppVersionDataModel
        }
        set (versionModel) {
        versionModel == nil ? memory.removeObject(forKey: "app_Version_info") : memory.set(NSKeyedArchiver.archivedData(withRootObject: versionModel!), forKey: "app_Version_info")
        memory.synchronize()
        }
    }
    static var coinPurchaseInfo: [purchasesModel]? {
        get {
            let decoded     = memory.object(forKey: "coin_Purchase_Info") as? Data
            return decoded == nil ? nil : NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? [purchasesModel]
        }
        set (purchaseModel) {
            purchaseModel == nil ? memory.removeObject(forKey: "coin_Purchase_Info") : memory.set(NSKeyedArchiver.archivedData(withRootObject: purchaseModel!), forKey: "coin_Purchase_Info")
            memory.synchronize()
        }
    }
    static var gamePriceInfo: gamePriceModel? {
        get {
            let decoded     = memory.object(forKey: "game_Price_Info") as? Data
            return decoded == nil ? nil : NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? gamePriceModel
        }
        set (priceModel) {
            priceModel == nil ? memory.removeObject(forKey: "game_Price_Info") : memory.set(NSKeyedArchiver.archivedData(withRootObject: priceModel!), forKey: "game_Price_Info")
            memory.synchronize()
        }
    }
    static var gameCreatePriceInfo: pvtgamePriceModel? {
        get {
            let decoded     = memory.object(forKey: "game_Create_Price_Info") as? Data
            return decoded == nil ? nil : NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? pvtgamePriceModel
        }
        set (priceModel) {
            priceModel == nil ? memory.removeObject(forKey: "game_Create_Price_Info") : memory.set(NSKeyedArchiver.archivedData(withRootObject: priceModel!), forKey: "game_Create_Price_Info")
            memory.synchronize()
        }
    }
    static var secretToken: String {
        get {
            return memory.value(forKey: "x_secret_token") as? String ?? "a$x@d$swsZ4&!k*"
        }
        set (token){
            memory.setValue(token, forKey: "x_secret_token")
            memory.synchronize()
        }
    }
    static var isHideAds: Bool {
       get {
            return memory.bool(forKey: "isHideAds")
        }
        set (token){
            memory.setValue(token, forKey: "isHideAds")
            memory.synchronize()
        }
    }
    static var pageToShow: String? {
        get {
            return memory.value(forKey: "LandingPageToShow") as? String
        }
        set (pageStr){
            memory.setValue(pageStr, forKey: "LandingPageToShow")
            memory.synchronize()
        }
    }
    static var apiVersion: String {
           get {
               return memory.value(forKey: "apiVersion") as? String ?? "3.0"
           }
           set (pageStr){
               memory.setValue(pageStr, forKey: "apiVersion")
               memory.synchronize()
           }
       }
    static var projectBaseUrl: String {
        get {
            return memory.value(forKey: "baseURL") as? String ?? "https://tambolabingo.com/api/api/"
        }
        set (baseurl){
            memory.setValue(baseurl, forKey: "baseURL")
            memory.synchronize()
        }
    }
    static var googleAnalyticsKey: String {
        get {
            return memory.value(forKey: "GAKey") as? String ?? "UA-56436460-1"
        }
        set (key){
            memory.setValue(key, forKey: "GAKey")
            memory.synchronize()
        }
    }
    static var userNotification: String {
        get {
            return memory.value(forKey: "Notification") as? String ?? "1"
        }
        set (notification){
            memory.setValue(notification, forKey: "Notification")
            memory.synchronize()
        }
    }
    static var applicationSound: Bool {
        get {
            return memory.bool(forKey: "AppSound")
        }
        set (isSound){
            memory.setValue(isSound, forKey: "AppSound")
            memory.synchronize()
        }
    }
    // MARK: -  Remove/Clear All User Defaults 
    class func removeAllUserDefaults() {
        let deviceToken = self.deviceToken
        let domain = Bundle.main.bundleIdentifier!
        memory.removePersistentDomain(forName: domain)
        memory.synchronize()
        self.deviceToken = deviceToken
        self.pageToShow = ""
        //clear Local DB
        GameTicketsModel.shared.deleteAllGameTicketsData()
        MyGroupsDataModel.shared.deleteAllGroups()
        ConstantModel.appDelegate.saveContext()
    }
}
