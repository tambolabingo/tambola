//
//  ConstantModal.swift
//  Tambola Bingo
//

import UIKit
// swiftlint:disable line_length
// swiftlint:disable trailing_whitespace
class ConstantModel: NSObject {
    static let ProjectName          = "Tambola Bingo"
    static var fbImageURL           = "https://graph.facebook.com/%@/picture?type=large"
    static let appDelegate          = UIApplication.shared.delegate as! AppDelegate
    static let screenHeight         = UIScreen.main.bounds.height
    static let screenWidth          = UIScreen.main.bounds.width
    static let api                  = API()
    static let message              = AppMessage()
    static let btnText              = buttonText()
    static let pageName             = pageToShow()
    static let claim                = claimMessageText()
    static let btnImgName           = buttonImageName()
    static let appColor             = appColors()
    static let chatFormat           = chatMessageFormates()
    static let gai                  = GAIMessages()
//    For testing
    static let GADAdsUnitId         = "ca-app-pub-7893850935827339/3925590202"
    //For Live
//    static let GADAdsUnitId         = "ca-app-pub-3668093805407049/4696087211"
    
    static var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
    static var timeZoneVal = Double(secondsFromGMT) / 3600.0
    static let time_Zone = timeZoneVal > 0 ? "+" + String(timeZoneVal) : String(timeZoneVal)
    static var localTimeZoneName: String { return TimeZone.current.identifier }
    
}

struct appColors {
    let pinkColor =  UIColor(red: 205/255, green: 12/255, blue: 55/255, alpha: 1)
    let yellowColor =  UIColor(red: 240/255, green: 161/255, blue: 43/255, alpha: 1)
    let blueColor =  UIColor(red: 0/255, green: 27/255, blue: 214/255, alpha: 1)
    let blackColor = UIColor(red: 20/255, green: 21/255, blue: 20/255, alpha: 1)
    let brownColor = UIColor(red: 92/255, green: 19/255, blue: 19/255, alpha: 1)
}
struct API {
    let ServiceFailure      = "Something went wrong, please try again."
    let WeakInternet        = "Please check your internet connection before using."
    let NoInternet          = "No internet connection."
    let defaultMtnsMessage  = "Apologies for the inconvenience caused. We are in maintenance mode. We will be back soon. Please visit the app again later."
    let forceUpdateMessage  = "We have an awesome update available for you, with lots of new features. Update the app before using it."
    let MobileRegistration  = "Registration with phone number is only valid for indian phone numbers."
}
struct chatMessageFormates {
    let publicMessage       = "pm/%@:%@;%@"
    let privateMessage      = "m/%@:%@;%@"
    let quitJoinMessage     = "a/%@:%@;%@"
    let blockMessage        = "b/%@:%@;%@"
    let claimMessage        = "c/%@:%@;%@"
    let gameTimerFormat     = "Time Left - %02d hr: %02d min: %02d sec"
    let rewardPointFormat   = "Welcome! You have got %@ coins."
}
struct pageToShow {
    let DashBoardScreen     = "DashBoardView"
}
struct AppMessage {
    let noRecord            = "No Records Found."
    let fetchList           = "Fetching Game List Data ..."
    let reloadList          = "Do you want to reload ?"
    let workInprogress      = "Work In Progress!"
    let tokenError          = "tokenExpired"
    let SelectDate          = "Please select date for the game first."
    let FillAllField        = "Please fill in all required fields."
    let PlayerName          = "Please enter Player Name."
    let validOTP            = "Please enter OTP before submit."
    let EnterName           = "Please enter your first name and last name."
    let EnterEmail          = "Please enter email address"
    let EnterUserName       = "Please enter user phone number or email address"
    let EnterPhone          = "Please enter phone number"
    let EnterPassword       = "Please enter password"
    let ValidEmail          = "Valid email required"
    let ValidPhone          = "Valid phone number required"
    let ValidPassword       = "Please create a password between 6-16 characters with at least any of the following alphabet, number and allowed special characters e.g. !@#$%^&*+=?-"
    let MatchPassword       = "The passwords you’ve entered don’t match, please review and try again"
    let ImageSize            = "Please select a Image Max 8MB in size."
    let notificationError   = "Login with facebook is required for using the notification service settings."
    let GameJoindMessage    = "You are already member of this game."
    let NotAParticipate     = "You need to participate in game to view the details."
    let quitTitle           = "Are you sure you want to quit the game."
    let QuitGame            = "Please note that your current state of the game would be lost.You will get a new ticket and would be charged coins again to join back."
    let GameOver            = "Please note that the current game is over and you can join next game to play."
    let fbLoginGameMessage      = "Login with facebook is required to play public or private games. You can play practice games as guest. Do you want to continue Login with Facebook?"
    let fbLoginFeatureMessage      = "Login with facebook is required to access friends invite, Shop coins and Leaderboard feature. Do you want to continue Login with Facebook?"
    let fbLoginShareMessage      = "Login with facebook is required to share result. Do you want to continue Login with Facebook?"
    let agreePolicy = "Before registration please accept the privacy policy."
    let GameName          = "Please enter Game Name."
    let EnterKey          = "Please enter game key first."
    let DateTime          = "Before creating your game, please select Date and Time."
    let enableAutoCut     = "Autocut feature enabled."
    let disableAutoCut    = "Autocut feature disabled"
    let purchaseAutoCut    = "This feature will charge %@ coins from you and this is chargeable only one time per game. You can then enable and disable this feature any time."
    let publicPrivateFeature    = "This feature is only available in private/Public games."
    let appInvite = "Facebook App Invite Feature. Pending!"
    let EnterGroupName          = "Please enter Group Name."
    let NoUserSelected          = "No user selected."
    let NoMember                = "For Creating or Updateing a group atleast one member is required."
    let NoGroup                 = "No group found."
    let NoFriends               = "No more friends to add."
    let GroupNameExist          = "Group name already exist.\n You need to change your group name."
    let SetGroupImage          = "Would you like to set Group Image?"
    let inviteMessage           = "Invitation Sent Successfully."
    let whatsAppError           = "Your device has no WhatsApp installed."
    let EmailError           = "Your device could not send e-mail.  Please check e-mail configuration and try again."
    let emailSubject        = "You are invited to play Tambola"
    let SMSError           = "Your device doesn't support SMS."
    let emptyMessageError = "Just write up to share with others."
    let NoFBFriends               = "No facebook friends are available on Tambola Bingo."
    let FBFriendSelectError               = "Please select atleast one Friend."
    let postOnWall = "Successfully posted on your wall."
    let gameQuotMessage = "Playing Tambola Bingo And Have some fun with Tambola Bingo."
    let requestCancel = "Request has been canceled."
    let purchaseFailedMsg = "Coins purchased failed!!!!"
    let upgardeSucessFull = "Upgrade to Pro Version Restore Successfull."
    let spamMessage = "Thank you for reporting spam, requesed user spam successfully."
    let blockmessage = "User has been blocked successfully."
    let unblockMessage = "User has been unblocked successfully."
}

struct buttonText {
     let btnCancel           = "Cancel"
     let btnRetry           = "Retry"
     let btnOk               = "Ok"
     let btnContinue         = "Continue"
     let btnDone             = "Done"
     let btnSelect           = "Please select"
     let btnTakePhoto        = "Take Photo"
     let btnSelectPhoto      = "Select Photo"
     let btnYes              = "Yes"
     let btnNo               = "No"
     let btnBuy              = "Buynow"
    let btnCreate         = "Create New Group"
    
}
struct buttonImageName {
    //Claim button Image Names
    let btnCBottomLine           = "BottomLineCrossbtn"
    let btnCCenter               = "CenterCrossBtn"
    let btnCEarlyFive            = "EarlyFiveCrossBtn"
    let btnCFourCorner           = "FourCornerCrossBtn"
    let btnCFullHouse            = "FullHouseCrossBtn"
    let btnCMiddleLine           = "MiddleLineCrossBtn"
    let btnCPyramid              = "PyramidCrossBtn"
    let btnCtopLine              = "TopLineCrossBtn"
    let btnCLuckySeven           = "LuckySevenCrossBtn"
    let btnCCircle               = "CircleCrossBtn"
}
struct claimMessageText {
    
    let claimPriceHTML = "<font size=2 color=#F9F908> %@ <font size=2 color=white>Completed <font size=2 color=#F9F908>%@"
    let boggiedHTML = "<font size=2 color=white>%@ <font size=2 color=#F9F908>You got boggied"
    let claimEarlyFive = "Early five"
    let claimLuckySeven = "Lucky Seven"
    let claimCircle = "Circle"
    let claimTopLine = "First Row"
    let claimMiddleLine = "Second Row"
    let claimBottomLine = "Third Row"
    let claimPyramid = "Pyramid"
    let claimCorner = "4 Corner"
    let claimCenter = "Center"
    let claimFullHouse = "House Full"
    let claimboogied = "got boogied"
    
    let claimAPIEarlyFive = "early_five_status"
    let claimAPILuckySeven = "lucky_seven_status"
    let claimAPICircle = "circle_status"
    let claimAPITopLine = "firstline_status"
    let claimAPIMiddleLine = "secondline_status"
    let claimAPIBottomLine = "thirdline_status"
    let claimAPIPyramid = "pyramid_status"
    let claimAPICorner = "corner_status"
    let claimAPICenter = "center_status"
    let claimAPIFullHouse = "housefull_status"
    let claimAPIboogied = "got boogied"
    
    let claimDone = "Claimed and Done"
    let gameOverMessage = "Game Over. %@ Wins the game."
    let boggiesMessage = "%@ not completed,you got boggied"
    let gameWinMessageOne = "You completed %@ in this game '%@' via Tambola Bingo."
    let gameWinMessageTwo = "You completed %@ in this game '%@' via Tambola Bingo and earned %@ coins."
    let gameLossMessage = "It was fun playing '%@' via Tambola Bingo."
}
struct GAIMessages {
    let DashBoardScreen = "Common Screen"
    let friendsScreen = "Facebook friendInvite"
    let leaderBoardScreen = "LeaderBoard Class"
    let baseScreen = "Landing Screen"
    let createPrivateScreen = "Create game Screen"
    let gameDetailScreen = "Game Detail Screen"
    let gameHistoryScreen = "History Game Result"
    let MyGamesScreen = "My Games Screen"
    let privateGameListScreen = "Private Game Screen"
    let publicGameListScreen = "Public Game Screen"
    let shopScreen = "Shopping Screen"
    let playGameScreen = "Play Games Screen"
    let settingsScreen = "Settings Screen"
    let loginScreen = "Login Screen"
    let signUpScreen = "SignUp Screen"
    let otpScreen = "OTP Screen"
    let forgotPasswordScreen = "Forgot Password Screen"
    
}
/*
 let claimEarlyFive = claimData["EARLY_FIVE"] as? [String: Any]
        let claimLuckySeven = claimData["LUCKY_SEVEN"] as? [String: Any]
        let claimTopLine = claimData["FIRST_LINE"] as? [String: Any]
        let claimMiddleLine = claimData["SECOND_LINE"] as? [String: Any]
        let claimBottomLine = claimData["THIRD_LINE"] as? [String: Any]
        let claimPyramid = claimData["PYRAMID"] as? [String: Any]
        let claimCorner = claimData["CORNER"] as? [String: Any]
        let claimCenter = claimData["CENTER"] as? [String: Any]
        let claimCircle = claimData["CIRCLE"] as? [String: Any]
        let claimFullHouse = claimData["HOUSEFULL"] as? [String: Any]
 */
