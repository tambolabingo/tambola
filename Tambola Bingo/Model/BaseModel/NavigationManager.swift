 //
//  EventManager.swift
 // Tambola Bingo
 //
 //  Created by Signity on 12/02/18.
 //  Copyright © 2018 Signitya. All rights reserved.
 //

import UIKit

class NavigationManager: NSObject {
    static let storyBoard = UIStoryboard(name: "Main", bundle: nil)
   
     
// MARK:   Method for check navigate user 
    class func checkWhereToNav() {
        if UserDefaultsHandler.pageToShow != nil {
            let pageName = UserDefaultsHandler.pageToShow
            switch pageName {
            case ConstantModel.pageName.DashBoardScreen? :
                self.navToDashBoardView()
            default:
                UserDefaultsHandler.removeAllUserDefaults()
                self.navToAppLandingView()
            }
        }
    }
// MARK:   Method for navigate Application to Login Signup View 
    class func navToALoginView(_ delegate: UIViewController,_ type: String) {
        let logInVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewViewController") as? LoginViewViewController
        logInVC?.loginType = type
        delegate.navigationController?.pushViewController(logInVC!, animated: true)
    }
    class func navToSignUpView(_ delegate: UIViewController,_ type: String) {
        let signUpVC = storyBoard.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController
        signUpVC?.loginType = type
        delegate.navigationController?.pushViewController(signUpVC!, animated: true)
    }
    class func navToOTPView(_ delegate: UIViewController,_ phone: String) {
        let otpVC = storyBoard.instantiateViewController(withIdentifier: "OTPViewController") as? OTPViewController
        otpVC?.phoneNumber = phone
        delegate.navigationController?.pushViewController(otpVC!, animated: true)
    }
    class func navToForgotPassView(_ delegate: UIViewController,_ type: String) {
        let forgotPassVC = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController
        forgotPassVC?.loginType = type
        delegate.navigationController?.pushViewController(forgotPassVC!, animated: true)
    }
// MARK:   Method for navigate Application to DashBoard View 
    class func navToAppLandingView() {
        UserDefaultsHandler.removeAllUserDefaults()
        let landingVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as? BaseViewController
        self.pushToNavigate(navToVC: landingVC!, isHidden: true)
    }
    class func navToDashBoardView() {
        let dashBoardVC = storyBoard.instantiateViewController(withIdentifier: "GameDashBoardViewController") as? GameDashBoardViewController
        self.pushToNavigate(navToVC: dashBoardVC!, isHidden: true)
    }
    class func navToPrivateGameListView(_ gameKey: String) {
        let privateVC = storyBoard.instantiateViewController(withIdentifier: "PrivateGameListViewController") as? PrivateGameListViewController
        privateVC!.gamekey = gameKey
        self.pushToNavigate(navToVC: privateVC!, isHidden: true)
    }
// MARK:   Method for navigate to Ad'sd View 
    class func navToFullAdsView(delegate: UIViewController, myAdsView:ShowiAdsView) {
        myAdsView.initializeView(frame: (delegate.view.window?.frame)!)
        myAdsView.senderController = delegate
        myAdsView.loadInterstitialAds()
        ConstantModel.appDelegate.window?.addSubview(myAdsView)
    }
    class func displayAdsBannerView(delegate: UIViewController, myAdsView:ShowiAdsView) {
            myAdsView.initializeView(frame: (delegate.view.window?.frame)!)
            myAdsView.senderController = delegate
            myAdsView.loadBannerForAdd()
        }
// MARK: -  Navigation to play game Dash Board
    class func navigateToPlayGame(delegate: UIViewController, gameData: PlayGameDataModel){
        let playGameVC = storyBoard.instantiateViewController(withIdentifier: "PlayGameViewController") as? PlayGameViewController
        playGameVC?.playGameData          = gameData
        delegate.navigationController?.pushViewController(playGameVC!, animated: true)
    }
// MARK: -  Navigation to play game Dash Board
    class func navigateToFriendsDetail(delegate: UIViewController, friendData: LeadersDataModel, game_Id: String){
        let friendDetailVC = storyBoard.instantiateViewController(withIdentifier: "FriendsDetailViewController") as? FriendsDetailViewController
        friendDetailVC?.myFriendData          = friendData
        friendDetailVC?.gameID                = game_Id
        friendDetailVC?.blockcallback = { status in
            if let gamedetailVC = delegate as? GameDetailViewController {
                let index = gamedetailVC.gameData.participant.firstIndex(where: { (item) -> Bool in
                    item.id == friendData.user_id
                })
                index != nil ? (gamedetailVC.gameData.participant[index!].blockStatus = status!) : nil
            }
        }
        delegate.addChild(friendDetailVC!)
        delegate.view.addSubview((friendDetailVC?.view)!)
    }
// MARK: -  Navigation to play game Dash Board
    class func navigateToFeedback(_ delegate: UIViewController){
        let feedBackVC = storyBoard.instantiateViewController(withIdentifier: "WebContentViewController") as? WebContentViewController
        feedBackVC?.screenTitle = "Feedback"
        feedBackVC?.contentURL = "https://www.tambolabingo.com/feedback.php"
        delegate.navigationController?.pushViewController(feedBackVC!, animated: true)
    }
// MARK: -  Navigation to play game Dash Board
    class func navigateToPrivacyPolicy(_ delegate: UIViewController){
        let feedBackVC = storyBoard.instantiateViewController(withIdentifier: "WebContentViewController") as? WebContentViewController
        feedBackVC?.screenTitle = "Privacy Policy"
        feedBackVC?.contentURL = "https://tambolabingo.com/privacy.html"
        delegate.navigationController?.pushViewController(feedBackVC!, animated: true)
    }
// MARK: -  Navigation to play game Dash Board
    class func navigateToGameResult(_ delegate: UIViewController, resultData: [ResultDataModel], isBoogied: Bool, earnPoints: String, gameName: String, gameId: String, isfromePlayGame: Bool){
        let gameResultVC = storyBoard.instantiateViewController(withIdentifier: "GameResultViewController") as? GameResultViewController
        gameResultVC!.resultData             = resultData
        gameResultVC!.isBoggied              = isBoogied
        gameResultVC!.gameName               = gameName
        gameResultVC!.gameId                 = gameId
        gameResultVC!.isFromPlayGame         = isfromePlayGame
        gameResultVC!.earnPoints             = earnPoints
        gameResultVC!.isHistory              = true
        delegate.navigationController?.pushViewController(gameResultVC!, animated: true)
    }
// MARK: -  Navigation to play game Dash Board
    class func navigateToGameDetail(_ delegate: UIViewController, gameId: String, userType: String, isPublic: Bool){
        let gameDetailVC = storyBoard.instantiateViewController(withIdentifier: "GameDetailViewController") as? GameDetailViewController
        gameDetailVC!.gameId = gameId
        gameDetailVC!.userType = userType
        gameDetailVC!.isPublic = isPublic
        gameDetailVC!.timercallback = {
            if let publicGameVC = delegate as? PublicGameListViewController {
                publicGameVC.invalidateTimer()
            }
        }
        delegate.navigationController?.pushViewController(gameDetailVC!, animated: true)
    }
// MARK: -  Navigation to Create New Game
    class func navigateToBuyCoin(_ delegate: UIViewController,_ isHistory: Bool){
        let shopVC = storyBoard.instantiateViewController(withIdentifier: "ShopCoinsViewController") as? ShopCoinsViewController
        shopVC?.isFrom      = isHistory ? "history" : "Result"
        delegate.navigationController?.pushViewController(shopVC!, animated: true)
    }
// MARK: -  Navigation to Create New Game
    class func navigateToCreateGame(_ delegate: UIViewController, group: MyGroups, isFromGroup: Bool){
        let gameVC = storyBoard.instantiateViewController(withIdentifier: "CreateGameViewController") as? CreateGameViewController
        gameVC!.myGroup = group
        gameVC!.isFromGroup = isFromGroup
        delegate.navigationController?.pushViewController(gameVC!, animated: true)
    }
// MARK: -  Navigation to Add Or Invite Group 
    class func navigateToAddInviteGroup(_ delegate: UIViewController, isInvite: Bool, isgroup: Bool,userId: String, gameId: String, userName: String){
        let addInviteVC = storyBoard.instantiateViewController(withIdentifier: "AddOrInviteGroupViewController") as? AddOrInviteGroupViewController
        addInviteVC!.selectedUserId = userId
        addInviteVC!.selectedUserName = userName
        addInviteVC!.isInvite = isInvite
        addInviteVC!.gameId = gameId
        addInviteVC!.isGroup = isgroup
        delegate.addChild(addInviteVC!)
        delegate.view.addSubview((addInviteVC?.view)!)
    }
    // MARK: -  Navigation to Facebook Friend Invite  
    class func navigateToFBFriendInvite(_ delegate: UIViewController, gameId: String){
        let addInviteVC = storyBoard.instantiateViewController(withIdentifier: "InviteFaceBookFriendsViewController") as? InviteFaceBookFriendsViewController
        addInviteVC?.game_id = gameId
        delegate.addChild(addInviteVC!)
        delegate.view.addSubview((addInviteVC?.view)!)
    }

    
// MARK:   Method for push To Navigation View 
    class func pushToNavigate(navToVC: UIViewController, isHidden:Bool) {
        let navigationController = UINavigationController(rootViewController: navToVC)
        navigationController.navigationBar.isHidden = isHidden
        ConstantModel.appDelegate.window?.rootViewController = navigationController
        ConstantModel.appDelegate.window?.makeKeyAndVisible()
    }

    
}
