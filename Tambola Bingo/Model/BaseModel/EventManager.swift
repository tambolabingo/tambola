//
//  EventManager.swift
//  Tambola Bingo
//
//  Created by Signity on 23/05/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import Reachability
var hideClose = false
// swiftlint:disable line_length
// swiftlint:disable trailing_whitespace
typealias ActionHandler = ((String?) -> Void)?
class EventManager: NSObject {
   
// MARK: -  Show Native Alert 
    class func showAlert(alertMessage: String?, delegate: UIViewController) {
        let alert=UIAlertController(title: ConstantModel.ProjectName, message: alertMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: ConstantModel.btnText.btnOk, style: UIAlertAction.Style.default, handler: nil))
        delegate.present(alert, animated: true, completion: nil)
    }
// MARK: -  Show Native Alert with custom Actions 
    class func showActionAlert(alertMessage: String?, btn1Tit: String?, btn2Tit: String?, sender: UIViewController, action: ActionHandler) {
        let alert = UIAlertController(title: ConstantModel.ProjectName, message: alertMessage, preferredStyle: .alert)
        if btn1Tit != nil {
            alert.addAction(UIAlertAction(title: btn1Tit, style: UIAlertAction.Style.default, handler: {_ in
                action?(btn1Tit)
            }))
        }
        if btn2Tit != nil {
            alert.addAction(UIAlertAction(title: btn2Tit, style: UIAlertAction.Style.default, handler: {_ in
                action?(btn2Tit)
            }))
        }
        sender.present(alert, animated: true, completion: nil)
    }
// MARK: -  Show Custom Alert ViewController with custom Actions 
    class func showAlertWithAction(alertMessage: String?, btn1Tit: String?, btn2Tit: String?, sender: UIViewController, action: ActionHandler) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let addAlertVC = storyBoard.instantiateViewController(withIdentifier: "CustomAlertViewController") as? CustomAlertViewController
        addAlertVC?.btnOneTitle = btn1Tit ?? ""
        addAlertVC?.btnTwoTitle = btn2Tit ?? ""
        addAlertVC?.Message = alertMessage ?? ""
        addAlertVC?.alertAction = { sender in
            action?(sender)
        }
        sender.addChild(addAlertVC!)
        sender.view.addSubview((addAlertVC?.view)!)
    }
// MARK: -  Check for Internet 
    class func checkForInternetConnection() -> Bool {
        let reachability = try? Reachability().connection
        return (reachability == nil || reachability == .unavailable) ? false : true
    }
// MARK: -  Get Date Time From Seconds 
    class func getDateTimeFromSeconds(_ DataStr:String) -> Date {
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(DataStr)!/1000)  //UTC time
        let dateinString = DateConvertor.convertIntoString(slotDate: dateTimeStamp as Date, dateFormat: DateFormats.ServerDateFormat)
        let convertedDate = DateConvertor.convertIntoDate(slotDate: dateinString, dateFormat: DateFormats.ServerDateFormat)
        return convertedDate
    }
// MARK: -  Get Time Difference Value in Minutes 
    class func getTimeDifferenceValue(serverTime: Date, startTime: Date) -> Double {
        let elapsed = serverTime.timeIntervalSince(startTime)
//        let duration = Int(elapsed)
        let minutes = (elapsed / 60.0) * -1
        return Double(minutes)
        
    }
// MARK: -  Get Time Difference Value in Seconds 
    class func getDifferenceInSeconds(serverTime: Date, startTime: Date) -> Int {
        let elapsed = serverTime.timeIntervalSince(startTime)
        let duration = Int(elapsed)
        return duration
        
    }
// MARK: -  Make Call  
    class func makeACall(phoneNumber:String) {
        let phone = "tel://" + phoneNumber
        if let url = URL(string: phone), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
// MARK: -  Take Screenshot Function 
    class func getScreenShot(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
// MARK: -  Loader 
    class func showloader() {
        MBProgressHUD.showAdded(to: ConstantModel.appDelegate.window!, animated: true)
    }
    
    class func hideloader() {
        MBProgressHUD.hide(for: ConstantModel.appDelegate.window!, animated: true)
    }

// MARK: -  Get UIColor From Hex 
    class func hexStringToUIColor(hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if (cString.count) != 6 {
            return UIColor.gray
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

// MARK: -   Create Input view with toolbar 
    class func inputV(_ inputVV: UIView, sender: UIViewController) -> UIView {
        let inputView = UIView(frame: inputVV.frame)
        inputVV.frame.size.width = UIScreen.main.bounds.height
        inputView.addSubview(inputVV)
        inputView.addSubview(self.toolBar(sender, btn1Tit: ConstantModel.btnText.btnCancel, btn2Tit: ConstantModel.btnText.btnDone))
        return inputView
    }
// MARK: -   Add Toolbar to Any view controller 
    class func toolBar (_ target: UIViewController, btn1Tit: String?, btn2Tit: String? ) -> UIToolbar {
        let  toolBar  = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 255/255, green: 92/255, blue: 54/255, alpha: 1)
        
        let cancelButton = UIBarButtonItem(title: btn1Tit, style: UIBarButtonItem.Style.done, target: target, action: NSSelectorFromString("resignTextField:"))
        cancelButton.tag = 100
        
        let doneButton   = UIBarButtonItem(title: btn2Tit, style: UIBarButtonItem.Style.done, target: target, action: NSSelectorFromString("resignTextField:"))
        doneButton.tag   = 101
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
    }
// MARK: -   Apply Shadow to UIView or UIButtons 
    //    EventManager.applyShadow(height: 1.5, radius: 1.5, opacity: 0.20, view: cell)
    class func applyShadow(height: Double, radius: CGFloat, opacity: Float, view: Any?) {
        let senderView =  view is UIView ? view as? UIView : view as? UIButton
        senderView?.layer.shadowOffset = CGSize(width: 0.0, height: height)
        senderView?.layer.shadowRadius = radius
        senderView?.layer.shadowOpacity = opacity
        senderView?.layer.masksToBounds = false
    }
    class func applyCellBorderShadow(_ cell: UICollectionViewCell){
//        cell.contentView.backgroundColor = ConstantModel.appColor.blackColor
        cell.contentView.layer.cornerRadius = 3.0
        cell.contentView.layer.borderWidth = 2.0
        cell.contentView.layer.borderColor = UIColor.white.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = true
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
    }
// MARK: -   Generate Dummy Numbers Sequence 
    
    class func getDummyNumberSequence() -> [String]{
        var numberArray = [String]()
        while numberArray.count != 90 {
            let randNum = Int(arc4random() % (91 - 1) + 1)
            let obj = String(randNum)
            if !numberArray.contains(obj) {
                numberArray.append(obj)
            }
        }
       return numberArray
    }
// MARK: -   check Dummy user ticket contain anounce Number  
    class func checkAnounceNumber(ticket: TicketsModel, anounceNum: String) -> Bool {
        var contained = false
        for i in 0..<27 {
            let num =  ticket.ticketsArray[0][i].number
            if num == anounceNum{
                contained = true
                break
            }
        }
        return contained
    }
// MARK: -   Generate Ticket's 
    class func createTicket(_ ticketCount: Int) -> [[String : [String]]]{
        var dictForMulti_tic  = [[String : [String]]]()
        var tagTicketDict = [String : [String]]()
        var i = 0
        while i < ticketCount{
            tagTicketDict = self.generateTagTicket()
            dictForMulti_tic.append(tagTicketDict)
            i += 1
        }
        return dictForMulti_tic
    }
    class func generateTagTicket() -> [String : [String]]{
        var tagArray = [String]()
        var tempNumArray = [String]()
        var sortingTag = [String]()
        var newTagArray = [[String]]()
        while newTagArray.count != 3 {
            //            var tempArray: [AnyHashable] = []
            var tempArray = [String]()
            if newTagArray.count < 1 {
                tempArray = ["7", "13", "19"]
                let obj1 = ["1", "4"][Int(arc4random() % 2)]
                tempArray.append(obj1)
                let obj6 = ["22", "25"][Int(arc4random() % 2)]
                tempArray.append(obj6)
            } else if newTagArray.count < 2 {
                tempArray = ["2", "17", "23"]
                let obj2 = ["5", "8"][Int(arc4random() % 2)]
                tempArray.append(obj2)
                let obj3 = ["11", "14"][Int(arc4random() % 2)]
                tempArray.append(obj3)
            }else {
                tempArray = ["6", "12", "27"]
                let obj4 = ["15", "18"][Int(arc4random() % 2)]
                tempArray.append(obj4)
                let obj5 = ["21", "24"][Int(arc4random() % 2)]
                tempArray.append(obj5)
            }
            sortingTag.append(contentsOf: tempArray)
            tempArray.sort { $0.compare($1, options: .numeric) == .orderedAscending }
            newTagArray.append(tempArray)
        }
        
        sortingTag.sort { $0.compare($1, options: .numeric) == .orderedAscending }
        tagArray = sortingTag
        
        var first: Int = 0
        var second: Int = 0
        var third: Int = 0
        var forth: Int = 0
        var fifth: Int = 0
        var sixth: Int = 0
        var seventh: Int = 0
        var eighth: Int = 0
        var ninth: Int = 0
        var i: Int = 0
        for index in 0..<tagArray.count {
            if Int(tagArray[index])! < 4 {
                first += 1
            } else if Int(tagArray[index])! < 7 {
                second += 1
            } else if Int(tagArray[index])! < 10 {
                third += 1
            } else if Int(tagArray[index])! < 13 {
                forth += 1
            } else if Int(tagArray[index])! < 16 {
                fifth += 1
            } else if Int(tagArray[index])! < 19 {
                sixth += 1
            } else if Int(tagArray[index])! < 22 {
                seventh += 1
            } else if Int(tagArray[index])! < 25 {
                eighth += 1
            } else {
                ninth += 1
            }
        }
        
        
        while i < first {
            let randNum = Int(arc4random() % (10 - 1) + 1)
            let obj = String(randNum)
            if !tempNumArray.contains(obj) {
                tempNumArray.append(obj)
                i += 1
            }
        }
        i = 0
        while i < second {
            let randNum = Int(arc4random() % (20 - 10) + 10)
            let obj = String(randNum)
            if !tempNumArray.contains(obj) {
                tempNumArray.append(obj)
                i += 1
            }
        }
        i = 0
        while i < third {
            let randNum = Int(arc4random() % (30 - 20) + 20)
            let obj = String(randNum)
            if !tempNumArray.contains(obj) {
                tempNumArray.append(obj)
                i += 1
            }
        }
        i = 0
        while i < forth {
            let randNum = Int(arc4random() % (40 - 30) + 30)
            let obj = String(randNum)
            if !tempNumArray.contains(obj) {
                tempNumArray.append(obj)
                i += 1
            }
        }
        i = 0
        while i < fifth {
            let randNum: Int = Int(arc4random() % (50 - 40) + 40)
            let obj = String(randNum)
            if !tempNumArray.contains(obj) {
                tempNumArray.append(obj)
                i += 1
            }
        }
        i = 0
        while i < sixth {
            let randNum: Int = Int(arc4random() % (60 - 50) + 50)
            let obj = String(randNum)
            if !tempNumArray.contains(obj) {
                tempNumArray.append(obj)
                i += 1
            }
        }
        i = 0
        while i < seventh {
            let randNum: Int = Int(arc4random() % (70 - 60) + 60)
            let obj = String(randNum)
            if !tempNumArray.contains(obj) {
                tempNumArray.append(obj)
                i += 1
            }
        }
        i = 0
        while i < eighth {
            let randNum: Int = Int(arc4random() % (80 - 70) + 70)
            let obj = String(randNum)
            if !tempNumArray.contains(obj) {
                tempNumArray.append(obj)
                i += 1
            }
        }
        i = 0
        while i < ninth {
            let randNum: Int = Int(arc4random() % (91 - 80) + 80)
            let obj = String(randNum)
            if !tempNumArray.contains(obj) {
                tempNumArray.append(obj)
                i += 1
            }
        }
        tempNumArray.sort { $0.compare($1, options: .numeric) == .orderedAscending }
        var recordTrack: [String : [String]] = [:]
        recordTrack["number"] = tempNumArray
        recordTrack["tag"] = tagArray
        
        var tagTicketDict: [String : Any] = [:]
        tagTicketDict["TicketDict"] = recordTrack
        tagTicketDict["tagDict"] = newTagArray
        return recordTrack
    }
}
