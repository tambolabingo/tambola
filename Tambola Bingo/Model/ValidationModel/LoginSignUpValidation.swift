//
//  LoginSignUpValidation.swift
//  Tambola Bingo
//
//  Created by Pooja Rana on 31/08/17.
//  Copyright © 2017 Pooja Rana. All rights reserved.
//

import Foundation
import UIKit

extension GuestLoginViewController {
    
    func validateLogin() -> Bool {
        
        self.view.endEditing(true)
        if nameTxtField.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.PlayerName, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }
        return true
    }
}
extension RegisterViewController {
    func validateRegister() -> Bool {
        
        self.view.endEditing(true)
        if txtFirstName.text!.isStringEmpty() || txtLastName.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.EnterName, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if txtEmailPhone.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: loginType == "email" ? ConstantModel.message.EnterEmail : ConstantModel.message.EnterPhone, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if txtPassword.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.EnterPassword, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if loginType == "email" && txtEmailPhone.text!.isNotValidateEmailId(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.ValidEmail, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }else if loginType == "mobile" && !txtEmailPhone.text!.isValidPhone(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.ValidPhone, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if self.agreePolicyBtn.tag == 0 {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.agreePolicy, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }
        return true
    }
}
extension OTPViewController {
    
    func validateOTP() -> Bool {
        
        self.view.endEditing(true)
        if txtOTP.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.validOTP, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }
        return true
    }
}
extension ForgotPasswordViewController {
    
    func validateEmailPhone() -> Bool {
        
        self.view.endEditing(true)
        if txtEmailPhone.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: loginType == "email" ? ConstantModel.message.EnterEmail : ConstantModel.message.EnterPhone, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }else if loginType == "email" && txtEmailPhone.text!.isNotValidateEmailId(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.ValidEmail, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }else if loginType == "mobile" && !txtEmailPhone.text!.isValidPhone(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.ValidPhone, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }
        return true
    }
}
extension GameDashBoardViewController {
    func validateUserName() -> Bool {
        
        self.view.endEditing(true)
        let uername = txtUserName.text!.replacingOccurrences(of: "+91", with: "")
        if uername.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.EnterUserName, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if uername.isNumeric() && !uername.isValidPhone() {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.ValidPhone, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if !uername.isNumeric() && uername.isNotValidateEmailId() {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.ValidEmail, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }else if uername.isNumeric() && uername.count != 10 {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.ValidPhone, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }
        return true
    }
}
extension LoginViewViewController {
    
    func validateLogin() -> Bool {
        self.view.endEditing(true)
        if txtUserName.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: loginType == "email" ? ConstantModel.message.EnterEmail : ConstantModel.message.EnterPhone, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if txtPassword.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.EnterPassword, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if loginType == "email" && txtUserName.text!.isNotValidateEmailId(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.ValidEmail, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if loginType == "mobile" && !txtUserName.text!.isValidPhone(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.ValidPhone, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }
        return true
    }
}
extension CreateGameViewController {
    
    func validateFields() -> Bool {
        self.view.endEditing(true)
        if txtGameName.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.GameName, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if lblGameDate.text!.isStringEmpty() || lblGameTime.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.DateTime, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }
        return true
    }
}
extension PrivateGameListViewController {
    func validateField() -> Bool {
        self.view.endEditing(true)
        if txtEnterKey.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.EnterKey, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        }
        return true
    }
}
extension CreateNewGroupViewController {
    func validateData() -> Bool {
        self.view.endEditing(true)
        if groupNameText.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.EnterGroupName, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else if memberSelected.count == 0 {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.NoUserSelected, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            return false
        } else {
            let groupName = groupNameText.text!
            let grouplist = MyGroupsDataModel.shared.getGroupsFromDB()
            let contained = grouplist?.filter { $0.name == groupName}
            if (contained?.count)! > 0 {
               EventManager.showAlertWithAction(alertMessage: ConstantModel.message.GroupNameExist, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                return false
            }
        }
        //Check for Group Name
        return true
    }
}
