//
//  CreateGameDataModel.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 13/05/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit
import CoreData
class CreateGameDataModel: NSObject {

    static let shared = CreateGameDataModel()
    
    // MARK: -  Save User 
    func saveCreateGameDataToDB(_ gamelist: [[String: Any]]) {
        
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        
        let entity                   =  NSEntityDescription.entity(forEntityName: "CreateGame",
                                                                   in: managedContext)
        for game in gamelist {
            let gameModel               =  CreateGame(entity: entity!, insertInto: managedContext)
                   
            gameModel.id              =  game["id"] as? String
            gameModel.coins             =  game["coins"] as? String
            gameModel.users             =  Int32(game["no_of_users"] as? String ?? "0")!
        }
        //4
        ConstantModel.appDelegate.saveContext()
    }
    
    // MARK: -  Get User 
    func getGameInfoFromDB() -> [CreateGame]? {
        
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        let fetchRequest   = NSFetchRequest<NSFetchRequestResult>(entityName: "CreateGame")
        do {
            let results =
                try (managedContext.fetch(fetchRequest) as? [CreateGame])!
            if results.count > 0 {
                return results
            } else {
                return []
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    // MARK: -  Delete Jobs 
    func deleteAllGameData()  {
        let allGames = CreateGameDataModel.shared.getGameInfoFromDB()
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        for game in allGames! {
            managedContext.delete(game)
        }
        ConstantModel.appDelegate.saveContext()
        
    }
}
