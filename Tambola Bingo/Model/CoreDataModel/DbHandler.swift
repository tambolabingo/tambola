//
//  DbHandler.swift
// Tambola Bingo
//

import UIKit
import CoreData
// swiftlint:disable trailing_whitespace
class DbHandler: NSObject {
    
    static let shared = DbHandler()
    // MARK: -  User DB 
    func saveGroup(group: [String: Any]) {
        MyGroupsDataModel.shared.saveGroupToDB(group)
    }
    func updateGroup(myGroup: [String: Any]) {
        let allGroups = MyGroupsDataModel.shared.getGroupsFromDB()
        for group in allGroups!{
            let groupName = myGroup["name"] as? String
            if groupName == group.name {
                MyGroupsDataModel.shared.updateGroupsToDB(groupData: myGroup, group)
            }
        }
    }
    // MARK: -  Create Game Info DB 
    func saveGameInfo(_ gamelist: [[String: Any]]) {
        CreateGameDataModel.shared.saveCreateGameDataToDB(gamelist)
    }
    // MARK: -  Create Game Info DB 
    func saveGameTickets(_ gameData: [String: Any]) {
        GameTicketsModel.shared.saveGameTicketsDataToDB(gameData)
    }
    
}
