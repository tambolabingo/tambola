//
//  GameTicketsModel.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 20/05/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit
import CoreData
class GameTicketsModel: NSObject {

    static let shared = GameTicketsModel()
    
    // MARK: -  Save User 
    func saveGameTicketsDataToDB(_ gameData: [String: Any]) {
        
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        
        let entity                   =  NSEntityDescription.entity(forEntityName: "GameTickets",
                                                                   in: managedContext)
            let gameTicketModel               =  GameTickets(entity: entity!, insertInto: managedContext)
                   
            gameTicketModel.gameId              =  gameData["gameid"] as? String
            let allTickets = gameData["tickets"] as? [[String : [String]]]
            let arrTickets = NSMutableArray()
            for ticket in allTickets! {
                arrTickets.add(ticket)
            }
            gameTicketModel.tickets             = arrTickets
        gameTicketModel.isTicketOneBoggied      = false
        gameTicketModel.isTicketTwoBoggied      = false
        gameTicketModel.isTicketThreeBoggied    = false
        
        //4
        ConstantModel.appDelegate.saveContext()
    }
    
    // MARK: -  Get User 
    func getGameTicketsFromDB() -> [GameTickets]? {
        
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        let fetchRequest   = NSFetchRequest<NSFetchRequestResult>(entityName: "GameTickets")
        do {
            let results =
                try (managedContext.fetch(fetchRequest) as? [GameTickets])!
            if results.count > 0 {
                return results
            } else {
                return []
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    // MARK: -  Get Selected Task  
    func getTicketsFromDB(_ gameId: String) -> GameTickets? {
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        let fetchRequest   = NSFetchRequest<NSFetchRequestResult>(entityName: "GameTickets")
        fetchRequest.predicate = NSPredicate(format: "gameId == %@", gameId)
        do {
            let results =
                try (managedContext.fetch(fetchRequest) as? [GameTickets])!
            if results.count > 0 {
                return results[0]
            } else {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
     // MARK: -  Update Selected Task  
    func updateTicketData(_ gameId: String,_ ticketOne:Bool,_ ticketTwo: Bool,_ ticketThree: Bool){
        let myticket = self.getTicketsFromDB(gameId)
        myticket?.isTicketOneBoggied = ticketOne
        myticket?.isTicketTwoBoggied = ticketTwo
        myticket?.isTicketThreeBoggied = ticketThree
        //4
        ConstantModel.appDelegate.saveContext()
    }
    // MARK: -  Delete User 
    func deleteMyTicketFromDB(_ myTicket: GameTickets) {
         let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
         managedContext.delete(myTicket)
         ConstantModel.appDelegate.saveContext()
    }
    // MARK: -  Delete Tickets 
    func deleteAllGameTicketsData()  {
        let allTicketsArray = self.getGameTicketsFromDB()
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        for ticket in allTicketsArray! {
            managedContext.delete(ticket)
        }
    }
}
