//
//  MyGroupsDataModel.swift
//  Tambola Bingo
//
//  Created by signity on 09/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import CoreData
class MyGroupsDataModel: NSObject {

    static let shared = MyGroupsDataModel()
    
    // MARK: -  Save User 
    func saveGroupToDB(_ group: [String: Any]) {
        
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        
        let entity                   =  NSEntityDescription.entity(forEntityName: "MyGroups",
                                                                   in: managedContext)
        let groupModel               =  MyGroups(entity: entity!, insertInto: managedContext)
        
        groupModel.name              =  group["name"] as? String
        groupModel.image             =  group["group_img"] as? Data
        let members                  =  group["members"] as? [[String : Any]]
        if (members != nil) {
            groupModel.members             = members! as NSObject
//            let arrMembers = NSMutableArray()
//            for part in members!{
//                let particpentDict        = NSMutableDictionary()
//                particpentDict.setValue(part["username"] as? String, forKey: "username")
//                particpentDict.setValue(part["fb_id"] as? String, forKey: "fb_id")
//                arrMembers.add(part)
//            }
//            groupModel.members             = arrMembers
        }
        ConstantModel.appDelegate.saveContext()
    }
    
    // MARK: -  Get User 
    func getGroupsFromDB() -> [MyGroups]? {
        
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        let fetchRequest   = NSFetchRequest<NSFetchRequestResult>(entityName: "MyGroups")
        do {
            let results =
                try (managedContext.fetch(fetchRequest) as? [MyGroups])!
            if results.count > 0 {
                return results
            } else {
                return []
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    // MARK: -  Update User 
    func updateGroupsToDB(groupData: [String: Any], _ groupModel: MyGroups) {
        groupModel.name              =  groupData["name"] as? String
        groupModel.image             =  groupData["group_img"] as? Data
        let members                  =  groupData["members"] as? [[String : Any]]
        if (members != nil) {
            groupModel.members             =  members! as NSObject
        }
        ConstantModel.appDelegate.saveContext()
    }
    // MARK: -  Delete User 
    func deleteGroupFromDB(_ group: MyGroups) {
        
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        managedContext.delete(group)
        ConstantModel.appDelegate.saveContext()
    }
    // MARK: -  Delete Jobs 
    func deleteAllGroups()  {
        let allGroups = MyGroupsDataModel.shared.getGroupsFromDB()
        let managedContext =  ConstantModel.appDelegate.getManagedObjectContext()
        for group in allGroups! {
            managedContext.delete(group)
        }
    }
}

