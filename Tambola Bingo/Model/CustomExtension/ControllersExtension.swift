//
//  FontExtension.swift
//  Tow Assist
//

import Foundation
import UIKit
public extension Sequence where Element : Hashable {
    func contains(_ elements: [Element]) -> Bool {
        return Set(elements).isSubset(of:Set(self))
    }
}
public extension String {
    func containsIgnoringCase(_ find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    func containsWhitespace() -> Bool{
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
    func encodeUrl() -> String?
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    func decodeUrl() -> String?
    {
        return self.removingPercentEncoding
    }
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        
        return self
    }
    
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
                return nil
        }
        return String(data: data, encoding: .utf8)
    }
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

// MARK: -   Extension For Array 
extension Array where Element: Equatable {
    func contains(array: [Element]) -> Bool {
        for item in array {
            if !self.contains(item) { return false }
        }
        return true
    }
}
// MARK: -   Extension For Int 
extension Int {
    var boolValue: Bool { return self != 0 }
}
// MARK: -   Extension For UIFont 
extension UIFont {
    static func setFontMedium(textFontSize: CGFloat) -> UIFont {
        return UIFont(name: "System-Medium", size: textFontSize)!
    }
    
    static func setFontBold(textFontSize: CGFloat) -> UIFont {
        return UIFont(name: "System-Bold", size: textFontSize)!
    }
    static func setFontRegular(textFontSize: CGFloat) -> UIFont {
        return UIFont(name: "System-Regular", size: textFontSize)!
    }
    
    static func setTitleFont(textFontSize: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: textFontSize)
    }
}
// MARK: -   Extension For UIButton 
extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }}
// MARK: -   Extension For UIViewController 
extension UIViewController {
    // MARK: -   set navigation bar for ViewController 
    func setNavigationBarTitle() {
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 101/255, green: 16/255, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont.setTitleFont(textFontSize: 24.0)]
    }
     // MARK:- Add animation for keyboard.
    func animateTextField(textField: UITextField, up: Bool)
      {
          let movementDistance:CGFloat = -100
          let movementDuration: Double = 0.3

          var movement:CGFloat = 0
          if up
          {
              movement = movementDistance
          }
          else
          {
            movement = self.view.frame.origin.y == 0 ? 0 : -movementDistance
          }
          UIView.beginAnimations("animateTextField", context: nil)
          UIView.setAnimationBeginsFromCurrentState(true)
          UIView.setAnimationDuration(movementDuration)
          self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
          UIView.commitAnimations()
      }
    // MARK:- Add observer for keyboard.
    func addDoneButtonOnKeyboard() -> UIToolbar{
           let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
           doneToolbar.barStyle = .default

           let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
           let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

           let items = [flexSpace, done]
           doneToolbar.items = items
           doneToolbar.sizeToFit()

           return doneToolbar
       }
        @objc func doneButtonAction(){
            self.view.endEditing(true)
          }
}
// MARK: -   Extension For UIView 
extension UIView {
    func roundCorners(radius: CGFloat, isBottomLeft: Bool) {
        if #available(iOS 11.0, *) {
            self.clipsToBounds = true
            self.layer.cornerRadius = 10
            self.layer.maskedCorners = isBottomLeft ? [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner] : [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
            let corners: UIRectCorner = isBottomLeft ? [.topLeft, .topRight, .bottomRight] : [.topLeft, .topRight, .bottomLeft]
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height:  radius))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }
}
extension UITableView {
    func scrollToLastCell(animated : Bool) {
        let lastSectionIndex = self.numberOfSections - 1 // last section
        let lastRowIndex = self.numberOfRows(inSection: lastSectionIndex) - 1 // last row
        self.scrollToRow(at: IndexPath(row: lastRowIndex, section: lastSectionIndex), at: .bottom, animated: animated)
    }
}
