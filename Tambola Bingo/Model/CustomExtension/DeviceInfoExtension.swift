//
//  DeviceInfoExtension.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 25/04/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit

class DeviceInfoExtension: NSObject {
    class func machineName() -> String {
      var systemInfo = utsname()
      uname(&systemInfo)
      let machineMirror = Mirror(reflecting: systemInfo.machine)
      return machineMirror.children.reduce("") { identifier, element in
        guard let value = element.value as? Int8, value != 0 else { return identifier }
        return identifier + String(UnicodeScalar(UInt8(value)))
      }
    }
}
