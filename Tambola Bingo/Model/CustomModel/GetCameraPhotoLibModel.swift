//
//  GetCameraPhotoLibModel.swift
//  Tambola Bingo
//
//  Created by Signity on 12/02/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import MobileCoreServices
var selectedImageHandler : ((UIImage?) -> Void)?
extension UIImage {
    func scaleAndRotate() -> UIImage {
        let destinationSize = CGSize(width: 600, height: 600)
        UIGraphicsBeginImageContext(destinationSize)
        self.draw(in: CGRect(x: 0, y: 0, width: destinationSize.width, height: destinationSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
extension URL {
    static var documentsDirectory: URL {
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        return URL(string:documentsDirectory)!
    }
    
    static func urlInDocumentsDirectory(with filename: String) -> URL {
        return documentsDirectory.appendingPathComponent(filename)
    }
}
class GetOrSaveImage: NSObject {
    static let shared = GetOrSaveImage()
    func saveImageDocumentDirectory(image: UIImage) -> String{
        let fileManager = FileManager.default
        let path = URL.urlInDocumentsDirectory(with: "profileImage.png").path
        let imageData = image.jpegData(compressionQuality: 0.5)
        fileManager.createFile(atPath: path as String, contents: imageData, attributes: nil)
        return path as String
    }
    func getImage() -> UIImage?{
        let fileManager = FileManager.default
        let imagePAth = URL.urlInDocumentsDirectory(with: "profileImage.png").path
        if fileManager.fileExists(atPath: imagePAth){
            return UIImage(contentsOfFile: imagePAth)
        }else{
            print("No Image")
            return nil
        }
    }
}
class GetCameraPhotoLibModel: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    static let shared = GetCameraPhotoLibModel()
    // MARK: -  Check Image Size 
    class func checkImageSize(image:UIImage!) -> Bool{
        let imgData: NSData = NSData(data: image.pngData()!)
        return self.checkFileSize(fileData: imgData)
    }
    class func checkFileSize(fileData: NSData) -> Bool{
        let fileSize: Int = fileData.length
        let sizeinKB = Double(fileSize) / 1024.0
        return sizeinKB > 8000 ? false : true
    }
      // MARK: -  get Camera/Photos alert controller 
     func getCameraPhotos(picker: UIImagePickerController, sender: UIViewController)  {
        picker.delegate = self
        let actionSheetController: UIAlertController = UIAlertController(title: ConstantModel.btnText.btnSelect, message: "", preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnCancel, style: .cancel, handler: nil))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnTakePhoto, style: .default, handler: { action in
           self.openCamera(picker: picker, sender: sender)
        }))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnSelectPhoto, style: .default, handler: { action in
            self.openphotoLibrary(picker: picker, sender: sender, mediaType: [kUTTypeImage as String])
        }))
        sender.present(actionSheetController, animated: true, completion: nil)
    }
   
    //MARK: -  Open Camera For User Profile Image 
     func openCamera(picker: UIImagePickerController, sender: UIViewController) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.allowsEditing = true
            picker.modalPresentationStyle = .fullScreen
            sender.present(picker,animated: true,completion: nil)
        }
    }
    func openphotoLibrary(picker: UIImagePickerController, sender: UIViewController, mediaType: [String]) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            picker.allowsEditing = true
            picker.modalPresentationStyle = .fullScreen
            picker.mediaTypes = mediaType // Add media type
            sender.present(picker,animated: true,completion: nil)
        }
    }
 
    // MARK: -  UIImagePicker Delegates 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        var  chosenImage = UIImage()
        chosenImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as! UIImage //2
        let userProfileImage = chosenImage.scaleAndRotate()
        GetCameraPhotoLibModel.checkImageSize(image: userProfileImage) ? selectedImageHandler!(userProfileImage) : GetCameraPhotoLibModel.showErrorMessage(picker, controller: nil, message: ConstantModel.message.ImageSize)
        }
   
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    // MARK: -  Error message if size of Image/Video/Document is more then 10MB 
    class func showErrorMessage(_ picker: UIImagePickerController?, controller: UIDocumentPickerViewController?, message:String) {
        
        picker != nil ? picker?.dismiss(animated: true, completion: nil) : nil
        controller != nil ? controller?.dismiss(animated: true, completion: nil) : nil
        EventManager.showAlertWithAction(alertMessage: message, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: (ConstantModel.appDelegate.window?.rootViewController)!, action: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
