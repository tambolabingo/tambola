//
//  LeaderBoardViewController.swift
//  Tambola Bingo
//
//  Created by signity on 07/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class LeaderBoardViewController: UIViewController {
    
    @IBOutlet weak var noRecord: UILabel!
    @IBOutlet weak var userCoinsText: UILabel!
    @IBOutlet weak var fbFriendBtn: UIButton!
    @IBOutlet weak var globalFriendBtn: UIButton!
    @IBOutlet weak var todayBtn: UIButton!
    @IBOutlet weak var weeklyBtn: UIButton!
    @IBOutlet weak var allTimeBtn: UIButton!
    @IBOutlet weak var leadersTableView: UITableView!
    
    var myFriendIdArr = [String]()
    var fbFriends = [LeadersDataModel]()
    var globalFriend  = [LeadersDataModel]()
    var mainFriendList  = [LeadersDataModel]()
    var isFbFriends = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getFaceBookFriendList()
        self.setUserCoin()
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.leaderBoardScreen)

        // Do any additional setup after loading the view.
    }
    func setUserCoin(){
        let user = UserDefaultsHandler.user
        self.userCoinsText.text = user != nil ? user?.userCoins : "0000"
    }
    // MARK: -  IBOutlet Action 
    @IBAction func BackButtonAction(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    //Daily,Weekly
    @IBAction func todayWeeklyButtonAction(sender:UIButton){
        let tag = sender.tag
        switch tag {
        case 1001:
            //Today
            self.todayBtn.setImage(UIImage(named:"TodayActive"), for: .normal)
            self.weeklyBtn.setImage(UIImage(named:"Weekly"), for: .normal)
            self.allTimeBtn.setImage(UIImage(named:"AllTime"), for: .normal)
            let friends = self.myFriendIdArr.joined(separator: ",")
            self.getLeaderBoardDataAPI(parameters: GetParamModel.shared.getLeaderBoardParam("Daily", myFriends: friends)!)
            break
        case 1002:
            //Weekly
            self.todayBtn.setImage(UIImage(named:"Today"), for: .normal)
            self.weeklyBtn.setImage(UIImage(named:"WeeklyActive"), for: .normal)
            self.allTimeBtn.setImage(UIImage(named:"AllTime"), for: .normal)
            let friends = self.myFriendIdArr.joined(separator: ",")
            self.getLeaderBoardDataAPI(parameters: GetParamModel.shared.getLeaderBoardParam("Weekly", myFriends: friends)!)
            break
        case 1003:
            //All Days
            self.todayBtn.setImage(UIImage(named:"Today"), for: .normal)
            self.weeklyBtn.setImage(UIImage(named:"Weekly"), for: .normal)
            self.allTimeBtn.setImage(UIImage(named:"AllTimeActive"), for: .normal)
            let friends = self.myFriendIdArr.joined(separator: ",")
            self.getLeaderBoardDataAPI(parameters: GetParamModel.shared.getLeaderBoardParam("", myFriends: friends)!)
            break
        default:
            break
        }
        
    }
    @IBAction func fbOrGlobalButtonAction(sender:UIButton){
        let tag = sender.tag
        switch tag {
        case 2001:
            //FB Friends List
            self.fbFriendBtn.setImage(UIImage(named:"fbFriendBtnActive"), for: .normal)
            self.globalFriendBtn.setImage(UIImage(named:"globalFriend"), for: .normal)
           
            self.isFbFriends = true
            self.loadMainListView()
            break
        case 2002:
            //Global Friends List
            self.fbFriendBtn.setImage(UIImage(named:"fbFriendBtn"), for: .normal)
            self.globalFriendBtn.setImage(UIImage(named:"globalFriendActive"), for: .normal)
          
            self.isFbFriends = false
            self.loadMainListView()
            break
        default:
            break
        }
    }
    func loadMainListView(){
        if isFbFriends {
            //load fbFirend tableView
            if self.fbFriends.count == 0 {
                self.noRecord.isHidden = false
                self.leadersTableView.isHidden = true
            } else {
                self.noRecord.isHidden = true
                self.leadersTableView.isHidden = false
                self.mainFriendList = self.fbFriends
                self.leadersTableView.reloadData()
            }
        } else {
            //Load Global Friends TableView
            if self.globalFriend.count == 0 {
                self.noRecord.isHidden = false
                self.leadersTableView.isHidden = true
            } else {
                self.noRecord.isHidden = true
                self.leadersTableView.isHidden = false
                self.mainFriendList = self.globalFriend
                self.leadersTableView.reloadData()
            }
        }
    }

}
// MARK: -  Extension For UITableView 
extension LeaderBoardViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: -  TableView Delegate and Data Source 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mainFriendList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "leaderBoardCell", for: indexPath) as? leaderBoardCell
        if indexPath.row % 2 == 0 {
            cell?.backgroundColor = UIColor.clear
        }else {
            cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
        let imageURL = self.mainFriendList[indexPath.row].img_url
        cell?.userImage.sd_setImage(with: URL.init(string: imageURL), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
        })
        cell?.lblUserName.text = self.mainFriendList[indexPath.row].username
        let luck = String(format: "%.2f", self.mainFriendList[indexPath.row].luckRate)
        cell?.lblLuckRating.text = luck + "%"
        cell?.lblGamesPlayed.text = self.mainFriendList[indexPath.row].gamePlayed
        if (UserDefaultsHandler.user?.userId == self.mainFriendList[indexPath.row].user_id){
            cell?.lblViewDetail.isHidden = true
            cell?.ShareBtn.isHidden = false
        } else {
            cell?.lblViewDetail.isHidden = false
            cell?.ShareBtn.isHidden = true
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        NavigationManager.navigateToFriendsDetail(delegate: self, friendData: self.mainFriendList[indexPath.row], game_Id: "")
    }
    
}
// MARK: -  Extension For API Call's 
extension LeaderBoardViewController{
    // MARK: -  Check Public/Private Game Claim's API 
    func getFaceBookFriendList(){
//        LoginViaFBModel.shared.facebookFriendList(sender: self, success: { response in
//            let result = response
//            let friendData = result["data"] as? [[String: Any]]
//            for friend in friendData!{
//                let id = friend["id"] as? String
//                self.myFriendIdArr.append(id!)
//            }
//            let friends = self.myFriendIdArr.joined(separator: ",")
//            self.getLeaderBoardDataAPI(parameters: GetParamModel.shared.getLeaderBoardParam("Daily", myFriends: friends)!)
//        }) { error in
//            print(error)
//        }
        LoginViaFBModel.shared.facebookFriendList(sender: self, success: { response in
            let result = response
            let friendData = result["data"] as? [[String: Any]]
            for friend in friendData!{
                let id = friend["id"] as? String
                self.myFriendIdArr.append(id!)
            }
            let friends = self.myFriendIdArr.joined(separator: ",")
            self.getLeaderBoardDataAPI(parameters: GetParamModel.shared.getLeaderBoardParam("Daily", myFriends: friends)!)
        }, failure: { error in
            print(error)
        })
    }
    func getLeaderBoardDataAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.leaderboard, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseLeaderBoardListResponse(response: response, sender: self, callback: { result in
                //Share URl on facebook and pop to root view
                let data = result as? [String: Any]
                self.fbFriends = (data!["fbFriends"] as? [LeadersDataModel])!
                self.globalFriend = (data!["GlobalFriends"] as? [LeadersDataModel])!
                self.loadMainListView()
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getLeaderBoardDataAPI(parameters:parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
}
