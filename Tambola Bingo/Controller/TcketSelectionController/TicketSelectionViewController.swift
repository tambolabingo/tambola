//
//  TicketSelectionViewController.swift
//  Tambola Bingo
//
//  Created by signity on 28/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class TicketSelectionViewController: UIViewController {
    @IBOutlet weak var userNameText: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var oneTicketBtn: UIButton!
    @IBOutlet weak var twoTicketBtn: UIButton!
    @IBOutlet weak var threeTicketBtn: UIButton!
    var tickets = 1
    var gameType = "Practice"
    var players = [PlayersDataModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        fillUserData()
        self.selectTicket(sender: oneTicketBtn)
        // Do any additional setup after loading the view.
    }
    func fillUserData(){
        let user = UserDefaultsHandler.user
        if user != nil {
            let imageURL = user?.profilePhotoURL
            self.userNameText.text = (user?.fName)! + " " + (user?.lName)!
            if (user?.isGuest)! {
                userImage.image =  GetOrSaveImage.shared.getImage()
                self.getDummyUsersAPI()
            } else {
                userImage.sd_setImage(with: URL.init(string: imageURL!), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
                })
                //For Testing only
                self.getDummyUsersAPI()
            }
        }
    }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func selectTicket(sender:UIButton){
        var imageOne = ""
        var imageTwo = ""
        var imageThree = ""
        switch sender.tag {
        case 101:
            //One Ticket Selection
             imageOne = "TicketOneSelected"
             imageTwo = "TicketTwo"
             imageThree = "TicketThree"
             tickets = 1
            break
        case 102:
            //Two Ticket Selection
             imageOne = "TicketOne"
             imageTwo = "TicketTwoSelected"
             imageThree = "TicketThree"
             tickets = 2
            break
        case 103:
            //Three Ticket Selection
             imageOne = "TicketOne"
             imageTwo = "TicketTwo"
             imageThree = "TicketThreeSelected"
             tickets = 3
            break
        default: break
        }
        self.oneTicketBtn.setImage(UIImage(named: imageOne), for: .normal)
        self.twoTicketBtn.setImage(UIImage(named: imageTwo), for: .normal)
        self.threeTicketBtn.setImage(UIImage(named: imageThree), for: .normal)
    }
    @IBAction func playNowAction(sender:UIButton){
        self.performSegue(withIdentifier: "PlayGameScreen", sender: true)
    }
    
    // MARK: -  API Call Dummy Users   
    func getDummyUsersAPI() {
        //API Call
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.dummyUsers, parameters: nil, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseDummyUserResponse(response: response, sender: self, callback: { result in
                // Get Dummy User's for using as a players
                self.players = (result as? [PlayersDataModel])!
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getDummyUsersAPI()
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        })
    }
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlayGameScreen" {
            let nextVC                    = segue.destination as? PlayGameViewController
            let sequenceArray = EventManager.getDummyNumberSequence()
            let playDetail = PlayGameDataModel.init(tickets, gameType: "1", gameName: "Parctice Game", gameId: "", startTime: "", serverDTime: "", players: players, sequence: sequenceArray, practice: true, claims: ["":0], isPublic: false)
            nextVC?.playGameData = playDetail
        }
    }
    
}
