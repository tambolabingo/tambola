//
//  InviteFaceBookFriendsViewController.swift
//  Tambola Bingo
//
//  Created by signity on 04/09/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class InviteFaceBookFriendsViewController: UIViewController {
    @IBOutlet weak var closeFbFriendListView: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var fbFriendsListTableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblSelectAll: UILabel!
    @IBOutlet weak var selectAllBtn: UIButton!
    
    var friendsList = [FBFriendsDataModel]()
    var friendsOrignalArray  = [FBFriendsDataModel]()
    var addFriendIdArray = [String]()
    var addFriendNameArray = [String]()
    var game_id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getFBFriends()
        // Do any additional setup after loading the view.
    }
    // MARK: -  IBOutlet Action 
    @IBAction func closeButtonAction(){
        self.view.removeFromSuperview()
    }
    @IBAction func doneButtonAction(sender:UIButton){
        //Send Invite to all Selected Friends
        if addFriendIdArray.count > 0{
            self.inviteFBFriendAPI()
        } else {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.FBFriendSelectError, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        }
    }
    @IBAction func addRemoveMember(sender:UIButton){
        if sender.accessibilityLabel == "Selected" {
            //UnSelect
            sender.setImage(UIImage(named: "checkbox"), for: .normal)
            sender.accessibilityLabel = ""
            let index = addFriendIdArray.firstIndex(where: { (item) -> Bool in
                item == self.friendsList[sender.tag].user_id
            })
            index != nil ? _ = addFriendIdArray.remove(at: index!) : nil
            index != nil ? _ = addFriendNameArray.remove(at: index!) : nil
        } else {
            //Select
            sender.setImage(UIImage(named: "checkboxSelected"), for: .normal)
            sender.accessibilityLabel = "Selected"
            self.addFriendIdArray.append(self.friendsList[sender.tag].user_id)
            self.addFriendNameArray.append(self.friendsList[sender.tag].username)
        }
    }
    @IBAction func addRemoveAllMember(sender:UIButton){
            self.addFriendIdArray.removeAll()
            self.addFriendNameArray.removeAll()
            if sender.tag == 0 {
                sender.tag = 1
                self.selectAllBtn.setImage(UIImage(named: "checkboxSelected"), for: .normal)
                for member in self.friendsList {
                    self.addFriendIdArray.append(member.user_id)
                    self.addFriendNameArray.append(member.username)
                }
            } else {
                self.selectAllBtn.setImage(UIImage(named: "checkbox"), for: .normal)
                sender.tag = 0
            }
            self.fbFriendsListTableView.reloadData()
        
    }
    func getFBFriends(){
        EventManager.showloader()
        LoginViaFBModel.shared.facebookFriendList(sender: self, success: { response in
            let result = response["data"] as? [[String: Any]]
            if result?.count != 0 {
                for friend in result!{
                    let fbFriend = FBFriendsDataModel(friend)
                    self.friendsList.append(fbFriend)
                    self.friendsOrignalArray.append(fbFriend)
                }
                
                EventManager.hideloader()
                self.fbFriendsListTableView.reloadData()
            } else {
                EventManager.hideloader()
                EventManager.showAlertWithAction(alertMessage: ConstantModel.message.NoFBFriends, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
            
        }) { error in
            print(error)
            EventManager.hideloader()
            EventManager.showAlertWithAction(alertMessage: ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            
        }
    }
    func inviteFBFriendAPI(){
        let parameters = GetParamModel.shared.getInviteFriendsParam(self.game_id, invitee_id: addFriendIdArray.joined(separator: ","), invitee_name:addFriendNameArray.joined(separator: ","))
        BaseAPIHandler.shared.inviteFBFriendsAPI(parameters!, self) { isCallSucess in
            if isCallSucess {
                EventManager.showAlertWithAction(alertMessage: ConstantModel.message.inviteMessage, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: {_ in
                    self.closeButtonAction()
                })
                
            }
        }
    }
}
// MARK: -  Extension For UITableView 
extension InviteFaceBookFriendsViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: -  TableView Delegate and Data Source 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friendsList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupMemberCell", for: indexPath) as? groupMemberCell
        if indexPath.row % 2 == 0 {
            cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        } else {
            cell?.backgroundColor = UIColor.clear
        }
        
        let imageURL = self.friendsList[indexPath.row].img_url
        cell?.memberImage.sd_setImage(with: URL.init(string: imageURL), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
            })
        cell?.lblMemberName.text = self.friendsList[indexPath.row].username
        let contained = self.addFriendIdArray.filter { $0 == self.friendsList[indexPath.row].user_id}
        
        if contained.count > 0 {
            cell?.btnAddDelete.setImage(UIImage(named: "checkboxSelected"), for: .normal)
            cell?.btnAddDelete.accessibilityLabel = "Selected"
        } else {
            cell?.btnAddDelete.setImage(UIImage(named: "checkbox"), for: .normal)
            cell?.btnAddDelete.accessibilityLabel = ""
        }
//        cell?.btnAddDelete.setImage(UIImage(named: "checkbox"), for: .normal)
//        cell?.btnAddDelete.accessibilityLabel = ""
        cell?.btnAddDelete.tag = indexPath.row
        return cell!
    }
}
// MARK: -  SearchBar Delegate Extension 
extension InviteFaceBookFriendsViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            let contained = friendsOrignalArray.filter { $0.username.containsIgnoringCase(searchText)}
            let isUserSearching = (searchText.containsWhitespace() || searchText != "") ? true : false
            self.friendsList = isUserSearching ? contained : friendsOrignalArray
            self.fbFriendsListTableView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}
