//
//  FriendsAndGroupsViewController.swift
//  Tambola Bingo
//
//  Created by signity on 08/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class FriendsAndGroupsViewController: UIViewController {

    @IBOutlet weak var userCoinsText: UILabel!
    @IBOutlet weak var noRecordText: UILabel!
    @IBOutlet weak var fbFriendBtn: UIButton!
    @IBOutlet weak var myGroupBtn: UIButton!
    @IBOutlet weak var createGroupBtn: UIButton!
    @IBOutlet weak var friendsTableView: UITableView!
    
    var myFriendIdArr = [String]()
    var mainFriendList  = [LeadersDataModel]()
    var groupsList = [MyGroups]()
    var isMyFriends = true
    var isGroupEdit = false
    var selectedGroup = MyGroups()
 // MARK: -  ViewController LifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noRecordText.isHidden = true
        self.createGroupBtn.isHidden = true
        self.createGroupBtn.setImage(UIImage(named:"fb_invite"), for: .normal)
        self.createGroupBtn.tag = 555
        self.getMyFriendListDataAPI(parameters: GetParamModel.shared.getMyFriendslistParam()!)
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.friendsScreen)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.groupsList = MyGroupsDataModel.shared.getGroupsFromDB()!
        self.loadMainView()
    }
    // MARK: -  IBOutlet Action 
    @IBAction func BackButtonAction(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func friendOrGroupButtonAction(sender:UIButton){
        switch sender.tag {
        case 1001:
            //My Fb Friend List
            isMyFriends = true
            self.createGroupBtn.isHidden = true
            self.createGroupBtn.setImage(UIImage(named:"fb_invite"), for: .normal)
            self.createGroupBtn.tag = 555
            self.fbFriendBtn.setImage(UIImage(named: "my_friend_active"), for: .normal)
            self.myGroupBtn.setImage(UIImage(named: "my_groups_deactive"), for: .normal)
            self.loadMainView()
            break
        case 1002:
            //My Group List
            isMyFriends = false
            self.createGroupBtn.isHidden = false
            self.createGroupBtn.setImage(UIImage(named:"create-new-group"), for: .normal)
            self.createGroupBtn.tag = 333
            self.fbFriendBtn.setImage(UIImage(named: "my_friend_deactive"), for: .normal)
            self.myGroupBtn.setImage(UIImage(named: "my_groups_active"), for: .normal)
            self.loadMainView()
            break
        default:
            break
        }
    }
    @IBAction func inviteOrCreateGroupButtonAction(sender:UIButton){
        switch sender.tag {
        case 555:
            //Invite Action
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.appInvite, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            break
        case 333:
            //Create Group Action
            isGroupEdit = false
            self.performSegue(withIdentifier: "CreateNewGroupView", sender: true)
            break
        default:
            break
        }
    }
    func loadMainView(){
        let user = UserDefaultsHandler.user
        self.userCoinsText.text = user != nil ? user?.userCoins : "0000"
        if isMyFriends {
            if mainFriendList.count == 0 {
                self.noRecordText.isHidden = false
                self.friendsTableView.isHidden = true
            } else {
                self.noRecordText.isHidden = true
                self.friendsTableView.isHidden = false
                self.friendsTableView.reloadData()
            }
        } else {
            if self.groupsList.count == 0 {
                self.noRecordText.isHidden = false
                self.friendsTableView.isHidden = true
            } else {
                self.noRecordText.isHidden = true
                self.friendsTableView.isHidden = false
                self.friendsTableView.reloadData()
            }
        }
    }
 // MARK: -  Fb Friends Action 
    @IBAction func viewDetailButtonAction(sender:UIButton){
        self.mainFriendList[sender.tag].isDetailAdded ? NavigationManager.navigateToFriendsDetail(delegate: self, friendData: self.mainFriendList[sender.tag], game_Id: "") : self.getUserDetailDataAPI(parameters: GetParamModel.shared.getUserRecordsParam(userId: self.mainFriendList[sender.tag].user_id)!, index: sender.tag)
    }
    @IBAction func addToGroupButtonAction(sender:UIButton){
        if self.groupsList.count > 0 {
        NavigationManager.navigateToAddInviteGroup(self, isInvite: false, isgroup: true, userId: self.mainFriendList[sender.tag].user_id, gameId: "", userName: self.mainFriendList[sender.tag].username)
        } else {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.NoGroup, btn1Tit: ConstantModel.btnText.btnCancel, btn2Tit: ConstantModel.btnText.btnCreate, sender: self, action:{ btntitle in
                if btntitle == ConstantModel.btnText.btnCreate {
                    self.isGroupEdit = false
                    self.performSegue(withIdentifier: "CreateNewGroupView", sender: true)
                }
            })
        }
    }
// MARK: -  My Groups Action 
    @IBAction func deleteGroupButtonAction(sender:UIButton){
        let group = self.groupsList[sender.tag]
        MyGroupsDataModel.shared.deleteGroupFromDB(group)
        self.groupsList = MyGroupsDataModel.shared.getGroupsFromDB()!
        self.loadMainView()
    }
    @IBAction func editGroupButtonAction(sender:UIButton){
        isGroupEdit = true
        self.selectedGroup = self.groupsList[sender.tag]
        self.performSegue(withIdentifier: "CreateNewGroupView", sender: true)
    }
    @IBAction func createGameButtonAction(sender:UIButton){
        //Navigate to create New Game and Send invite to all group members using invite API
        self.selectedGroup = self.groupsList[sender.tag]
        NavigationManager.navigateToCreateGame(self, group: self.selectedGroup, isFromGroup: true)
    }
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateNewGroupView" {
            let nextVC                   = segue.destination as? CreateNewGroupViewController
            nextVC?.myFbFriendsArray      = self.mainFriendList
            nextVC?.isEdit                = self.isGroupEdit
            if isGroupEdit{
                nextVC?.myGroup  = self.selectedGroup
            }
        }
    }
}
// MARK: -  Extension For UITableView 
extension FriendsAndGroupsViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: -  TableView Delegate and Data Source 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if isMyFriends {
                //My friend List
                return self.mainFriendList.count
            } else {
                //My Group List
                return self.groupsList.count
            }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 70
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if isMyFriends {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myFriendsCell", for: indexPath) as? myFriendsCell
                if indexPath.row % 2 == 0 {
                    cell?.backgroundColor = UIColor.clear
                }else {
                    cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
                }
                let imageURL = self.mainFriendList[indexPath.row].img_url
                cell?.userImage.sd_setImage(with: URL.init(string: imageURL), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
                })
                cell?.lblUserName.text = self.mainFriendList[indexPath.row].username
                cell?.lblUserCoin.text = self.mainFriendList[indexPath.row].coins
                cell?.btnAddToGroup.tag = indexPath.row
                cell?.btnViewDetail.tag = indexPath.row
                return cell!
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myGroupsCell", for: indexPath) as? myGroupsCell
                if indexPath.row % 2 == 0 {
                    cell?.backgroundColor = UIColor.clear
                } else {
                    cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
                }
                cell?.groupImage.image = UIImage(data: self.groupsList[indexPath.row].image!)
                cell?.lblGroupName.text = self.groupsList[indexPath.row].name
                cell?.btnDeleteGroup.tag = indexPath.row
                cell?.btnCreateGame.tag = indexPath.row
                cell?.btnEditGroup.tag = indexPath.row
                return cell!
            }
    }
}
// MARK: -  Extension For API Call's 
extension FriendsAndGroupsViewController{
    // MARK: -  Check Public/Private Game Claim's API 
    func getMyFriendListDataAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.myLatestInvite, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseMyFriendsListResponse(response: response, sender: self, callback: { result in
                //Share URl on facebook and pop to root view
                self.mainFriendList = (result as? [LeadersDataModel])!
                self.loadMainView()
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getMyFriendListDataAPI(parameters:parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    func getUserDetailDataAPI(parameters: [String: Any], index: Int){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.myDetails, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //Share URl on facebook and pop to root view
                let data = result as? [String: Any]
                let score = data!["scores"] as? [String: Any]
                self.mainFriendList[index].claims = score!["claims"] as? String ?? ""
                self.mainFriendList[index].gamePlayed  = score!["gamePlayed"] as? String ?? ""
                self.mainFriendList[index].luckRate = score!["LuckRate"] as? Double ?? 0.00
                self.mainFriendList[index].totalwins = score!["Totalwins"] as? String ?? ""
                self.mainFriendList[index].isDetailAdded =  true
                NavigationManager.navigateToFriendsDetail(delegate: self, friendData: self.mainFriendList[index], game_Id: "")
                //Show Detail View
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getUserDetailDataAPI(parameters:parameters, index: index)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
}

