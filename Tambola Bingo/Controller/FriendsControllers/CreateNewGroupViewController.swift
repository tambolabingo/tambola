//
//  CreateNewGroupViewController.swift
//  Tambola Bingo
//
//  Created by signity on 09/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class CreateNewGroupViewController: UIViewController {
    @IBOutlet weak var groupNameText: UITextField!
    @IBOutlet weak var imgGroupName: UIImageView!
    @IBOutlet weak var imgNameCheck: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var createBtn: UIButton!
    
    @IBOutlet weak var membersTableView: UITableView!
    
    @IBOutlet weak var closeSearchBtn: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var lblSelectAll: UILabel!
    @IBOutlet weak var selectAllBtn: UIButton!
    @IBOutlet weak var selectAllTopConstrain:NSLayoutConstraint!
    @IBOutlet weak var tableTopConstrain:NSLayoutConstraint!
    
    var isEdit = false
    var myGroup = MyGroups()
    var myFbFriendsArray  = [LeadersDataModel]()
    var memberSelected = [[String:Any]]()
    
    //Variables for Search FB Friends
    var fbFriendsOrignalArray  = [LeadersDataModel]()
    var nonMemberOrignalArray  = [LeadersDataModel]()
    //Add More Members Variables
    var nonMemberArray  = [LeadersDataModel]()
    var nonmemberSelected = [[String:Any]]()
    @IBOutlet weak var addMemberView: UIView!
    @IBOutlet weak var closeMemberView: UIButton!
    @IBOutlet weak var memberSearchBar: UISearchBar!
    @IBOutlet weak var selectallNonMemberBtn: UIButton!
    @IBOutlet weak var participentTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        fbFriendsOrignalArray = myFbFriendsArray
        // Do any additional setup after loading the view.
    }
    func setUpUI(){
        if isEdit{
            self.tableTopConstrain.constant = 0
            self.lblGroupName.isHidden = false
            self.groupNameText.isHidden = true
            self.lblSelectAll.isHidden = true
            self.selectAllBtn.isHidden = true
            self.searchBar.isHidden = true
            self.closeSearchBtn.isHidden = true
            self.searchBtn.isHidden = true
            self.imgGroupName.isHidden = true
            self.imgNameCheck.isHidden = true
            self.createBtn.setImage(UIImage(named:"add_friends_icon_new"), for: .normal)
            self.createBtn.tag = 1
            self.lblGroupName.text = myGroup.name
            let members = self.myGroup.members as? [[String : Any]]
            for mem in members!{
                self.memberSelected.append(mem)
            }
        } else {
            self.selectAllTopConstrain.constant = 10
            self.tableTopConstrain.constant = 40
            self.lblGroupName.isHidden = true
            self.searchBar.isHidden = true
            self.closeSearchBtn.isHidden = true
            
            self.lblSelectAll.isHidden = false
            self.selectAllBtn.isHidden = false
            self.searchBtn.isHidden = false
            self.imgGroupName.isHidden = false
            //Check Later
            self.imgNameCheck.isHidden = true
            self.groupNameText.isHidden = false
            self.groupNameText.attributedPlaceholder = NSAttributedString(string: "Group Name",
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
        self.addMemberView.isHidden = true
        self.membersTableView.reloadData()
    }
    // MARK: -  IBOutlet Action 
    @IBAction func BackButtonAction(sender:UIButton){
        isEdit ? (self.memberSelected.count == 0 ? (EventManager.showAlertWithAction(alertMessage: ConstantModel.message.NoMember, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)) : (_ = self.navigationController?.popViewController(animated: true))) : (_ = self.navigationController?.popViewController(animated: true))
    }
    @IBAction func searchButtonAction(sender:UIButton){
        self.selectAllTopConstrain.constant = 60
        self.tableTopConstrain.constant = 90
        self.searchBar.isHidden = false
        self.closeSearchBtn.isHidden = false
        self.searchBtn.isHidden = true
    }
    @IBAction func closeSearchButtonAction(sender:UIButton){
        self.selectAllTopConstrain.constant = 10
        self.tableTopConstrain.constant = 40
        self.searchBar.isHidden = true
        self.closeSearchBtn.isHidden = true
        self.searchBtn.isHidden = false
        self.searchBar.text = ""
        self.myFbFriendsArray = fbFriendsOrignalArray
        self.membersTableView.reloadData()
    }
    @IBAction func createGroupButtonAction(sender:UIButton){
        if sender.tag == 0{
        if self.validateData(){
            let image = UIImage(named: "userImg")//Get this from Camera
            let image_data = (image)!.pngData()
            let dataDict = ["name":self.groupNameText.text ?? "Default Group", "group_img":image_data!, "members":self.memberSelected] as [String : Any]
            DbHandler.shared.saveGroup(group: dataDict)
            self.navigationController?.popViewController(animated: true)
            }}
        else {
            //Show Add more members
            nonMemberArray.removeAll()
            for fbFriend in myFbFriendsArray{
                let memberId = fbFriend.user_id
                let contained = self.memberSelected.filter { $0["fb_id"] as? String == memberId}
                if contained.count == 0 {
                    nonMemberArray.append(fbFriend)
                }
            }
            nonMemberOrignalArray = nonMemberArray
            nonMemberArray.count > 0 ? self.showMembersView() : EventManager.showAlertWithAction(alertMessage: ConstantModel.message.NoFriends, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        }
    }
   
    @IBAction func addRemoveMember(sender:UIButton){
        if sender.accessibilityLabel == "Delete" {
            self.memberSelected.remove(at: sender.tag)
            let dataDict = ["name":myGroup.name ?? "Default Group", "group_img":myGroup.image ?? "", "members":self.memberSelected] as [String : Any]
            DbHandler.shared.updateGroup(myGroup: dataDict)
            self.membersTableView.reloadData()
        } else if sender.accessibilityLabel == "Selected" {
            if isEdit {
                let memberId = self.nonMemberArray[sender.tag].user_id
                let index = nonmemberSelected.firstIndex(where: { (item) -> Bool in
                    item["fb_id"] as? String == memberId
                })
                self.nonmemberSelected.remove(at: index!)
                self.reloadparticipenRow(rowNum: sender.tag)
            }else {
                let memberId = self.myFbFriendsArray[sender.tag].user_id
                let index = memberSelected.firstIndex(where: { (item) -> Bool in
                    item["fb_id"] as? String == memberId
                })
                self.memberSelected.remove(at: index!)
                self.reloadRow(rowNum: sender.tag)
            }
        } else {
            if isEdit {
                let memberDict = ["username":self.nonMemberArray[sender.tag].username, "fb_id":self.nonMemberArray[sender.tag].user_id]
                self.nonmemberSelected.append(memberDict)
                self.reloadparticipenRow(rowNum: sender.tag)
            }else {
                let memberDict = ["username":self.myFbFriendsArray[sender.tag].username, "fb_id":self.myFbFriendsArray[sender.tag].user_id]
                self.memberSelected.append(memberDict)
                self.reloadRow(rowNum: sender.tag)
            }
        }
        
    }
    @IBAction func addRemoveAllMember(sender:UIButton){
        if isEdit {
            self.nonmemberSelected.removeAll()
            if sender.tag == 0 {
                sender.tag = 1
                self.selectallNonMemberBtn.setImage(UIImage(named: "checkboxSelected"), for: .normal)
                for member in self.nonMemberArray {
                    let memberDict = ["username":member.username, "fb_id":member.user_id]
                    self.nonmemberSelected.append(memberDict)
                }
            } else {
                self.selectallNonMemberBtn.setImage(UIImage(named: "checkbox"), for: .normal)
                sender.tag = 0
            }
            self.participentTableView.reloadData()
        } else {
            self.memberSelected.removeAll()
            if sender.tag == 0 {
                sender.tag = 1
                self.selectAllBtn.setImage(UIImage(named: "checkboxSelected"), for: .normal)
                for member in self.myFbFriendsArray {
                    let memberDict = ["username":member.username, "fb_id":member.user_id]
                    self.memberSelected.append(memberDict)
                }
            } else {
                self.selectAllBtn.setImage(UIImage(named: "checkbox"), for: .normal)
                sender.tag = 0
            }
            self.membersTableView.reloadData()
        }
    }
    
    func reloadRow(rowNum: Int){
        let indexPath = IndexPath(item: rowNum, section: 0)
        self.membersTableView.reloadRows(at: [indexPath], with: .none)
    }
    func reloadparticipenRow(rowNum: Int){
        let indexPath = IndexPath(item: rowNum, section: 0)
        self.participentTableView.reloadRows(at: [indexPath], with: .none)
    }
}
// MARK: -  Extension For UITableView 
extension CreateNewGroupViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: -  TableView Delegate and Data Source 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == participentTableView {
            return self.nonMemberArray.count
        } else {
            if isEdit{
                return self.memberSelected.count
            } else{
                return self.myFbFriendsArray.count
            }
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == participentTableView {
            return 50
        } else {
            return 70
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "groupMemberCell", for: indexPath) as? groupMemberCell
            if indexPath.row % 2 == 0 {
                cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
            } else {
                cell?.backgroundColor = UIColor.clear
            }
            if tableView == participentTableView {
                cell?.lblMemberName.text = nonMemberArray[indexPath.row].username
                let imgUrl =  self.nonMemberArray[indexPath.row].img_url
                cell?.memberImage.sd_setImage(with: URL.init(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
                })
                let memberId = self.nonMemberArray[indexPath.row].user_id
                let contained = self.nonmemberSelected.filter { $0["fb_id"] as? String == memberId}
                
                if contained.count > 0 {
                    cell?.btnAddDelete.setImage(UIImage(named: "checkboxSelected"), for: .normal)
                    cell?.btnAddDelete.accessibilityLabel = "Selected"
                } else {
                    cell?.btnAddDelete.setImage(UIImage(named: "checkbox"), for: .normal)
                    cell?.btnAddDelete.accessibilityLabel = ""
                }
            } else {
                if isEdit{
                    cell?.lblMemberName.text = self.memberSelected[indexPath.row]["username"] as? String
                    let imgUrl = String(format:ConstantModel.fbImageURL, (self.memberSelected[indexPath.row]["fb_id"] as? String)!)
                    cell?.memberImage.sd_setImage(with: URL.init(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
                    })
                    cell?.btnAddDelete.setImage(UIImage(named: "delete_icon._new"), for: .normal)
                    cell?.btnAddDelete.accessibilityLabel = "Delete"
                } else {
                    let imgUrl = self.myFbFriendsArray[indexPath.row].img_url
                    cell?.memberImage.sd_setImage(with: URL.init(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
                })
                    cell?.lblMemberName.text = self.myFbFriendsArray[indexPath.row].username
                    let memberId = self.myFbFriendsArray[indexPath.row].user_id
                    let contained = self.memberSelected.filter { $0["fb_id"] as? String == memberId}

                    if contained.count > 0 {
                        cell?.btnAddDelete.setImage(UIImage(named: "checkboxSelected"), for: .normal)
                        cell?.btnAddDelete.accessibilityLabel = "Selected"
                    } else {
                        cell?.btnAddDelete.setImage(UIImage(named: "checkbox"), for: .normal)
                        cell?.btnAddDelete.accessibilityLabel = ""
                    }
                }
                    cell?.btnAddDelete.tag = indexPath.row
        }
            return cell!
        
    }
}
// MARK: -  Add More Members Extension 
extension CreateNewGroupViewController{
    func showMembersView(){
        self.addMemberView.isHidden = false
        self.nonmemberSelected.removeAll()
        self.selectallNonMemberBtn.setImage(UIImage(named: "checkbox"), for: .normal)
        self.selectallNonMemberBtn.tag = 0
        self.participentTableView.reloadData()
    }
    @IBAction func closeMemberView(sender:UIButton){
        self.addMemberView.isHidden = true
    }
    @IBAction func doneButtonAction(sender:UIButton){
        for member in nonmemberSelected{
        self.memberSelected.append(member)
        }
        let dataDict = ["name":myGroup.name ?? "Default Group", "group_img":myGroup.image ?? "", "members":self.memberSelected] as [String : Any]
        DbHandler.shared.updateGroup(myGroup: dataDict)
        self.addMemberView.isHidden = true
        self.membersTableView.reloadData()
    }
    
}
// MARK: -  SearchBar Delegate Extension 
extension CreateNewGroupViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == memberSearchBar{
            let contained = nonMemberOrignalArray.filter { $0.username.containsIgnoringCase(searchText)}
            let isUserSearching = (searchText.containsWhitespace() || searchText != "") ? true : false
            self.nonMemberArray = isUserSearching ? contained : nonMemberOrignalArray
            self.participentTableView.reloadData()
        } else {
            let contained = fbFriendsOrignalArray.filter { $0.username.containsIgnoringCase(searchText)}
            let isUserSearching = (searchText.containsWhitespace() || searchText != "") ? true : false
            self.myFbFriendsArray = isUserSearching ? contained : fbFriendsOrignalArray
            self.membersTableView.reloadData()
        }
    }
}
