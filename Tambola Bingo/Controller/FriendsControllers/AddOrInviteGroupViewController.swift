//
//  AddOrInviteGroupViewController.swift
//  Tambola Bingo
//
//  Created by signity on 16/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class AddOrInviteGroupViewController: UIViewController {
    //Add to Group Variables
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var addGroupListView: UIView!
    @IBOutlet weak var closeGroupListView: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var groupListTableView: UITableView!
    
    var groupsList = [MyGroups]()
    var selectedGroupList = [MyGroups]()
    
    var friendsList = [LeadersDataModel]()
    var selectedFriends = [LeadersDataModel]()
    
    var selectedUserId = ""
    var selectedUserName = ""
    var addMemberArray = [[String:Any]]()
    var isInvite = false //Incase of add group false and incase of invite group or friend true
    var isGroup = true //Incese of add or invite group true and incase of invite friends false
    var gameId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isGroup {
            self.groupsList = MyGroupsDataModel.shared.getGroupsFromDB()!
        } else {
            self.getMyFriendListDataAPI(parameters: GetParamModel.shared.getMyFriendslistParam()!)
        }
        isInvite ? doneButton.setImage(#imageLiteral(resourceName: "InviteButton"), for: .normal) : doneButton.setImage(#imageLiteral(resourceName: "done_btn"), for: .normal)
        doneButton.tag = isInvite ? 0 : 1
        lblTitle.text = isGroup ? "Group" : "Recent Friends"
        // Do any additional setup after loading the view.
    }
    // MARK: -  IBOutlet Action 
    @IBAction func cloaseButtonAction(sender:UIButton){
        self.view.removeFromSuperview()
    }
    @IBAction func doneButtonAction(sender:UIButton){
        if sender.tag == 0 {
            //Invite Button Action
            var inviteID = [String]()
            var inviteNames = [String]()
            if isGroup {
                for group in selectedGroupList {
                    for member in group.members as! [[String: Any]]{
                        let contained = inviteID.filter { $0 == member["fb_id"] as? String}
                        if contained.count == 0 {
                            inviteID.append((member["fb_id"] as? String)!)
                            inviteNames.append((member["username"] as? String)!)
                        }
                    }
                }
            } else {
                for friend in selectedFriends {
                    inviteID.append(friend.user_id)
                    inviteNames.append(friend.username)
                }
            }
            if inviteID.count == 0 {
                EventManager.showAlertWithAction(alertMessage:ConstantModel.message.FBFriendSelectError, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            } else {
            let params = GetParamModel.shared.getInviteFriendsParam(gameId, invitee_id: inviteID.joined(separator: ","), invitee_name:inviteNames.joined(separator: ","))
            self.inviteGroupMembersAPI(parameters: params!)
            }
        } else {
            //Done Button Action
            for data in addMemberArray {
                let group = groupsList.filter { $0.name == data["group"] as? String}
                for gp in group{
                    var members = gp.members as? [[String : Any]]
                    let contained = members?.filter { $0["fb_id"] as? String == data["fb_id"] as? String}
                    if contained?.count == 0 {
                        members?.append(["username":data["username"] as? String ?? "", "fb_id":data["fb_id"] as? String ?? ""])
                    }
                    let dataDict = ["name":gp.name!, "group_img":gp.image!, "members":members!] as [String : Any]
                    DbHandler.shared.updateGroup(myGroup: dataDict)
                }
            }
            self.view.removeFromSuperview()
        }
        
    }
    @IBAction func addRemoveMember(sender:UIButton){
        if sender.accessibilityLabel == "Selected" {
            sender.setImage(UIImage(named: "checkbox"), for: .normal)
            sender.accessibilityLabel = ""
            let gName = isGroup ? groupsList[sender.tag].name : friendsList[sender.tag].username
            if isInvite {
                if isGroup {
                    let index = selectedGroupList.firstIndex(where: { (item) -> Bool in
                    item.name == gName
                    })
                    index != nil ? _ = selectedGroupList.remove(at: index!) : nil
                } else {
                    let index = selectedFriends.firstIndex(where: { (item) -> Bool in
                        item.username == gName
                    })
                    index != nil ? _ = selectedFriends.remove(at: index!) : nil
                }
            } else {
                let index = addMemberArray.firstIndex(where: { (item) -> Bool in
                    item["group"] as? String == gName
                })
                index != nil ? _ = addMemberArray.remove(at: index!) : nil
            }
        } else {
            if isInvite {
                 if isGroup {
                    selectedGroupList.append(groupsList[sender.tag])
                 } else {
                    selectedFriends.append(friendsList[sender.tag])
                }
            } else {
                let dict = ["username":selectedUserName, "fb_id":selectedUserId, "group": groupsList[sender.tag].name ?? ""] as [String : Any]
                addMemberArray.append(dict)
            }
            sender.setImage(UIImage(named: "checkboxSelected"), for: .normal)
            sender.accessibilityLabel = "Selected"
            
        }
        
    }
}
// MARK: -  Extension For UITableView 
extension AddOrInviteGroupViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: -  TableView Delegate and Data Source 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isGroup ? self.groupsList.count : self.friendsList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupMemberCell", for: indexPath) as? groupMemberCell
        if indexPath.row % 2 == 0 {
            cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        } else {
            cell?.backgroundColor = UIColor.clear
        }
        if isGroup {
            cell?.lblMemberName.text = self.groupsList[indexPath.row].name
            cell?.memberImage.image = UIImage(data: self.groupsList[indexPath.row].image!)
            let members = self.groupsList[indexPath.row].members as? [[String: Any]]
            let contained = members?.filter { $0["fb_id"] as? String == self.selectedUserId}
            if (contained?.count)! > 0 {
                if isInvite {
                    self.selectedGroupList.append(self.groupsList[indexPath.row])
                } else {
                    let dict = ["username":self.selectedUserName, "fb_id":self.selectedUserId, "group": self.groupsList[indexPath.row].name ?? ""] as [String : Any]
                    self.addMemberArray.append(dict)
                }
                cell?.btnAddDelete.setImage(UIImage(named: "checkboxSelected"), for: .normal)
                cell?.btnAddDelete.accessibilityLabel = "Selected"
            } else {
                cell?.btnAddDelete.setImage(UIImage(named: "checkbox"), for: .normal)
                cell?.btnAddDelete.accessibilityLabel = ""
            }
        } else {
            let imageURL = self.friendsList[indexPath.row].img_url
            cell?.memberImage.sd_setImage(with: URL.init(string: imageURL), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
            })
            cell?.lblMemberName.text = self.friendsList[indexPath.row].username
            cell?.btnAddDelete.setImage(UIImage(named: "checkbox"), for: .normal)
            cell?.btnAddDelete.accessibilityLabel = ""
        }
        cell?.btnAddDelete.tag = indexPath.row
        return cell!
    }
}
// MARK: -  Extension For API Call's 
extension AddOrInviteGroupViewController{
    func getMyFriendListDataAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.myLatestInvite, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseMyFriendsListResponse(response: response, sender: self, callback: { result in
                //Share URl on facebook and pop to root view
                self.friendsList = (result as? [LeadersDataModel])!
                self.groupListTableView.reloadData()
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getMyFriendListDataAPI(parameters:parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
// MARK: -  Invte Group Friends API 
    func inviteGroupMembersAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.inviteFriends, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //  invite Group Members Sucessfully
                let data = result as? [String: Any]
                EventManager.showAlertWithAction(alertMessage: data!["message"] as? String ?? ConstantModel.message.inviteMessage, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: { _ in
                        self.view.removeFromSuperview()
                })
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.self.inviteGroupMembersAPI(parameters: parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
}
