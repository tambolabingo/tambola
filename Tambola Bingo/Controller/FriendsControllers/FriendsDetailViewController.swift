//
//  FriendsDetailViewController.swift
//  Tambola Bingo
//
//  Created by signity on 09/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class FriendsDetailViewController: UIViewController {
    var blockcallback:((String?) -> Void)?
    //Leaders Detail View Variables
    @IBOutlet weak var friendsDetailView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var leaderCoinsText: UILabel!
    @IBOutlet weak var leaderNameText: UILabel!
    @IBOutlet weak var centerText: UILabel!
    @IBOutlet weak var circleText: UILabel!
    @IBOutlet weak var cornerText: UILabel!
    @IBOutlet weak var earlyFiveText: UILabel!
    @IBOutlet weak var firstLineText: UILabel!
    @IBOutlet weak var houseFullText: UILabel!
    @IBOutlet weak var luckSevenText: UILabel!
    @IBOutlet weak var pyramidText: UILabel!
    @IBOutlet weak var secondLineText: UILabel!
    @IBOutlet weak var thirdLineText: UILabel!
    @IBOutlet weak var playedGamesText: UILabel!
    @IBOutlet weak var totalWinsText: UILabel!
    @IBOutlet weak var LuckRatingText: UILabel!
    
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnFbShare: UIButton!
    
    var myFriendData  = LeadersDataModel()
    var gameID = ""
    var userDetailImg = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadDetailViewData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: -  Leader Detail View Functions and Action 
    @IBAction func cloaseButtonAction(sender:UIButton){
        self.view.removeFromSuperview()
    }
    @IBAction func refreshButtonAction(sender:UIButton){
        self.getRefreshDetailDataAPI(parameters: GetParamModel.shared.getUserRecordsParam(userId: self.myFriendData.user_id)!)
    }
    @IBAction func reportBlockUnblockAction(sender:UIButton){
        switch sender.tag {
        case 1001:
            //Report As Spam Call report API
            self.reportUserAPI(parameters: GetParamModel.shared.getSpamUserParam(myFriendData.user_id)!)
            break
        case 1002:
            //Block User Call BlockUnblock API
//            status”:(0 to unblock and 1 for block),
            self.blockUnblockUserAPI(parameters: GetParamModel.shared.getBlockUnblockParam(myFriendData.user_id,self.gameID,1)!, isBlock: true)
            break
        case 1003:
            //UnBlock User Call BlockUnblock API
            self.blockUnblockUserAPI(parameters: GetParamModel.shared.getBlockUnblockParam(myFriendData.user_id,self.gameID,0)!, isBlock: false)
            break
        default:
            break
        }
    }
    @IBAction func fbShareButtonAction(sender:UIButton){
        //Share Screen Shot On Fb Same as Share result
        self.userDetailImg = EventManager.getScreenShot(self.view)
        self.shareUserDetailOnFBAPI(parameters: GetParamModel.shared.getShareResultParam(resultImage: self.userDetailImg)!)
    }
    func loadDetailViewData(){
        //FbShareBtn and Report/Block unblock Buttons Hide show
        /*FbShareBtn : show in case of users own detail and Hide for others
            Report Spam Button Show for Public Game Playes detail
            Block Unblock for Private users deatil
        */
        if UserDefaultsHandler.user?.fbID == myFriendData.user_id{
            //Show Share on fb Button as this is user own detail
            self.btnReport.isHidden = true
            self.btnFbShare.isHidden = false
        } else if myFriendData.gameType == "Public" {
            //Show Report Spam Button
            self.btnReport.isHidden = false
            self.btnReport.tag = 1001
            self.btnFbShare.isHidden = true
        }else if myFriendData.gameType == "Private" {
            //Show Block Unblock and check for empty
            self.btnFbShare.isHidden = true
            self.btnReport.setImage(UIImage(named: ""), for: .normal)
            if myFriendData.user_blocked == "blocked"{
                //Set button title as unblock and back color green 25,94,7
                self.btnReport.isHidden = false
                self.btnReport.backgroundColor = UIColor(red: 25/255, green: 94/255, blue: 07/255, alpha: 1)
                self.btnReport.setTitle("  Unblock  ", for: .normal)
                self.btnReport.tag = 1003
            } else if myFriendData.user_blocked == "unblocked"{
                //Set button title as block and back color red
                self.btnReport.isHidden = false
                self.btnReport.backgroundColor = UIColor.red
                self.btnReport.setTitle("  Block  ", for: .normal)
                self.btnReport.tag = 1002
            } else{
                //Hide block/ubblocked Button
                self.btnReport.isHidden = true
            }
        }else if myFriendData.gameType == "" {
            //Hide block/ubblocked and share on fb Button
            self.btnFbShare.isHidden = true
            self.btnReport.isHidden = true
        }
        let imageURL = myFriendData.img_url
        userImage.sd_setImage(with: URL.init(string: imageURL), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
        })
        leaderCoinsText.text = myFriendData.coins
        let userNameArr = myFriendData.username.components(separatedBy: ";")
        leaderNameText.text = userNameArr.count == 0 ? myFriendData.username : userNameArr[0]
        playedGamesText.text = myFriendData.gamePlayed
        totalWinsText.text = myFriendData.totalwins
        let luck = String(format: "%.2f", myFriendData.luckRate)
        LuckRatingText.text = luck + "%"
        centerText.text = String(myFriendData.score.center)
        circleText.text = String(myFriendData.score.circle)
        cornerText.text = String(myFriendData.score.corner)
        earlyFiveText.text = String(myFriendData.score.earlyFive)
        firstLineText.text = String(myFriendData.score.firstLine)
        houseFullText.text = String(myFriendData.score.houseFull)
        luckSevenText.text = String(myFriendData.score.luckSeven)
        pyramidText.text = String(myFriendData.score.pyramid)
        secondLineText.text = String(myFriendData.score.secondLine)
        thirdLineText.text = String(myFriendData.score.thirdLine)
    }
    func updateData(data: [String: Any]){
        self.myFriendData.coins = data["coins"] as? String ?? self.myFriendData.coins
        let score = data["scores"] as? [String: Any]
        self.myFriendData.claims = score!["claims"] as? String ?? ""
        self.myFriendData.gamePlayed  = score!["gamePlayed"] as? String ?? ""
        self.myFriendData.luckRate = score!["LuckRate"] as? Double ?? 0.00
        self.myFriendData.totalwins = score!["Totalwins"] as? String ?? ""
        self.myFriendData.isDetailAdded =  true
        self.myFriendData.score.center = score!["CENTER"] as? Int ?? 0
        self.myFriendData.score.circle = score!["CIRCLE"] as? Int ?? 0
        self.myFriendData.score.corner = score!["CORNER"] as? Int ?? 0
        self.myFriendData.score.earlyFive = score!["EARLY_FIVE"] as? Int ?? 0
        self.myFriendData.score.firstLine = score!["FIRST_LINE"] as? Int ?? 0
        self.myFriendData.score.houseFull = score!["HOUSEFULL"] as? Int ?? 0
        self.myFriendData.score.luckSeven = score!["LUCKY_SEVEN"] as? Int ?? 0
        self.myFriendData.score.pyramid = score!["PYRAMID"] as? Int ?? 0
        self.myFriendData.score.secondLine = score!["SECOND_LINE"] as? Int ?? 0
        self.myFriendData.score.thirdLine = score!["THIRD_LINE"] as? Int ?? 0
        loadDetailViewData()
    }
}
// MARK: -  Extension For API Call's 
extension FriendsDetailViewController{
   //Refresh and Update User Data
    func getRefreshDetailDataAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.myDetails, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //Share URl on facebook and pop to root view
                let detailData = result as? [String: Any]
                self.updateData(data: detailData!)
                //Show Detail View
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getRefreshDetailDataAPI(parameters:parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
// MARK: -  API call for Report User As Spam 
    func reportUserAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.reportUserSpam, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //User Reported sucessfully
                EventManager.showAlertWithAction(alertMessage: ConstantModel.message.spamMessage, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.reportUserAPI(parameters:parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
// MARK: -  API call for Block/Unblock User 
    func blockUnblockUserAPI(parameters: [String: Any],isBlock: Bool){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.blockUnblockUser, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //User blocked/unblocked sucessfully perform changes
                let data = result as? [String: Any]
                let message =  isBlock ? ConstantModel.message.blockmessage : ConstantModel.message.unblockMessage
                self.blockcallback!(isBlock ? "block" : "unblocked")
                EventManager.showAlertWithAction(alertMessage: data!["message"] as? String ?? message, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: { _ in
                    if !isBlock{
                        //Set button title as unblock and back color green
                        self.myFriendData.user_blocked = "unblocked"
                        self.btnReport.isHidden = false
                        self.btnReport.backgroundColor = UIColor.green
                        self.btnReport.setTitle("  Unblock  ", for: .normal)
                        self.btnReport.tag = 1003
                    } else {
                        //Set button title as block and back color red
                        self.btnReport.isHidden = false
                        self.btnReport.backgroundColor = UIColor.red
                        self.btnReport.setTitle("  Block  ", for: .normal)
                        self.btnReport.tag = 1002
                        self.myFriendData.user_blocked = "blocked"
                    }
                })
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.blockUnblockUserAPI(parameters:parameters, isBlock: isBlock)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
// MARK: -   Share Result on Fb and Upload Image on server API 
    func shareUserDetailOnFBAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.shareResult, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //Share URl on facebook and pop to root view
                let data = result as? [String: Any]
                let resultURL = data!["url"] as? String
                FacebookShareModel.shared.shareImageOnFb(imageURL: resultURL!, delegate: self)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.shareUserDetailOnFBAPI(parameters:parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
}
