//
//  CreateGameViewController.swift
//  Tambola Bingo
//
//  Created by signity on 05/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import VerticalSteppedSlider
class CreateGameViewController: UIViewController {
    @IBOutlet weak var lblGameDate: UILabel!
    @IBOutlet weak var lblGameTime: UILabel!
    @IBOutlet weak var txtGameName: UITextField!
    @IBOutlet weak var btnGameDate: UIButton!
    @IBOutlet weak var btnGameTime: UIButton!
    @IBOutlet weak var btnCreateGame: UIButton!
    @IBOutlet weak var DatePickerView:UIDatePicker!
    @IBOutlet weak var toolBar:UIToolbar!
    
    //Invite View and controles
    @IBOutlet weak var inviteFriendsView: UIView!
    @IBOutlet weak var btnfbInvite: UIButton!
    @IBOutlet weak var btnShareKey: UIButton!
    @IBOutlet weak var btnInviteRecent: UIButton!
    @IBOutlet weak var btnInviteGroup: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    //Creat new game with two options controls
    @IBOutlet weak var gameTypeScrollView: UIScrollView!
    @IBOutlet weak var gameTypeOptionView: UIView!
    @IBOutlet weak var btnUserCoins: UIButton!
    @IBOutlet weak var btnGameCreateCoins: UIButton!
    @IBOutlet weak var btnGamePlayersCount: UIButton!
    @IBOutlet weak var userSliderView:VSSlider!
    @IBOutlet weak var userCountStack: UIStackView!
    @IBOutlet weak var lblUserMinCount: UILabel!
    @IBOutlet weak var lblUserMaxCount: UILabel!
    @IBOutlet weak var btnOptionOne: UIButton!
    @IBOutlet weak var btnOptionTwo: UIButton!
    @IBOutlet weak var btnOrganiserGame: UIButton!
    @IBOutlet weak var btnNormalGame: UIButton!
    @IBOutlet weak var btnAutocutCoins: UIButton!
    @IBOutlet weak var btnOrganiserCoins: UIButton!
    @IBOutlet weak var btnPlayersCoin: UIButton!
    @IBOutlet weak var optionOneHeightConst: NSLayoutConstraint!
    @IBOutlet weak var optionTwoHeightConst: NSLayoutConstraint!
    @IBOutlet weak var optionViewHeightConst: NSLayoutConstraint!
    
    var selectedDate = ""
    var selectedTime = ""
    var selectedOption = "2"
    var selectedUsersCount = "0"
    var isDate = false
    var myGroup = MyGroups()
    var allGames = [CreateGame]()
    var isFromGroup = false
    var game_id = ""
    var game_key = ""
    var game_url = ""
    var pvtgamePrices:pvtgamePriceModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DatePickerView.backgroundColor = UIColor.gray
        self.DatePickerView.isHidden = true
        self.toolBar.isHidden = true
        self.btnfbInvite.isHidden = true
        self.showHideInviteView(false)
        self.fillUIData()
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.createPrivateScreen)

    }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func createButtonAction(){
        if self.validateFields() {
            let DateTime = self.lblGameDate.text! + " " + self.lblGameTime.text! + ":00"
            self.createNewGameAPI(parameters: GetParamModel.shared.getCreateGameParam(gameName: txtGameName.text!, staretTime: DateTime, gametype: selectedOption, playersCount: selectedUsersCount))
        }
    }
    @IBAction func selectDateTimeButtonAction(sender: UIButton){
        self.view.endEditing(true)
        if sender.tag == 1001{
            self.isDate = true
            self.DatePickerView.datePickerMode = UIDatePicker.Mode.date
            self.DatePickerView.minimumDate = Date()
            self.DatePickerView.isHidden = false
            self.toolBar.isHidden = false
        } else {
            if selectedDate == ""{
                EventManager.showAlertWithAction(alertMessage: ConstantModel.message.SelectDate, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }else {
                let currentDate = DateConvertor.convertToString(slotDate: Date(), dateFormat: DateFormats.ServerDateTypeFormat)
                self.DatePickerView.minimumDate = selectedDate == currentDate ? Date() : nil
                self.DatePickerView.datePickerMode = UIDatePicker.Mode.time
                self.DatePickerView.isHidden = false
                self.toolBar.isHidden = false
            }
        }
    }
    @IBAction func dueDateChanged(sender:UIDatePicker){
        if isDate {
        self.selectedDate = DateConvertor.convertIntoString(slotDate: sender.date, dateFormat: DateFormats.ServerDateTypeFormat)
        } else {
         self.selectedTime = DateConvertor.convertIntoString(slotDate: sender.date, dateFormat: DateFormats.GameTimeFormat)
        }
    }
    @IBAction func doneButtonAction(_ sender: UIBarButtonItem) {
        if isDate {
            let date = DateConvertor.convertIntoString(slotDate: Date(), dateFormat: DateFormats.ServerDateTypeFormat)
            self.selectedDate = self.selectedDate != "" ? self.selectedDate : date
            self.lblGameDate.text = self.selectedDate
        } else {
            let time = DateConvertor.convertIntoString(slotDate: Date(), dateFormat: DateFormats.GameTimeFormat)
            self.selectedTime = self.selectedTime != "" ? self.selectedTime : time
            self.lblGameTime.text = self.selectedTime
        }
        self.isDate = false
        self.toolBar.isHidden = true
        self.DatePickerView.isHidden = true
    }
    @IBAction func cancelButtonAction(_ sender: UIBarButtonItem) {
        self.isDate = false
        self.toolBar.isHidden = true
        self.DatePickerView.isHidden = true
    }
    @IBAction func inviteButtonsActions(_ sender: UIButton){
        switch sender.tag {
        case 0:
            //fb invite friends by fb
            NavigationManager.navigateToFBFriendInvite(self,gameId: game_id)
            break
        case 1:
            //recent Share
            NavigationManager.navigateToAddInviteGroup(self, isInvite: true, isgroup: false, userId: "", gameId: game_id, userName: "")
            break
        case 2:
            //my group Share
            let groupsList = MyGroupsDataModel.shared.getGroupsFromDB()!
            groupsList.count == 0 ? EventManager.showAlertWithAction(alertMessage: ConstantModel.message.NoGroup, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil) : NavigationManager.navigateToAddInviteGroup(self, isInvite: true, isgroup: true, userId: "", gameId: game_id, userName: "")
            break
        case 3:
            //key Share
            ShareLinksManager.shared.showShareOptions(game_url, controller: self)
            break
        default:
            break
        }
    }
    func showHideInviteView(_ showInvite: Bool){
        if showInvite{
            //Show Invite View and Disable All Fields
            self.btnCreateGame.isHidden = true
            self.gameTypeOptionView.isHidden = true
            self.inviteFriendsView.isHidden = false
            self.txtGameName.isEnabled = false
            self.btnGameDate.isEnabled = false
            self.btnGameTime.isEnabled = false
            self.gameTypeScrollView.contentSize.height = 200
        } else {
            //Hide Invite View and Enable All Fields
            self.btnCreateGame.isHidden = false
            self.gameTypeOptionView.isHidden = false
            self.inviteFriendsView.isHidden = true
            self.txtGameName.isEnabled = true
            self.btnGameDate.isEnabled = true
            self.btnGameTime.isEnabled = true
        }
    }
}
// MARK: -  Extension For API Call's 
extension CreateGameViewController{
    // MARK: -  GET Game List data API 
    func createNewGameAPI(parameters: [String: Any]?){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.createNewGame, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //  fetch Games Sucessfully
                let data = result as? [String: Any]
                self.addToCalendar(data!)
                self.game_id = (data!["game_id"] as? String)!
                self.game_key = (data!["game_key"] as? String)!
                self.game_url = (data!["game_key_url"] as? String)!
                let message = (data!["message"] as? String)!
                EventManager.showAlertWithAction(alertMessage: message, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                self.isFromGroup ? self.inviteGroupMembers(): BaseAPIHandler.shared.getUserCoinsAPI(delegate: self) { isCallSucess in
                    //Show Invite View
                    self.showHideInviteView(true)
                }
               
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.createNewGameAPI(parameters: parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
// MARK: -  Invite Group Members (Facebook Friends) For My Game 
    func addToCalendar(_ response:[String: Any]){
        CalendarManager.addEventToCalender(gameScheduleData: response) { isSuccess in
            if !isSuccess {
                //If error while adding to callender
            }
        }
    }
// MARK: -  Invite Group Members (Facebook Friends) For My Game 
    func inviteGroupMembers(){
        var inviteID = [String]()
        var inviteNames = [String]()
        for member in self.myGroup.members as! [[String: Any]]{
            inviteID.append((member["fb_id"] as? String)!)
            inviteNames.append((member["username"] as? String)!)
        }
        let parameters = GetParamModel.shared.getInviteFriendsParam(self.game_id, invitee_id: inviteID.joined(separator: ","), invitee_name:inviteNames.joined(separator: ","))
        BaseAPIHandler.shared.inviteFBFriendsAPI(parameters!, self) { isCallSucess in
            if isCallSucess {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
// MARK: -  Extension For TextField Delegate 
extension CreateGameViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 30
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
extension CreateGameViewController {
    func fillUIData(){
        self.optionOneHeightConst.constant = 335
        self.optionTwoHeightConst.constant = 60
        self.optionViewHeightConst.constant = 15 + (self.optionOneHeightConst.constant + self.optionTwoHeightConst.constant)
        pvtgamePrices = UserDefaultsHandler.gameCreatePriceInfo
        btnUserCoins.setTitle(UserDefaultsHandler.user?.userCoins, for: .normal)
        btnUserCoins.layer.borderWidth = 2
        btnUserCoins.layer.borderColor = UIColor.red.cgColor
        lblUserMinCount.text = pvtgamePrices.gameMinPlayer
        lblUserMaxCount.text = pvtgamePrices.gameMaxPlayer
        userSliderView.maximumValue = Float(pvtgamePrices.gameMaxPlayer)!
        userSliderView.minimumValue = Float(pvtgamePrices.gameMinPlayer)!
        userSliderView.value = Float(pvtgamePrices.gameMinPlayer)!
        userSliderView.increment = 1.0
        selectedUsersCount = pvtgamePrices.gameMinPlayer
        btnGameCreateCoins.setTitle(getGameCostPerPlayer(), for: .normal)
        btnGameCreateCoins.layer.borderWidth = 2
        btnGameCreateCoins.layer.borderColor = UIColor.red.cgColor
        btnGamePlayersCount.setTitle(selectedUsersCount, for: .normal)
        btnGamePlayersCount.layer.borderWidth = 2
        btnGamePlayersCount.layer.borderColor = UIColor.red.cgColor
        btnOptionOne.setImage(UIImage(named: "check"), for: .normal)
        btnOptionTwo.setImage(UIImage(named: "uncheck"), for: .normal)
        userSliderView.isEnabled = true
        self.btnOrganiserGame.tag = 1
        let autcutPrice = String(UserDefaultsHandler.gamePriceInfo?.autoCut ?? 0) + " Coins"
        let organiserPrice = String(UserDefaultsHandler.gamePriceInfo?.gameCreateLowLimt ?? 0) + " Coins"
        let plyerPrice = String(UserDefaultsHandler.gamePriceInfo?.gameAccept ?? 0) + " Coins"
        btnAutocutCoins.setTitle(autcutPrice, for: .normal)
        btnOrganiserCoins.setTitle(organiserPrice, for: .normal)
        btnPlayersCoin.setTitle(plyerPrice, for: .normal)
    }
    @IBAction func sliderValueChange(){
        selectedUsersCount = String(userSliderView.value).replacingOccurrences(of: ".0", with: "")
        btnGameCreateCoins.setTitle(getGameCostPerPlayer(), for: .normal)
        btnGamePlayersCount.setTitle(selectedUsersCount, for: .normal)
    }
    func getGameCostPerPlayer() -> String{
        let playerCost = Int(pvtgamePrices.gamePlayerCost) ?? 0
        let gameCost = Int(pvtgamePrices.gameCreateCost) ?? 0
        let playerCount = Int(userSliderView.value)
        let totalCost = gameCost + (playerCost*playerCount)
        return String(totalCost)
    }
    //325 200
    @IBAction func opencloseOptionOne(_ sender: UIButton){
        //Option One selected and Open Close
        if self.btnOrganiserGame.tag == 0 {
            self.btnOrganiserGame.tag = 1
            self.optionOneHeightConst.constant = 335
            btnOrganiserGame.setImage(UIImage(named: "upArrow"), for: .normal)
        } else {
            self.btnOrganiserGame.tag = 0
            self.optionOneHeightConst.constant = 70
            btnOrganiserGame.setImage(UIImage(named: "downArrow"), for: .normal)
        }
        self.btnNormalGame.tag = 0
        self.optionTwoHeightConst.constant = 60
        self.optionViewHeightConst.constant = 15 + (self.optionOneHeightConst.constant + self.optionTwoHeightConst.constant)
        selectedOption = "2"
        btnOptionOne.setImage(UIImage(named: "check"), for: .normal)
        btnOptionTwo.setImage(UIImage(named: "uncheck"), for: .normal)
        userSliderView.isEnabled = true
        btnNormalGame.setImage(UIImage(named: "downArrow"), for: .normal)
        
    }
    @IBAction func opencloseOptionTwo(_ sender: UIButton){
        //Option two selected and Open close
        if self.btnNormalGame.tag == 0 {
            self.btnNormalGame.tag = 1
            self.optionTwoHeightConst.constant = 200
            btnNormalGame.setImage(UIImage(named: "upArrow"), for: .normal)
        } else {
            self.btnNormalGame.tag = 0
            self.optionTwoHeightConst.constant = 60
            btnNormalGame.setImage(UIImage(named: "downArrow"), for: .normal)
        }
        self.btnOrganiserGame.tag = 0
        self.optionOneHeightConst.constant = 70
        self.optionViewHeightConst.constant = 15 + (self.optionOneHeightConst.constant + self.optionTwoHeightConst.constant)
        selectedOption = "1"
        btnOptionOne.setImage(UIImage(named: "uncheck"), for: .normal)
        btnOptionTwo.setImage(UIImage(named: "check"), for: .normal)
        userSliderView.isEnabled = false
        btnOrganiserGame.setImage(UIImage(named: "downArrow"), for: .normal)
        
    }
}
