//
//  PrivateGameListViewController.swift
//  Tambola Bingo
//
//  Created by signity on 05/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit

class PrivateGameListViewController: UIViewController  {
    
    @IBOutlet weak var myGamesTableView: UITableView!
    @IBOutlet weak var lblCoins: UILabel!
    @IBOutlet weak var btnCreateGame: UIButton!
    //Create New Game
    @IBOutlet weak var createNewView: UIView!
    @IBOutlet weak var createNewGameBtn: UIButton!
    @IBOutlet weak var enterKeyBtn: UIButton!
    //Enter Key Invite
    @IBOutlet weak var enterInviteKeyView: UIView!
    @IBOutlet weak var submitKeyBtn: UIButton!
    @IBOutlet weak var txtEnterKey: UITextField!
    
    @IBOutlet weak var lblNoRecord: UILabel!
    
    var privateGamesArray = [GameListDataModel]()
    let dispatchGroup = DispatchGroup()
    var actionSheetController: UIAlertController!
    var gamekey = ""
    var globalTimer: Timer?
    var serverTime = Date()
    var minute = 0
    
    var isdeepLink = false
    private let refreshControl = UIRefreshControl()
    var myAdsView = ShowiAdsView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myAdsView.senderController = self
        self.lblNoRecord.isHidden = true
        self.setRefreshController()
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.privateGameListScreen)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.myAdsView.loadInterstitialAds()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.setUserCoin()
        self.dispatchGroup.enter()
        self.getMyGameList(parameters: GetParamModel.shared.getPrivateGameListParam(), showloader: false)
    }
    func setUserCoin(){
        let user = UserDefaultsHandler.user
        self.lblCoins.text = user != nil ? user?.userCoins : "0000"
        gamekey != "" ? self.inviteViaKeyAPI(parameters: GetParamModel.shared.getInviteKeyGameParam(gamekey)): nil
    }
    func addToCalendar(_ response:[String: Any]){
        CalendarManager.addEventToCalender(gameScheduleData: response) { isSuccess in
            if !isSuccess {
                //If error while adding to callender
            }
        }
    }
    func setRefreshController() {
        myGamesTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshListData(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle = NSAttributedString(string: ConstantModel.message.fetchList,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    @objc private func refreshListData(_ sender: Any) {
        //        Private Game List data
        self.myGamesTableView.isUserInteractionEnabled = false
        self.dispatchGroup.enter()
        self.getMyGameList(parameters: GetParamModel.shared.getPrivateGameListParam(), showloader: true)
    }
     // MARK: -  Global Timer Start/Update
        func startGlobalTimer() {
            self.invalidateTimer()
            globalTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        }
        func invalidateTimer(){
            if self.globalTimer != nil && (self.globalTimer?.isValid)! {
                self.globalTimer?.invalidate()
                self.globalTimer = nil
            }
        }
        @objc func updateTimer() {
            let currentTime = Calendar.current.date(byAdding: .second, value: 1, to: self.serverTime)!
            self.serverTime = currentTime
            minute = minute + 1
            if minute == 60 {
    //            print("Reload table after one minute")
                minute = 0
                self.myGamesTableView.reloadData()
            }
        }
    
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        if isdeepLink {
            NavigationManager.checkWhereToNav()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func createButtonAction(){
        self.createNewView.isHidden = false
    }
    @IBAction func closeCreateViewButtonAction(){
        self.createNewView.isHidden = true
    }
    @IBAction func createGameButtonAction(){
        self.createNewView.isHidden = true
        self.performSegue(withIdentifier: "CreateNewGameView", sender: true)
    }
    @IBAction func enterKeyButtonAction(){
        self.createNewView.isHidden = true
        self.enterInviteKeyView.isHidden = false
        
    }
    @IBAction func closeEnterKeyViewButtonAction(){
        self.enterInviteKeyView.isHidden = true
    }
    @IBAction func submitKeyButtonAction(){
        //Call API
        let key = txtEnterKey.text
        self.validateField() ? self.inviteViaKeyAPI(parameters: GetParamModel.shared.getInviteKeyGameParam(key!)) : nil
        
    }
    @IBAction func playButtonAction(sender:UIButton){
        self.showAddMethod(index: sender.tag)
    }
    
    @IBAction func acceptRejectButtonAction(sender:UIButton){
        let gameId = self.privateGamesArray[sender.tag].game_id
        if sender.accessibilityLabel == "Accept" {
            self.requestAcceptRejectAPI(parameters: GetParamModel.shared.getAcceptRejectGameParam(gameId: gameId, status: "1"), status: "Accept")
        } else {
            self.requestAcceptRejectAPI(parameters: GetParamModel.shared.getAcceptRejectGameParam(gameId: gameId, status: "2"), status: "Reject")
        }
    }
    @IBAction func inviteButtonAction(sender:UIButton){
        let gameId = self.privateGamesArray[sender.tag].game_id
        let sharekey = self.privateGamesArray[sender.tag].game_key_url
        self.actionSheetController = UIAlertController(title: ConstantModel.btnText.btnSelect, message: "", preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnCancel, style: .cancel, handler: nil))
//        actionSheetController.addAction(UIAlertAction.init(title: "Facebook Friends", style: .default, handler: { _ in
//            //fb invite friends by fb
//            NavigationManager.navigateToFBFriendInvite(self,gameId: gameId)
//            }))
        actionSheetController.addAction(UIAlertAction.init(title: "Recent", style: .default, handler: { _ in
            NavigationManager.navigateToAddInviteGroup(self, isInvite: true, isgroup: false, userId: "", gameId: gameId, userName: "")
            }))
        actionSheetController.addAction(UIAlertAction.init(title: "My Groups", style: .default, handler: { _ in
            NavigationManager.navigateToAddInviteGroup(self, isInvite: true, isgroup: true, userId: "", gameId: gameId, userName: "")
            }))
        actionSheetController.addAction(UIAlertAction.init(title: "Share key", style: .default, handler: { _ in
            self.actionSheetController.dismiss(animated: true, completion: nil)
            ShareLinksManager.shared.showShareOptions(sharekey, controller: self)
            }))
        self.present(actionSheetController, animated: true, completion: nil)
    }
}
// MARK: -  UITableView Data Source and Delegate 
extension PrivateGameListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.privateGamesArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myGamesCell", for: indexPath) as? myGamesCell
        if indexPath.row % 2 == 0 {
            cell?.backgroundColor = UIColor.clear
        }else {
            cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
        cell?.gameTitleText.text = self.privateGamesArray[indexPath.row].game_name
        let start_Time = self.privateGamesArray[indexPath.row].start_time
        cell?.gameDateText.text = DateConvertor.getLocalFormateDateTime(gameDateTime: start_Time, startSpace: "")
        
        //Hide all Buttons first
        cell?.acceptbutton.isHidden = true
        cell?.rejectbutton.isHidden = true
        cell?.playbutton.isHidden = true
        cell?.invitebutton.isHidden = true
        cell?.lblViewHistory.isHidden = true
        //set indexpath.row to all button tag
        cell?.acceptbutton.tag = indexPath.row
        cell?.acceptbutton.accessibilityLabel = "Accept"
        cell?.rejectbutton.tag = indexPath.row
        cell?.rejectbutton.accessibilityLabel = "Reject"
        cell?.playbutton.tag = indexPath.row
        cell?.invitebutton.tag = indexPath.row
        
        let isMygame = self.privateGamesArray[indexPath.row].isMyGames
        let startDateTime = DateConvertor.convertToDate(slotDate: start_Time, dateFormat: DateFormats.ServerDateFormat)
        let date = DateConvertor.convertIntoString(slotDate: self.serverTime, dateFormat: DateFormats.ServerDateFormat)
        let currentDateTime = DateConvertor.convertToDate(slotDate: date, dateFormat: DateFormats.ServerDateFormat)
        let calendar = Calendar.current
        let gameEndTime = calendar.date(byAdding: .minute, value: 60, to: startDateTime)
        
//        let timeDiff = EventManager.getTimeDifferenceValue(serverTime: startDateTime, startTime: currentDateTime )
        if isMygame.boolValue {
            if currentDateTime >= gameEndTime! {
                cell?.lblViewHistory.text = "Game Expired"
                cell?.lblViewHistory.isHidden = false
            } else {
                if self.privateGamesArray[indexPath.row].user_type == "admin" {
                    cell?.gameImage.image = UIImage(named: "PriceCrown")
                    currentDateTime >= startDateTime ? (cell?.playbutton.isHidden = false) : (cell?.invitebutton.isHidden = false)
                } else {
                    if self.privateGamesArray[indexPath.row].participant_status == "0" {
                        cell?.acceptbutton.isHidden = false
                        cell?.rejectbutton.isHidden = false
                    } else {
                        self.privateGamesArray[indexPath.row].game_status == "started" ? (cell?.playbutton.isHidden = false) : (cell?.invitebutton.isHidden = false)
                        if self.privateGamesArray[indexPath.row].game_status
                            != "started" {
                            self.observeChannels(indexPath.row)
                        }
                    }
                }
            }
        } else {
            //Show Only History View
            cell?.lblViewHistory.isHidden = false
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let isMygame = self.privateGamesArray[indexPath.row].isMyGames
        let gameId   = self.privateGamesArray[indexPath.row].game_id
        if isMygame.boolValue {
                if self.privateGamesArray[indexPath.row].participant_status == "0" {
//                   "You need to participate in game to view the details."
                    EventManager.showAlertWithAction(alertMessage: ConstantModel.message.NotAParticipate, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                } else {
                    //Call playingGameStatus
                    //Navigate to Game Detail
                    let userType = self.privateGamesArray[indexPath.row].user_type
                    NavigationManager.navigateToGameDetail(self, gameId: gameId, userType: userType, isPublic: false)
                }
        } else {
            //Call Game HIstory API
            self.getGameHistoryAPI(parameters: GetParamModel.shared.getGameHistoryParam(gameId: gameId), index: indexPath.row)
            
        }
    }
}
// MARK: -  Extension For API Call's 
extension PrivateGameListViewController{
    // MARK: -  GET Game List data API 
    func getMyGameList(parameters: [String: Any]?, showloader: Bool){
        ApiCallModel.shared.apiCall(isSilent: showloader, url: UserDefaultsHandler.projectBaseUrl + APIConstants.MyGamesList, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            let server_Time = response?["response_time"] as? String
            if server_Time != nil || server_Time != ""{
                let serverDateTime = DateConvertor.convertToDate(slotDate: server_Time!
                    , dateFormat: DateFormats.ServerDateFormat)
                self.serverTime = serverDateTime
            }
            APIResponseModel.shared.parseMyGameListResponse(response: response, sender: self, callback: { result in
                //  fetch Games Sucessfully
                self.lblNoRecord.isHidden = true
                self.dispatchGroup.leave()
                self.privateGamesArray = (result as? [GameListDataModel])!
                if self.privateGamesArray.count > 0{
                    self.startGlobalTimer()
                }
                self.myGamesTableView.reloadData()
                self.refreshControl.endRefreshing()
                self.myGamesTableView.isUserInteractionEnabled = true
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getMyGameList(parameters: parameters, showloader: showloader)
                    }
                })
            } else if failureResponse == ConstantModel.message.noRecord {
                self.dispatchGroup.leave()
                self.myGamesTableView.isUserInteractionEnabled = true
                self.refreshControl.endRefreshing()
                self.lblNoRecord.isHidden = false
            } else {
                self.myGamesTableView.isUserInteractionEnabled = true
                let failerMessage = (failureResponse ?? ConstantModel.api.ServiceFailure) + " " + ConstantModel.message.reloadList
                EventManager.showAlertWithAction(alertMessage: failerMessage, btn1Tit: ConstantModel.btnText.btnContinue, btn2Tit: ConstantModel.btnText.btnCancel, sender: self, action:{ action in
                    action == ConstantModel.btnText.btnContinue ? (self.getMyGameList(parameters: parameters, showloader: showloader)) : (_ = self.navigationController?.popViewController(animated: true))
                })
            }
        } )
    }
    // MARK: -  GET Game History data API 
    func getGameHistoryAPI(parameters: [String: Any]?, index: Int){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.GamesHistory, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseGameHistoryResponse(response: response, sender: self, callback: { result in
                //Show Only History View i.e Game Result View
                let resultData = (result as? [ResultDataModel])!
                let gameName = self.privateGamesArray[index].game_name
                let gameId = self.privateGamesArray[index].game_id
                NavigationManager.navigateToGameResult(self, resultData: resultData, isBoogied: false, earnPoints: "No", gameName: gameName, gameId: gameId, isfromePlayGame: false)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getGameHistoryAPI(parameters: parameters, index: index)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    // MARK: -  Show Ads Banner full screen 
    func showAddMethod(index: Int){
        let gameId = self.privateGamesArray[index].game_id
        let startTime = self.privateGamesArray[index].start_time
        let gameTitle = self.privateGamesArray[index].game_name
        
        if UserDefaultsHandler.isHideAds {
            //Admin
            if self.privateGamesArray[index].user_type == "admin" {
                self.playGameAPI(parameters: GetParamModel.shared.getPlayGameParam(gameId: gameId, startTime: startTime), gameTypeURL: APIConstants.StartOrganizerGame, gameName: gameTitle)
            } else {
            //Invitee
                self.playGameAPI(parameters: GetParamModel.shared.getPlayPublicGameParam(gameId: gameId), gameTypeURL: APIConstants.StartPublicGame, gameName: gameTitle)
            }
        } else {
            //Show Banner Google Ads
             self.myAdsView.AdsAction = { isClose in
                 if isClose {
                     //Navigate to next view to display
                     //Admin
                     if self.privateGamesArray[index].user_type == "admin" {
                         self.playGameAPI(parameters: GetParamModel.shared.getPlayGameParam(gameId: gameId, startTime: startTime), gameTypeURL: APIConstants.StartOrganizerGame, gameName: gameTitle)
                     } else {
                     //Invitee
                         self.playGameAPI(parameters: GetParamModel.shared.getPlayPublicGameParam(gameId: gameId), gameTypeURL: APIConstants.StartPublicGame, gameName: gameTitle)
                     }
                 }
             }
            self.myAdsView.showAds()
        }
        
    }
    // MARK: -  Play My Game API 
    func playGameAPI(parameters: [String: Any]?, gameTypeURL: String, gameName: String){
        self.myAdsView.loadInterstitialAds()
        let baseUrl = UserDefaultsHandler.projectBaseUrl + gameTypeURL
        ApiCallModel.shared.apiCall(isSilent: false, url: baseUrl, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parsePlayPublicGameResponse(response: response, isPublic: false, sender: self, callback: { result in
                //  Start Games Sucessfully show alret and update status "JOINED" and refresh cell
                let gameDetails = result as? PlayGameDataModel
                gameDetails?.gameName = gameName
                gameDetails?.gameId = parameters!["game_id"] as! String
                NavigationManager.navigateToPlayGame(delegate: self, gameData: gameDetails!)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.playGameAPI(parameters: parameters, gameTypeURL: gameTypeURL, gameName: gameName)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    // MARK: -  Play My Game API 
    func requestAcceptRejectAPI(parameters: [String: Any]?, status: String){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.AcceptRejectGame, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //  Call Get Coin API and Add to Calender event
                
                self.dispatchGroup.enter()
                self.getMyGameList(parameters: GetParamModel.shared.getPrivateGameListParam(), showloader: false)
                
                self.dispatchGroup.enter()
                BaseAPIHandler.shared.getUserCoinsAPI(delegate: self) { isCallSucess in
                    self.dispatchGroup.leave()
                }
                self.dispatchGroup.notify(queue: .main) {
                    self.setUserCoin()
                    status == "Accept" ? self.addToCalendar(result as! [String : Any]) : nil
                    print("Both functions complete 👍")
                }
                
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.requestAcceptRejectAPI(parameters: parameters, status: status)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
        
    }
    
    // MARK: -  GET Game List data API 
    func inviteViaKeyAPI(parameters: [String: Any]?){
        if self.gamekey != ""{
            self.gamekey = ""
            self.isdeepLink = true
        }
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.inviteViaKey, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //  fetch Games Sucessfully
                self.enterInviteKeyView.isHidden = true
                self.dispatchGroup.enter()
                self.getMyGameList(parameters: GetParamModel.shared.getPrivateGameListParam(), showloader: false)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.inviteViaKeyAPI(parameters: parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
}
