//
//  LoginViewViewController.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 21/04/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField


class LoginViewViewController: UIViewController {
    @IBOutlet var txtUserName                : SkyFloatingLabelTextField!
    @IBOutlet var txtPassword                : SkyFloatingLabelTextField!
    @IBOutlet var loginBtn  : UIButton!
    @IBOutlet var registerBtn  : UIButton!
    
    var loginType = "email" //mobile
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        txtUserName.text = ""
        txtPassword.text = ""
    }
    func setupUI(){
        txtUserName.inputAccessoryView = addDoneButtonOnKeyboard()
        txtPassword.inputAccessoryView = addDoneButtonOnKeyboard()
        txtUserName.delegate = self
        txtPassword.delegate = self
        if loginType == "email"{
            txtUserName.placeholder = "Enter Email"
            txtUserName.title   = "Email"
            txtUserName.selectedTitle = "Email"
        } else {
            txtUserName.keyboardType = .numberPad
            txtUserName.placeholder = "Enter Phone Number"
            txtUserName.title   = "Phone Number"
            txtUserName.selectedTitle   = "Phone Number"
        }
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.loginScreen)

        //New User? Register here.
        let message = registerBtn.titleLabel?.text
        let rangeStr = (message! as NSString).range(of: "Register")
        let rangeLeft = (message! as NSString).range(of: "New User?")
        let rangeLast = (message! as NSString).range(of: "here.")
        let attributedString = NSMutableAttributedString(string:message!)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: ConstantModel.appColor.brownColor , range: rangeLeft)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: ConstantModel.appColor.brownColor , range: rangeLast)
        attributedString.addAttributes([.foregroundColor: UIColor.white, .underlineStyle: NSUnderlineStyle.single.rawValue], range: rangeStr)
        self.registerBtn.setAttributedTitle(attributedString, for: .normal)
    }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func forgotPassAction(){
//        self.performSegue(withIdentifier: "ForgotPassScreen", sender: true)
        NavigationManager.navToForgotPassView(self, self.loginType)
    }
    @IBAction func registerAction(){
        NavigationManager.navToSignUpView(self, self.loginType)
    }
    @IBAction func logInAction(_ sender: UIButton){
            if self.validateLogin(){
                let params = GetParamModel.shared.getSignInParm(emailText: txtUserName.text!, password: txtPassword.text!, acc_type: loginType)
                self.loginUser(params as! [String : String])
            }
    }
    func loginUser(_ params: [String:String]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.Signin, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseLoginResponse(response: response, accType: self.loginType, isSocial: false, sender: self, callback: { result in
                        // Logged in User Sucessfully
                        
                        UserDefaultsHandler.pageToShow = ConstantModel.pageName.DashBoardScreen
                        NavigationManager.checkWhereToNav()
                    })
                }, failure: { failureResponse in
                    if failureResponse == ConstantModel.message.tokenError {
                        //Call token API and recall API called
                        BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                            if isCallSucess{
                                self.loginUser(params)
                            }
                        })
                    } else {
                        EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                    }
                } )

    }
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signUpScreen" {
            let nextVC          = segue.destination as? RegisterViewController
            nextVC?.loginType   = self.loginType
        } else if segue.identifier == "ForgotPassScreen" {
            let nextVC          = segue.destination as? ForgotPasswordViewController
            nextVC?.loginType   = self.loginType
        }
    }
}
// MARK:- Text field extension 
extension LoginViewViewController : UITextFieldDelegate{
    
     // MARK:- Text field should return  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtUserName{
            txtPassword.becomeFirstResponder()
        } else if textField == txtPassword{
            txtPassword.resignFirstResponder()
        }
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:true)
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:false)
    }
}
