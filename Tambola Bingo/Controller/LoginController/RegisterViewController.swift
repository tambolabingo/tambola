//
//  RegisterViewController.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 21/04/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class RegisterViewController: UIViewController {

    @IBOutlet var txtFirstName               : SkyFloatingLabelTextField!
    @IBOutlet var txtLastName                : SkyFloatingLabelTextField!
    @IBOutlet var txtEmailPhone              : SkyFloatingLabelTextField!
    @IBOutlet var txtPassword                : SkyFloatingLabelTextField!
    @IBOutlet var registerBtn  : UIButton!
    @IBOutlet var agreePolicyBtn  : UIButton!
    
    var loginType = "email"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
           self.view.endEditing(true)
       }
    func setupUI(){
            txtFirstName.inputAccessoryView = addDoneButtonOnKeyboard()
            txtLastName.inputAccessoryView = addDoneButtonOnKeyboard()
            txtEmailPhone.inputAccessoryView = addDoneButtonOnKeyboard()
            txtPassword.inputAccessoryView = addDoneButtonOnKeyboard()
        
            txtFirstName.delegate = self
            txtLastName.delegate = self
            txtEmailPhone.delegate = self
            txtPassword.delegate = self
        
        agreePolicyBtn.setImage(UIImage(named: "checkbox"), for: .normal)
        if loginType == "email"{
            txtEmailPhone.placeholder = "Enter Email"
            txtEmailPhone.title   = "Email"
            txtEmailPhone.selectedTitle = "Email"
        } else {
            txtEmailPhone.keyboardType = .numberPad
            txtEmailPhone.placeholder = "Enter Phone Number"
            txtEmailPhone.title   = "Phone Number"
            txtEmailPhone.selectedTitle   = "Phone Number"
            EventManager.showAlertWithAction(alertMessage: ConstantModel.api.MobileRegistration, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        }
         ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.signUpScreen)
       }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func policyAction(){
        //Navigate To Email signin Screen
        NavigationManager.navigateToPrivacyPolicy(self)
    }
    @IBAction func agreePrivcyPolicyAction(_ sender: UIButton){
        if sender.tag == 0 {
            sender.tag = 1
            agreePolicyBtn.setImage(UIImage(named: "checkboxSelected"), for: .normal)
        } else {
            sender.tag = 0
            agreePolicyBtn.setImage(UIImage(named: "checkbox"), for: .normal)
        }
    }
    @IBAction func registerAction(_ sender: UIButton){
            if self.validateRegister(){
                let params = GetParamModel.shared.getSignUpParam(fNameText: txtFirstName.text!, lNameText: txtLastName.text!, emailText: txtEmailPhone.text!, password: txtPassword.text!, acc_type: loginType, code: "91")
                self.registerUser(params as! [String : String])
            }
    }
    func registerUser(_ params: [String:String]){
//        let phone = "7835974622"
//        NavigationManager.navToOTPView(self, phone)
//        return
        let urlLink = loginType == "email" ? APIConstants.SignUp : APIConstants.MobileSignUp
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + urlLink, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            if response!["success"] != nil && (response!["success"] as? Bool)!{
                        APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                            // Logged in User Sucessfullyx
                            print(result as Any)
                            let data = result as? [String: Any]
                            let message = data!["message"] as? String
                            EventManager.showAlertWithAction(alertMessage: message, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: { _ in
                                if self.loginType != "email" {
                                    //Navigate to OTP screen  OTPScreen
//                                    self.performSegue(withIdentifier: "OTPScreen", sender: true)
                                    let phone = self.txtEmailPhone.text!
                                    NavigationManager.navToOTPView(self, phone)
                                } else {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            })
                        })
            } else {
                 let message = response!["message"] as? String
                EventManager.showAlertWithAction(alertMessage: message ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
                }, failure: { failureResponse in
                    if failureResponse == ConstantModel.message.tokenError {
                        //Call token API and recall API called
                        BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                            if isCallSucess{
                                self.registerUser(params)
                            }
                        })
                    } else {
                        EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                    }
                } )

    }
    // MARK: -  Navigation 
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "OTPScreen" {
               let nextVC          = segue.destination as? OTPViewController
                nextVC?.phoneNumber   = self.txtEmailPhone.text!
           }
       }
}
// MARK:- Text field extension 
extension RegisterViewController : UITextFieldDelegate{
    
     // MARK:- Text field should return  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFirstName{
            txtLastName.becomeFirstResponder()
        } else if textField == txtLastName{
            txtEmailPhone.becomeFirstResponder()
        } else if textField == txtEmailPhone{
            txtPassword.becomeFirstResponder()
        } else if textField == txtPassword{
            txtPassword.resignFirstResponder()
        }
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:true)
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:false)
    }
}
