//
//  GuestLoginViewController.swift
//  Tambola Bingo
//
//  Created by signity on 22/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class GuestLoginViewController: UIViewController {
        @IBOutlet weak var nameTxtField: CustomAnimatableTextfield!
        @IBOutlet weak var userImage: UIImageView!
        var userProfileImage: UIImage?
        var profileImageURl = ""
        let imagePicker = UIImagePickerController()
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        userProfileImage = UIImage(named:"userImg")
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectProfileImageAction(_ sender: UIButton) {
        GetCameraPhotoLibModel.shared.getCameraPhotos(picker: imagePicker, sender: self)
        selectedImageHandler = { Image in
            self.userProfileImage = Image
            self.userImage.image = Image
            self.imagePicker.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func logInAsGuest(_ sender: UIButton){
        if self.validateLogin(){
            let imageUrl =   GetOrSaveImage.shared.saveImageDocumentDirectory(image: self.userProfileImage!)
            let userDict = ["first_name":nameTxtField.text ?? "",
                            "last_name":"",
                            "email":"",
                            "fb_id": "",
                            "PhotoURL":imageUrl,
                            "isGuest": true,
                            "acc_type": "Guest"] as [String : Any]
//            DbHandler.shared.saveUser(user: userDict)
            UserDefaultsHandler.user = UserDataModel.init(userDict: userDict)
            UserDefaultsHandler.pageToShow = ConstantModel.pageName.DashBoardScreen
            NavigationManager.checkWhereToNav()
        }
    }
}
