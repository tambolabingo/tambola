//
//  ForgotPasswordViewController.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 22/04/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class ForgotPasswordViewController: UIViewController {
    @IBOutlet var txtEmailPhone                : SkyFloatingLabelTextField!
    @IBOutlet var lblTitleOne                : UILabel!
    @IBOutlet var lblTitleTwo                : UILabel!
    var loginType = "email" //mobile
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
            // Do any additional setup after loading the view.
        }
        func setupUI(){
            txtEmailPhone.delegate = self
            txtEmailPhone.inputAccessoryView = addDoneButtonOnKeyboard()
            if loginType == "email"{
                txtEmailPhone.placeholder = "Enter Email"
                txtEmailPhone.title   = "Email"
                txtEmailPhone.selectedTitle = "Email"
                lblTitleOne.text = "Please enter your registered Email ID"
                lblTitleTwo.text = "We will send a verification code to your registered email ID"
            } else {
                txtEmailPhone.keyboardType = .numberPad
                txtEmailPhone.placeholder = "Enter Phone Number"
                txtEmailPhone.title   = "Phone Number"
                txtEmailPhone.selectedTitle   = "Phone Number"
                lblTitleOne.text = "Please enter your registered Phone Number"
                lblTitleTwo.text = "We will send a verification code to your registered Phone Number"
            }
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.forgotPasswordScreen)
        }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: UIButton){
        if validateEmailPhone(){
            let usertype = loginType == "email" ? "email" : "mobile_no"
            let email = txtEmailPhone.text!
            let parameter = [usertype:email as Any, "user_acc_type" : loginType] as [String : Any]
            self.sharePassword(parameter)
        }
        
    }
    func sharePassword(_ params: [String:Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.forgotPass, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
                    APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                        // Logged in User Sucessfullyx
                        print(result as Any)
                        let data = result as? [String: Any]
                        let message = data!["message"] as? String
                        EventManager.showAlertWithAction(alertMessage: message, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: { _ in
                           //Restart timer
                            self.navigationController?.popViewController(animated: true)
                        })
                    })
                }, failure: { failureResponse in
                    if failureResponse == ConstantModel.message.tokenError {
                        //Call token API and recall API called
                        BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                            if isCallSucess{
                                self.sharePassword(params)
                            }
                        })
                    } else {
                        EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                    }
                } )

    }
}
// MARK:- Text field extension 
extension ForgotPasswordViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:true)
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:false)
    }
}
