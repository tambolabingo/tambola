//
//  OTPViewController.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 21/04/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class OTPViewController: UIViewController {
    @IBOutlet var txtOTP                : SkyFloatingLabelTextField!
    @IBOutlet var lblPhoneNumer : UILabel!
    @IBOutlet var lblTimer : UILabel!
    var phoneNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        txtOTP.delegate = self
        txtOTP.inputAccessoryView = addDoneButtonOnKeyboard()
        let lasttext = String(phoneNumber.suffix(3))
        lblPhoneNumer.text = "A OTP has been sent to XXXXXXX" + lasttext
        lblTimer.isHidden = true
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.otpScreen)
    }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func ResendOTP(_ sender: UIButton){
        let parameter = ["mobile_no":phoneNumber]
        self.resendLink(parameter)
    }
    @IBAction func submitOTP(_ sender: UIButton){
        if self.validateOTP() {
            let params = GetParamModel.shared.getVerifyMobileParam(mobileNum: phoneNumber, otp: txtOTP.text!, acc_type: "mobile")
            self.verifyMobile(params as! [String : String])
        }
    }
    func verifyMobile(_ params: [String:String]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.verifyOtp, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
                    APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                        // Logged in User Sucessfullyx
                        print(result as Any)
                        let data = result as? [String: Any]
                        let message = data!["message"] as? String
                        let alert=UIAlertController(title: ConstantModel.ProjectName, message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnOk, style: .default, handler: { action in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    })
                }, failure: { failureResponse in
                    if failureResponse == ConstantModel.message.tokenError {
                        //Call token API and recall API called
                        BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                            if isCallSucess{
                                self.verifyMobile(params)
                            }
                        })
                    } else {
                        EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                    }
                } )

    }
    func resendLink(_ params: [String:String]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.resendOTP, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
                    APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                        // Logged in User Sucessfullyx
                        print(result as Any)
                        let data = result as? [String: Any]
                        let message = data!["message"] as? String
                        EventManager.showAlertWithAction(alertMessage: message, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: { _ in
                           //Restart timer
                        })
                    })
                }, failure: { failureResponse in
                    if failureResponse == ConstantModel.message.tokenError {
                        //Call token API and recall API called
                        BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                            if isCallSucess{
                                self.resendLink(params)
                            }
                        })
                    } else {
                        EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                    }
                } )

    }
}
// MARK:- Text field extension 
extension OTPViewController : UITextFieldDelegate{
    
     // MARK:- Text field should return  
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if textField == txtOTP{
//            txtOTP.resignFirstResponder()
//        }
//        return false
//    }
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:true)
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:false)
    }
}
