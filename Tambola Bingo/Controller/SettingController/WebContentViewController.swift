//
//  WebContentViewController.swift
//  Tambola Bingo
//
//  Created by signity on 27/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import WebKit

class WebContentViewController: UIViewController, WKNavigationDelegate{
    var screenTitle = ""
    var contentURL = ""
    @IBOutlet weak var lblContentTitle:UILabel!
    @IBOutlet weak var contentWebView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblContentTitle.text = self.screenTitle
        self.contentWebView.scrollView.showsVerticalScrollIndicator = false
        contentWebView.navigationDelegate = self
        self.loadData()
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        EventManager.hideloader()
    }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func loadData(){
            self.contentWebView.load(URLRequest(url: URL(string: self.contentURL)!))
    }
    // MARK: -  WebView Delegates 
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
        EventManager.showloader()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        EventManager.hideloader()
    }
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
}
