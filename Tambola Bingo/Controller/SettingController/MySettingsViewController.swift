//
//  MySettingsViewController.swift
//  Tambola Bingo
//
//  Created by signity on 27/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class MySettingsViewController: UIViewController{
    @IBOutlet weak var settingsTableView: UITableView!
    var titleArray = ["Notification Sound","Application Sound", "Privacy Policy", "How To Play","Prizes","Frequently Asked Questions"]
    var webTitle = ""
    var webURL = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.settingsScreen)

    }
    // MARK: -  IBOutlet Action 
    @IBAction func BackButtonAction(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func LogOutButtonAction(sender:UIButton){
        self.logOutUser(GetParamModel.shared.getLogoutParam()!)
    }
    @IBAction func switchButtonAction(sender:UIButton){
        if sender.tag == 101{
            if (UserDefaultsHandler.user?.isGuest)! {
                EventManager.showAlertWithAction(alertMessage: ConstantModel.message.notificationError, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            } else {
            //Notification on off
            UserDefaultsHandler.userNotification = UserDefaultsHandler.userNotification == "1" ? "0" : "1"
            self.onOffNotificationAPI(parameters: GetParamModel.shared.getNotificationParam(state: UserDefaultsHandler.userNotification))
            }
        } else {
          //Sound on off
            UserDefaultsHandler.applicationSound = UserDefaultsHandler.applicationSound ? false : true
            self.reloadRow(rowNum: 1)
        }
    }
    func reloadRow(rowNum: Int){
        let indexPath = IndexPath(item: rowNum, section: 0)
        self.settingsTableView.reloadRows(at: [indexPath], with: .none)
    }
   
    // MARK:   API Call For On/Off Notification API  
    func onOffNotificationAPI(parameters: [String: Any]?) {
        //API Call
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.notification, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                // Switch notification on/off
                self.reloadRow(rowNum: 0)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.onOffNotificationAPI(parameters: parameters)
                    } else {
                         UserDefaultsHandler.userNotification = UserDefaultsHandler.userNotification == "1" ? "0" : "1"
                    }
                })
            } else {
                UserDefaultsHandler.userNotification = UserDefaultsHandler.userNotification == "1" ? "0" : "1"
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        })
    }
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WebContent" {
            let nextVC            = segue.destination as? WebContentViewController
            nextVC?.screenTitle          = self.webTitle
            nextVC?.contentURL          = self.webURL
        }
    }
}
extension MySettingsViewController: UITableViewDelegate, UITableViewDataSource {
// MARK: -  TableView Delegate and Data Source 
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.titleArray.count
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "SettingListCell", for: indexPath) as? SettingListCell
    cell?.titleText.text = self.titleArray[indexPath.row]
    switch indexPath.row {
    case 0:
        var imageName = UserDefaultsHandler.userNotification == "1" ? "SwitchButtonOn" : "SwitchButtonOff"
        imageName = (UserDefaultsHandler.user?.isGuest)! ? "SwitchButtonOn" : imageName
        cell?.switchbutton.setImage(UIImage(named: imageName), for: .normal)
        cell?.showDetailArrow.isHidden = true
        cell?.switchbutton.isHidden = false
        cell?.switchbutton.tag  = 101
        break
    case 1:
        let imageName = UserDefaultsHandler.applicationSound ? "SwitchButtonOn" : "SwitchButtonOff"
        cell?.switchbutton.setImage(UIImage(named: imageName), for: .normal)
        cell?.showDetailArrow.isHidden = true
        cell?.switchbutton.isHidden = false
        cell?.switchbutton.tag  = 102
        break
    default:
        cell?.showDetailArrow.isHidden = false
        cell?.switchbutton.isHidden = true
        break
    }
    if indexPath.row % 2 == 0 {
        cell?.backgroundColor = UIColor.clear
    }else {
        cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
    }
    return cell!
}

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    switch indexPath.row {
    case 2:
        webTitle = "Privacy Policy"
        webURL = "https://tambolabingo.com/privacy.html"
        self.performSegue(withIdentifier: "WebContent", sender: true)
        break
    case 3:
        webTitle = "How to play"
        webURL = "https://tambolabingo.com/play.html"
        self.performSegue(withIdentifier: "WebContent", sender: true)
        break
    case 4:
        webTitle = "Prizes"
        webURL = "https://tambolabingo.com/prizes.html"
        self.performSegue(withIdentifier: "WebContent", sender: true)
        break
    case 5:
        webTitle = "FAQ"
        webURL = "https://tambolabingo.com/faq.html"
        self.performSegue(withIdentifier: "WebContent", sender: true)
        break
    default:
        webTitle = ""
        webURL = ""
        break
    }
    
}
}
extension MySettingsViewController {
    func logOutUser(_ params: [String:Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.Logout, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                        // Logged out User Sucessfully
                        //LogOut From Facebook and Clear core data + user defaults
                        LoginViaFBModel.shared.logOutFromFb()
                        FirbaseExtensionManager.shared.firbaseSignout()
                        UserDefaultsHandler.removeAllUserDefaults()
                        NavigationManager.checkWhereToNav()
                    })
                }, failure: { failureResponse in
                    if failureResponse == ConstantModel.message.tokenError {
                        //Call token API and recall API called
                        BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                            if isCallSucess{
                                self.logOutUser(params)
                            }
                        })
                    } else {
                        EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                    }
                } )

    }
}
