//
//  GameDashBoardViewController.swift
//  Tambola Bingo
//
//  Created by signity on 25/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds
import SkyFloatingLabelTextField
class GameDashBoardViewController: UIViewController {
    @IBOutlet weak var userNameText: UILabel!
    @IBOutlet weak var userCoinsText: UILabel!
    @IBOutlet weak var userCoinsImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnEarnFreecoins: UIButton!
    @IBOutlet weak var btnInvitecoins: UIButton!
    @IBOutlet weak var btnBuyCoins: UIButton!
    @IBOutlet weak var btnLeaderBoard: UIButton!
    @IBOutlet weak var adsBannerView:GADBannerView!
    
    @IBOutlet weak var purchseCoinForFriendView: UIView!
    @IBOutlet var txtUserName                : SkyFloatingLabelTextField!
    @IBOutlet var submitBtn  : UIButton!
    
    var myAdsView = ShowiAdsView()
    var shopform = "dashboard"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.maintenanceAPI()
//        UserDefaultsHandler.isHideAds = true
//        self.updateCoinsPurchasedAction()
//        btnBuyCoins.isHidden = true
        hideClose = false
        btnLeaderBoard.isHidden = true
        btnEarnFreecoins.isHidden = true
//        btnInvitecoins.isHidden = true
        self.myAdsView.senderController = self
        txtUserName.inputAccessoryView = addDoneButtonOnKeyboard()
        txtUserName.delegate = self
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.DashBoardScreen)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.myAdsView.loadInterstitialAds()
        self.shopform = "dashboard"
        self.topView.frame.origin.y = -80
        self.navigationController?.navigationBar.isHidden = true
        FirbaseExtensionManager.shared.firbaseloginAuth(self)
        UserDefaultsHandler.user?.fName != "" ? nil : self.getUserData()
        (UserDefaultsHandler.user?.isGuest)! ? self.fillUserData(): self.getUserCoins()
        self.checkPurchaseStatus()
//        self.myAdsView.loadGoogleAdsBanner(banner: adsBannerView)
    }
    //Add Test Coins
   func updateCoinsPurchasedAction(){
       //Call Set coin API with deduction of cions
//       let parameters = GetParamModel.shared.setUserCoinsParam("50000", game_id: "", transaction_type: "Credits", purpose:"inAppPurchase")
//       BaseAPIHandler.shared.setUserCoinsAPI(params: parameters!, delegate: self) { isCallSucess in
//           //Amount Credited Sucessfull
//       }
   }
    func checkPurchaseStatus(){
           if UserDefaultsHandler.transactionComplete && UserDefaultsHandler.friendTransaction {
               //Credit coins to friends account
               let parameters = GetParamModel.shared.setUserCoinsParam(UserDefaultsHandler.coinPurchased, "", "Credits","inAppPurchase", true)
               BaseAPIHandler.shared.setFriendsCoinsAPI(params: parameters!, delegate: self) { isCallSucess in
                   //coins added sucessfully
                   UserDefaultsHandler.transactionComplete = isCallSucess ? false : UserDefaultsHandler.transactionComplete
               }
           } else if UserDefaultsHandler.transactionComplete && !UserDefaultsHandler.friendTransaction {
               //Credit coins to users account
               let parameters = GetParamModel.shared.setUserCoinsParam(UserDefaultsHandler.coinPurchased, "", "Credits","inAppPurchase", false)
                      BaseAPIHandler.shared.setUserCoinsAPI(params: parameters!, delegate: self) { isCallSucess in
                          //Enable tickets and reload collection view
                          UserDefaultsHandler.transactionComplete = isCallSucess ? false : UserDefaultsHandler.transactionComplete
                          //Update coins on UI
                            self.fillUserData()
                      }
           }
       }
    func fillUserData(){
//        let user = UserDataManager.shared.getUserFromDB()
        let user = UserDefaultsHandler.user
        if user != nil {
            let imageURL = user?.profilePhotoURL
            self.userNameText.text = (user?.fName)! + " " + (user?.lName)!
            
            if (user?.isGuest)! {
                self.userCoinsText.isHidden = true
                self.userCoinsImage.isHidden = true
                userImage.image =  GetOrSaveImage.shared.getImage()
            } else {
            self.userCoinsText.isHidden = false
            self.userCoinsImage.isHidden = false
            self.userCoinsText.text = user?.userCoins
            userImage.sd_setImage(with: URL.init(string: imageURL!), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
            })
            }
        }
        self.showTopView()
    }
    func showTopView(){
        UIView.animate(withDuration: 0.75, delay: 0.0, options: UIView.AnimationOptions.transitionCurlDown, animations: {
            self.topView.frame.origin.y = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    func showAlertForLogIn(_ message:String){
        EventManager.showAlertWithAction(alertMessage: message, btn1Tit: ConstantModel.btnText.btnYes, btn2Tit: ConstantModel.btnText.btnNo, sender: self) { action in
            action == ConstantModel.btnText.btnYes ? self.loginWithFbAPI() : nil
        }
    }
    // MARK: -  IBOutlet Action 
    @IBAction func leaderBoardAction(sender:UIButton){
        (UserDefaultsHandler.user?.isGuest)! ? self.showAlertForLogIn(ConstantModel.message.fbLoginFeatureMessage) : self.showAddMethod(controllerIdentifier: "ShowLeaderView")
    }
    @IBAction func shoppingCoinsAction(sender:UIButton){
        
        (UserDefaultsHandler.user?.isGuest)! ? self.showAlertForLogIn(ConstantModel.message.fbLoginFeatureMessage) : self.showAddMethod(controllerIdentifier: "ShopCoinView")
    }
    @IBAction func earnFreeCoinsAction(sender:UIButton){
        (UserDefaultsHandler.user?.isGuest)! ? self.showAlertForLogIn(ConstantModel.message.fbLoginFeatureMessage) : self.showAddMethod(controllerIdentifier: "EarnFreeCoinView")
    }
    @IBAction func inviteEarnCoinsAction(sender:UIButton){
        //Purchase coin for friend
//        (UserDefaultsHandler.user?.isGuest)! ? self.showAlertForLogIn(ConstantModel.message.fbLoginFeatureMessage) : self.showAddMethod(controllerIdentifier: "InviteEarnCoinsView")
        self.txtUserName.text = ""
        (UserDefaultsHandler.user?.isGuest)! ? self.showAlertForLogIn(ConstantModel.message.fbLoginFeatureMessage) : (self.purchseCoinForFriendView.isHidden = false)
    }
    
    @IBAction func friendsAction(sender:UIButton){
        (UserDefaultsHandler.user?.isGuest)! ? self.showAlertForLogIn(ConstantModel.message.fbLoginFeatureMessage) : self.showAddMethod(controllerIdentifier: "ShowFriendsView")
        
    }
    @IBAction func SettingsAction(sender:UIButton){
        self.performSegue(withIdentifier: "mySettings", sender: true)
    }
    @IBAction func practiceGameAction(sender:UIButton){
        self.performSegue(withIdentifier: "TicketSelection", sender: true)
    }
    
    @IBAction func praviteGameAction(sender:UIButton){
        
        (UserDefaultsHandler.user?.isGuest)! ? self.showAlertForLogIn(ConstantModel.message.fbLoginGameMessage) : self.showAddMethod(controllerIdentifier: "PrivetGameList")
    }
    @IBAction func publicGameAction(sender:UIButton){
        
        (UserDefaultsHandler.user?.isGuest)! ? self.showAlertForLogIn(ConstantModel.message.fbLoginGameMessage) : self.showAddMethod(controllerIdentifier: "PublicGameList")
        
    }
    // MARK: -  Show Ads Banner full screen 
    func showAddMethod(controllerIdentifier: String){
        if UserDefaultsHandler.isHideAds {
            self.performSegue(withIdentifier: controllerIdentifier, sender: true)
        } else {
            //Show Banner Google Ads
             self.myAdsView.AdsAction = { isClose in
                 if isClose {
                     //Navigate to next view to display
                     self.performSegue(withIdentifier: controllerIdentifier, sender: true)
                 }
             }
            EventManager.showloader()
            // change to desired number of seconds (in this case 5 seconds)
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when){
              // your code with delay
              EventManager.hideloader()
              self.myAdsView.showAds()
            }
            
        }
        
    }
    // MARK: -  Check for Maintenance API Call 
    func maintenanceAPI() {
        BaseAPIHandler.shared.getMaintenanceAPI(delegate: self) { isCallSucess in
            if isCallSucess{
              // Get all data sucessfully
            } else {
               //Logout and try again
//                LoginViaFBModel.shared.logOutFromFb()
//                UserDefaultsHandler.removeAllUserDefaults()
//                NavigationManager.checkWhereToNav()
            }
        }
    }
    // MARK: -  Get User Coins API Call 
    func getUserCoins(){
        BaseAPIHandler.shared.getUserCoinsAPI(delegate: self) { isCallSucess in
            self.fillUserData()
        }
    }
    
    // MARK: -  Get User Coins API Call 
    func getUserData(){
        let params = GetParamModel.shared.getUserCoinsParam()
        BaseAPIHandler.shared.getUserInfoAPI(isMyinfo: true, params: params, delegate: self) { isCallSucess in
            self.fillUserData()
        }
    }
    // MARK: -  Login With Facebook API Call 
    func loginWithFbAPI(){
        FBLoginManager.shared.loginWithFaceBook(delegate: self){ isCallSucess in
            if isCallSucess{
                // Loggedin sucessfully
                    UserDefaultsHandler.pageToShow = ConstantModel.pageName.DashBoardScreen
                    NavigationManager.checkWhereToNav()
            } else {
                
            }
        }
    }

}
extension GameDashBoardViewController: UITextFieldDelegate{
    
     // MARK:- Text field should return  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtUserName{
            txtUserName.resignFirstResponder()
        }
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:true)
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.animateTextField(textField: textField, up:false)
    }
    @IBAction func closeViewAction(){
        self.purchseCoinForFriendView.isHidden = true
    }
    @IBAction func submitBtnAction(){
        //Check fileds and check for user API call
        if validateUserName() {
            let uername = txtUserName.text!.replacingOccurrences(of: "+91", with: "")
            let isnum = uername.isNumeric()
            let params = GetParamModel.shared.getUserDetailParam(txtUserName.text!, isnum)
            BaseAPIHandler.shared.getUserInfoAPI(isMyinfo: false, params: params, delegate: self) { isCallSucess in
                //Save user info and pass to next purchase screen
                self.purchseCoinForFriendView.isHidden = true
                self.shopform = "Friend"
                isCallSucess ? self.performSegue(withIdentifier: "ShopCoinView", sender: true) : nil
            }
        }
    }
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShopCoinView" {
            let nextVC          = segue.destination as? ShopCoinsViewController
            nextVC?.isFrom   = shopform
        }
    }
}
