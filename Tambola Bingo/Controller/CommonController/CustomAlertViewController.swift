//
//  CustomAlertViewController.swift
//  Tambola Bingo
//
//  Created by signity on 05/09/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class CustomAlertViewController: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    var Message = ""
    var btnOneTitle = ""
    var btnTwoTitle = ""
    var alertAction:((String) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        if btnTwoTitle == ""{
            self.btnContinue.isHidden = true
            self.btnTwoTitle = btnOneTitle
            let titleTwo = "  " + btnOneTitle + "  "
            self.btnCancel.setTitle(titleTwo, for: .normal)
        } else {
            let titleOne = "  " + btnOneTitle + "  "
            self.btnContinue.setTitle(titleOne, for: .normal)
            let titleTwo = "  " + btnTwoTitle + "  "
            self.btnCancel.setTitle(titleTwo, for: .normal)
        }
        btnClose.isHidden = hideClose
        self.lblMessage.text = Message
    }
    // MARK: -  IBOutlet Action 
    @IBAction func cancelButtonAction(){
        self.view.removeFromSuperview()
    }
    @IBAction func closeButtonAction(){
        self.view.removeFromSuperview()
        alertAction!(btnTwoTitle)
    }
    @IBAction func continueButtonAction(){
        self.view.removeFromSuperview()
        alertAction!(btnOneTitle)
    }
}
