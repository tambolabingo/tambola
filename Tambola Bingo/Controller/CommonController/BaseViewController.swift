//
//  ViewController.swift
//  Tambola Bingo
//
//  Created by signity on 22/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import AuthenticationServices
import KeychainSwift
import VerticalSteppedSlider
class BaseViewController: UIViewController {
    @IBOutlet weak var signInButtonStack: UIStackView!
    @IBOutlet weak var tryAgainBtn:UIButton!
    @IBOutlet weak var fbLoginBtn:UIButton!
    @IBOutlet weak var appleLoginBtn:UIButton!
    @IBOutlet weak var emailLoginBtn:UIButton!
    @IBOutlet weak var phoneLoginBtn:UIButton!
//    @IBOutlet weak var guestLoginBtn:UIButton!
    @IBOutlet weak var policyBtn:UIButton!
    @IBOutlet weak var orView:UIView!
    @IBOutlet weak var noteLable:UILabel!
//    @State var appleSignInDelegates: SignInWithAppleDelegates! = nil

    var appleFname = ""
    var appleLast = ""
    var appleEmail = ""
    var appleId = ""
    var loginType = ""
// MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        hideClose = true
        self.navigationController?.navigationBar.isHidden = true
    }
    func setupUI(){
        
//        let str = "aGVsbG8gYWxsIGFzZGFzICpAJiUkJipAISUkKCElJCEmKkAoQCkhKl4kISUmQCReQCVAJiQhKQ=="
//        let encodedstr = str.fromBase64()
//        tryAgainBtn.isHidden = false
        signInButtonStack.isHidden = true
        policyBtn.isHidden = true
        noteLable.isHidden = true
        appleLoginBtn.addTarget(self, action: #selector(appleloginAction), for: .touchUpInside)
        self.maintenanceAPI()
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.baseScreen)
    }
// MARK: -  IBOutlet Action 
    @IBAction func tryAgainAction(sender:UIButton){
        self.setupUI()
    }
    @IBAction func loginWithFaceBook(){
        FBLoginManager.shared.loginWithFaceBook(delegate: self){ logInCompleted in
            //Sucessfully Loged in
            if logInCompleted {
                UserDefaultsHandler.pageToShow = ConstantModel.pageName.DashBoardScreen
                NavigationManager.checkWhereToNav()
            }
        }
    }
    
    @IBAction func loginAsGuest(){
        self.performSegue(withIdentifier: "GuestLogin", sender: true)
    }
    @IBAction func privacyPolicyAction(){
        NavigationManager.navigateToPrivacyPolicy(self)
    }
    // MARK: -  Check for Maintenance API Call 
    func maintenanceAPI() {
        BaseAPIHandler.shared.getMaintenanceAPI(delegate: self) { isCallSucess in
            if isCallSucess{
                self.signInButtonStack.isHidden = false
                self.policyBtn.isHidden = false
                self.noteLable.isHidden = false
                self.tryAgainBtn.isHidden = true
            } else {
                self.tryAgainBtn.isHidden = false
            }
        }
    }
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoginScreen" {
            let nextVC          = segue.destination as? LoginViewViewController
            nextVC?.loginType   = self.loginType
        }
    }
}
// MARK: -  Email and Phone Login Methods 
extension BaseViewController{
    @IBAction func loginWithEmail(){
        //Navigate To Email signin Screen
        self.loginType = "email"
        NavigationManager.navToALoginView(self, self.loginType)
        
    }
    @IBAction func loginWithPhone(){
        //Navigate To Phone signin Screen
        self.loginType = "mobile"
        NavigationManager.navToALoginView(self, self.loginType)
    }
}
// MARK: -  Apple Login Methods 
extension BaseViewController: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    @objc func appleloginAction(){
        if #available(iOS 13.0, *) {
            let request = ASAuthorizationAppleIDProvider().createRequest()
             request.requestedScopes = [.fullName, .email]
            let controller = ASAuthorizationController(authorizationRequests: [request])
            controller.delegate = self
            controller.presentationContextProvider = self
            controller.performRequests()
        } else {
            // Fallback on earlier versions
            EventManager.showAlertWithAction(alertMessage: "Sign in with Apple is available on Apple devices with the latest software—iOS 13 or later", btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        }
       
    }
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return globalPresentationAnchor ?? ASPresentationAnchor()
      }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
    if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
        appleId = appleIDCredential.user
        appleLast = appleIDCredential.fullName?.familyName ?? ""
        appleFname = appleIDCredential.fullName?.givenName ?? ""
        appleEmail = appleIDCredential.email?.description ?? ""
        self.loginByAppleAPI()
        
        }
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
        // Fallback on error
        EventManager.showAlertWithAction(alertMessage: error.localizedDescription, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
    }
    // MARK: -  Login by Apple API 
        @objc func loginByAppleAPI(){
            let userDict = ["first_name":appleFname,
                                            "last_name":appleLast,
                                            "email":appleEmail,
                                            "apple_id": appleId,
                                            "PhotoURL":"",
                                            "isGuest": false,
                                            "acc_type": "apple"] as [String : Any]
            UserDefaultsHandler.user = UserDataModel.init(userDict: userDict)
                
            let params = GetParamModel.shared.getAppleSigninParam(fNameText: appleFname,lNameText: appleLast , emailText: appleEmail, appleId: appleId)
            ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.Signin, parameters: params, type: APIConstants.PostAPI, isMultipart: true,isKey: false, delegate: self, success: { response in
                APIResponseModel.shared.parseLoginResponse(response: response, accType: "apple", isSocial: true, sender: self, callback: { result in
                    // Logged in User Sucessfullyx
                    
                    UserDefaultsHandler.pageToShow = ConstantModel.pageName.DashBoardScreen
                    NavigationManager.checkWhereToNav()
                })
            }, failure: { failureResponse in
                if failureResponse == ConstantModel.message.tokenError {
                    //Call token API and recall API called
                    BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                        if isCallSucess{
                            self.loginByAppleAPI()
    //                        self.loginByAppleAPI(id, name, email)
                        }
                    })
                } else {
                    EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                }
            } )
            
        }
}
