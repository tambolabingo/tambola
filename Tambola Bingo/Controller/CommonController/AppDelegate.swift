//
//  AppDelegate.swift
//  Tambola Bingo
//
//  Created by signity on 22/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import Fabric
import Crashlytics
import UserNotifications
import GoogleMobileAds
import Firebase
import AuthenticationServices
import UserNotifications

var globalPresentationAnchor: ASPresentationAnchor? = nil

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var background_task: UIBackgroundTaskIdentifier?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Initialize the Google Mobile Ads SDK.
        // Sample AdMob app ID: ca-app-pub-3940256099942544~1458002511
        GADMobileAds.configure(withApplicationID: ConstantModel.GADAdsUnitId )
        
        //Configure Firebase
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        //Register Push Notification
        self.registerPush(application: application)
        
        //Google Analytics
        self.setupGoogleAnalytics()
        //Crash Analytics
        Fabric.with([Crashlytics.self])
        //Facebook Configuration
//        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        FBSDKCoreKit.ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        if UserDefaultsHandler.pageToShow != nil {
            NavigationManager.checkWhereToNav()
        }
        globalPresentationAnchor = window
        return true
    }
    //MARK: Handle Universal linking  
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        if (userActivity.activityType == NSUserActivityTypeBrowsingWeb) {
            let url: URL? = userActivity.webpageURL
            //http://tambolabingo.com/api/tracking/inviteKey/454872
            if url?.pathComponents.contains("inviteKey") ?? false {
//                let components =  url?.pathComponents
//                print(components?[0])
                if UserDefaultsHandler.user != nil {
                    NavigationManager.navToPrivateGameListView("")
                } else {
                    NavigationManager.checkWhereToNav()
                }
            }
        }
        return true
    }
     // MARK: -  Setup Google Analytics 
    func setupGoogleAnalytics() {
        // Configure tracker from GoogleService-Info.plist.
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return
        }
//        UA-56436460-1 make this dynamic from server
//        gai.tracker(withTrackingId: "UA-56436460-3")
        // Optional: automatically report uncaught exceptions.
        gai.trackUncaughtExceptions = true
        // Optional: set Logger to VERBOSE for debug information.
        // Remove before app release.
        gai.logger.logLevel = .verbose;
    }
    func setGoogleAnalyticsTrcker(){
        GAI.sharedInstance().tracker(withTrackingId: UserDefaultsHandler.googleAnalyticsKey)
    }
    func createGAScreenLog(_ screenTag: String) -> Void {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: screenTag)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    func createGAEventLog(_ gaCategory: String,_ gaAction: String,_ gaLable: String) -> Void {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        guard let builder = GAIDictionaryBuilder.createEvent(withCategory: gaCategory, action: gaAction, label: gaLable, value: 1) else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
     // MARK: -  FBSDKApplicationDelegate 
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        if ApplicationDelegate.shared.application(app, open: url, options: options) {
        if FBSDKCoreKit.ApplicationDelegate.shared.application(app, open: url, options: options) {
            return true
        } else {
            let components =  url.pathComponents
            if components.count > 1 {
                let gamekey = components[1]
                    if UserDefaultsHandler.user != nil {
                        NavigationManager.navToPrivateGameListView(gamekey)
                    } else {
                        NavigationManager.checkWhereToNav()
                    }
            }
        }
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        self.beginBackgroundUpdateTask()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    // MARK: -  Register Push Notifications 
    func registerPush (application: UIApplication){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let token = convertDeviceTokenToString(deviceToken: deviceToken as NSData)
//        UserDefaultsHandler.deviceToken = token
        // Convert token to string
        Messaging.messaging().apnsToken = deviceToken
//        Messaging.messaging().apnsToken = deviceToken
    }
    private func convertDeviceTokenToString(deviceToken:NSData) -> String {
        //  Convert binary Device Token to a String (and remove the <,> and white space charaters).
        var deviceTokenStr = deviceToken.description.replacingOccurrences(of: ">", with: "")
        deviceTokenStr = deviceTokenStr.replacingOccurrences(of: "<", with: "")
        deviceTokenStr = deviceTokenStr.replacingOccurrences(of: " ", with: "")
        return deviceTokenStr
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
//        #if targetEnvironment(simulator)
//            let deviceTokenString = "6c3b78fc1bdee5b81a49c991bd631d4a31f0de54500cebc15c10d1ea137b2b"
//            UserDefaultsHandler.deviceToken = deviceTokenString
//        #endif
        print("APNs registration failed: \(error)")
    }
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        RemoteNotificationManager.handleRemoteNotification(response: userInfo)
        // Print full message.
        print(userInfo)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Print full message.
        print(userInfo)
        RemoteNotificationManager.handleRemoteNotification(response: userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // MARK: -  BackGround Thread 
    func beginBackgroundUpdateTask()
    {
        UIApplication.shared.beginBackgroundTask {
            self.endBackgroundUpdateTask()
        }
    }
    func endBackgroundUpdateTask()
    {

        if(background_task != nil && background_task! != UIBackgroundTaskIdentifier.invalid){
            UIApplication.shared.endBackgroundTask(background_task!)
            background_task = UIBackgroundTaskIdentifier(rawValue: convertFromUIBackgroundTaskIdentifier(UIBackgroundTaskIdentifier.invalid))
        }
    }
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Tambola_Bingo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
    }
    @available(iOS 9, *)
    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    @available(iOS 9, *)
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "Tambola_Bingo", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    @available(iOS 9, *)
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    @available(iOS 9, *)
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    // MARK: - Core Data Saving support
    func getManagedObjectContext () -> NSManagedObjectContext {
        let managedContext: NSManagedObjectContext!
        // iOS 10 support
        if #available(iOS 10, *) {
            managedContext = self.persistentContainer.viewContext
        } else {
            managedContext = self.managedObjectContext
        }
        return managedContext
    }
    func getStoreCoordinatorContext () -> NSPersistentStoreCoordinator {
        let coordinator: NSPersistentStoreCoordinator!
        // iOS 10 support
        if #available(iOS 10, *) {
            coordinator = self.persistentContainer.persistentStoreCoordinator
        } else {
            coordinator = self.persistentStoreCoordinator
        }
        return coordinator
    }

}
// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print full message.
        print(userInfo)
        RemoteNotificationManager.handleRemoteNotification(response: userInfo)
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print full message.
        print(userInfo)
        RemoteNotificationManager.handleRemoteNotification(response: userInfo)
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension AppDelegate: MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        UserDefaultsHandler.deviceToken = token!
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
        RemoteNotificationManager.handleRemoteNotification(response: remoteMessage.appData)
    }
    // [END ios_10_data_message]
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIBackgroundTaskIdentifier(_ input: UIBackgroundTaskIdentifier) -> Int {
	return input.rawValue
}
