//
//  EarnFreeCoinsViewController.swift
//  Tambola Bingo
//
//  Created by signity on 21/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class EarnFreeCoinsViewController: UIViewController {
    @IBOutlet weak var offersCollectionView: UICollectionView!
    @IBOutlet weak var userCoinsText: UILabel!
    var offersList = [MyOffersDataModel]()
    var coinPurchased = "00"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUserCoin()
        self.getOffersListDataAPI()
        // Do any additional setup after loading the view.
    }
    func setUserCoin(){
        let user = UserDefaultsHandler.user
        self.userCoinsText.text = user != nil ? user?.userCoins : "0000"
    }
    // MARK: -  IBOutlet Action 
    @IBAction func BackButtonAction(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}

extension EarnFreeCoinsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    // MARK: -  UICollectionView Delegate, DelegateFlowLayout and Data Source 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth   = self.offersCollectionView.frame.width / 2
        return CGSize(width: cellWidth - 20, height: 70)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offersList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shopeCollectionViewCell", for: indexPath) as? shopeCollectionViewCell
        EventManager.applyCellBorderShadow(cell!)
        cell?.lblTitle.text = offersList[indexPath.row].title
        let imageURL = offersList[indexPath.row].image_url
        print(imageURL)
        cell?.imgShopping.sd_setImage(with: URL.init(string: imageURL), placeholderImage: #imageLiteral(resourceName: "CoinCart"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
            print(error?.localizedDescription ?? "")
        })
        cell?.btnBuy.setTitle(offersList[indexPath.row].coins, for: .normal)
        cell?.btnBuy.backgroundColor = ConstantModel.appColor.blueColor
        cell?.btnBuy.tag = indexPath.row
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
    }
}
// MARK: -  Extension For API Call's 
extension EarnFreeCoinsViewController{
    // MARK: -  Check Public/Private Game Claim's API 
    func getOffersListDataAPI(){
        let params = GetParamModel.shared.getUserCoinsParam()
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.getFreeCoinOffers, parameters: params, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseOffersAPIResponse(response: response, sender: self, callback: { result in
                self.offersList = (result as? [MyOffersDataModel])!
                self.offersCollectionView.reloadData()
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getOffersListDataAPI()
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    func updateCoinsPurchasedAction(){
        //Call Set coin API with deduction of cions
        
        let parameters = GetParamModel.shared.setUserCoinsParam(self.coinPurchased, "", "Credits","inAppPurchase", false)
        BaseAPIHandler.shared.setUserCoinsAPI(params: parameters!, delegate: self) { isCallSucess in
            //Enable tickets and reload collection view
            isCallSucess ? self.setUserCoin() : EventManager.showAlertWithAction(alertMessage: ConstantModel.message.purchaseFailedMsg, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        }
    }
}
