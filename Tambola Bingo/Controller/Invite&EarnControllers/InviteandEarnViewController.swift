//
//  InviteandEarnViewController.swift
//  Tambola Bingo
//
//  Created by signity on 21/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class InviteandEarnViewController: UIViewController {
    @IBOutlet weak var userCoinsText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
         self.setUserCoin()
        // Do any additional setup after loading the view.
    }
    func setUserCoin(){
        let user = UserDefaultsHandler.user
        self.userCoinsText.text = user != nil ? user?.userCoins : "0000"
    }
    // MARK: -  IBOutlet Action 
    @IBAction func BackButtonAction(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
