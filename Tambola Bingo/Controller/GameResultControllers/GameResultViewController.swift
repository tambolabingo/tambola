//
//  GameResultViewController.swift
//  Tambola Bingo
//
//  Created by signity on 23/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class GameResultViewController: UIViewController {
    @IBOutlet weak var gameResultTableView: UITableView!
    @IBOutlet weak var resultImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblMyResult: UILabel!
    //@IBOutlet weak var btnFeedBack: UIButton!
    @IBOutlet weak var btnBuyCoin: UIButton!
    @IBOutlet weak var btnShareResult: UIButton!
    @IBOutlet weak var btnClose: UIButton!
     var resultData = [ResultDataModel]()
    var isBoggied = false
    var gameName    = "Parctice Game"
    var earnPoints  = "No"
    var resultImg = UIImage()
    var isHistory = false
    //After Game Play Game Data variables
    var isFromPlayGame = false
    var gameId = ""
    var claimPrices = [String:Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gameResultTableView.estimatedRowHeight = 70
//        self.setUpUI()
        isFromPlayGame ? self.getGameClaimsAPI() : self.setUpUI()
        // Do any additional setup after loading the view.
        isHistory ?         ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.gameHistoryScreen) : nil
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    func setUpUI(){
       // self.btnFeedBack.isHidden = isHistory
//        let userName = (UserDefaultsHandler.user?.fName)! + " " + (UserDefaultsHandler.user?.lName)!
        let userID = UserDefaultsHandler.user?.userId
        if (UserDefaultsHandler.user?.isGuest)! {
            userImage.image =  GetOrSaveImage.shared.getImage()
        }else {
        let imageURL = UserDefaultsHandler.user?.profilePhotoURL
        self.userImage.sd_setImage(with: URL.init(string: imageURL!), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
        })
        }
        var message = ""
        let gameNameStr = "'" + self.gameName + "'"
        if isBoggied {
            resultImage.image = UIImage(named: "BetterLuckText")
            message = String(format: ConstantModel.claim.gameLossMessage, self.gameName)
            let rangeStr = (message as NSString).range(of: gameNameStr)
            let attributedString = NSMutableAttributedString(string:message)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.yellow , range: rangeStr)
            self.lblMyResult.attributedText = attributedString
        } else {
            let dataArray = resultData.filter{$0.userId == userID}
            if dataArray.count == 0 {
                resultImage.image = UIImage(named: "BetterLuckText")
                message = String(format: ConstantModel.claim.gameLossMessage, self.gameName)
                let rangeStr = (message as NSString).range(of: gameNameStr)
                let attributedString = NSMutableAttributedString(string:message)
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.yellow , range: rangeStr)
                self.lblMyResult.attributedText = attributedString
            }else {
                resultImage.image = UIImage(named: "CongratulationText")
                let result = dataArray[0].resultArray.joined(separator: ", ")
                if self.earnPoints == "No" || self.earnPoints == "0"{
                    message = String(format: ConstantModel.claim.gameWinMessageOne, result, self.gameName)
                } else {
                    message = String(format: ConstantModel.claim.gameWinMessageTwo, result, self.gameName, self.earnPoints)
                }
                let rangeClaimStr = (message as NSString).range(of: result)
                let rangeStr = (message as NSString).range(of: gameNameStr)
                let attributedString = NSMutableAttributedString(string:message)
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.yellow , range: rangeClaimStr)
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.yellow , range: rangeStr)
                self.lblMyResult.attributedText = attributedString
            }
        }
        self.gameResultTableView.reloadData()
        self.removeGameTickets()
    }
    func removeGameTickets(){
        let myticket = GameTicketsModel.shared.getTicketsFromDB(gameId)
        myticket != nil ? GameTicketsModel.shared.deleteMyTicketFromDB(myticket!) : nil
    }
    // MARK: -  IBOutlet Action 
    @IBAction func closeButtonAction(){
         isHistory ?  (_ = self.navigationController?.popViewController(animated: true)) : (_ = self.navigationController?.popToRootViewController(animated: true))
    }
    @IBAction func shareResult(){
         self.resultImg = EventManager.getScreenShot(self.view)
        (UserDefaultsHandler.user?.isGuest)! ? self.showAlertForLogIn() : self.shareResultAPI(parameters: GetParamModel.shared.getShareResultParam(resultImage: self.resultImg)!)
       
        //Call Share API and Share API responce image URL on FB
        
    }
    @IBAction func feedBackButtonAction(){
        NavigationManager.navigateToFeedback(self)
    }
    @IBAction func buyCoinButtonAction(){
        //Navigate to next view to display
        NavigationManager.navigateToBuyCoin(self, isHistory)
    }
    func showAlertForLogIn(){
        EventManager.showAlertWithAction(alertMessage: ConstantModel.message.fbLoginShareMessage, btn1Tit: ConstantModel.btnText.btnYes, btn2Tit: ConstantModel.btnText.btnNo, sender: self) { action in
            action == ConstantModel.btnText.btnYes ? self.loginWithFbAPI() : nil
        }
    }
}
extension GameResultViewController: UITableViewDelegate, UITableViewDataSource{
// MARK: -  TableView Delegate and Data Source 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultData.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gameResultCell", for: indexPath) as? gameResultCell
        let imageURL = self.resultData[indexPath.row].image_url
        cell?.userImage.sd_setImage(with: URL.init(string: imageURL), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
        })
        cell?.lblName.text = self.resultData[indexPath.row].name
        let result = self.resultData[indexPath.row].resultArray
        let message = result.joined(separator: ", ") + "."
        let resultStr = "Completed " +  message
        let rangeStr = (resultStr as NSString).range(of: message)
        let attributedString = NSMutableAttributedString(string: resultStr)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.yellow , range: rangeStr)
        cell?.lblResult.attributedText = attributedString
        if indexPath.row % 2 == 0 {
            cell?.backgroundColor = UIColor.clear
        }else {
            cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
        return cell!
    }
}
// MARK: -  Extension For API Call's 
extension GameResultViewController {
    // MARK: -   Share Result on Fb and Upload Image on server API 
    func shareResultAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.shareResult, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //Share URl on facebook and pop to root view
                let data = result as? [String: Any]
                let resultURL = data!["url"] as? String
                FacebookShareModel.shared.shareImageOnFb(imageURL: resultURL!, delegate: self)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.shareResultAPI(parameters:parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    // MARK: -  Login With Facebook API Call 
    func loginWithFbAPI(){
        FBLoginManager.shared.loginWithFaceBook(delegate: self){ isCallSucess in
            if isCallSucess{
                // Loggedin sucessfully
                self.shareResultAPI(parameters: GetParamModel.shared.getShareResultParam(resultImage: self.resultImg)!)
            } else {
                
            }
        }
    }
    // MARK: -  Check Public/Private Game Claim's API 
    func getGameClaimsAPI(){
        let parameters = GetParamModel.shared.getGameClaimsParam(gameId: gameId)
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.GetGameClaims, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseGameClaimsResponse(response: response, claimPrices:self.claimPrices, sender: self, callback: { result in
                //Update Claim Data if any Update is there
                self.resultData = (result as? [ResultDataModel])!
                self.earnPoints = String(UserDefaultsHandler.winningAmount)
                self.setUpUI()
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getGameClaimsAPI()
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnRetry, btn2Tit: ConstantModel.btnText.btnCancel, sender: self, action:{ action in
                    action == ConstantModel.btnText.btnRetry ? self.getGameClaimsAPI() :( _ = self.navigationController?.popToRootViewController(animated: true))
                })
            }
        } )
    }
}
