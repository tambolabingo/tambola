//
//  ShowiAdsViewController.swift
//  Tambola Bingo
//
//  Created by signity on 22/11/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ShowiAdsView: UIView {
    @IBOutlet var popUpView: UIView!
    var senderController: UIViewController!
    
    @IBOutlet weak var adsBannerView:GADBannerView!
    @IBOutlet var interstitial: GADInterstitial!
    var AdsAction:((Bool) -> Void)?
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("ShowiAdsView", owner: self, options: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeView(frame:CGRect){
        
        self.frame = frame
        popUpView.frame = frame
        self.addSubview(popUpView)
    }
    // MARK: -  IBOutlet Action 
    @IBAction func btnDismissTapped(_ sender: Any) {
        //close the add banner
        AdsAction!(true)
        self.removeFromSuperview()
    }
    func loadBannerForAdd() {
        adsBannerView.adUnitID = ConstantModel.GADAdsUnitId
        adsBannerView.rootViewController = senderController
        adsBannerView.load(GADRequest())
    }
    func loadGoogleAdsBanner(banner: GADBannerView) {
           banner.adUnitID = ConstantModel.GADAdsUnitId
           banner.rootViewController = senderController
           banner.delegate = self
           banner.load(GADRequest())
       }
    func loadInterstitialAds(){
        interstitial = GADInterstitial(adUnitID: ConstantModel.GADAdsUnitId)
        interstitial.delegate = self
       loadAds()
    }
    func loadAds(){
         let request = GADRequest()
        request.testDevices = [(kGADSimulatorID as! String)]
        interstitial.load(request)
    }
    func showAds(){
        if interstitial.isReady {
          interstitial.present(fromRootViewController: senderController)
        } else {
            loadAds()
            interstitial.present(fromRootViewController: senderController)
          print("Ad wasn't ready")
        }
    }
}
extension ShowiAdsView: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
        //close the add banner
        AdsAction!(true)
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
        //Navigate to open app link
    }
}
extension ShowiAdsView: GADInterstitialDelegate {
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
         //close the add View
        AdsAction!(true)
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
        //Navigate to open app link
    }
}
