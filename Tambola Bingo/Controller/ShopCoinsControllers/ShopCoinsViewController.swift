//
//  ShopCoinsViewController.swift
//  Tambola Bingo
//
//  Created by signity on 21/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class ShopCoinsViewController: UIViewController {
    @IBOutlet weak var shopCollectionView: UICollectionView!
    @IBOutlet weak var userCoinsText: UILabel!
    
    @IBOutlet weak var friendNameText: UILabel!
    @IBOutlet weak var friendEmailPhone: UILabel!
    @IBOutlet weak var friendImg: UIImageView!
    @IBOutlet weak var friendViewHeight: NSLayoutConstraint!
    
    var purchaseList = [purchasesModel]()
//    var coinPurchased = "0000"
    var selectedIndex = 100
    var isFrom = "dashboard"
    var productIdList = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        //For Testing Purpose Add Direct Coins
        friendViewHeight.constant = 0
        self.shopCollectionView.isHidden = true
        self.setUserCoin()
        self.showFriendUI()
//        self.getMyPurchaseListDataAPI()
        purchaseList = (UserDefaultsHandler.coinPurchaseInfo)!
        if purchaseList.count == 0 {
            //Call Puchse API
            self.getMyPurchaseListDataAPI()
        } else {
            EventManager.hideloader()
            self.setUpIAP()
        }
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.shopScreen)

        // Do any additional setup after loading the view.
    }
    func setUserCoin(){
        let user = UserDefaultsHandler.user
        self.userCoinsText.text = user != nil ? user?.userCoins : "0000"
    }
    func setUpIAP(){
        EventManager.showloader()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            DispatchQueue.main.async {
            EventManager.hideloader()
            guard self != nil else{ return }
            if type == .purchased {
                print(type.message())
                self?.isFrom == "Friend" ? self?.purchaseCoinForFriendAction() : self?.updateCoinsPurchasedAction()
            } else if type == .restored{
                print(type.message())
                EventManager.showAlertWithAction(alertMessage: ConstantModel.message.upgardeSucessFull, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self!, action: nil)
            }else if type == .canceled || type == .disabled{
                print(type.message())
                UserDefaultsHandler.transactionError = UserDefaultsHandler.transactionError == "" ? type.message() : UserDefaultsHandler.transactionError
                EventManager.showAlertWithAction(alertMessage: type.message(), btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self!, action: {_ in
                    self?.updatePurchasedInfoAction()
                })
            } else if type == .fetched {
                self?.purchaseList = (UserDefaultsHandler.coinPurchaseInfo)!
                self?.shopCollectionView.reloadData()
                self?.shopCollectionView.isHidden = false
            }
            }
        }
        for product in purchaseList {
            self.productIdList.append(product.product_id)
        }
        IAPHandler.shared.fetchAvailableProducts(self.productIdList)
    }
    func showFriendUI(){
        if isFrom == "Friend"{
            UserDefaultsHandler.friendTransaction = true
            let friendData = UserDefaultsHandler.friendData
            if friendData != nil {
                    let imageURL = friendData?.profilePhotoURL
                    friendNameText.text = "Buy coin for " + (friendData?.fName)! + " " + (friendData?.lName)!
                    let emailPhonetext = (friendData?.email)!
                    friendEmailPhone.text = emailPhonetext.isNumeric() ? ( "Phone Number: : " + emailPhonetext) : ( "Email Id: " + emailPhonetext)
                    friendImg.sd_setImage(with: URL.init(string: imageURL!), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
                })
                friendViewHeight.constant = 60
            }
        } else {
            UserDefaultsHandler.friendTransaction = false
            friendNameText.isHidden = true
            friendEmailPhone.isHidden = true
            friendImg.isHidden = true
        }
    }
    // MARK: -  IBOutlet Action 
    @IBAction func BackButtonAction(sender:UIButton){
        if isFrom == "Result"{
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func restoreButtonAction(sender:UIButton){
        EventManager.showloader()
        IAPHandler.shared.restorePurchase()
    }
}
extension ShopCoinsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    // MARK: -  UICollectionView Delegate, DelegateFlowLayout and Data Source 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth   = self.shopCollectionView.frame.width / 3
        return CGSize(width: cellWidth - 20, height: 160)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return purchaseList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buyCoinCollectionViewCell", for: indexPath) as? buyCoinCollectionViewCell
        cell?.lblCoins.text = purchaseList[indexPath.row].coins
        let price = purchaseList[indexPath.row].label
        cell?.btnBuy.setTitle(price, for: .normal)
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            EventManager.showloader()
            self.selectedIndex = indexPath.row
            UserDefaultsHandler.coinPurchased = purchaseList[indexPath.row].coins
            IAPHandler.shared.purchaseMyProduct(purchaseList[indexPath.row].product_id)
        }
}
// MARK: -  Extension For API Call's 
extension ShopCoinsViewController{
    // MARK: -  Check Public/Private Game Claim's API 
    func getMyPurchaseListDataAPI(){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.purchaseCoins, parameters: nil, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parsePurchaseAPIResponse(response: response, sender: self, callback: { result in
                if result as! Bool {
                    self.purchaseList = (UserDefaultsHandler.coinPurchaseInfo)!
                    self.setUpIAP()
                    self.shopCollectionView.reloadData()
                }
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getMyPurchaseListDataAPI()
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    func updatePurchasedInfoAction(){
           let coinId = purchaseList[self.selectedIndex].id
           //Call Set coin API with deduction of cions
           let parameters = GetParamModel.shared.updatePurchaseInfoParam(coinId)
           BaseAPIHandler.shared.updateUserPurchaseAPI(params: parameters!, delegate: self) { isCallSucess in
           }
       }
    func updateCoinsPurchasedAction(){
        //Call Set coin API with deduction of cions
         let parameters = GetParamModel.shared.setUserCoinsParam(UserDefaultsHandler.coinPurchased, "", "Credits","inAppPurchase", false)
        BaseAPIHandler.shared.setUserCoinsAPI(params: parameters!, delegate: self) { isCallSucess in
            //Enable tickets and reload collection view
            UserDefaultsHandler.isHideAds = self.selectedIndex == 0 ? true : UserDefaultsHandler.isHideAds
            UserDefaultsHandler.transactionComplete = isCallSucess ? false : UserDefaultsHandler.transactionComplete
            isCallSucess ? self.setUserCoin() : EventManager.showAlertWithAction(alertMessage: ConstantModel.message.purchaseFailedMsg, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        }
    }
    func purchaseCoinForFriendAction(){
        //Call Set coin API with deduction of cions
        let parameters = GetParamModel.shared.setUserCoinsParam(UserDefaultsHandler.coinPurchased, "", "Credits","inAppPurchase", true)
        BaseAPIHandler.shared.setFriendsCoinsAPI(params: parameters!, delegate: self) { isCallSucess in
            //coins added sucessfully
            UserDefaultsHandler.transactionComplete = isCallSucess ? false : UserDefaultsHandler.transactionComplete
            isCallSucess ? (_ = self.navigationController?.popViewController(animated: true)) : nil
        }
    }
}
