//
//  PlayGameAPIExtension.swift
//  Tambola Bingo
//
//  Created by signity on 29/08/18.
//  Copyright © 2018 signity. All rights reserved.
//
import UIKit

// MARK: -  Extension For API Call's 
extension PlayGameViewController {
    // MARK: -  Check Public/Private Game Claim's API 
    func getGameClaimsAPI(isSilentCall: Bool){
        let parameters = GetParamModel.shared.getGameClaimsParam(gameId: playGameData.gameId)
        ApiCallModel.shared.apiCall(isSilent: isSilentCall, url: UserDefaultsHandler.projectBaseUrl + APIConstants.GetGameClaims, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseGameClaimsResponse(response: response, claimPrices: nil, sender: self, callback: { result in
                //Update Claim Data if any Update is there
                self.resultData = (result as? [ResultDataModel])!
                DispatchQueue.main.async {
                    for res in self.resultData {
                        let name = res.name
                        let id = res.userId
                        for price in res.resultArray {
                            self.checkClaimMessage(Name: name, claimOption: price, userId: id)
                        }
                    }
                }
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getGameClaimsAPI(isSilentCall: isSilentCall)
                    }
                })
            }/*
            else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
 */
        } )
    }
    
    // MARK: -  Check Public/Private Game Claim's API 
    func claimPriceAPI(parameters: [String: Any], ClaimPrice: String, index: Int){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.claimPrice, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseClaimPriceResponse(response: response, sender: self, callback: { result in
//                let fileName = ClaimPrice + " " + ConstantModel.claim.claimDone
                //Call Claim Price File player
                index != 2 ? self.playClaimFiles(index) : nil
//                self.playMp3File(fileName)
                let priceData = result as? [String: Any]
                //Game Claim price message
                index != 2 ? self.sendDataToFirbase(self.playGameData.gameId, chatFormat: ConstantModel.chatFormat.claimMessage, Message: ClaimPrice, isFrom: "Claim") : nil
//                self.sendToORTCWithData(self.playGameData.gameId, chatFormat: ConstantModel.chatFormat.claimMessage, Message: ClaimPrice)
                //Update Claim Data if any Update is there
                //Call User Coins and Claim Status API
                DispatchQueue.main.async {
                    self.getGameClaimsAPI(isSilentCall: false)
                }
                if priceData!["prizeAmount"] != nil {
                    let priceAmount = priceData!["prizeAmount"] as? String
                    self.totalClaimAmount(price: priceAmount)
                    DispatchQueue.main.async {
                        BaseAPIHandler.shared.getUserCoinsAPI(delegate: self) { isCallSucess in
                            EventManager.showAlertWithAction(alertMessage: priceData?["message"] as? String, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                        }
                    }
                } else {
                    EventManager.showAlertWithAction(alertMessage: priceData?["message"] as? String, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                }
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.claimPriceAPI(parameters: parameters, ClaimPrice: ClaimPrice, index: index)
                    }
                })
            } else {
                let userName = (UserDefaultsHandler.user?.fName)! + " " + (UserDefaultsHandler.user?.lName)!
                self.showboggiesMessageAction(name: userName, ClaimPrice: ClaimPrice)
            }
        } )
    }
    // MARK: -  Quit Public/Private Game Claim's API 
    func quitGameAPI(){
        let parameters = GetParamModel.shared.getQuitGameParam(gameId: playGameData.gameId)
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.quitGame, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                self.backFromPracticeGame()
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.quitGameAPI()
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    // MARK: -  Full House and End Public/Private Game Claim's API 
    func houseFullEndGameAPI(){
        let parameters = GetParamModel.shared.getEndGameParam(gameId: playGameData.gameId)
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.endGame, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                
                self.performSegue(withIdentifier: "GameResultView", sender: true)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.houseFullEndGameAPI()
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
// MARK: -  Enable Public/Private Ticket API 
    func enableTicketAPI(_ ticketNum: Int){
        //Call Set coin API with deduction of cions
        let parameters = GetParamModel.shared.setUserCoinsParam((UserDefaultsHandler.gamePriceInfo?.ticketprice)!, playGameData.gameId, "Debits","ticketpurchase", false)
        BaseAPIHandler.shared.setUserCoinsAPI(params: parameters!, delegate: self) { isCallSucess in
            //Enable tickets and reload collection view
            if isCallSucess {
                //Change Lock Image into enable disable ticket
                self.playGameData.ticketStatusArray[ticketNum] = true
                self.selectedTicket = ticketNum
                ticketNum == 1 ? self.ticketTwoButton.setImage(UIImage(named: "TicketTwoBtn"), for: .normal) : self.ticketThreeButton.setImage(UIImage(named: "TicketThreeBtn"), for: .normal)
                self.getClaimData()
                self.ticketCollectionView.reloadData()
                if self.isAutoCut {
                    for index in 0..<self.timerVal {
                        let anunceNum = self.playGameData.sequenceArray[index]
                        self.createCrossSequence(anunceNum)
                    }
                }
            }
        }
    }
// MARK: -  Enable AutoCut Public/Private Feature API 
    func autoCutUserCoinAPI(){
        let parameters = GetParamModel.shared.setUserCoinsParam(String(UserDefaultsHandler.gamePriceInfo?.autoCut ?? 0), playGameData.gameId, "Debits","AutoCut", false)
        BaseAPIHandler.shared.setUserCoinsAPI(params: parameters!, delegate: self) { isCallSucess in
            //Enable Auto Cut
            if isCallSucess {
                self.isAutoCut = true
                self.autoCutPurchased = true
                self.showAutoCutButton.setImage(UIImage(named: "AutoCutOn"), for: .normal)
                self.playClaimFiles(1)
//                self.playMp3File(PlayGameManager.autoCutEnable)
                self.showClaimMessage(message: ConstantModel.message.enableAutoCut)
                for index in 0..<self.timerVal {
                    let anunceNum = self.playGameData.sequenceArray[index]
                    self.createCrossSequence(anunceNum)
                }
                
            }
        }
    }
    func getPlayerDetailAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.myDetails, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //Share URl on facebook and pop to root view
                let data = result as? [String: Any]
                let score = data!["scores"] as? [String: Any]
                self.playerDetail.coins = data!["coins"] as? String ?? ""
                self.setPlayerData(score!)
                NavigationManager.navigateToFriendsDetail(delegate: self, friendData: self.playerDetail, game_Id: self.playGameData.gameId)
                //Show Detail View
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getPlayerDetailAPI(parameters:parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    func setPlayerData(_ score:[String: Any]){
        
        self.playerDetail.claims = score["claims"] as? String ?? ""
        self.playerDetail.gamePlayed  = score["gamePlayed"] as? String ?? ""
        self.playerDetail.luckRate = score["LuckRate"] as? Double ?? 0.00
        self.playerDetail.totalwins = score["Totalwins"] as? String ?? ""
        self.playerDetail.score.center = score["CENTER"] as? Int ?? 0
        self.playerDetail.score.circle = score["CIRCLE"] as? Int ?? 0
        self.playerDetail.score.corner = score["CORNER"] as? Int ?? 0
        self.playerDetail.score.earlyFive = score["EARLY_FIVE"] as? Int ?? 0
        self.playerDetail.score.firstLine = score["FIRST_LINE"] as? Int ?? 0
        self.playerDetail.score.houseFull = score["HOUSEFULL"] as? Int ?? 0
        self.playerDetail.score.luckSeven = score["LUCKY_SEVEN"] as? Int ?? 0
        self.playerDetail.score.pyramid = score["PYRAMID"] as? Int ?? 0
        self.playerDetail.score.secondLine = score["SECOND_LINE"] as? Int ?? 0
        self.playerDetail.score.thirdLine = score["THIRD_LINE"] as? Int ?? 0
        self.playerDetail.isDetailAdded =  true
    }
}
