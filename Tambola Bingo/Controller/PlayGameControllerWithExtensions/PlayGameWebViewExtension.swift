//
//  PlayGameWebViewExtension.swift
//  Tambola Bingo
//
//  Created by signity on 29/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import AVFoundation
import WebKit

//extension PlayGameViewController: WKNavigationDelegate {
//
//    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        print("Strat to load")
//    }
//
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        print("finish to load")
//        gameInstructionWebView.scrollView.contentSize = CGSize(width: 100, height: 600)
//        self.scoreBoardLoader.stopAnimating()
//        self.scoreBoardLoader.isHidden = true
//    }
//
//    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
//        print(error.localizedDescription)
//        self.scoreBoardLoader.stopAnimating()
//        self.scoreBoardLoader.isHidden = true
//    }
//}
// MARK: -  Extension For Play sounds for Game 
extension PlayGameViewController: AVAudioPlayerDelegate {
    
    func playMp3File(_ fileNameString: String?) {
        if isGameSound {
            let utterance = AVSpeechUtterance(string: fileNameString!)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
        }
    }
    func playNumberFiles(_ index: Int){
         if isGameSound {
            let numberArr = ["gameover","number_1","number_2","number_3","number_4","number_5","number_6",
                             "number_7","number_8","number_9","number_10","number_11","number_12","number_13",
                             "number_14","number_15","number_16","number_17","number_18","number_19","number_20",
                             "number_21","number_22","number_23","number_24","number_25","number_26","number_27",
                             "number_28","number_29","number_30","number_31","number_32","number_33","number_34",
                             "number_35","number_36","number_37","number_38","number_39","number_40", "number_41",
                             "number_42","number_43", "number_44","number_45","number_46", "number_47", "number_48",
                             "number_49","number_50", "number_51","number_52","number_53","number_54","number_55",
                             "number_56","number_57","number_58","number_59","number_60","number_61","number_62",
                             "number_63","number_64","number_65","number_66","number_67","number_68","number_69",
                             "number_70","number_71","number_72","number_73","number_74","number_75","number_76",
                             "number_77","number_78","number_79","number_80","number_81","number_82","number_83",
                             "number_84","number_85","number_86","number_87","number_88","number_89","number_90",
                             "congratulations"]
            guard let url = Bundle.main.url(forResource: numberArr[index], withExtension: "mp3") else {
                return
            }
            let playerItem:AVPlayerItem = AVPlayerItem(url: url)
            player = AVPlayer(playerItem: playerItem)
            player!.play()
        }
    }
    func playClaimFiles(_ index: Int){
         if isGameSound {
            let claimArr = ["autocut_disabled","autocut_enabled","boogie","center","circle","lucky_seven","pyramid","success_early","success_four","success_full","success_top","success_middle","success_bottom"]
            guard let url = Bundle.main.url(forResource: claimArr[index], withExtension: "mp3") else {
                return
            }
            let playerItem:AVPlayerItem = AVPlayerItem(url: url)
            player = AVPlayer(playerItem: playerItem)
            player!.play()
        }
    }
    
}

