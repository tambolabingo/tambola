//
//  PlayGameFirebaseMsgExtension.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 05/04/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
// MARK: -  Extension For Firbase game Chat  
extension PlayGameViewController{
    override func viewDidDisappear(_ animated: Bool) {
        self.stopSequenceTimer()
        self.stopGameClaimTimer()
        if !playGameData.isPracticeGame {
            
            let ref = Database.database().reference().child("Tambola").child(playGameData.gameId)
            let refOne = ref.child("Players")
            let refTwo = ref.child("Group_chat")
            let refThree = ref.child("claimresult")
            refOne.removeAllObservers()
            refTwo.removeAllObservers()
            refThree.removeAllObservers()
        }
    }
//    func authGameToFirbase(){
//        let gametoken = UserDefaultsHandler.gamefirebasetoken
//        if gametoken != ""{
//            Auth.auth().signIn(withCustomToken: gametoken ) { (user, error) in
//              // ...
//                print("Signin to firebase sucessfully")
//            }
//            
//        }
//    }
    func joinFirbaseUserMessage(){
        //Game Join Message Players
        self.sendDataToFirbase(playGameData.gameId, chatFormat: ConstantModel.chatFormat.quitJoinMessage, Message: "joined", isFrom: "Players")
    }
    //Mark:- Send message to firebase
    func sendDataToFirbase(_ channel: String, chatFormat:String, Message: String, isFrom: String)
    {
        let name = (user?.fName)! + " " + (user?.lName)!
        let userId = (user?.userId)!
        let customMessage =  String(format: chatFormat, userId, name, Message)
        switch isFrom {
        case "Players":
            //Joined or left
            self.updateUserAdded(customMessage, name, userId)
            break
        case "Chat":
        //JGriup Messages
            self.sendMsginGroup(Message, name, userId)
        break
        case "Claim":
        //Claim message
            self.sendClaimMessage(customMessage)
        break
        default:
            break
        }
        
    }
    func updateUserAdded(_ myString: String,_ userName: String,_ userId: String) {
            self.view.endEditing(true)
            if myString != "" {
                let ref = Database.database().reference()
                let thisUsersRef = ref.child("Tambola")
//                    .child(getTimeStamp())
                let childRef = thisUsersRef.child(playGameData.gameId).child("Players")
                let messageref = childRef.child(userId)
                let accType = UserDefaultsHandler.user?.accType as Any
                let dictMessage = ["Uid": (user?.userId)!,"Username": userName,"accountType": accType,"ortcmsg": myString] as [String : Any]
                messageref.setValue(dictMessage)
//                messageref.updateChildValues(dictMessage)
            }
    }
    func sendMsginGroup(_ myString: String,_ userName: String,_ userId: String) {
        self.view.endEditing(true)
        if myString != "" {
            let ref = Database.database().reference().child("Tambola")
//                .child(getTimeStamp())
            let childRef = ref.child(playGameData.gameId).child("Group_chat")
            let messageref = childRef.childByAutoId()
            let finalMessage =  UserDefaultsHandler.isEncodingEnable == "false" ? myString.encodeUrl() ?? myString : myString.toBase64()
            let dictMessage = ["Uid": userId,"Username": userName,"message": finalMessage] as [String : Any]
//            messageref.updateChildValues(dictMessage)
            messageref.setValue(dictMessage)
        }
    }
    func sendClaimMessage(_ myString: String) {
        self.view.endEditing(true)
        if myString != "" {
            let ref = Database.database().reference().child("Tambola")
//                .child(getTimeStamp())
            let childRef = ref.child(playGameData.gameId).child("claimresult")
            let messageref = childRef.childByAutoId()
            let dictMessage = ["claim_msg": myString] as [String : Any]
//            messageref.updateChildValues(dictMessage)
            messageref.setValue(dictMessage)
        }
    }
//    func getTimeStamp() -> String{
//        let gameStartTime = playGameData.gameStartTime
//        let startDTime = DateConvertor.convertToDate(slotDate: gameStartTime, dateFormat: DateFormats.ServerDateFormat)
////        let startDTime = DateConvertor.convertIntoDate(slotDate: gameStartTime, dateFormat: DateFormats.ServerDateFormat)
//        print("Game Start Time")
//        print(startDTime)
//        let timestamp = (startDTime.timeIntervalSince1970 * 1000)
//        let intVal = Int(timestamp)
//        print("Game TimeStamp")
//        print(intVal)
//        return String(intVal)
//    }
    //MARK:- Fetch data from the firebase
    func observeChannels() {
        let ref = Database.database().reference().child("Tambola")
//            .child(getTimeStamp())
        ref.child(playGameData.gameId).child("Players").observe(.childAdded, with: { (data) in
              if let postDict = data.value as? Dictionary<String, AnyObject> {
                //Added New Message or User
                let message = postDict["ortcmsg"] as! String
                let acc_type = postDict["accountType"] as? String ?? "facebook"
                self.checkMessageAction(message, acc_type)
            }
        })
        ref.child(playGameData.gameId).child("Players").observe(.childChanged, with: { (data) in
              if let postDict = data.value as? Dictionary<String, AnyObject> {
                //updated Message or User
                let message = postDict["ortcmsg"] as! String
                 let acc_type = postDict["accountType"] as? String ?? "facebook"
                self.checkMessageAction(message, acc_type)
            }
        })
        ref.child(playGameData.gameId).child("Players").observe(.childRemoved, with: { (data) in
              if let postDict = data.value as? Dictionary<String, AnyObject> {
                //updated Message or User
                let message = postDict["ortcmsg"] as! String
                let acc_type = postDict["accountType"] as? String ?? "facebook"
                let finalMessage = message.replacingOccurrences(of: "joined", with: "left")
                self.checkMessageAction(finalMessage, acc_type)
            }
        })
        ref.child(playGameData.gameId).child("Group_chat").observe(.childAdded, with: { (data) in
             if let postDict = data.value as? Dictionary<String, AnyObject> {
                //Added New Message or User
                let message = postDict["message"] as! String
                print(message)
                let finalMessage = UserDefaultsHandler.isEncodingEnable == "false" ? message.decodeUrl() ?? message : message.fromBase64() ?? message
                let userId = postDict["Uid"] as! String
                let name = postDict["Username"] as! String
                self.addToChatList(userId, name,finalMessage)
               // let customMessage =  String(format: ConstantModel.chatFormat.publicMessage, userId, name, finalMessage.replacingOccurrences(of: "+", with: " "))
                //self.checkMessageAction(customMessage)
//                self.addToChatList(userId, name,finalMessage.replacingOccurrences(of: "+", with: " ") )
                if self.isChatOpen {
                    self.reloadTable()
                } else {
                    //Show Chat Count
                    self.chatCountButton.setTitle(String(self.chatCount), for: .normal)
                    self.chatCountButton.isHidden = self.chatCount == 0 ? true : false
                }
            }
        })
        ref.child(playGameData.gameId).child("claimresult").observe(.childAdded, with: { (data) in
            if let postDict = data.value as? Dictionary<String, AnyObject> {
                //Added New Message or User
                let message = postDict["claim_msg"] as! String
                self.checkMessageAction(message, "")
            }
        })
    }
    
}
