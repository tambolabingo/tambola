//
//  GameChatExtension.swift
//  Tambola Bingo
//
//  Created by signity on 23/08/18.
//  Copyright © 2018 signity. All rights reserved.
//
import RealtimeMessaging_iOS

// MARK: -  Extension For ORTC game Chat  
extension PlayGameViewController {
    
// : OrtcClientDelegate{
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.chatTopConst.constant = 30 - keyboardSize.height
            self.chatBottomConst.constant = 50 + keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    @objc func keyboardWillHide(notification: NSNotification){
            self.chatTopConst.constant = 30
            self.chatBottomConst.constant = 30
            self.view.layoutIfNeeded()
    }
//    func configOrtcClient(){
//        ortc = OrtcClient.ortcClient(withConfig: self) as? OrtcClient
//        ortc!.clusterUrl = "https://ortc-developers.realtime.co/server/ssl/2.1/"
//        ortc!.connect("ZXrgWk", authenticationToken: "5dd602551c2243d8a554a3b20ee81e38")
//        let name = (user?.fName)! + " " + (user?.lName)!
//        ortc!.connectionMetadata = String(format: "%@-%@", name, (user?.userId)!)
//        ortc!.connectionTimeout = 5
//        ortc!.enableHeartbeat()
//        ortc!.setHeartbeatFails(1)
//        ortc!.setHeartbeatTime(10)
//    }
    func reloadTable(){
        self.chatDetailTbl.reloadData()
        if self.messageListArr.count > 1{
            self.chatDetailTbl.scrollToLastCell(animated: true)
        }
    }
    @IBAction func sendMessageButtonAction(){
        if messageTxtField.text!.isStringEmpty(){
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.emptyMessageError, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        }else {
            if isOrtcConnected {
                let data: Data? = messageTxtField.text?.data(using: String.Encoding(rawValue: String.Encoding.nonLossyASCII.rawValue))
                var valueUnicode = ""
                if let aData = data {
                    valueUnicode = String(data: aData, encoding: .utf8)!
                }
                 //Game Group Chat Message
                playGameData.isPublic ? self.sendDataToFirbase(playGameData.gameId, chatFormat: ConstantModel.chatFormat.publicMessage, Message: valueUnicode, isFrom: "Chat") : self.sendDataToFirbase(playGameData.gameId, chatFormat: ConstantModel.chatFormat.privateMessage, Message: valueUnicode, isFrom: "Chat")
//                playGameData.isPublic ? self.sendToORTCWithData(playGameData.gameId, chatFormat: ConstantModel.chatFormat.publicMessage, Message: valueUnicode) : self.sendToORTCWithData(playGameData.gameId, chatFormat: ConstantModel.chatFormat.privateMessage, Message: valueUnicode)
                
                messageTxtField.text = ""
            } else {
                EventManager.showAlertWithAction(alertMessage: ConstantModel.api.WeakInternet, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        }
    }
  
    func checkMessageAction(_ messageStr: String,_ acc_type: String){
        let compOneArr = messageStr.components(separatedBy: "/")
        switch compOneArr[0] {
        case "pm":
            //Public Message
            self.addToList(compOneArr[1])
            break
        case "m":
            //Private Message
            self.addToList(compOneArr[1])
            break
        case "a":
            //Quit Or Joined The game Message
            self.showMessageAlert(compOneArr[1], acc_type)
            break
        case "b":
            //User Blocked Message
            self.showMessageAlert(compOneArr[1], acc_type)
            break
        case "c":
            //boogied or Claim Message "b/%@:%@;%@"
            let compTwoArr = compOneArr[1].components(separatedBy: ":")
            let compThreeArr = compTwoArr[1].components(separatedBy: ";")
            //Add to score board list insted of alert
            if compThreeArr[1] == "got boogied" {
                let strMessage = compThreeArr[0] + " " + compThreeArr[1]
                self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
                self.showInstruction()
                self.showClaimMessage(message: strMessage)
            } else {
               self.checkClaimMessage(Name: compThreeArr[0], claimOption: compThreeArr[1], userId: compTwoArr[0])
            }
            
//            compThreeArr[1] == "got boogied" ? self.showClaimMessage(message: compThreeArr[0] + " " + compThreeArr[1]) : self.checkClaimMessage(Name: compThreeArr[0], claimOption: compThreeArr[1], userId: compTwoArr[0])
            break
        default:
            
            break
        }
        
    }
    func addToPlayerList(_ userID: String, UserName: String, accType: String){
        let imgUrl = (accType == "facebook" || accType == "") ? String(format:ConstantModel.fbImageURL, userID) : ""
        let player = PlayersDataModel(imgUrl, UserName, userID, false)
        //Check if exist
        let index = playGameData.players.firstIndex(where: { (item) -> Bool in
            item.playerId == userID
        })
        index == nil ? playGameData.players.append(player) : nil
        self.userCountButton.setTitle(String(playGameData.players.count), for: .normal)
        self.playersCollectionView.reloadData()
    }
    func removerPlayer(_ UserId: String){
        let index = playGameData.players.firstIndex(where: { (item) -> Bool in
            item.playerId == UserId
        })
        index != nil ? (_ = playGameData.players.remove(at: index!)) : nil
        self.userCountButton.setTitle(String(playGameData.players.count), for: .normal)
        self.playersCollectionView.reloadData()
    }
    func addToList(_ messageData: String){
        let compTwoArr = messageData.components(separatedBy: ":")
        let compThreeArr = compTwoArr[1].components(separatedBy: ";")
        let message = MessageModel()
        message.messageId = compTwoArr[0]
        message.authorName = compThreeArr[0]
        message.message = compThreeArr[1]
        message.messageType = compTwoArr[0] == user?.userId ? "selfOwner" : "Recived"
        self.messageListArr.append(message)
        self.chatCount = self.chatCount + 1
    }
    func addToChatList(_ messageId: String, _ authorName: String, _ messageData: String){
           let message = MessageModel()
           message.messageId = messageId
           message.authorName = authorName
           message.message = messageData
           message.messageType = messageId == user?.userId ? "selfOwner" : "Recived"
           self.messageListArr.append(message)
           self.chatCount = self.chatCount + 1
       }
    func showMessageAlert(_ messageData: String,_ acc_type: String){
        let compTwoArr = messageData.components(separatedBy: ":")
        let compThreeArr = compTwoArr[1].components(separatedBy: ";")
        var message = ""
        let userId = compTwoArr[0]
        let name = compTwoArr[1]
        switch compThreeArr[1] {
        case "joined":
            message = " joined the game"
            self.addToPlayerList(userId, UserName: name, accType: acc_type)
            break
        case "left":
            message = " left the game"
            self.removerPlayer(userId)
            break
        default:
            message = " has been blocked."
            break
        }
        //Append the message to score board
//        self.showClaimMessage(message: compThreeArr[0] + message)
        let strMessage = compThreeArr[0] + " " + message
        self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
        self.showInstruction()
    }
   
    
}
extension PlayGameViewController: UITableViewDelegate, UITableViewDataSource{
    // MARK: -  UITableView Delegate 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = messageListArr[indexPath.row]
        if model.messageType == "selfOwner"{
            let cell = chatDetailTbl.dequeueReusableCell(withIdentifier: "messageSend", for: indexPath) as? messageSendRecivedCell
            cell?.MessageCellSetUp(messageListArr[indexPath.row], isRecived: false)
            return cell!
        } else {
            let cell = chatDetailTbl.dequeueReusableCell(withIdentifier: "messageRecived", for: indexPath) as? messageSendRecivedCell
            cell?.MessageCellSetUp(messageListArr[indexPath.row], isRecived: true)
            return cell!
        }
    }
}
/*
//ORTC Delegates and Send action
extension PlayGameViewController{
    
    func sendToORTCWithData(_ channel: String, chatFormat:String, Message: String)
      {
          let name = (user?.fName)! + " " + (user?.lName)!
          let customMessage =  String(format: chatFormat,(user?.userId)!, name, Message)
          ortc?.send(channel, message: customMessage)
      }
    // MARK: -  ORTC Delegate functions 
       func onConnected(_ ortc: OrtcClient!) {
           isOrtcConnected = true
           ortc?.subscribe(playGameData.gameId, subscribeOnReconnected: true, onMessage: { (ortcClient, channel, message) in
               print("Message Recived and Send: ")
               print(message ?? "No message Available")
               self.checkMessageAction(message!)
               if self.isChatOpen {
                   self.reloadTable()
               } else {
                   //Show Chat Count
                   self.chatCountButton.setTitle(String(self.chatCount), for: .normal)
                   self.chatCountButton.isHidden = self.chatCount == 0 ? true : false
               }
           })
       }
       
       func onDisconnected(_ ortc: OrtcClient!) {
           
       }
       
       func onSubscribed(_ ortc: OrtcClient!, channel: String!) {
        //Game Join Message Players
           self.sendToORTCWithData(playGameData.gameId, chatFormat: ConstantModel.chatFormat.quitJoinMessage, Message: "joined")
       }
       
       func onUnsubscribed(_ ortc: OrtcClient!, channel: String!) {
           ortc.disconnect()
       }
       
       func onException(_ ortc: OrtcClient!, error: Error!) {
           isOrtcConnected = false
           print(error.localizedDescription)
       }
       
       func onReconnecting(_ ortc: OrtcClient!) {
           isOrtcConnected = false
       }
       
       func onReconnected(_ ortc: OrtcClient!) {
           isOrtcConnected = true
       }
       
}
*/
