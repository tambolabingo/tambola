//
//  PlayGameCollectionExtension.swift
//  Tambola Bingo
//
//  Created by signity on 29/08/18.
//  Copyright © 2018 signity. All rights reserved.
//
import UIKit

extension PlayGameViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    // MARK: -  UICollectionView Reload Cell 
    func reloadCollectionCell(cellNum: Int,isMain: Bool){
        let indexPath = IndexPath(item: (cellNum - 1), section: 0)
        let cell = scoresCollectionView.cellForItem(at: indexPath)
        cell?.contentView.backgroundColor = isMain ? UIColor.red : UIColor.blue
    }
    // MARK: -  UICollectionView Delegate, DelegateFlowLayout and Data Source 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case self.scoresCollectionView:
            //Create 10 rows and 10 coloums
            //Anouncement DashBoard
            let cellWidth   = self.scoresCollectionView.frame.width / 10
            let cellheight  = self.scoresCollectionView.frame.height / 10
            return CGSize(width: cellWidth, height: cellheight)
        case self.playersCollectionView:
            //Players Collection
            //Create 2 coloums
            let cellWidth   = self.playersCollectionView.frame.width / 2
            return CGSize(width: cellWidth - 2, height: 80)
        case self.ticketCollectionView:
            //Create 3 rows and 9 coloums
            //Tickect Collection
            let cellWidth   = self.ticketCollectionView.frame.width / 9
            let cellheight  = self.ticketCollectionView.frame.height / 3
            return CGSize(width: cellWidth - 1, height: cellheight)
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.scoresCollectionView:
            //fixed total game numbers cells
            return 90
        case self.playersCollectionView:
            // Get from array total number of players
            return playGameData.players.count
        case self.ticketCollectionView:
            //Fixed ticket cells
            return 27
        default:
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.scoresCollectionView:
            //Anouncement DashBoard
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "scoreCollectionViewCell", for: indexPath) as? scoreCollectionViewCell
            cell?.contentView.layer.borderWidth = 0.5
            cell?.contentView.layer.borderColor = UIColor.white.cgColor
            cell?.lblNumber.text = String(self.score)
            cell?.lblNumber.font = (cell?.contentView.frame.size.width)! <= CGFloat(20.0) ?  cell?.lblNumber.font.withSize(6) : cell?.lblNumber.font.withSize(9)
            let seqVal = self.anouncedNumberArray.filter{$0 == String(self.score)}
            cell?.contentView.backgroundColor = seqVal.count == 0 ? UIColor.clear : UIColor.blue
            score += 1
            // cell background color change pending depend on number anounced
            return cell!
        case self.playersCollectionView:
            //Players Collection
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playersCollectionViewCell", for: indexPath) as? playersCollectionViewCell
            //Players detail from API
            cell?.lblName.text = playGameData.players[indexPath.row].name
            let imageURL = playGameData.players[indexPath.row].image_url
            cell?.imgPlayer.sd_setImage(with: URL.init(string: imageURL), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
            })
            return cell!
        default:
            //Tickect Collection
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TicketCollectionViewCell", for: indexPath) as? TicketCollectionViewCell
            //Pending depend on logic
            let ticketNumber = myTicketsArray.ticketsArray[self.selectedTicket][indexPath.row].number
            let ticketState = myTicketsArray.ticketsArray[self.selectedTicket][indexPath.row].isCross
            let ticketTag = myTicketsArray.ticketsArray[self.selectedTicket][indexPath.row].tag
            
            cell?.tag = Int(ticketTag)!
            cell?.lblNumber.text = ticketNumber == "0" ? "" : ticketNumber
            cell?.imgCross.isHidden = ticketNumber == "0" ? ticketState : !ticketState
            return cell!
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Check for boggied
        if collectionView == ticketCollectionView && !isBoggied {
            let ticketNumber = myTicketsArray.ticketsArray[self.selectedTicket][indexPath.row].number
            if myTicketsArray.ticketsArray[self.selectedTicket][indexPath.row].isCross && ticketNumber != "0"{
                    myTicketsArray.ticketsArray[self.selectedTicket][indexPath.row].isCross = false
                    let selectedArr = crossedSequenceArray[self.selectedTicket]
                    let index = selectedArr.firstIndex(where: { (item) -> Bool in
                        item == ticketNumber
                    })
                    index != nil ? _ = crossedSequenceArray[self.selectedTicket].remove(at: index!) : nil
                self.ticketCollectionView.reloadItems(at: [indexPath])
            } else if ticketNumber != "0"{
                myTicketsArray.ticketsArray[self.selectedTicket][indexPath.row].isCross = true
                crossedSequenceArray[self.selectedTicket].append(ticketNumber)
                self.ticketCollectionView.reloadItems(at: [indexPath])
            }
            
        } else if collectionView == playersCollectionView && !playGameData.isPracticeGame{
            self.showPlayerDetailView(playGameData.players[indexPath.row])
        }
    }
    func showPlayerDetailView(_ playerData: PlayersDataModel){
        playerDetail.username = playerData.name
        playerDetail.img_url = playerData.image_url
        playerDetail.user_id = playerData.playerId
        //Add Values for BlockUnblock
//        playerDetail.user_blocked = playerData.blockStatus
        playerDetail.gameType = playGameData.isPublic ? "Public" : "Private"
        self.getPlayerDetailAPI(parameters:GetParamModel.shared.getUserRecordsParam(userId: playerData.playerId)!)
    }
    
}
