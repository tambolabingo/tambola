//
//  PlayGamePlayExtension.swift
//  Tambola Bingo
//
//  Created by signity on 29/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
extension PlayGameViewController {
// MARK: -  Create Cross Sequence for AutoCut enabled User 
    func createCrossSequence(_ anunceNum:String){
        for i in 0..<playGameData.totalTickets {
            if playGameData.ticketStatusArray[i] {
                for cell in 0..<27{
                    let num = myTicketsArray.ticketsArray[i][cell].number
                    if num == anunceNum && !(self.crossedSequenceArray[i].contains(anunceNum)){
                        self.crossedSequenceArray[i].append(anunceNum)
                        myTicketsArray.ticketsArray[i][cell].isCross = true
                        let indexPath = IndexPath(item: cell, section: 0)
                        self.ticketCollectionView.reloadItems(at: [indexPath])
                    }
                }
            }
        }
    }
// MARK: -  Start and Stop Game Claim API Timer Function 
func startGameClaimTimer(){
    self.stopGameClaimTimer()
    self.gameClaimTimer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.claimStatus), userInfo: nil, repeats: true)
}
@objc func claimStatus(){
    self.getGameClaimsAPI(isSilentCall: true)
}
func stopGameClaimTimer(){
    if self.gameClaimTimer != nil && (self.gameClaimTimer?.isValid)! {
        self.gameClaimTimer?.invalidate()
        print("Game Claim Timer Stop")
    }
}
// MARK: -  Anounce Numbers and Show On Board Functions For Privat/Public/Practice game 
//Aniounce Number Function
func anounceNumbers(){
    self.updateAnounceBoard()
    self.startTimer()
}
//Start Timer for Anounce Numbers
func startTimer(){
    self.stopSequenceTimer()
    self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.updateAnounceBoard), userInfo: nil, repeats: true)
}
//Stop Timer for Anuonce number Finish
@objc func stopSequenceTimer(){
    if self.timer != nil && (self.timer?.isValid)! {
        self.timer?.invalidate()
        print("Timer Stop")
    }
}
//Update Anounce Number on Board Buttons and create Anounce Number array
@objc func updateAnounceBoard () {
    //Fix the crash
    let seqVal = playGameData.sequenceArray[timerVal]
    self.anouncedNumberArray.append(seqVal)
//    let fileName = PlayGameManager.shared.getAnounceFileName(AnounceNum: seqVal)
    self.playNumberFiles(Int(seqVal)!)
//    self.playMp3File(fileName)
    self.anounceMainBtn.setTitle(seqVal, for: .normal)
    if playGameData.isPracticeGame {
        self.createCrossSequenceDummyUsers(anunceNum: seqVal)
        self.autoClaimForDummyUsers()
    }
    if isAutoCut {
        self.createCrossSequence(seqVal)
    }
    var firstVal = ""
    var secondVal = ""
    var ThirdVal = ""
    var fourthVal = ""
    switch timerVal {
    case 0:
        //All empty
        break
    case 1:
        firstVal = playGameData.sequenceArray[timerVal - 1]
        break
    //three empty
    case 2:
        firstVal = playGameData.sequenceArray[timerVal - 1]
        secondVal = playGameData.sequenceArray[timerVal - 2]
        break
    //two empty
    case 3:
        firstVal = playGameData.sequenceArray[timerVal - 1]
        secondVal = playGameData.sequenceArray[timerVal - 2]
        ThirdVal = playGameData.sequenceArray[timerVal - 3]
        break
    //one empty
    default:
        firstVal = playGameData.sequenceArray[timerVal - 1]
        secondVal = playGameData.sequenceArray[timerVal - 2]
        ThirdVal = playGameData.sequenceArray[timerVal - 3]
        fourthVal = playGameData.sequenceArray[timerVal - 4]
        break
        //Fill all
    }
    self.anounceOneBtn.setTitle(firstVal, for: .normal)
    self.anounceTwoBtn.setTitle(secondVal, for: .normal)
    self.anounceThreeBtn.setTitle(ThirdVal, for: .normal)
    self.anounceFourBtn.setTitle(fourthVal, for: .normal)
    if firstVal == ""{
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            self.reloadCollectionCell(cellNum: Int(seqVal)!, isMain: true)
        })
    } else {
        self.reloadCollectionCell(cellNum: Int(playGameData.sequenceArray[timerVal])!, isMain: true)
        self.reloadCollectionCell(cellNum: Int(playGameData.sequenceArray[timerVal - 1])!, isMain: false)
    }
    timerVal == 89 ? self.stopSequenceTimer() : (timerVal += 1)
}
// MARK: -  Create and Show Public/private Game Claim Messages Function 
    func checkClaimMessage(Name: String,claimOption: String, userId:String){
    switch claimOption {
    case ConstantModel.claim.claimEarlyFive:
        self.claimearlyfiveAction(Name)
        break
    case ConstantModel.claim.claimAPIEarlyFive:
        self.claimearlyfiveAction(Name)
        break
    case ConstantModel.claim.claimLuckySeven:
        self.claimlucksevenAction(Name)
        break
    case ConstantModel.claim.claimAPILuckySeven:
        self.claimlucksevenAction(Name)
        break
    case ConstantModel.claim.claimTopLine:
        self.claimtoplineAction(Name)
        break
    case ConstantModel.claim.claimAPITopLine:
        self.claimtoplineAction(Name)
        break
    case ConstantModel.claim.claimMiddleLine:
        self.claimSecondlineAction(Name)
        break
    case ConstantModel.claim.claimAPIMiddleLine:
        self.claimSecondlineAction(Name)
        break
    case ConstantModel.claim.claimBottomLine:
        self.claimthirdlineAction(Name)
        break
    case ConstantModel.claim.claimAPIBottomLine:
        self.claimthirdlineAction(Name)
        break
    case ConstantModel.claim.claimPyramid:
        self.claimpyraidAction(Name)
        break
    case ConstantModel.claim.claimAPIPyramid:
        self.claimpyraidAction(Name)
        break
    case ConstantModel.claim.claimCircle:
        self.claimcircleAction(Name)
        break
    case ConstantModel.claim.claimAPICircle:
        self.claimcircleAction(Name)
        break
    case ConstantModel.claim.claimCorner:
        self.claimcornerAction(Name)
        break
    case ConstantModel.claim.claimAPICorner:
        self.claimcornerAction(Name)
        break
    case ConstantModel.claim.claimCenter:
        self.claimcenterAction(Name)
        break
    case ConstantModel.claim.claimAPICenter:
        self.claimcenterAction(Name)
        break
    case ConstantModel.claim.claimFullHouse:
        self.claimfullHouseAction(Name, userId)
        break
    case ConstantModel.claim.claimAPIFullHouse:
        self.claimfullHouseAction(Name, userId)
        break
    default:
        break
    }
}
    func claimtoplineAction(_ Name: String){
        //1
        if !self.claimTopLine {
            self.playClaimFiles(10)
            self.showClaimMessage(message: Name + " Completed " + ConstantModel.claim.claimTopLine)
            self.claimTopLine = true
            self.btnClaimTopLine.setImage(UIImage(named: ConstantModel.btnImgName.btnCtopLine), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimTopLine)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>"  + strMessage
            self.showInstruction()
        }
    }
    func claimSecondlineAction(_ Name: String){
        //2
        if !self.claimMiddleLine {
            self.playClaimFiles(11)
            self.showClaimMessage(message: Name + " Completed " + ConstantModel.claim.claimMiddleLine)
            self.claimMiddleLine = true
            self.btnClaimMiddleLine.setImage(UIImage(named: ConstantModel.btnImgName.btnCMiddleLine), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimMiddleLine)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            self.showInstruction()
        }
    }
    func claimthirdlineAction(_ Name: String){
        //3
        if !self.claimBottomLine {
            self.playClaimFiles(12)
            self.showClaimMessage(message: Name + " Completed " + ConstantModel.claim.claimBottomLine)
            self.claimBottomLine = true
            self.btnClaimBottomLine.setImage(UIImage(named: ConstantModel.btnImgName.btnCBottomLine), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimBottomLine)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            self.showInstruction()
        }
    }
    func claimcenterAction(_ Name: String){
        //4
        if !self.claimCenter {
            self.playClaimFiles(3)
            self.showClaimMessage(message: Name + " Completed " + ConstantModel.claim.claimCenter)
            self.claimCenter = true
            self.btnClaimCenter.setImage(UIImage(named: ConstantModel.btnImgName.btnCCenter), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML , Name, ConstantModel.claim.claimCenter)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            self.showInstruction()
        }
    }
    func claimcircleAction(_ Name: String){
        //5
        if !self.claimCircle {
            self.playClaimFiles(4)
            self.showClaimMessage(message: Name + " Completed " + ConstantModel.claim.claimCircle)
            self.claimCircle = true
            self.btnClaimCircle.setImage(UIImage(named: ConstantModel.btnImgName.btnCCircle), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimCircle)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            self.showInstruction()
        }
    }
    func claimearlyfiveAction(_ Name: String){
        //6
        if !self.claimEarlyFive {
            self.playClaimFiles(7)
            self.showClaimMessage(message: Name + " Completed " + ConstantModel.claim.claimEarlyFive)
            self.claimEarlyFive = true
            self.btnClaimEarlyFive.setImage(UIImage(named: ConstantModel.btnImgName.btnCEarlyFive), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimEarlyFive)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            self.showInstruction()
        }
    }
    func claimlucksevenAction(_ Name: String){
        //7
        if !self.claimLuckySeven {
            self.playClaimFiles(5)
            self.showClaimMessage(message: Name + " Completed " + ConstantModel.claim.claimLuckySeven)
            self.claimLuckySeven = true
            self.btnClaimLuckySeven.setImage(UIImage(named: ConstantModel.btnImgName.btnCLuckySeven), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimLuckySeven)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            self.showInstruction()
        }
    }
    func claimpyraidAction(_ Name: String){
        //8
        if !self.claimPyramid {
            self.playClaimFiles(6)
            self.showClaimMessage(message: Name + " Completed " + ConstantModel.claim.claimPyramid)
            self.claimPyramid = true
            self.btnClaimPyramid.setImage(UIImage(named: ConstantModel.btnImgName.btnCPyramid), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimPyramid)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            self.showInstruction()
        }
    }
    func claimcornerAction(_ Name: String){
        //9
        if !self.claimCorner {
                   self.playClaimFiles(8)
                   self.showClaimMessage(message: Name + " Completed " + ConstantModel.claim.claimCorner)
                   self.claimCorner = true
                   self.btnClaimCorner.setImage(UIImage(named: ConstantModel.btnImgName.btnCFourCorner), for: .normal)
                   let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimCorner)
                   self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
                   self.showInstruction()
               }
    }
    func claimfullHouseAction(_ Name: String,_ id: String){
        //10
        self.playClaimFiles(9)
        if !self.claimFullHouse {
            self.claimFullHouse(name: Name, imgUrl: "", userId: id)
        }
    }
// MARK: -  Dummy User Auto Play Functions For Practice game 
// Create Cross Sequence for Dummy User checks
func createCrossSequenceDummyUsers(anunceNum: String){
    for player in playGameData.players {
        let contained = EventManager.checkAnounceNumber(ticket: player.userTicketsArray, anounceNum: anunceNum)
        if contained {
            player.crossedSequenceArray.append(anunceNum)
        }
    }
}

// Claim for Dummy User checks
func autoClaimForDummyUsers(){
    for player in playGameData.players {
        if player.crossedSequenceArray.count == 15 {
            let contain = claimFullHouse ? false : self.anouncedNumberArray.contains(player.crossedSequenceArray)
            contain ? self.claimFullHouse(name: player.name, imgUrl: player.image_url, userId: player.playerId) : nil
        } else if player.crossedSequenceArray.count >= 5 {
            let earlyFive = claimEarlyFive ? false : self.anouncedNumberArray.contains(player.crossedSequenceArray)
            let luckySeven = claimLuckySeven ? false : (player.crossedSequenceArray.count >= 7 ? self.anouncedNumberArray.contains(player.crossedSequenceArray): false )
            let containCircle = claimCircle ? false : self.checkArrayContainSubArray(mainArray: player.crossedSequenceArray, subArray: player.circleArray)
            let containTopLine = claimTopLine ? false : self.checkArrayContainSubArray(mainArray: player.crossedSequenceArray, subArray: player.topLineArray)
            let containMiddelLine = claimMiddleLine ? false : self.checkArrayContainSubArray(mainArray: player.crossedSequenceArray, subArray: player.middleLineArray)
            let containBottomLine = claimBottomLine ? false : self.checkArrayContainSubArray(mainArray: player.crossedSequenceArray, subArray: player.thirdLineArray)
            let containPyramid = claimPyramid ? false : self.checkArrayContainSubArray(mainArray: player.crossedSequenceArray, subArray: player.pyramidArray)
            let containFourCorner = claimCorner ? false : self.checkArrayContainSubArray(mainArray: player.crossedSequenceArray, subArray: player.cornerArray)
            self.getCliamHTMLString(earlyFive: earlyFive, TopLine: containTopLine, MiddelLine: containMiddelLine, BottomLine: containBottomLine, Pyramid: containPyramid, FourCorner: containFourCorner, luckySeven: luckySeven, circle: containCircle, Name: player.name, imageUrl: player.image_url, userId: player.playerId)
        }
        let containCenter = claimCenter ? false : self.checkArrayContainSubArray(mainArray: player.crossedSequenceArray, subArray: [player.middleLineArray[2]])
        containCenter ? self.claimCenter(name: player.name, imgUrl: player.image_url, userId: player.playerId) : nil
        }
    }
    
    // MARK: -  Create Claim Array's and Show Game Detail Instructions Functions 
    func getClaimData(){
        topLineArray.removeAll()
        middleLineArray.removeAll()
        thirdLineArray.removeAll()
        cornerArray.removeAll()
        for i in 0..<9 {
            let num =  myTicketsArray.ticketsArray[self.selectedTicket][i].number
            if num != "0"{
                topLineArray.append(num)
            }
        }
        for i in 9..<18 {
            let num =  myTicketsArray.ticketsArray[self.selectedTicket][i].number
            if num != "0"{
                middleLineArray.append(num)
            }
        }
        for i in 18..<27 {
            let num =  myTicketsArray.ticketsArray[self.selectedTicket][i].number
            if num != "0"{
                thirdLineArray.append(num)
            }
        }
        
        let countOne = topLineArray.count
        let countLast = thirdLineArray.count
        cornerArray = [topLineArray[0], topLineArray[countOne - 1], thirdLineArray[0], thirdLineArray[countLast - 1]]
        pyramidArray = [topLineArray[2], middleLineArray[1], middleLineArray[3], thirdLineArray[0], thirdLineArray[2], thirdLineArray[4]]
        circleArray = [topLineArray[2], middleLineArray[1], middleLineArray[3], thirdLineArray[2]]
        self.showInstruction()
    }
    
    // MARK: -  Hide and Show Claim Price User Alert Action 
    func showClaimMessage(message: String){
        self.lblClaimMessage.text = "     " + message + "     "
        self.lblClaimMessage.isHidden = false
        self.messageTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideClaimMessages), userInfo: nil, repeats: true)
    }
    @objc func hideClaimMessages(){
        if self.messageTimer != nil && (self.messageTimer?.isValid)! {
            self.messageTimer?.invalidate()
            print("Message Timer Stop")
        }
        DispatchQueue.main.async {
            UIView.transition(with: self.lblClaimMessage, duration: 0.25, options: .transitionCrossDissolve, animations: {
                self.lblClaimMessage.isHidden = true
            }, completion: nil)
        }
    }
    // MARK: -  Calculate Total winning amount by player 
    func totalClaimAmount(price: String?){
        let amount = Int(price ?? "0")
        self.winningAmount = amount! + self.winningAmount
    }
    // MARK: -  Create Result Data and Navigate To Result Screen Action 
    func showResultView(){
        if !self.playGameData.isPracticeGame{
             //Game Leave Message Players
            self.sendDataToFirbase(self.playGameData.gameId, chatFormat: ConstantModel.chatFormat.quitJoinMessage, Message: "left", isFrom: "Players")
//            self.sendToORTCWithData(self.playGameData.gameId, chatFormat: ConstantModel.chatFormat.quitJoinMessage, Message: "left")
//            self.ortc?.disconnect()
//            self.ortc?.disableHeartbeat()
        }
        self.stopSequenceTimer()
        self.stopGameClaimTimer()
        self.playNumberFiles(0)
//        self.playMp3File(PlayGameManager.gameOver)
        playGameData.isPracticeGame ? self.performSegue(withIdentifier: "GameResultView", sender: true) : self.houseFullEndGameAPI()
    }
    func createResultDataModel(resultModel:ResultDataModel, Name: String, claimPrice: String){
        self.showClaimMessage(message: Name + " Completed " + claimPrice)
        self.showInstruction()
        if resultData.count > 0 {
            var winnerContain = false
            for winner in resultData {
                if winner.name == Name {
                    winnerContain = true
                    winner.resultArray.append(claimPrice)
                }
            }
            winnerContain ? nil : resultData.append(resultModel)
        } else {
            resultData.append(resultModel)
        }
    }
}
