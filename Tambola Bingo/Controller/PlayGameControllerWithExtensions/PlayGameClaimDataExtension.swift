//
//  PlayGameClaimDataExtension.swift
//  Tambola Bingo
//
//  Created by signity on 29/08/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
extension PlayGameViewController {
    func showInstruction(){
        self.scoreBoardLoader.startAnimating()
        self.scoreBoardLoader.isHidden = false
        let gameName = String(format: "<font size=2 color=#FFFF00>Game Name: %@<br></font>", playGameData.gameName)
        let instructionStr = "<font size=2 color=#FFFF00>Instructions for ticket:<br>1.</font><font size=2 color=white> Cut your ticket for number occures on dashboard.</font><font size=2 color=#FFFF00><br>2.</font><font size=2 color=white> Any first five number will make</font><font size=2 color=#FFFF00> Early five.<br>3.</font><font size=2 color=white> Any first seven number will make</font><font size=2 color=#FFFF00> Lucky Seven.</font>"
        let centerStr = String(format: "<font size=2 color=#FFFF00>4.</font><font size=2 color=white> %@ will make</font><font size=2 color=#FFFF00> Center.</font>", middleLineArray[2])
        let cornerStr = String(format: "<font size=2 color=#FFFF00>5.</font><font size=2 color=white> %@ will make</font><font size=2 color=#FFFF00> 4 Corner.</font>", cornerArray.joined(separator: ", "))
        let topLineStr = String(format: "<font size=2 color=#FFFF00>6.</font><font size=2 color=white> %@ will make</font><font size=2 color=#FFFF00> Top line.</font>", topLineArray.joined(separator: ", "))
        let middlelineStr = String(format: "<font size=2 color=#FFFF00>7.</font><font size=2 color=white> %@ will make</font><font size=2 color=#FFFF00> Middle line.</font>", middleLineArray.joined(separator: ", "))
        let thirdLineStr = String(format: "<font size=2 color=#FFFF00>8.</font><font size=2 color=white> %@ will make</font><font size=2 color=#FFFF00> Bottom line.</font>", thirdLineArray.joined(separator: ", "))
        let pyramidStr = String(format: "<font size=2 color=#FFFF00>9.</font><font size=2 color=white> %@ will make</font><font size=2 color=#FFFF00> Pyramid.</font>", pyramidArray.joined(separator: ", "))
        let circleStr = String(format: "<font size=2 color=#FFFF00>10.</font><font size=2 color=white> %@ will make</font><font size=2 color=#FFFF00> Circle.</font>", circleArray.joined(separator: ", "))
        let fullHouseStr = "<font size=2 color=#FFFF00>11.</font><font size=2 color=white> All numbers will make</font><font size=2 color=#FFFF00> House full.</font>"
        self.instructionHTMLStr = gameName + "<br>" +  instructionStr + "<br>" + centerStr + "<br>" + cornerStr + "<br>" + topLineStr + "<br>" + middlelineStr + "<br>" + thirdLineStr + "<br>" + pyramidStr + "<br>" + circleStr + "<br>" + fullHouseStr
        let finalHtmlStr =  self.instructionHTMLStr + self.claimPriseHTMLStr
        let htmlWithBody = "<html><body><p>" + finalHtmlStr + "</p></body></html>"
//        self.gameInstructionWebView.loadHTMLString(htmlWithBody, baseURL: nil)
        self.gameInstTextView.attributedText = htmlWithBody.htmlToAttributedString
//        let location = self.gameInstTextView.text.count - 1
        if claimPriseHTMLStr == "" {
            claimPriseHTMLStr = "<br>"
            let top = NSMakeRange(0, 1)
            self.gameInstTextView.scrollRangeToVisible(top)
        }
        self.scoreBoardLoader.stopAnimating()
        self.scoreBoardLoader.isHidden = true
    }
    // MARK: -  Create and Show Claim Messages Action 
    func getCliamHTMLString(earlyFive:Bool, TopLine:Bool, MiddelLine:Bool, BottomLine:Bool, Pyramid:Bool, FourCorner: Bool,luckySeven: Bool,circle: Bool, Name: String, imageUrl: String, userId: String ){
        if earlyFive {
            self.claimEarlyFive = true
            self.btnClaimEarlyFive.setImage(UIImage(named: ConstantModel.btnImgName.btnCEarlyFive), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimEarlyFive)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            let WinnerModel = ResultDataModel(userId, imageUrl, Name,ConstantModel.claim.claimEarlyFive)
            self.createResultDataModel(resultModel: WinnerModel, Name: Name, claimPrice: ConstantModel.claim.claimEarlyFive)
        }
        if luckySeven {
            self.claimLuckySeven = true
            self.btnClaimLuckySeven.setImage(UIImage(named: ConstantModel.btnImgName.btnCLuckySeven), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimLuckySeven)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            let WinnerModel = ResultDataModel(userId, imageUrl, Name,ConstantModel.claim.claimLuckySeven)
            self.createResultDataModel(resultModel: WinnerModel, Name: Name, claimPrice: ConstantModel.claim.claimLuckySeven)
        }
        if circle {
            self.claimCircle = true
            self.btnClaimCircle.setImage(UIImage(named: ConstantModel.btnImgName.btnCCircle), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimCircle)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
            let WinnerModel = ResultDataModel(userId, imageUrl, Name,ConstantModel.claim.claimCircle)
            self.createResultDataModel(resultModel: WinnerModel, Name: Name, claimPrice: ConstantModel.claim.claimCircle)
        }
        if TopLine {
            self.claimTopLine = true
            self.btnClaimTopLine.setImage(UIImage(named: ConstantModel.btnImgName.btnCtopLine), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimTopLine)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>"  +  strMessage
            let WinnerModel = ResultDataModel(userId, imageUrl, Name,ConstantModel.claim.claimTopLine)
            self.createResultDataModel(resultModel: WinnerModel, Name: Name, claimPrice: ConstantModel.claim.claimTopLine)
        }
        if MiddelLine {
            self.claimMiddleLine = true
            self.btnClaimMiddleLine.setImage(UIImage(named: ConstantModel.btnImgName.btnCMiddleLine), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimMiddleLine)
            self.claimPriseHTMLStr =  self.claimPriseHTMLStr + "<br>" + strMessage
            let WinnerModel = ResultDataModel(userId, imageUrl, Name,ConstantModel.claim.claimMiddleLine)
            self.createResultDataModel(resultModel: WinnerModel, Name: Name, claimPrice: ConstantModel.claim.claimMiddleLine)
        }
        if BottomLine {
            self.claimBottomLine = true
            self.btnClaimBottomLine.setImage(UIImage(named: ConstantModel.btnImgName.btnCBottomLine), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimBottomLine)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>"  +  strMessage
            let WinnerModel = ResultDataModel(userId, imageUrl, Name,ConstantModel.claim.claimBottomLine)
            self.createResultDataModel(resultModel: WinnerModel, Name: Name, claimPrice: ConstantModel.claim.claimBottomLine)
        }
        if Pyramid {
            self.claimPyramid = true
            self.btnClaimPyramid.setImage(UIImage(named: ConstantModel.btnImgName.btnCPyramid), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimPyramid)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>"  +  strMessage
            let WinnerModel = ResultDataModel(userId, imageUrl, Name,ConstantModel.claim.claimPyramid)
            self.createResultDataModel(resultModel: WinnerModel, Name: Name, claimPrice: ConstantModel.claim.claimPyramid)
        }
        if FourCorner {
            self.claimCorner = true
            self.btnClaimCorner.setImage(UIImage(named: ConstantModel.btnImgName.btnCFourCorner), for: .normal)
            let strMessage = String(format: ConstantModel.claim.claimPriceHTML, Name, ConstantModel.claim.claimCorner)
            self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>"  +  strMessage
            let WinnerModel = ResultDataModel(userId, imageUrl, Name,ConstantModel.claim.claimCorner)
            self.createResultDataModel(resultModel: WinnerModel, Name: Name, claimPrice: ConstantModel.claim.claimCorner)
        }
    }
    func claimFullHouse(name: String, imgUrl: String, userId: String){
        self.claimFullHouse = true
        self.btnClaimFullHouse.setImage(UIImage(named: ConstantModel.btnImgName.btnCFullHouse), for: .normal)
        let strMessage = String(format: ConstantModel.claim.claimPriceHTML, name, ConstantModel.claim.claimFullHouse)
        self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
        if playGameData.isPracticeGame {
            let WinnerModel = ResultDataModel(userId, imgUrl, name,ConstantModel.claim.claimFullHouse)
            self.createResultDataModel(resultModel: WinnerModel, Name: name, claimPrice: ConstantModel.claim.claimFullHouse)
        } else {
            self.showClaimMessage(message: name + " Completed " + ConstantModel.claim.claimFullHouse)
            self.showInstruction()
        }
        
        hideClose = true
        EventManager.showAlertWithAction(alertMessage: String(format:ConstantModel.claim.gameOverMessage, name), btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self) { action in
            //Game Completed
            self.showResultView()
        }
        hideClose = false
     
    }
    func claimCenter(name: String, imgUrl: String, userId: String){
        self.claimCenter = true
        self.btnClaimCenter.setImage(UIImage(named: ConstantModel.btnImgName.btnCCenter), for: .normal)
        let strMessage = String(format: ConstantModel.claim.claimPriceHTML , name, ConstantModel.claim.claimCenter)
        self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>" + strMessage
        let WinnerModel = ResultDataModel(userId, imgUrl, name,ConstantModel.claim.claimCenter)
        self.createResultDataModel(resultModel: WinnerModel, Name: name, claimPrice: ConstantModel.claim.claimCenter)
        //Game Completed
    }
    func showboggiesMessageAction(name: String, ClaimPrice: String){
        //Check Ticket and make boggied
        self.isBoggied = true
        self.selectedTicket == 0 ? (self.isTicketOneBoggied = true) : ((self.selectedTicket == 1) ? (self.isTicketTwoBoggied = true) : (self.isTicketThreeBoggied = true))
        //UpdateDB For boggied
        
        playGameData.gameType == "2" ? GameTicketsModel.shared.updateTicketData(playGameData.gameId, isTicketOneBoggied, isTicketTwoBoggied, isTicketThreeBoggied) : nil
        
        self.playClaimFiles(2)
//        self.playMp3File(PlayGameManager.boogie)
        if !playGameData.isPracticeGame {
            //Call API for claim with claim status 3(For Boogie)
            let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "3", numberSequence: "", claimName: ClaimPrice)
            //Boggied
            self.claimPriceAPI(parameters: params!, ClaimPrice: ClaimPrice, index: 2)
             //Game Claim price message
            self.sendDataToFirbase(self.playGameData.gameId, chatFormat: ConstantModel.chatFormat.claimMessage, Message: ConstantModel.claim.claimboogied, isFrom: "Claim")
//            self.sendToORTCWithData(self.playGameData.gameId, chatFormat: ConstantModel.chatFormat.claimMessage, Message: "got boogied")
        }
        let strMessage = String(format: ConstantModel.claim.boggiedHTML, name)
        self.claimPriseHTMLStr = self.claimPriseHTMLStr + "<br>"  +  strMessage
        self.showInstruction()
        let message = String(format:ConstantModel.claim.boggiesMessage, ClaimPrice )
        EventManager.showAlertWithAction(alertMessage: message, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        // Freeze the game after this
        self.btnClaimOptions.setImage(UIImage(named: "QuitGameButton"), for: .normal)
        self.claimViewHeight.constant =  0
        self.view.layoutIfNeeded()
    }
    @IBAction func checkClainData(sender: UIButton){
        var contained = false
        let userName = (UserDefaultsHandler.user?.fName)! + " " + (UserDefaultsHandler.user?.lName)!
        let userId = (UserDefaultsHandler.user?.userId)!
        switch sender.tag {
        case 1001:
            //Top Line
            if !claimTopLine {
                if playGameData.isPracticeGame {
                    contained = (self.checkArrayContainSubArray(mainArray: self.crossedSequenceArray[self.selectedTicket], subArray: self.topLineArray))
                    contained ? self.getCliamHTMLString(earlyFive: false, TopLine: true, MiddelLine: false, BottomLine: false, Pyramid: false, FourCorner: false, luckySeven: false, circle: false, Name: userName, imageUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimTopLine)
                } else {
                    contained = self.crossedSequenceArray[self.selectedTicket].contains(self.topLineArray)
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: topLineArray.joined(separator: ","), claimName: ConstantModel.claim.claimAPITopLine)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimTopLine, index: 10) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimTopLine)
                }
            }
            break
        case 1002:
            //Middle Line
            if !claimMiddleLine {
                if playGameData.isPracticeGame {
                    contained = (self.checkArrayContainSubArray(mainArray: self.crossedSequenceArray[self.selectedTicket], subArray: self.middleLineArray))
                    contained ? self.getCliamHTMLString(earlyFive: false, TopLine: false, MiddelLine: true, BottomLine: false, Pyramid: false, FourCorner: false, luckySeven: false, circle: false, Name: userName, imageUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimMiddleLine)
                } else {
                    contained = self.crossedSequenceArray[self.selectedTicket].contains(self.middleLineArray)
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: middleLineArray.joined(separator: ","), claimName: ConstantModel.claim.claimAPIMiddleLine)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimMiddleLine, index: 11) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimMiddleLine)
                }
            }
            break
        case 1003:
            //Bottom Line
            if !claimBottomLine {
                if playGameData.isPracticeGame {
                    contained = (self.checkArrayContainSubArray(mainArray: self.crossedSequenceArray[self.selectedTicket], subArray: self.thirdLineArray))
                    contained ? self.getCliamHTMLString(earlyFive: false, TopLine: false, MiddelLine: false, BottomLine: true, Pyramid: false, FourCorner: false, luckySeven: false, circle: false, Name: userName, imageUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimBottomLine)
                } else {
                    contained = self.crossedSequenceArray[self.selectedTicket].contains(self.thirdLineArray)
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: thirdLineArray.joined(separator: ","), claimName: ConstantModel.claim.claimAPIBottomLine)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimBottomLine, index: 12) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimBottomLine)
                }
            }
            break
        case 1004:
            //Center (center/3rd index number of middle line)
            if !claimCenter {
                if playGameData.isPracticeGame {
                    contained = (self.checkArrayContainSubArray(mainArray: self.crossedSequenceArray[self.selectedTicket], subArray: [self.middleLineArray[2]]))
                    contained ? self.claimCenter(name: userName, imgUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimCenter)
                } else {
                    contained = self.crossedSequenceArray[self.selectedTicket].contains([self.middleLineArray[2]])
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: self.middleLineArray[2], claimName: ConstantModel.claim.claimAPICenter)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimCenter, index: 3) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimCenter)
                }
            }
            
            break
        case 1005:
            //Circle (3rd index of top line 2nd and 4th index of middle line and 3rd index of bottom line)
            if !claimCircle {
                if playGameData.isPracticeGame {
                    contained = self.checkArrayContainSubArray(mainArray: self.crossedSequenceArray[self.selectedTicket], subArray: self.circleArray)
                    contained ? self.getCliamHTMLString(earlyFive: false, TopLine: false, MiddelLine: false, BottomLine: false, Pyramid: false, FourCorner: false, luckySeven: false, circle: true, Name: userName, imageUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimCircle)
                } else {
                    contained = self.crossedSequenceArray[self.selectedTicket].contains(self.circleArray)
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: circleArray.joined(separator: ","), claimName: ConstantModel.claim.claimAPICircle)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimCircle, index: 4) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimCircle)
                }
            }
            break
        case 1006:
            //Early Five
            if !claimEarlyFive {
                if playGameData.isPracticeGame {
                    contained = (self.crossedSequenceArray[self.selectedTicket].count >= 5 ? self.anouncedNumberArray.contains(self.crossedSequenceArray[self.selectedTicket]) : false)
                    contained ? self.getCliamHTMLString(earlyFive: true, TopLine: false, MiddelLine: false, BottomLine: false, Pyramid: false, FourCorner: false, luckySeven: false, circle: false, Name: userName, imageUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimEarlyFive)
                } else {
                    contained = (self.crossedSequenceArray[self.selectedTicket].count >= 5 ? true : false)
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: self.crossedSequenceArray[self.selectedTicket].joined(separator: ","), claimName: ConstantModel.claim.claimAPIEarlyFive)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimEarlyFive, index: 7) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimEarlyFive)
                }
            }
            break
        case 1007:
            //Four Corner
            if !claimCorner {
                if playGameData.isPracticeGame {
                    contained = self.checkArrayContainSubArray(mainArray: self.crossedSequenceArray[self.selectedTicket], subArray: self.cornerArray)
                    contained ? self.getCliamHTMLString(earlyFive: false, TopLine: false, MiddelLine: false, BottomLine: false, Pyramid: false, FourCorner: true, luckySeven: false, circle: false, Name: userName, imageUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimCorner)
                } else {
                    contained = self.crossedSequenceArray[self.selectedTicket].contains(self.cornerArray)
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: cornerArray.joined(separator: ","), claimName: ConstantModel.claim.claimAPICorner)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimCorner, index: 8) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimCorner)
                }
            }
            break
        case 1008:
            //  Pyramid (3rd index in top line/ 2nd & 4th index in middle line / 1st 3rd and 5th index in third line)
            if !claimPyramid {
                if playGameData.isPracticeGame {
                    contained = self.checkArrayContainSubArray(mainArray: self.crossedSequenceArray[self.selectedTicket], subArray: self.pyramidArray)
                    contained ? self.getCliamHTMLString(earlyFive: false, TopLine: false, MiddelLine: false, BottomLine: false, Pyramid: true, FourCorner: false, luckySeven: false, circle: false, Name: userName, imageUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimPyramid)
                } else {
                    contained = self.crossedSequenceArray[self.selectedTicket].contains(self.pyramidArray)
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: pyramidArray.joined(separator: ","), claimName: ConstantModel.claim.claimAPIPyramid)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimPyramid, index: 6) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimPyramid)
                }
            }
            break
        case 1009:
            //Lucky Seven
            if !claimLuckySeven {
                if playGameData.isPracticeGame {
                    contained = (self.crossedSequenceArray[self.selectedTicket].count >= 7 ? self.anouncedNumberArray.contains(self.crossedSequenceArray[self.selectedTicket]) : false)
                    contained ? self.getCliamHTMLString(earlyFive: false, TopLine: false, MiddelLine: false, BottomLine: false, Pyramid: false, FourCorner: false, luckySeven: true, circle: false, Name: userName, imageUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimLuckySeven)
                } else {
                    contained = (self.crossedSequenceArray[self.selectedTicket].count >= 7 ? true : false)
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: self.crossedSequenceArray[self.selectedTicket].joined(separator: ","), claimName: ConstantModel.claim.claimAPILuckySeven)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimLuckySeven, index: 5) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimLuckySeven)
                }
            }
            break
        case 1010:
            //Full House
            if !claimFullHouse {
                if playGameData.isPracticeGame {
                    (self.crossedSequenceArray[self.selectedTicket].count == 15) ? (self.anouncedNumberArray.contains(self.crossedSequenceArray[self.selectedTicket]) ? self.claimFullHouse(name: userName, imgUrl: (UserDefaultsHandler.user?.profilePhotoURL)!, userId: userId) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimFullHouse)) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimFullHouse)
                } else {
                    contained = (self.crossedSequenceArray[self.selectedTicket].count == 15)
                    let params = GetParamModel.shared.getClaimPriceParam(gameId: playGameData.gameId, claimStatus: "2", numberSequence: self.crossedSequenceArray[self.selectedTicket].joined(separator: ","), claimName: ConstantModel.claim.claimAPIFullHouse)
                    contained ? self.claimPriceAPI(parameters:params!, ClaimPrice: ConstantModel.claim.claimFullHouse, index: 9) : self.showboggiesMessageAction(name: userName, ClaimPrice: ConstantModel.claim.claimFullHouse)
                }
            }
            break
            
        default:
            break
        }
        
        print(contained)
    }
}
