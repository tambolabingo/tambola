//
//  PlayGameViewController.swift
//  Tambola Bingo
//
//  Created by signity on 05/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import WebKit
import Speech
//import RealtimeMessaging_iOS

class PlayGameViewController: UIViewController {
    // MARK: -IBOutlets for Main Screen
    @IBOutlet weak var lblClaimMessage: UILabel!
    @IBOutlet weak var chatTopConst: NSLayoutConstraint!
    @IBOutlet weak var chatBottomConst: NSLayoutConstraint!
    @IBOutlet weak var claimViewHeight: NSLayoutConstraint!
    @IBOutlet weak var claimOptionView: UIView!
    @IBOutlet weak var scoreBoardView: UIView!
    @IBOutlet weak var numAnounceBoardView: UIView!
    @IBOutlet weak var ticketCollectionView: UICollectionView!
    @IBOutlet weak var playersCollectionView: UICollectionView!
    @IBOutlet weak var scoresCollectionView: UICollectionView!
    @IBOutlet weak var ticketOneButton: UIButton!
    @IBOutlet weak var ticketTwoButton: UIButton!
    @IBOutlet weak var ticketThreeButton: UIButton!
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var showAutoCutButton: UIButton!
    @IBOutlet weak var userCountButton: UIButton!
    // MARK: -IBOutlets for Claim View
    @IBOutlet weak var btnClaimEarlyFive: UIButton!
    @IBOutlet weak var btnClaimTopLine: UIButton!
    @IBOutlet weak var btnClaimMiddleLine: UIButton!
    @IBOutlet weak var btnClaimBottomLine: UIButton!
    @IBOutlet weak var btnClaimPyramid: UIButton!
    @IBOutlet weak var btnClaimCorner: UIButton!
    @IBOutlet weak var btnClaimCenter: UIButton!
    @IBOutlet weak var btnClaimCircle: UIButton!
    @IBOutlet weak var btnClaimLuckySeven: UIButton!
    @IBOutlet weak var btnClaimFullHouse: UIButton!
    @IBOutlet weak var btnClaimOptions: UIButton!
    // MARK: -IBOutlets for Anounce Board
    @IBOutlet weak var anounceMainBtn: UIButton!
    @IBOutlet weak var anounceOneBtn: UIButton!
    @IBOutlet weak var anounceTwoBtn: UIButton!
    @IBOutlet weak var anounceThreeBtn: UIButton!
    @IBOutlet weak var anounceFourBtn: UIButton!
//    @IBOutlet weak var gameInstructionWebView: WKWebView!
    @IBOutlet weak var gameInstTextView: UITextView!
    @IBOutlet weak var scoreBoardLoader: UIActivityIndicatorView!
// MARK: -Timer and Score Variables
    var timer: Timer?
    var messageTimer: Timer?
    var gameClaimTimer: Timer?
    var timerVal = 0
    var score = 1
    var selectedTicket = 0
    var winningAmount = 0
    var isAutoCut = false
    var isGameSound = true
    var autoCutPurchased = false
    var audioPlayer: AVAudioPlayer?
    
// MARK: -Tickets and Claim Array
    var playGameData: PlayGameDataModel!
    var playerDetail  = LeadersDataModel()
    var myTicketsArray: TicketsModel!
    var ticketsArray        = [[String : [String]]]()
    var topLineArray        = [String]()
    var middleLineArray     = [String]()
    var thirdLineArray      = [String]()
    var pyramidArray        = [String]()
    var cornerArray         = [String]()
    var circleArray         = [String]()
    var instructionHTMLStr = ""
    var claimPriseHTMLStr = ""
    
// MARK: --Claim Options Variables
    var crossedSequenceArray = [[String](),[String](),[String]()]
    var anouncedNumberArray = [String]()
    var claimEarlyFive = false
    var claimLuckySeven = false
    var claimTopLine = false
    var claimMiddleLine = false
    var claimBottomLine = false
    var claimPyramid = false
    var claimCorner = false
    var claimCenter = false
    var claimCircle = false
    var claimFullHouse = false
// MARK: --Final Result's Variables
    var resultData = [ResultDataModel]()
    var isBoggied = false
    var isTicketOneBoggied = false
    var isTicketTwoBoggied = false
    var isTicketThreeBoggied = false
// MARK: -- Game Chat Variables
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var chatDetailTbl: UITableView!
    @IBOutlet weak var messageTxtField: CustomAnimatableTextfield!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var chatCountButton: UIButton!
//  var player
    var player:AVPlayer?
//    var ortc: OrtcClient?
    var messageListArr = [MessageModel]()
    var isChatOpen = false
    let user = UserDefaultsHandler.user
    var chatCount = 0
    var isOrtcConnected = true
    // MARK: -  UIViewController LifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
//        gameInstructionWebView.scrollView.showsVerticalScrollIndicator = true
//        gameInstructionWebView.scrollView.showsHorizontalScrollIndicator = false
//        gameInstructionWebView.navigationDelegate = self
//        gameInstructionWebView.isOpaque = false
//        self.gameInstructionWebView.sizeToFit()
//        gameInstructionWebView.backgroundColor = .clear
//        authGameToFirbase()
        setUpUIData()
        setupKeybordMethods()
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.playGameScreen)
    }
    
    func setupKeybordMethods(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    // MARK: -  SetUp UI as per the requesed number of tickes 
    func setUpUIData(){
//        self.userCountButton.isHidden = true
        self.userCountButton.setTitle(String(playGameData.players.count), for: .normal)
        self.isGameSound = UserDefaultsHandler.applicationSound
        let imgName = isGameSound ? "Sound" : "SoundOff"
        self.soundButton.setImage(UIImage(named: imgName), for: .normal)
        self.scoreBoardView.isHidden = true
        self.chatCountButton.isHidden = true
        switch playGameData.totalTickets {
        case 1:
            self.ticketOneButton.isHidden = false
            self.ticketTwoButton.isHidden = true
            self.ticketThreeButton.isHidden = true
            break
        case 2:
            self.ticketOneButton.isHidden = false
            self.ticketTwoButton.isHidden = false
            self.ticketThreeButton.isHidden = true
            break
        case 3:
            self.ticketOneButton.isHidden = false
            self.ticketTwoButton.isHidden = false
            self.ticketThreeButton.isHidden = false
            break
        default:
            break
        }
        //Set Claim Buttons
        btnClaimEarlyFive.isHidden = (playGameData.claimPrices["EARLY_FIVE"] != nil && playGameData.claimPrices["EARLY_FIVE"] == 0) ? true : false
        btnClaimTopLine.isHidden = (playGameData.claimPrices["FIRST_LINE"] != nil && playGameData.claimPrices["FIRST_LINE"] == 0) ? true : false
        btnClaimMiddleLine.isHidden = (playGameData.claimPrices["SECOND_LINE"] != nil && playGameData.claimPrices["SECOND_LINE"] == 0) ? true : false
        btnClaimBottomLine.isHidden = (playGameData.claimPrices["THIRD_LINE"] != nil && playGameData.claimPrices["THIRD_LINE"] == 0) ? true : false
        btnClaimPyramid.isHidden = (playGameData.claimPrices["PYRAMID"] != nil && playGameData.claimPrices["PYRAMID"] == 0) ? true : false
        btnClaimCorner.isHidden = (playGameData.claimPrices["CORNER"] != nil && playGameData.claimPrices["CORNER"] == 0) ? true : false
        btnClaimCenter.isHidden = (playGameData.claimPrices["CENTER"] != nil && playGameData.claimPrices["CENTER"] == 0) ? true : false
        btnClaimCircle.isHidden = (playGameData.claimPrices["CIRCLE"] != nil && playGameData.claimPrices["CIRCLE"] == 0) ? true : false
        btnClaimLuckySeven.isHidden = (playGameData.claimPrices["LUCKY_SEVEN"] != nil && playGameData.claimPrices["LUCKY_SEVEN"] == 0) ? true : false
        btnClaimFullHouse.isHidden = (playGameData.claimPrices["HOUSEFULL"] != nil && playGameData.claimPrices["HOUSEFULL"] == 0) ? true : false
        self.savegetTicketsFromDb()
        
        self.getClaimData()
        
        playGameData.isPracticeGame ? nil : self.startGameClaimTimer()
        playGameData.isPracticeGame ? self.anounceNumbers() : self.setAnounceVariables()
        playGameData.isPracticeGame ? nil : joinFirbaseUserMessage()
        playGameData.isPracticeGame ? nil : self.observeChannels()
        
//        playGameData.isPracticeGame ? nil : configOrtcClient()
    }
    func savegetTicketsFromDb(){
        ticketsArray = EventManager.createTicket(playGameData.totalTickets)
        if playGameData.gameType == "2" {
            var myTickets = GameTicketsModel.shared.getTicketsFromDB(playGameData.gameId)
            if myTickets == nil {
                //UpdateDB For boggied values false for all tickest
                let gameData = ["gameid": playGameData.gameId, "tickets":ticketsArray] as [String : Any]
                DbHandler.shared.saveGameTickets(gameData)
                myTickets = GameTicketsModel.shared.getTicketsFromDB(playGameData.gameId)
            }
            myTicketsArray = TicketsModel(myTickets?.tickets as! [[String : [String]]])
            self.autoCutPurchased = true
            self.isTicketOneBoggied = myTickets?.isTicketOneBoggied ?? false
            self.isTicketTwoBoggied = myTickets?.isTicketTwoBoggied ?? false
            self.isTicketThreeBoggied = myTickets?.isTicketThreeBoggied ?? false
            self.isBoggied = self.isTicketOneBoggied
            self.isBoggied ? self.btnClaimOptions.setImage(UIImage(named: "QuitGameButton"), for: .normal) : nil
            /*
             Need To set after game start
             self.isAutoCut = true
             self.showAutoCutButton.setImage(UIImage(named: "AutoCutOn"), for: .normal)
             for index in 0..<self.timerVal {
                 let anunceNum = self.playGameData.sequenceArray[index]
                 self.createCrossSequence(anunceNum)
             }
             */
            self.playGameData.ticketStatusArray[1] = true
            self.playGameData.ticketStatusArray[2] = true
            self.ticketTwoButton.setImage(UIImage(named: "TicketTwoBtn"), for: .normal)
            self.ticketThreeButton.setImage(UIImage(named: "TicketThreeBtn"), for: .normal)
            //get values of boggied for all tickets
        } else {
            myTicketsArray = TicketsModel(ticketsArray)
        }
    }
    func setAnounceVariables(){
        let startDTimeStr = playGameData.gameStartTime
        let startDTime = DateConvertor.convertToDate(slotDate: startDTimeStr, dateFormat: DateFormats.ServerDateFormat)
        let serverDTimeStr = playGameData.serverTime
        let serverDTime = DateConvertor.convertToDate(slotDate: serverDTimeStr, dateFormat: DateFormats.ServerDateFormat)
        let difference = EventManager.getDifferenceInSeconds(serverTime: serverDTime, startTime: startDTime)
        let totalSlotsdecials = Double(difference) / 10.00
        let totalSlots = round(totalSlotsdecials)
       
        let slot = totalSlots < 0 ? 0 : Int(totalSlots)
        if slot >= playGameData.sequenceArray.count{
             //Put check for index == 90 put end game message
            //Game Leave Message Players
            self.sendDataToFirbase(self.playGameData.gameId, chatFormat: ConstantModel.chatFormat.quitJoinMessage, Message: "left", isFrom: "Players")
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.GameOver, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self) { action in
                self.stopGameClaimTimer()
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            //Add TO anounced sequence array
            for index in 0..<slot{
                let seqVal = playGameData.sequenceArray[index]
                self.anouncedNumberArray.append(seqVal)
            }
            //Change Collor of collection view cell
            self.scoresCollectionView.reloadData()
            //Set and Start Anouncement
            self.timerVal = slot
            self.anounceNumbers()
        }
    }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        if playGameData.gameType == "2" {
                self.quitGameAPI()
        } else {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.QuitGame, btn1Tit: ConstantModel.btnText.btnYes, btn2Tit: ConstantModel.btnText.btnNo, sender: self) { action in
            action == ConstantModel.btnText.btnYes ? (self.playGameData.isPracticeGame ? self.backFromPracticeGame() : self.quitGameAPI()) : nil
            }
        }
        
    }
    func backFromPracticeGame(){
        self.stopSequenceTimer()
        self.stopGameClaimTimer()
        if !self.playGameData.isPracticeGame{
            //Game Leave Message Players
            self.sendDataToFirbase(playGameData.gameId, chatFormat: ConstantModel.chatFormat.quitJoinMessage, Message: "left", isFrom: "Players")
//            self.sendToORTCWithData(self.playGameData.gameId, chatFormat: ConstantModel.chatFormat.quitJoinMessage, Message: "left")
//            self.ortc?.disconnect()
//            self.ortc?.disableHeartbeat()
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
        
        
    }
    @IBAction func ShowChatButtonAction(sender:UIButton){
        //Show Chat View From UI
        if playGameData.isPracticeGame {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.publicPrivateFeature, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        } else {
            self.reloadTable()
            isChatOpen = true
            self.chatCount = 0
            self.chatCountButton.isHidden = true
            self.chatView.isHidden = false
            self.view.bringSubviewToFront(chatView)
        }
    }
    @IBAction func closeChatButtonAction(sender:UIButton){
        //Hide Chat View From UI
        self.view.endEditing(true)
        isChatOpen = false
        self.chatCount = 0
        self.chatView.isHidden = true
    }
    @IBAction func soundButtonAction(sender:UIButton){
        if self.isGameSound {
            self.isGameSound = false
            self.soundButton.setImage(UIImage(named: "SoundOff"), for: .normal)
        } else {
            self.isGameSound = true
            self.soundButton.setImage(UIImage(named: "Sound"), for: .normal)
        }
    }
    @IBAction func autoCutButtonAction(sender:UIButton){
        if playGameData.isPracticeGame {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.publicPrivateFeature, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        } else {
        if self.isAutoCut {
            //Disable AutoCut
            self.isAutoCut = false
            self.showAutoCutButton.setImage(UIImage(named: "AutoCutOff"), for: .normal)
            self.showClaimMessage(message: ConstantModel.message.disableAutoCut)
//            self.playMp3File(PlayGameManager.autoCutDisable)
            self.playClaimFiles(0)
        } else {
            //Enable AutoCut
            if !self.autoCutPurchased {
                let autoCutMessage = String(format: ConstantModel.message.purchaseAutoCut, String(UserDefaultsHandler.gamePriceInfo?.autoCut ?? 0))
            EventManager.showAlertWithAction(alertMessage: autoCutMessage, btn1Tit: ConstantModel.btnText.btnBuy, btn2Tit: nil, sender: self) { action in
                if action == ConstantModel.btnText.btnBuy{
                   self.autoCutUserCoinAPI()
                }
            }
            } else {
                self.playClaimFiles(1)
//                self.playMp3File(PlayGameManager.autoCutEnable)
                self.showClaimMessage(message: ConstantModel.message.enableAutoCut)
                self.isAutoCut = true
                self.showAutoCutButton.setImage(UIImage(named: "AutoCutOn"), for: .normal)
                for index in 0..<self.timerVal {
                    let anunceNum = self.playGameData.sequenceArray[index]
                    self.createCrossSequence(anunceNum)
                }
            }
        }
        }
    }
    @IBAction func selectTicketButtonAction(sender:UIButton){
        if self.playGameData.ticketStatusArray[sender.tag] {
            if sender.tag == 0 {
                //Ticket 1
                self.isBoggied = self.isTicketOneBoggied
            } else if sender.tag == 1 {
                //Ticket 1
                self.isBoggied = self.isTicketTwoBoggied
            } else {
                //Ticket 3
                self.isBoggied = self.isTicketThreeBoggied
            }
            let imageName = self.isBoggied ? "QuitGameButton" : "ClaimButton"
            self.btnClaimOptions.setImage(UIImage(named: imageName), for: .normal)
            self.selectedTicket = sender.tag
            self.getClaimData()
            self.ticketCollectionView.reloadData()
        } else {
            EventManager.showAlertWithAction(alertMessage: UserDefaultsHandler.appVersionInfo?.purchaseTicketMessage, btn1Tit: ConstantModel.btnText.btnNo, btn2Tit: ConstantModel.btnText.btnYes, sender: self) { action in
                action == ConstantModel.btnText.btnYes ? self.enableTicketAPI(sender.tag) : nil
            }
        }
    }
    
    @IBAction func claimPriceAction(sender:UIButton){
        isBoggied ? self.backButtonAction() : DispatchQueue.main.async {
        UIView.animate(withDuration: 0.50, animations: {
            self.claimViewHeight.constant = 100
            self.view.layoutIfNeeded()
          }, completion: { status in
            
         })
        }
    }
    @IBAction func backToGameAction(sender:UIButton){
       DispatchQueue.main.async {
        UIView.animate(withDuration: 0.50, animations: {
            self.claimViewHeight.constant =  0
            self.view.layoutIfNeeded()
         }, completion: { status in
            
         })
       }
    }
    @IBAction func showHideScoreBoardAction(sender:UIButton){
        if sender.tag == 0 {
            //Show Score board and Hide Anouncement Board
            sender.tag = 1
            self.scoreBoardView.isHidden = false
            self.numAnounceBoardView.isHidden = true
           
        }else {
            //Show Anouncement Board and Hide Score board
            sender.tag = 0
            self.scoreBoardView.isHidden = true
            self.numAnounceBoardView.isHidden = false
        }
    }
    //Check for anounced array contain Claimed Array or not
    func checkArrayContainSubArray(mainArray:[String], subArray:[String]) -> Bool {
        let contained = (mainArray.contains(subArray) && self.anouncedNumberArray.contains(subArray))
        return contained
    }
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GameResultView" {
            let nextVC                     = segue.destination as? GameResultViewController
            nextVC?.resultData             = self.resultData
            nextVC?.isBoggied              = (self.isTicketOneBoggied && self.isTicketTwoBoggied && self.isTicketThreeBoggied)
            nextVC?.isFromPlayGame         = playGameData.isPracticeGame ? false : true
            nextVC?.gameId                 = playGameData.gameId
            nextVC?.gameName               = playGameData.gameName
            nextVC?.earnPoints             = String(winningAmount)
            nextVC?.isHistory              = false
            nextVC?.claimPrices            = playGameData.claimPrices
        }
    }
}
