//
//  GameDetailViewController.swift
//  Tambola Bingo
//
//  Created by signity on 16/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class GameDetailViewController: UIViewController {
    var timercallback:(() -> Void)?
    @IBOutlet weak var lblGameName: UILabel!
    @IBOutlet weak var lblGameTimer: UILabel!
    @IBOutlet weak var lblGameDateTime: UILabel!
    @IBOutlet weak var lblGameType: UILabel!
    @IBOutlet weak var lblParticipantCount: UILabel!
    @IBOutlet weak var btnPlayGame: UIButton!
    @IBOutlet weak var btnRefershGame: UIButton!
    @IBOutlet weak var btnInvitePlayer: UIButton!
    @IBOutlet weak var playersCollectionView: UICollectionView!
    var actionSheetController: UIAlertController!
    var gameId:String = ""
    var userType:String = ""
    var isPublic:Bool = false
    var gameData:GameDetailDataModel!
    var timer: Timer?
    var timerVal = 0
    var playerDetail  = LeadersDataModel()
    var isRefershing = false
    var isPlayPressed = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnPlayGame.isHidden = true
        self.btnInvitePlayer.isHidden = true
        self.GetPublicGameStatus(parameters: GetParamModel.shared.getGameStatusParam(gameId: gameId, userType: userType), isSilent: false)
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.gameDetailScreen)
        self.observeChannels()
    }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
        self.stopGameTimer()
    }
    @IBAction func refershButtonAction(){
        self.GetPublicGameStatus(parameters: GetParamModel.shared.getGameStatusParam(gameId: gameId, userType: userType), isSilent: false)
    }
    @IBAction func playGameButtonAction(){
        isPlayPressed ? nil :self.playPublicGameAPI(parameters: GetParamModel.shared.getPlayPublicGameParam(gameId: self.gameId))
    }
    @IBAction func inviteButtonAction(sender:UIButton){
            
        let sharekey = self.gameData.game_key
            self.actionSheetController = UIAlertController(title: ConstantModel.btnText.btnSelect, message: "", preferredStyle: .actionSheet)
            actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnCancel, style: .cancel, handler: nil))
    //        actionSheetController.addAction(UIAlertAction.init(title: "Facebook Friends", style: .default, handler: { _ in
    //            //fb invite friends by fb
    //            NavigationManager.navigateToFBFriendInvite(self,gameId: gameId)
    //            }))
            actionSheetController.addAction(UIAlertAction.init(title: "Recent", style: .default, handler: { _ in
                NavigationManager.navigateToAddInviteGroup(self, isInvite: true, isgroup: false, userId: "", gameId: self.gameId, userName: "")
                }))
            actionSheetController.addAction(UIAlertAction.init(title: "My Groups", style: .default, handler: { _ in
                NavigationManager.navigateToAddInviteGroup(self, isInvite: true, isgroup: true, userId: "", gameId: self.gameId, userName: "")
                }))
            actionSheetController.addAction(UIAlertAction.init(title: "Share key", style: .default, handler: { _ in
                self.actionSheetController.dismiss(animated: true, completion: nil)
                ShareLinksManager.shared.showShareOptions(sharekey, controller: self)
                }))
            self.present(actionSheetController, animated: true, completion: nil)
        }
    func setUpData(){
        self.lblGameName.text = self.gameData.game_name
        if userType == "admin"{
            self.lblGameType.text = "You"
        } else {
            self.lblGameType.text = self.gameData.game_type + " Game"
        }
        self.lblGameTimer.text = ""
        let start_Time = self.gameData.game_time
        self.lblGameDateTime.text = DateConvertor.getLocalFormateDateTime(gameDateTime: start_Time, startSpace: "     ")
        self.lblParticipantCount.text = String(self.gameData.participant.count) + " Participants"
        self.setupTimerData()
        //Set hidden status of play game button depend on time and game status
    }
  
    //Start Timer for Game Time Timer
    func setupTimerData(){
        let startDTimeStr = self.gameData.game_time
        let startDTime = DateConvertor.convertToDate(slotDate: startDTimeStr, dateFormat: DateFormats.ServerDateFormat)
        let currentDateStr = DateConvertor.convertIntoString(slotDate: Date(), dateFormat: DateFormats.ServerDateFormat)
        let serverTimeStr = self.gameData.server_time == "" ? currentDateStr : self.gameData.server_time
        let currentDTime = DateConvertor.convertToDate(slotDate: serverTimeStr, dateFormat: DateFormats.ServerDateFormat)
        let calendar = Calendar.current
        let gameEndTime = calendar.date(byAdding: .minute, value: 60, to: startDTime)
        if currentDTime >= gameEndTime! {
            lblGameTimer.text = "Play Game Time Expired."
            self.btnPlayGame.isHidden = true
            self.btnInvitePlayer.isHidden = true
            self.stopGameTimer()
        } else {
//            self.btnPlayGame.isHidden = false
            let difference = EventManager.getDifferenceInSeconds(serverTime: startDTime, startTime: currentDTime)
            timerVal = difference
            startTimer()
        }
    }
    func startTimer(){
        self.stopGameTimer()
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimerLable), userInfo: nil, repeats: true)
    }
    //Stop Timer for Anuonce number Finish
    @objc func stopGameTimer(){
        if self.timer != nil && (self.timer?.isValid)! {
            self.timer?.invalidate()
            print("Timer Stop")
        }
    }
    @objc func updateTimerLable(){
        if timerVal == 0 || timerVal < 0 {
            lblGameTimer.text = ""
            if isPublic {
                self.btnPlayGame.isHidden = false
            } else {
                showPlayButton()
            }
            self.stopGameTimer()
        } else {
            self.btnPlayGame.isHidden = true
            self.btnInvitePlayer.isHidden = isPublic ? true : false
            let hours = timerVal / 3600;
            let minutes = (timerVal % 3600) / 60;
            let seconds = (timerVal % 3600) % 60;
            lblGameTimer.text = String(format: ConstantModel.chatFormat.gameTimerFormat, hours, minutes, seconds)
            timerVal -= 1
        }
    }
    func showPlayButton(){
        if self.userType == "admin" {
            self.btnPlayGame.isHidden = false
            self.btnInvitePlayer.isHidden = false
            self.lblGameTimer.text = ""
        } else {
            self.btnInvitePlayer.isHidden = false
            self.btnPlayGame.isHidden = gameData.game_status == "n" ?  true : false
            self.lblGameTimer.text = "Waiting for oragnizer to start game."
            self.lblGameTimer.isHidden = gameData.game_status == "n" ? false : true
            
        }
    }
}// MARK: -  Extension for Game Detail API 
extension GameDetailViewController {
    // MARK: -  Get Public Game Status API 
    func GetPublicGameStatus(parameters: [String: Any]?, isSilent: Bool){
        ApiCallModel.shared.apiCall(isSilent: isSilent, url: UserDefaultsHandler.projectBaseUrl + APIConstants.PublicGameStatus, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parsePublicGameDetailResponse(response: response, sender: self, callback: { result in
                let data = result as? [String: Any]
                if data != nil{
                    let failureResponse = data!["data"] as? String
                    EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: { _ in
                            self.backButtonAction()
                    })
                    
                } else {
                //  Get Games Data Sucessfully Reload View
                    self.isRefershing = false
                    self.gameData = result as? GameDetailDataModel
                    self.setUpData()
                    self.playersCollectionView.reloadData()
                }
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.GetPublicGameStatus(parameters: parameters, isSilent: isSilent)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    // MARK: -  Play Public Game API 
    func playPublicGameAPI(parameters: [String: Any]?){
        isPlayPressed = true
        let baseUrl = UserDefaultsHandler.projectBaseUrl + (isPublic ? APIConstants.StartPublicGame : APIConstants.StartOrganizerGame)
        
        ApiCallModel.shared.apiCall(isSilent: false, url: baseUrl, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parsePlayPublicGameResponse(response: response, isPublic: self.isPublic, sender: self, callback: { result in
                //  Start Games Sucessfully show alret and update status "JOINED" and refresh cell
                self.timercallback!()
                self.isPlayPressed = false
                let gameDetails = result as? PlayGameDataModel
                gameDetails?.gameName = self.gameData.game_name
                gameDetails?.gameId = self.gameId
                NavigationManager.navigateToPlayGame(delegate: self, gameData: gameDetails!)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    self.isPlayPressed = false
                    if isCallSucess{
                        self.playPublicGameAPI(parameters: parameters)
                    }
                })
            } else {
//                self.isPlayPressed = false
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    func getPlayerDetailAPI(parameters: [String: Any]){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.myDetails, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                //Share URl on facebook and pop to root view
                let data = result as? [String: Any]
                let score = data!["scores"] as? [String: Any]
                self.setPlayerData(score!)
                NavigationManager.navigateToFriendsDetail(delegate: self, friendData: self.playerDetail, game_Id: self.gameId)
                //Show Detail View
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getPlayerDetailAPI(parameters:parameters)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    func setPlayerData(_ score:[String: Any]){
        self.playerDetail.claims = score["claims"] as? String ?? ""
        self.playerDetail.gamePlayed  = score["gamePlayed"] as? String ?? ""
        self.playerDetail.luckRate = score["LuckRate"] as? Double ?? 0.00
        self.playerDetail.totalwins = score["Totalwins"] as? String ?? ""
        self.playerDetail.score.center = score["CENTER"] as? Int ?? 0
        self.playerDetail.score.circle = score["CIRCLE"] as? Int ?? 0
        self.playerDetail.score.corner = score["CORNER"] as? Int ?? 0
        self.playerDetail.score.earlyFive = score["EARLY_FIVE"] as? Int ?? 0
        self.playerDetail.score.firstLine = score["FIRST_LINE"] as? Int ?? 0
        self.playerDetail.score.houseFull = score["HOUSEFULL"] as? Int ?? 0
        self.playerDetail.score.luckSeven = score["LUCKY_SEVEN"] as? Int ?? 0
        self.playerDetail.score.pyramid = score["PYRAMID"] as? Int ?? 0
        self.playerDetail.score.secondLine = score["SECOND_LINE"] as? Int ?? 0
        self.playerDetail.score.thirdLine = score["THIRD_LINE"] as? Int ?? 0
        self.playerDetail.isDetailAdded =  true
    }
}

extension GameDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // MARK: -  UICollectionView Delegate, DelegateFlowLayout and Data Source 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.gameData == nil ? 0 : self.gameData.participant.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "participantCollectionViewCell", for: indexPath) as? participantCollectionViewCell
        cell?.lblName.text = self.gameData.participant[indexPath.row].name
        let imageURL = self.gameData.participant[indexPath.row].image_url
        cell?.imgParticipant.sd_setImage(with: URL.init(string: imageURL), placeholderImage: #imageLiteral(resourceName: "userImg"), options: [.retryFailed, .refreshCached], completed: { (downloadedImage, error, SDImageCacheType, url) in
        })
//        let user = UserDataManager.shared.getUserFromDB()
        if UserDefaultsHandler.user?.fbID == self.gameData.participant[indexPath.row].id {
        cell?.lblTitle.text = "You" // Purpul/blue
        cell?.lblTitle.backgroundColor = UIColor.purple
        } else {
        cell?.lblTitle.text = "Player"
        cell?.lblTitle.backgroundColor = UIColor.blue
        }
            return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Show Player Detail
        self.showPlayerDetailView(indexPath.row)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isRefershing {
            isRefershing = true
        let offset: CGPoint = scrollView.contentOffset
        let bounds: CGRect = scrollView.bounds
        let size: CGSize = scrollView.contentSize
        let inset: UIEdgeInsets = scrollView.contentInset
        let y = Float(offset.x + bounds.size.width - inset.right)
        let h = Float(size.width)
        let reload_distance: Float = 75 //distance for which you want to load more
        if y > h + reload_distance {
            // write your code getting the more data
            self.GetPublicGameStatus(parameters: GetParamModel.shared.getGameStatusParam(gameId: gameId, userType: userType), isSilent: false)
        }
        }
    }

    func showPlayerDetailView(_ index: Int){
        let playerData = self.gameData.participant[index]
        playerDetail.username = playerData.name
        playerDetail.coins = playerData.coins
        playerDetail.img_url = playerData.image_url
        playerDetail.user_id = playerData.id
        //Add Values for BlockUnblock
        playerDetail.user_blocked = playerData.blockStatus
        playerDetail.gameType = isPublic ? "Public" : "Private"
        self.getPlayerDetailAPI(parameters:GetParamModel.shared.getUserRecordsParam(userId: playerData.id)!)
    }
}


