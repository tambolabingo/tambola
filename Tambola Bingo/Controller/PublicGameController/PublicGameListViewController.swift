//
//  PublicGameListViewController.swift
//  Tambola Bingo
//
//  Created by signity on 05/07/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class PublicGameListViewController: UIViewController {

    @IBOutlet weak var publicGamesTableView: UITableView!
    @IBOutlet weak var lblCoins: UILabel!
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    var publicGamesArray = [GameListDataModel]()
    private let refreshControl = UIRefreshControl()
    var globalTimer: Timer?
    var serverTime = Date()
    var minute = 0
    var myAdsView = ShowiAdsView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myAdsView.senderController = self
        self.setRefreshController()
        self.setUserCoin()
        self.getPublicGameList(parameters: GetParamModel.shared.getPublicGameListParam(), isLoader: false)
        ConstantModel.appDelegate.createGAScreenLog(ConstantModel.gai.publicGameListScreen)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.myAdsView.loadInterstitialAds()
    }
    func setUserCoin(){
        let user = UserDefaultsHandler.user
        self.lblCoins.text = user != nil ? user?.userCoins : "0000"
    }
    func setRefreshController() {
        publicGamesTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshListData(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle = NSAttributedString(string: ConstantModel.message.fetchList,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    // MARK: -  IBOutlet Action 
    @IBAction func backButtonAction(){
        self.invalidateTimer()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cellButtonAction(sender:UIButton){
        //Play(103) Join(101) and Joined(102) Button Actions
        switch sender.accessibilityLabel! {
        case "101":
            //Join button Action call API for Join Public Game
            let gameId = self.publicGamesArray[sender.tag].game_id
            self.joinPublicGame(parameters: GetParamModel.shared.getJoinPublicGameParam(gameId: gameId), index: sender.tag)
            break
        case "102":
        //Joined button Action
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.GameJoindMessage, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            break
        case "103":
        //Play Game Action
            self.showAddMethod(index: sender.tag)
//            let gameId = self.publicGamesArray[sender.tag].game_id
//            let gameName = self.publicGamesArray[sender.tag].game_name
//            self.StartPlayPublicGame(parameters: GetParamModel.shared.getPlayPublicGameParam(gameId: gameId), gameName: gameName)
            break
        default:
            break
        }
    }
    @objc private func refreshListData(_ sender: Any) {
//        Public Game List data
        self.publicGamesTableView.isUserInteractionEnabled = false
        self.getPublicGameList(parameters: GetParamModel.shared.getPublicGameListParam(), isLoader: true)
    }
    // MARK: -  Global Timer Start/Update
    func startGlobalTimer() {
        self.invalidateTimer()
        globalTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    func invalidateTimer(){
        if self.globalTimer != nil && (self.globalTimer?.isValid)! {
            self.globalTimer?.invalidate()
            self.globalTimer = nil
        }
    }
    @objc func updateTimer() {
        let currentTime = Calendar.current.date(byAdding: .second, value: 1, to: self.serverTime)!
        self.serverTime = currentTime
        minute = minute + 1
        if minute == 60 {
//            print("Reload table after one minute")
            minute = 0
            self.publicGamesTableView.reloadData()
        }
    }
}
// MARK: -  UITableView Data Source and Delegate 
extension PublicGameListViewController : UITableViewDelegate, UITableViewDataSource{
    func reloadRow(rowNum: Int){
        let indexPath = IndexPath(item: rowNum, section: 0)
        self.publicGamesTableView.reloadRows(at: [indexPath], with: .none)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.publicGamesArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pubileGamesCell", for: indexPath) as? pubileGamesCell
        if indexPath.row % 2 == 0 {
            cell?.backgroundColor = UIColor.clear
        }else {
            cell?.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
        cell?.gameTitleText.text = self.publicGamesArray[indexPath.row].game_name
        let start_Time = self.publicGamesArray[indexPath.row].start_time
        cell?.gameDateText.text = DateConvertor.getLocalFormateDateTime(gameDateTime: start_Time, startSpace: "     ")
        cell?.lblPlayesCount.text = self.publicGamesArray[indexPath.row].invitees_count
        let joiningStatus = self.publicGamesArray[indexPath.row].joining
        let participantstatus = self.publicGamesArray[indexPath.row].participant_status
//        let server_Time = self.publicGamesArray[indexPath.row].server_time
        let startDateTime = DateConvertor.convertToDate(slotDate: start_Time, dateFormat: DateFormats.ServerDateFormat)
//        let serverDateTime = DateConvertor.convertToDate(slotDate: server_Time, dateFormat: DateFormats.ServerDateFormat)
        let timeDiff = EventManager.getTimeDifferenceValue(serverTime: self.serverTime, startTime: startDateTime)
        if participantstatus == "UNKNOWN" {
            if joiningStatus == "open" {
                //Check for 10 min before and after the start time using server time
                if timeDiff >= -10{
                    cell?.gamebutton.isHidden = false
                    cell?.gamebutton.accessibilityLabel = "101" //For Join Button
                    cell?.gamebutton.setImage(UIImage(named: "JoinButton"), for: .normal)
                    cell?.lblgameClosed.isHidden = true
                } else {
                    cell?.gamebutton.isHidden = true
                    cell?.lblgameClosed.isHidden = false // Joining Closed
                }
              
            } else {
                cell?.gamebutton.accessibilityLabel = "100"
                cell?.gamebutton.isHidden = true
                cell?.lblgameClosed.isHidden = false
            }
        } else {
            cell?.lblgameClosed.isHidden = true
            // Check for server time if time is more then or equal start timer then show Play button other wise show Joined button
            if self.publicGamesArray[indexPath.row].game_status != "started" {
                if self.serverTime >= startDateTime {
                    cell?.gamebutton.accessibilityLabel = "103" //For Play Button
                    cell?.gamebutton.isHidden = false
                    cell?.gamebutton.setImage(UIImage(named: "PlayPrivateGameButton"), for: .normal)
                } else {
                    cell?.gamebutton.accessibilityLabel = "102" //For Joined Button
                    cell?.gamebutton.isHidden = false
                    cell?.gamebutton.setImage(UIImage(named: "JoinedButton"), for: .normal)
                    self.observeChannels(indexPath.row)
                }
            } else {
                cell?.gamebutton.accessibilityLabel = "103" //For Play Button
                cell?.gamebutton.isHidden = false
                cell?.gamebutton.setImage(UIImage(named: "PlayPrivateGameButton"), for: .normal)
            }
            
        }
        
        cell?.gamebutton.tag = indexPath.row
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.publicGamesArray[indexPath.row].participant_status == "JOINED" {
            //Show Game Detail View Controller
            let gameId = self.publicGamesArray[indexPath.row].game_id
            NavigationManager.navigateToGameDetail(self, gameId: gameId, userType: "Normal", isPublic: true)
        } else {
            EventManager.showAlertWithAction(alertMessage: ConstantModel.message.NotAParticipate, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
        }
    }
}
// MARK: -  Extension For API Call's 
extension PublicGameListViewController{
    // MARK: -  GET Public Game List data API 
    func getPublicGameList(parameters: [String: Any]?, isLoader: Bool){
        ApiCallModel.shared.apiCall(isSilent: isLoader, url: UserDefaultsHandler.projectBaseUrl + APIConstants.publicGamesList, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parsePublicGameListResponse(response: response, sender: self, callback: { result in
                //  fetch Games Sucessfully
                self.publicGamesArray = (result as? [GameListDataModel])!
                if self.publicGamesArray.count > 0{
                    let server_Time = self.publicGamesArray[0].server_time
                    let serverDateTime = DateConvertor.convertToDate(slotDate: server_Time, dateFormat: DateFormats.ServerDateFormat)
                    self.serverTime = serverDateTime
                    self.startGlobalTimer()
                }
                self.publicGamesTableView.reloadData()
                self.refreshControl.endRefreshing()
                self.publicGamesTableView.isUserInteractionEnabled = true
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.getPublicGameList(parameters: parameters, isLoader: isLoader)
                    }
                })
            } else {
                self.publicGamesTableView.isUserInteractionEnabled = true
                let failerMessage = (failureResponse ?? ConstantModel.api.ServiceFailure) + " " + ConstantModel.message.reloadList
                EventManager.showAlertWithAction(alertMessage: failerMessage, btn1Tit: ConstantModel.btnText.btnContinue, btn2Tit: ConstantModel.btnText.btnCancel, sender: self, action:{ action in
                    action == ConstantModel.btnText.btnContinue ? (self.getPublicGameList(parameters: parameters, isLoader: isLoader)) : (_ = self.navigationController?.popViewController(animated: true))
                })
            }
        } )
    }
    // MARK: -  Join Public Game API 
    func joinPublicGame(parameters: [String: Any]?, index: Int){
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.joinPublicGame, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parseJoinGameResponse(response: response, sender: self, callback: { result in
                //  join Games Sucessfully show alret and update status "JOINED" and refresh cell
                //Add to Native calander and Show alerts on time
                EventManager.showAlertWithAction(alertMessage: result as? String, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
                self.publicGamesArray[index].participant_status = "JOINED"
                let invteCount = Int(self.publicGamesArray[index].invitees_count)
                self.publicGamesArray[index].invitees_count = String(invteCount! + 1)
                self.reloadRow(rowNum: index)
                DispatchQueue.main.async {
                    BaseAPIHandler.shared.getUserCoinsAPI(delegate: self) { isCallSucess in
                        self.setUserCoin()
                    }
                }
                
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.joinPublicGame(parameters: parameters, index: index)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
    // MARK: -  Show Ads Banner full screen 
       func showAddMethod(index: Int){
            let gameId = self.publicGamesArray[index].game_id
            let gameName = self.publicGamesArray[index].game_name
           if UserDefaultsHandler.isHideAds {
               self.StartPlayPublicGame(parameters: GetParamModel.shared.getPlayPublicGameParam(gameId: gameId), gameName: gameName)
           } else {
               //Show Banner Google Ads
                self.myAdsView.AdsAction = { isClose in
                    if isClose {
                        //Navigate to next view to display
                        self.StartPlayPublicGame(parameters: GetParamModel.shared.getPlayPublicGameParam(gameId: gameId), gameName: gameName)
                    }
                }
               self.myAdsView.showAds()
           }
           
       }
    // MARK: -  Play Public Game API 
    func StartPlayPublicGame(parameters: [String: Any]?, gameName: String){
        self.myAdsView.loadInterstitialAds()
        ApiCallModel.shared.apiCall(isSilent: false, url: UserDefaultsHandler.projectBaseUrl + APIConstants.StartPublicGame, parameters: parameters, type: APIConstants.PostAPI, isMultipart: true, isKey: false, delegate: self, success: { response in
            APIResponseModel.shared.parsePlayPublicGameResponse(response: response, isPublic: true, sender: self, callback: { result in
                //  Start Games Sucessfully show alret and update status "JOINED" and refresh cell
                let gameDetails = result as? PlayGameDataModel
                gameDetails?.gameName = gameName
                gameDetails?.gameId = parameters!["game_id"] as! String
                self.invalidateTimer()
                NavigationManager.navigateToPlayGame(delegate: self, gameData: gameDetails!)
            })
        }, failure: { failureResponse in
            if failureResponse == ConstantModel.message.tokenError {
                //Call token API and recall API called
                BaseAPIHandler.shared.getSessionTokenAPI(delegate: self, callCompleted: { isCallSucess in
                    if isCallSucess{
                        self.StartPlayPublicGame(parameters: parameters, gameName: gameName)
                    }
                })
            } else {
                EventManager.showAlertWithAction(alertMessage: failureResponse ?? ConstantModel.api.ServiceFailure, btn1Tit: ConstantModel.btnText.btnOk, btn2Tit: nil, sender: self, action: nil)
            }
        } )
    }
}
