//
//  FirebaseObserverExtension.swift
//  Tambola Bingo
//
//  Created by Anisha Thakur on 04/06/20.
//  Copyright © 2020 signity. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

extension GameDetailViewController {
    override func viewDidDisappear(_ animated: Bool) {
            self.removeRefObserver()
    }
    func removeRefObserver(){
        let ref = Database.database().reference().child("Tambola").child(self.gameId)
        let refOne = ref.child("game_status")
        refOne.removeAllObservers()
    }
    func observeChannels() {
            let ref = Database.database().reference().child("Tambola")
            ref.child(self.gameId).child("game_status").observe(.value, with: { (data) in
                let status = data.value as? String
                if status == "1"{
                    //Enable Play Button
                    self.stopGameTimer()
                    self.removeRefObserver()
                    self.btnPlayGame.isHidden = false
                    self.lblGameTimer.isHidden = true
                }
//                if let postDict = data.value as? Dictionary<String, AnyObject> {
//                    //updated game play status
//                    let status = postDict["ortcmsg"] as! String
//                     //check and show play button
//                }
            })
    }
}

extension PrivateGameListViewController {
    override func viewDidDisappear(_ animated: Bool) {
        for game in privateGamesArray{
            self.removeRefObserver(game.game_id)
        }
        }
    func removeRefObserver(_ gameId:String){
        let ref = Database.database().reference().child("Tambola").child(gameId)
        let refOne = ref.child("game_status")
        refOne.removeAllObservers()
    }
    func observeChannels(_ index: Int) {
                let gameId = self.privateGamesArray[index].game_id
                let ref = Database.database().reference().child("Tambola")
                ref.child(gameId).child("game_status").observe(.value, with: { (data) in
                    let status = data.value as? String
                    if status == "1"{
                        //Enable Play Button
                        self.removeRefObserver(gameId)
                       self.privateGamesArray[index].game_status = "started"
                        self.myGamesTableView.reloadData()
                    }
                })
        }
}
extension PublicGameListViewController {
    override func viewDidDisappear(_ animated: Bool) {
        for game in publicGamesArray{
            self.removeRefObserver(game.game_id)
        }
        }
    func removeRefObserver(_ gameId:String){
        let ref = Database.database().reference().child("Tambola").child(gameId)
        let refOne = ref.child("game_status")
        refOne.removeAllObservers()
    }
    func observeChannels(_ index: Int) {
                let gameId = self.publicGamesArray[index].game_id
                let ref = Database.database().reference().child("Tambola")
                ref.child(gameId).child("game_status").observe(.value, with: { (data) in
                    let status = data.value as? String
                    if status == "1"{
                        //Enable Play Button
                        self.removeRefObserver(gameId)
                        self.publicGamesArray[index].game_status = "started"
                        self.publicGamesTableView.reloadData()
                    }
                })
        }
}
