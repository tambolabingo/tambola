#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "Balancer.h"
#import "OrtcClient.h"
#import "RealtimePushAppDelegate.h"

FOUNDATION_EXPORT double RealtimeMessaging_iOSVersionNumber;
FOUNDATION_EXPORT const unsigned char RealtimeMessaging_iOSVersionString[];

